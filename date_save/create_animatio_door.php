<?php
/**
 * A simple animation created from with 8 png images.
 *
 * The horse images were taken from this Wikipedia page
 *
 * https://en.wikipedia.org/wiki/Animated_cartoon
 */
$job_id= $_POST['job_no'];
use movemegif\domain\FileImageCanvas;
use movemegif\GifBuilder;

// just for debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

// include movemegif's namespace
//require_once __DIR__ . '/../php/autoloader.php';
require_once __DIR__ . '/php/autoloader.php';

// no width and height specified: they will be taken from the first frame
$builder = new GifBuilder();
$builder->setRepeat();

for ($i = 0; $i <= 18; $i++) {

    $builder->addFrame()
        ->setCanvas(new FileImageCanvas(__DIR__ . '/images/panel_drawing_3d_view/'.$job_id.'/animation_png/' . $i . '.png'))
        ->setDuration(16);
}

//$builder->output('horse.gif');
$builder->output('img/animation_panels.gif');
$builder->saveToFile('images/panel_drawing_3d_view/'.$job_id.'/animation_png/animation_panels.gif');


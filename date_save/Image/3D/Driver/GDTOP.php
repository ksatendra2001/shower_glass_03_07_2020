<?php

class Image_3D_Driver_GDTOP extends Image_3D_Driver
{
    
    protected $_filetype;

    public function __construct()
    {
        parent::__construct();
        
        $this->_filetype = 'png';
    }
    
    public function createImage($x, $y)
    {
        $this->_image = imagecreatetruecolor($x, $y);
    }
    
    protected function _getColor(Image_3D_Color $color)
    {
        $values = $color->getValues();

        $values[0] = (int) round($values[0] * 255);
        $values[1] = (int) round($values[1] * 255);
        $values[2] = (int) round($values[2] * 255);
        $values[3] = (int) round($values[3] * 127);

        if ($values[3] > 0) {
            // Tranzparente Farbe allokieren
            $color = imageColorExactAlpha($this->_image, $values[0], $values[1], $values[2], $values[3]);
            if ($color === -1) {
                // Wenn nicht Farbe neu alloziieren
                $color = imageColorAllocateAlpha($this->_image, $values[0], $values[1], $values[2], $values[3]);
            }
        } else {
            // Deckende Farbe allozieren
            $color = imageColorExact($this->_image, $values[0], $values[1], $values[2]);
            if ($color === -1) {
                // Wenn nicht Farbe neu alloziieren
                $color = imageColorAllocate($this->_image, $values[0], $values[1], $values[2]);
            }
        }
        
        return $color;
    }
    
    public function setBackground(Image_3D_Color $color)
    {
        $bg = $this->_getColor($color);
        imagefill($this->_image, 1, 1, $bg);
    }
    
    public function drawPolygon(Image_3D_Polygon $polygon)
    {
		
		
        //$red = imagecolorallocate($im, 255, 0, 0);
        // Get points
        $points = $polygon->getPoints();
        $coords = array();
        foreach ($points as $point) {
            $coords = array_merge($coords, $point->getScreenCoordinates()); 
        }
        $coordCount = (int) (count($coords) / 2);
        
        if (true) {
			//$green = imagecolorallocate($this->_image, 192, 192, 192);
            // $green   = imagecolorallocatealpha($this->_image, 234,250,237,90);
			
			//some examples
			// 1st
			// $green   = imagecolorallocatealpha($this->_image, 240, 247, 243,100);
			
			//2nd
            $green   = imagecolorallocatealpha($this->_image, 179, 183, 181,115);
			// $green   = imagecolorallocatealpha($this->_image, 209, 206, 209,120);
			
			$border = imagecolorallocate($this->_image, 64, 64, 64);
            // first the back drop 

			$fill = imagecolorallocatealpha($this->_image, 255, 255, 255, 100);
			imagefilltoborder($this->_image, 50, 50, $border, $fill);
			
			
			//for thickness
			imagesetthickness($this->_image, $_SESSION['panthickesd']);
			
			
            //imagePolygon($this->_image, $coords, $coordCount, $green);
            // imageFilledPolygon($this->_image, $coords, $coordCount, $this->_getColor($polygon->getColor()));
			
			imageFilledPolygon($this->_image, $coords, $coordCount, $border);
			imagePolygon($this->_image, $coords, $coordCount, $border);
			

			//imagePolygon($this->_image, $coords, $coordCount, $green);
        } else {
            imagePolygon($this->_image, $coords, $coordCount, $this->_getColor($polygon->getColor()));
        }
        //imagelayereffect($this->_image, true);
		imageSaveAlpha($this->_image, true);
    }
    
    public function drawGradientPolygon(Image_3D_Polygon $polygon)
    {
        $this->drawPolygon($polygon);
    }
    
    public function setFiletye($type)
    {
        $type = strtolower($type);
        if (in_array($type, array('png', 'jpeg'))) {
            $this->_filetype = $type;
            return true;
        } else {
            return false;
        }
    }
    
    public function save($file)
    {
        switch ($this->_filetype) {
        case 'png':
        //$pattern = imagecreatefromjpeg('images/enter888_resize.jpg');
        // $pattern = imagecreatefrompng('images/new_light2.png');
    //imagelayereffect($this->_image, IMG_EFFECT_OVERLAY);
//imagecopyresampled($this->_image, $pattern, 0, 0, 0, 0, 600, 450, 618, 463);
            return imagepng($this->_image, $file);
        case 'jpeg':
//          $pattern = imagecreatefromjpeg('images/testtt.jfif');
//     imagelayereffect($this->_image, IMG_EFFECT_OVERLAY);
// imagecopyresampled($this->_image, $pattern, 0, 0, 0, 0, 600, 450, 698, 501);
            return imagejpeg($this->_image, $file);
        }
    }

    public function getSupportedShading()
    {
        return array(Image_3D_Renderer::SHADE_NO, Image_3D_Renderer::SHADE_FLAT);
    }
}

?>

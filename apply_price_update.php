<?php

include('includes/header.php');
?>
  
  <style>
  
  *{
    margin: 0px;
}

.tablinks{
    background-color: rgba(29, 26, 26, 0.068);
    border:none;
}
.tablinks:hover{
    background-color: rgba(57, 57, 201, 0.432);
}
.inputt{
    width:30%;
}
.line{
    width: 100%;
    background-color: rgba(0, 0, 0, 0.438);
    height: 1px;
}
.preview{
    color:green;
    text-align: center;
}
.tableQuick{
    margin-top: 5px;
    border-collapse: collapse;
    border-color: rgba(0, 0, 0, 0.459);
    background-color: rgba(0, 0, 0, 0.185);
}
  </style>

</head>

<body>

  <div class="tab">
    <button class="tablinks" onclick="opentab(event, 'glass_event')">Glass</button>
    <button class="tablinks" onclick="opentab(event, 'process_event')">Process</button>
    <button class="tablinks" onclick="opentab(event, 'sundry_event')">Sundry</button>
  </div>
  <div class=line></div>
  <div>
   <table>
      <tr>
        <td>Price group</td>
        <td>Update By</td>
      </tr>
      <tr>
        <td> <select>
		 <option value="">All</option>
		<?php
		$guery_price_group="SELECT * FROM `estimate_default_item`";
		$exe_price_group=mysqli_query($con,$guery_price_group);
		while($get_price_group=mysqli_fetch_array($exe_price_group))
		{
			echo'<option>'.$get_price_group['estimate_name'].'</option>';
		}
		
		
		?>
           
          </select></td>
			<td> <select id="update_by" name="update_by" onchange="show_supplier();">

            <option value="">Select Update Target</option>
            <option>Supplier</option>
            <option >Price List</option>
          </select>
		  </td>
		  
		  <td > <select style="display:none;" id="supplier_data" name="supplier_data">
		 <option value="">All</option>
		<?php
		$guery_supplier="SELECT * FROM `supplier`";
		$exe_supplier=mysqli_query($con,$guery_supplier);
		while($get_supplier=mysqli_fetch_array($exe_supplier))
		{
			echo'<option>'.$get_supplier['supplier_name'].'</option>';
		}
		
		
		?>
           
          </select></td>
      </tr>
      <tr>
        <td style="text-align:right;">Price Update</td>
        <td><input class="inputt" type="text" value="0.00"><b>%</b>&nbsp;&nbsp;<button style="height:22px;" onclick="update_listed_price();">Update Listed Price</button></td>

      </tr>
    </table>
  </div>
  
  <script>
  
  
  function update_listed_price(){
	  alert('There is no selected price');
	  }
   function show_supplier(){
	  var update_by =$('#update_by :selected').text();
	  
	  if(update_by=='Supplier')
	  {
		$('#supplier_data').css("display","block");  
	  }
	  else
	  {
		$('#supplier_data').css("display","none");  
	  }
	  
  }
  </script>
  
  <div id="glass_event" class="tabcontent" style="display:block;">
   
    <div class="preview">Preview of Pices to Update</div>
    <div class=line></div>
    <table width="100%" class="tableQuick" border="1px">
      <tr>
        <td>Quick Code</td>
        <td>Glass Product</td>
        <td>Price group</td>
        <td>price Method</td>
        <td>current price</td>
        <td>New price</td>
      </tr>
    </table>

  </div>

  <div id="process_event" class="tabcontent" style="display: none;">
   
    <div class="preview">Preview of Pices to Update</div>
    <div class=line></div>
    <table width="100%" class="tableQuick" border="1px">
      <tr>
        <td>Process Band</td>
        <td>Usage</td>
        <td>Minun Thickness</td>
        <td>Maximum Thickness</td>
        <td>Pricing</td>
        <td>New price</td>
      </tr>
    </table>
  </div>
  <div id="sundry_event" class="tabcontent" style="display: none;">
    
    <div class="preview">Preview of Pices to Update</div>
    <div class=line></div>
    <table width="100%" class="tableQuick" border="1px">
      <tr>
        <th>Quick Code</th>
        <th>Glass Product</th>
        <th>Price group</th>
        <th>price Method</th>
        <th>current price</th>
        <th>New price</th>
      </tr>

     
    </table>
    
  </div>
  <script>
  
  
 
  function opentab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
  }</script>
</body>

</html>
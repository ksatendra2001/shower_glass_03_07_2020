<?php
include('includes/header.php');
require_once('includes/config.php');	
?>
<!DOCTYPE html>
<html>
<head>
    <title>Simple Notes</title>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
body { 
   margin-left: 0px;
 font-family: Verdana;
   margin-top: 0px;
}
.main_container{
    /*width: 690px;*/
    width: 100%;
    height: 96%;
}.left{
    height: 97%;
    width: 31%;
    background: white;
    float: left;
    border-bottom-color: grey;
    border-bottom-style: solid;
    border-bottom-width: 1px;
}.left button{
    width: 100%;
    text-align: left;
    border-top-width: 3px;
    border-bottom-width: 1px;
    padding-left: 0px;
}.left div{
    font-size: 11px;
    padding: 3px;
      -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}.left div:focus {
    background-color:#9CB6E4;
}.right{
    height: 97%;
    width: 68.8%;
    background: white;
    float: left;
    border-left-color: grey;
    border-left-style: solid;
    border-left-width: 1px; 
    border-bottom-color: grey;
    border-bottom-style: solid;
    border-bottom-width: 1px;
}.right_p{
    margin-top: 4px;
    margin-bottom: 4px;
    font-size: 11px;
    font-family: Arial;
    padding: 2px;
    margin-left: 2px;
}.right_div{
    width: 100%;
    height: 93.7%;
    border-right-color: grey;
    border-right-style: solid;
    border-right-width: 1px;    
    border-top-color: grey;
    border-top-style: solid;
    border-top-width: 1px;
}.bottom{
    width: 100%;
}.bottom p{
    margin-bottom: 0px;
    font-size: 12px;
    font-family: arial;
    float: left;
    margin-top: 7px;
    margin-right: 0%;
    /*width: 81%;*/
}.btn_1{
    width: 82%;
    padding-right: 0px;
    padding-left: 0px;
    font-size: 11px;
    border-left-width: 0px;
    border-right-width: 0px;
    float: left;
}.btn_2{
    width: 18%;
    font-size: 11px;
    border-left-width: 1px;
    border-left-style: solid;
    border-left-color: black;
    border-right-width: 0px;
    float: left;
}.txt_name{
    border: none;
    width: 97%;
}.txt_checkbox{
    margin-top: 5px;
    margin-left: 17px;
    margin-right: 0px;
    margin-bottom: 0px;
    font-size: 29px;
    width: 15px;
    height: 15px;
}.txt_name:focus{
    outline: none;
    border: 1px dotted black;
}.list_item{
    width: 100%;
    height: 93%;
    overflow: auto;
}.list_item li{
	list-style-type:none;
} li:active{
	    background: #b1caf5;	
}
</style>
<body>
    <div style="width: 100%;height: 100%;">
<div class="main_container">
    <div class="left">
        <button>Group Name</button>   
                <?php
                if(ISSET($_GET['group_name']))
                {
                    $group_name=$_GET['group_name'];
                }else{
                    $group_name = NULL;
                }
                $fetch_data = "SELECT * FROM `glass_group`";
                $res=mysqli_query($con,$fetch_data);
                while ($result=mysqli_fetch_array($res)) {
                    if ($result['group_name']==$group_name) {
                    echo '<a href="glass_group.php?group_name='.$result['group_name'].'" ><div style="background:#b1caf5;">'.$result['group_name'].'</div></a>'; 
                    }else{

                    echo '<a href="glass_group.php?group_name='.$result['group_name'].'"><div>'.$result['group_name'].'</div></a>'; 
                    }
                }
                ?>
    </div>
    <div class="right">
        <label for="GN">Group Name</label><input type="text" name="GN" id="GN" style="margin-top: 3px;margin-bottom: 3px;width: 78%;margin-left: 2%;">
        <div class="right_div">
            <button id="show_item" class="btn_1">Item Name</button>
            <button class="btn_2" >Included</button>
            <div class="list_item">
				<?php
				$fetch_data = "SELECT * FROM `glass_type_table`";
                $res=mysqli_query($con,$fetch_data);
                while ($result=mysqli_fetch_array($res)) {
                    echo '<li>'.$result['glass_type_name'].'</li>'; 
                }
				?>
            </div>
        </div>
    </div>
</div>
    <div class="bottom">
        <div class="btn_11" style="float: right;">
            <button class="delete_item" disabled=""><u>D</u>elete</button>
            <button class="new_item"><u>N</u>ew Glass Group</button>
            <button onclick="window.close()"><u>C</u>lose</button>
        </div>
        <div class="btn_22" style="float: right;display: none;">
            
            <button class="save_item" onclick="save();"><u>S</u>ave</button>
            <button class="cancel_item"><u>C</u>ancel</button>
        </div>
</div>
</div>    
<script>

  $('.new_item').click(function(){
    $('.btn_11').hide();
    $('.btn_22').show();
  });
  $('.cancel_item').click(function(){       
    $('.btn_11').show();
    $('.btn_22').hide();
  });
  function save(){
  	if (!$('#GN').val()) {
  		alert('Please Fill Group Name');
  	}else{
  		var val = $('#GN').val();
  		var data = '<a href="glass_group.php"><div>'+val+'</div></a>';
  		$('.left').append(data);
  		$.ajax({
  			type:'post',
  			url:'date_save/save_glass_group.php',
  			data:{val:val},
      			success: function(data){
        }
  		});
  	}
  }
</script>
</body>
</html>
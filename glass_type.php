<?php 
require_once('includes/config.php');

if(isset($_GET['new_ruleset']))
{
$new_ruleset=$_GET['new_ruleset'];
}
else{
    $new_ruleset="";
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Simple Notes</title>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
body { 
   margin-left: 0px;
 font-family: Verdana;
   margin-top: 0px;
   font-size: 11px;
}
.main_container{
    /*width: 800px;*/
    width: 100%;
    height: 592px;
}.heading button{
    font-size: 11px !important;
    border: none;
    background: transparent;
    border-left-width: 1px;
    border-left-style: solid;
    border-left-color: #9e9191;
    padding-left: 0px;
    margin: 2px 0px 2px 0px;
}.heading{
    background: #E6E6E6;
    border: 1px solid #9e9191;
    /*width: 100%;*/
}.middle_1{
    width: 100%;
    height: 38%;
    overflow: auto;
}.middle_2{
    width: 100%;
    height: 52%;
    /*background: #D7FDD0;*/
}.bottom{
    float: right;
    margin-right: 6px;
}.bottom button{
        padding: 5px;
}.tr:hover{
   background:  #a5c1ef;
}
</style>
<body>
<div class="main_container">
<div class="ajax-loader">
  <img src="img/loading-thickbox.gif" class="img-responsive" />
</div>

<style>

.ajax-loader {
  visibility: hidden;
  background-color: rgba(255,255,255,0.7);
  position: absolute;
  z-index: +100 !important;
  width: 100%;
  height:100%;
}

.ajax-loader img {
  position: relative;
  top:50%;
  left:50%;
}
</style>
    <div class="top">
        <label for="glass_type" style="margin-left: 10px;margin-right: 5px;">Glass Type</label><input type="text" name="glass_type">
        <input type="checkbox" name="chck"><label for="chck">In Use Only</label>
    </div>
    <div class="middle_1">
        <table class="table" cellspacing="0" cellpadding="0" style="width: 100%;">
            <thead>
                <tr class="heading">
                <td style="width: 200px;">
                   <button>Glass Type Name</button>
                </td>
                <td style="width: 62px;">
                    <button>Scope</button>
                </td>
                <td style="width: 62px;">
                    <button>In Use</button>
                </td>
                <td style="width: 62px;"> 
                    <button>State</button>
                </td>
                <td style="width: 62px;">
                    <button>3D Colour</button>
                </td>
                <td style="width: 70px;">
                    <button>Opacity</button>
                </td>
                <td style="width: 125px;">
                    <button>Special Surface</button>
                </td>
                <td style="width: 70px;">
                    <button>Patterned</button>
                </td>
                <td style="width: 70px;">
                    <button>Textured</button>
                </td>
                </tr>
            </thead>
            <tbody>
            <?php
                if(ISSET($_GET['id']))
                {
                    $id=$_GET['id'];
                }else{
                    $id = "";
                }

                $fetch_data = "SELECT * FROM `glass_type_table` ORDER BY `glass_type_id`  DESC";
                $res=mysqli_query($con,$fetch_data);
                while ($result=mysqli_fetch_array($res)) {
                    if ($result["glass_type_id"]==$id) {
                        if ($result["in_use"]==1) {$in_use =  "checked";}else{$in_use =  "unchecked";}
                        if ($result["patterned"]==1) {$patterned =  "checked";}else{$patterned =  "unchecked";}
                        if ($result["textured"]==1) {$textured =  "checked";}else{$textured =  "unchecked";}
echo '<tr class="tr"    id="'.$result["glass_type_id"].'"style="background:#a5c1ef;"onclick="select(this.id);">
                        <td style="" class="glass_type_name">
                           '.$result["glass_type_name"].'
                        </td>
                        <td style="">
                            '.$result["scope"].'
                        </td>
                        <td style="text-align: center;">
                            <input type="checkbox" name="checkbox" '.$in_use.'>
                        </td>
                        <td style=""> 
                             '.$result["state"].'
                        </td>
                        <td style="">
                            <div style="height: 15px;background: '.$result["color"].';"></div>
                        </td>
                        <td style="text-align: right;padding-right:4px;">
                            '.$result["opacity"].'
                        </td>
                        <td style="">
                            '.$result["special_surface"].'
                        </td>
                        <td style="text-align: center;">
                            <input type="checkbox" name="checkbox" '.$patterned.'>
                        </td>
                        <td style="text-align: center;">
                            <input type="checkbox" name="checkbox" '.$textured.'>
                        </td>
                    </tr>';

                    }else{
                        if ($result["in_use"]==1) {$in_use =  "checked";}else{$in_use =  "unchecked";}
                        if ($result["patterned"]==1) {$patterned =  "checked";}else{$patterned =  "unchecked";}
                        if ($result["textured"]==1) {$textured =  "checked";}else{$textured =  "unchecked";}
            echo '<tr class="tr" id="'.$result["glass_type_id"].'" onclick="select(this.id);">
                        <td style="" class="glass_type_name">
                           '.$result["glass_type_name"].'
                        </td>
                        <td style="">
                            '.$result["scope"].'
                        </td>
                        <td style="text-align: center;">
                            <input type="checkbox" name="checkbox" '.$in_use.'>
                        </td>
                        <td style=""> 
                             '.$result["state"].'
                        </td>
                        <td style="" class="color">
                            <div style="height: 15px;background: '.$result["color"].';"></div>
                        </td>
                        <td style="text-align: right;padding-right:4px;" class="opacity">
                            '.$result["opacity"].'
                        </td>
                        <td style="">
                            '.$result["special_surface"].'
                        </td>
                        <td style="text-align: center;">
                            <input type="checkbox" name="checkbox" '.$patterned.'>
                        </td>
                        <td style="text-align: center;">
                            <input type="checkbox" name="checkbox" '.$textured.'>
                        </td>
                    </tr>'; 
                    }
                }
            ?>
                
            </tbody>
        </table>

        <script>
            function select(id) {
                window.location.href = 'glass_type.php?id='+id;
            }
        </script>
    </div>
    <?php
        if (!$id && !$new_ruleset) {
        $fetch_dataa = "SELECT * FROM `glass_type_table` LIMIT 1";
                $ress=mysqli_query($con,$fetch_dataa);
                $resultt=mysqli_fetch_array($ress);
                $glass_type_name = $resultt['glass_type_name'];
                $opacity = $resultt['opacity'];
                $color3d = $resultt['color'];
                if ($resultt["in_use"]==1) {$in_usee =  "checked";}else{$in_usee =  "unchecked";}
                     $id=$resultt["glass_type_id"];

        }elseif($new_ruleset == "yes"){
            $glass_type_name = "";
            $opacity = "";
            $in_usee = "";
            $color3d = "rgb(252,252,252,1);";
            $id=9999999999;
        }else{
        $fetch_dataa = "SELECT * FROM `glass_type_table` where glass_type_id=".$id;
                $ress=mysqli_query($con,$fetch_dataa);
                $resultt=mysqli_fetch_array($ress);
                $glass_type_name = $resultt['glass_type_name'];
                $opacity = $resultt['opacity'];
                $color3d = $resultt['color'];

                if ($resultt["in_use"]==1) {$in_usee =  "checked";}else{$in_usee =  "unchecked";}
        }
    ?>
    <div class="middle_2">
       <fieldset style="margin-top: 10px;height: 95%;">
           <legend >Glass Type Details</legend>
              <div style="width: 100%;height: 31%;">
                <label for="GTN" style="margin-left: 5px;margin-right: 5px;">Glass Type Name</label>
                <input type="text" name="GTN" class="GTN" value="<?php echo $glass_type_name ?>" style="width: 24%;">
                <input type="checkbox" name="IU" class="IU" style="margin-left: 6px;margin-right: 0px;" <?php echo $in_usee;?> >
                <label for="IU" style="margin-right: 10px;">In Use</label>
                <label for="select">3D Colour</label>

                <select name="select" style="width: 10%;background-color: <?php echo $color3d ?>" id="select_color">
                    <option style="background:#a9a9a9;" value="rgb(169, 169, 169, 1)" data-color=""></option>
                    <option style="background:#357ae8;" value="rgb(53, 122, 232, 1)" data-color=""></option>
                    <option style="background:#f0c36d;" value="rgb(240, 195, 109, 1)" data-color=""></option>
                    <option style="background:#f50808;" value="rgb(245, 8, 8, 1)" data-color=""></option>
                    <option style="background:#9ef06d;" value="rgb(215, 253, 208, 1)" data-color=""></option>
                    <option style="background:#09f9fb;" value="rgb(9, 249, 251, 1)" data-color=""></option>
                    <option style="background:#eeeeee;" value="rgb(238, 238, 238, 1)" data-color=""></option>
                    <option style="background:#faff23;" value="rgb(250, 255, 35, 1)" data-color=""></option>
                </select>

                <input type="color" name="mycolor" id="mycolor" onchange="color_panel();">
                <button disabled="disabled">clear</button>
                <label for="OP">Opacity</label>
                <input type="text" name="OP" class="OP" value="<?php echo $opacity ?>" style="width: 8%;text-align: right;">
        <br>
                <input type="checkbox" name="OP" disabled="disabled"><label for="OP" disabled="disabled">Oriented Pattern?</label>
                <label for="ND">Default Orientation</label>
                <select style="width: 16%;">
                    <option>No Default</option>
                </select>
                <label for="blank" style="margin-left: 10px;">Texture Image</label>
                <select style="width: 24%;">
                    <option></option>
                </select>
                <button >Find...</button>
        <br>
                <label for="NSS" style="margin-left: 5px;">Special Surface</label>
                <select>
                    <option>No Special Surface</option>
                </select>
                <label for="DF">Defalut Face</label>
                <select>
                    <option>No Default</option>
                </select>
                <label for="TS" style="margin-left: 7px;">Texture Size</label>
                <input type="text" name="TS" style="width: 6%;">
                <button style="margin-left: 10%;" onclick="draw_panel_for_glass_type(this.form);" >Preview Glass</button>
            </div>
            <div style="width: 365px;height: 178px;border: 1px solid black;text-align: center;float: right;margin-right: 8px;"><p style="margin-top: 74px;">No Image Selected</p></div>
       </fieldset>
    </div>
    <div class="bottom">
        <button onclick="window.location.href='glass_products.php'">Glass <u>P</u>roducts...</button>
        <button id="delete" onclick="delete_f();"><u>D</u>elete</button>
        <button onclick="save_btn();" class="save_btn"><u>S</u>ave</button>
        <button class="ngt_btn" onclick="location.href='glass_type.php?new_ruleset=yes';"><u>N</u>ew Glass Type</button>
        <!-- <button class="ngt_btn" ><a href="glass_type.php?new_ruleset=yes"><u>N</u>ew Glass Type</a></button> -->
        <button onclick="window.close()"><u>C</u>lose</button>
    </div>
</div>
<script>

    $("#select_color").change(function() {
        var a=$(this).children("option:selected").val();
        var option_color = $(this).children("option:selected").data('color');
            $('#select_color').css("background-color",""+a+"");
});

function draw_panel_for_glass_type(form) {
        var selected_option_color=$("#select_color").children("option:selected").val();
        var id='test'; 
        $.ajax({
            type:'post',
           url:'date_save/draw_panel_for_glass_type.php',
            data:{id:id,selected_option_color:selected_option_color},
            beforeSend: function() {
                $('.ajax-loader').css("visibility", "visible");
            },
           success: function(data){
                $('.ajax-loader').css("visibility", "hidden");
                    window.location.href = 'view_3d_image_for_glass_type.php';
           }
        });
}
 
function save_btn() {
        var id = <?php echo $id;?>;
        var glass_type_name = $('.GTN').val();
        var scope = 'Local';
        var in_use = '1';
        var state = 'Float';
        var color = $('#select_color').val();
        var opacity = $('.OP').val();
        var special_surface ='No special surface';
        var patterned ='1';
        var textured ='1';
        $.ajax({
            type:'post',
            url:'date_save/save_glass_type.php',
            data:{id:id,glass_type_name:glass_type_name,scope:scope,in_use:in_use,state:state,color:color,opacity:opacity,special_surface:special_surface,patterned:patterned,textured:textured},
            success: function(data){
              window.location.reload();
            }
        });
    }        
function delete_f() {
        var id = <?php echo $id?>;
        $.ajax({
            type:'post',
            url:'date_save/delete_glass_type.php',
            data:{id:id},
            success: function(data){
                window.location.reload();
            }
        });
    }

function color_panel(){
    var code = $('#mycolor').val();
    alert(code);
}           
</script>
</body>
</html>
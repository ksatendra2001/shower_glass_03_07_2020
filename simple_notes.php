<?
require_once('includes/config.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Simple Notes</title>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
body { 
   margin-left: 0px;
 font-family: Verdana;
   margin-top: 0px;
}
.main_container{
    /*width: 690px;*/
    width: 100%;
    height: 572px;
}.left{
    height: 97%;
    width: 31%;
    background: white;
    float: left;
    border-bottom-color: grey;
    border-bottom-style: solid;
    border-bottom-width: 1px;
}.left a{
    text-decoration: none;
    color: black;
}.left button{
    width: 100%;
    text-align: left;
    border-top-width: 3px;
    border-bottom-width: 1px;
    padding-left: 0px;
}.left div{
    font-size: 11px;
    padding: 3px;
      -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}.left div:focus {
    background-color:#9CB6E4;
}.right{
    height: 97%;
    width: 68.8%;
    background: white;
    float: left;
    border-left-color: grey;
    border-left-style: solid;
    border-left-width: 1px; 
    border-bottom-color: grey;
    border-bottom-style: solid;
    border-bottom-width: 1px;
}.right_p{
    margin-top: 4px;
    margin-bottom: 4px;
    font-size: 11px;
    font-family: Arial;
    padding: 2px;
    margin-left: 2px;
}.right_div{
    width: 100%;
    height: 93.7%;
    border-right-color: grey;
    border-right-style: solid;
    border-right-width: 1px;    
    border-top-color: grey;
    border-top-style: solid;
    border-top-width: 1px;
}.bottom{
    width: 100%;
}.bottom p{
    margin-bottom: 0px;
    font-size: 12px;
    font-family: arial;
    float: left;
    margin-top: 7px;
    margin-right: 0%;
    /*width: 81%;*/
}.btn_1{
    width: 88%;
    padding-right: 0px;
    padding-left: 0px;
    font-size: 11px;
    border-left-width: 0px;
    border-right-width: 0px;
    float: left;
}.btn_2{
    width: 12%;
    font-size: 11px;
    border-left-width: 1px;
    border-left-style: solid;
    border-left-color: black;
    border-right-width: 0px;
    float: left;
}.txt_name{
    border: none;
    width: 97%;
}.txt_checkbox{
    margin-top: 5px;
    margin-left: 17px;
    margin-right: 0px;
    margin-bottom: 0px;
    font-size: 29px;
    width: 15px;
    height: 15px;
}.txt_name:focus{
    outline: none;
    border: 1px dotted black;
}.list_item{
    width: 100%;
    height: 100%;
}
</style>
<body>
    <div style="width: 100%;height: 100%;">
<div class="main_container">
    <div class="left">
        <button>Simple Note Type</button>   
            <a href="simple_notes.php?tabindex=1"><div tabindex="1" class="select_item">User Category</div></a>
            <a href="simple_notes.php?tabindex=2"><div tabindex="2" class="select_item">Splashback Location</div></a>
            <a href="simple_notes.php?tabindex=3"><div tabindex="3" class="select_item">Splashback Installation Comment</div></a>
            <a href="simple_notes.php?tabindex=4"><div tabindex="4" class="select_item">Shower Location</div></a>
            <a href="simple_notes.php?tabindex=5"><div tabindex="5" class="select_item">Shower Installation Comment</div></a>
            <a href="simple_notes.php?tabindex=6"><div tabindex="6" class="select_item">Shopfront Location</div></a>
            <a href="simple_notes.php?tabindex=7"><div tabindex="7" class="select_item">Shopfront Installation Comment</div></a>
            <a href="simple_notes.php?tabindex=8"><div tabindex="8" class="select_item">Railing Location</div></a>
            <a href="simple_notes.php?tabindex=9"><div tabindex="9" class="select_item">Raling Installation Comment</div></a>
            <a href="simple_notes.php?tabindex=10"><div tabindex="10" class="select_item">Job category</div></a>
            <a href="simple_notes.php?tabindex=11"><div tabindex="11" class="select_item">Hardware Note</div></a>
            <a href="simple_notes.php?tabindex=12"><div tabindex="12" class="select_item">Glass Order Note</div></a>
            <a href="simple_notes.php?tabindex=13"><div tabindex="13" class="select_item">Balustrade Location</div></a>
    </div>
    <div class="right">
        <p class="right_p">These are simple text fields and any changes made here *will not* automatically update existing data.</p>
        <div class="right_div">
            <button id="show_item" class="btn_1">User Category</button>
            <button class="btn_2" >In Use</button>
            <div class="list_item">
            <?php
                if (!isset($_GET['tabindex']) ){
                    $tabindex = 1;
                }else{
                    $tabindex = $_GET['tabindex'];
                }

                $fetch_data = "SELECT * FROM `simple_notes` WHERE tabindex=".$tabindex;
                $res=mysqli_query($con,$fetch_data);
                while ($result=mysqli_fetch_array($res)) {
                    echo '<div class="data">
                   
                       <input type="text" name="name" value="'.$result['data'].'"  onchange="update_simple_notes_data(this.value, this.id);" class="txt_name_1 txt_name" id="'.$result['id'].'"  style="float: left;width: 91%; margin-top:5px;">'; 
                    if ($result['in_use'] == 1)
                        {
                        echo '<input type="checkbox" name="check" class="txt_checkbox txt_checkboxx" checked value="0" id="'.$result['id'].'" onchange="update_chk(this.value,this.id );">';
                        }else{   
                        echo '<input type="checkbox" name="check" class="txt_checkbox txt_checkboxx"  value="1" id="'.$result['id'].'" onchange="update_chk(this.value,this.id );">';
                        }
                  
                  
                echo '</div>'; 
                }
                ?>
            </div>
        </div>
    </div>
</div>
    <div class="bottom">
        <p>Select the simple note type you wish to view.</p>
        <div class="btn_11" style="float: right;">
            <button class="new_item"><u>N</u>ew Item</button>
            <button onclick="window.close()"><u>C</u>lose</button>
        </div>
        <div class="btn_22" style="float: right;display: none;">
            <!-- <button class="delete_item"><u>D</u>elete</button> -->
            <button type="button" class="save_item" name="save_item" onclick="save_simple_notes_data(this.form);"><u>S</u>ave</button>
            <button class="cancel_item"><u>C</u>ancel</button>
        </div>
</div>
</div>
<script>
    var tabindex = "";
        var i = 2;
    $( document ).ready(function() {
     tabindex = <?php echo $_GET['tabindex']?>;
     if (!sessionStorage.getItem("last_name")){
       $('#show_item').text("User category");
     }else{
       $('#show_item').text( sessionStorage.getItem("last_name"));
        }
    });

    $('.select_item').click(function() {
        // body...
       var item_value =  $(this).text();
        sessionStorage.setItem("last_name", item_value);
       var tab =  $(this).val($(this).attr('tabindex'));
     tabindex = tab[0].tabIndex;
     tabindex = <?php echo $_GET['tabindex']?>;
       $('#show_item').text( sessionStorage.getItem("last_name"));


    });

    function save_simple_notes_data(form,id){
        var d = '.txt_name_'+i;
       var input_val =  $(d).val();

       if ($('.txt_checkboxxx').is(":checked"))
        {
           var in_use = 1;
        }else{
           var in_use = 0;
        }
        $.ajax({
      type:'post',
      url:"date_save/save_simple_notes.php",
      data:{id:id,val:input_val,tab:tabindex,in_use:in_use},
      success: function(data){
            window.location.reload();
        }
      });
        i++; 
    }

 function update_chk(checked_id,id){
          $.ajax({
      type:'post',
      url:"date_save/save_update_check_for_simple_note.php",
      data:{id:id,in_use:checked_id},
      success: function(data){
            window.location.reload();
        }
      });

 }

    function update_simple_notes_data(input_val, id){
        var tabindex = "9999999";
        $.ajax({
      type:'post',
      url:"date_save/save_simple_notes.php",
      data:{id:id,val:input_val,tab:tabindex},
      success: function(data){
            window.location.reload();
        }
      });

    }    

  $('.new_item').click(function(){

    var data = '<div class="data1">'+
                    '<div style="float: left;width: 91%;">'+
                       '<input type="text" name="name" value="" class="txt_name_'+i+' txt_name" autofocus /> '+
                    '</div>'+
                    '<div style="float: left;">'+
                        '<input type="checkbox" name="check" class="txt_checkbox txt_checkboxxx" checked="">'+
                    '</div>'+
                '</div>';        
    $('.list_item').append(data);
    $('.btn_11').hide();
    $('.btn_22').show();
  });
  $('.cancel_item').click(function(){       
    $('.data1').remove();
    $('.btn_11').show();
    $('.btn_22').hide();
  });
  
</script>
</body>
</html>
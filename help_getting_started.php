<div style="100%;">
<div style="width:100%;border:1px solid #e4dede;">
<h2 style="text-align:center;">Design Getting Started Guide</h2>
<hr >
<div style="width:98%;padding:5px;text-align:justify;">
<p>Welcome to ADM Shower!</p>
<p>When you first open the ADM Shower Design application, you will see the Project List screen. While this page is an overview of working with ADM Shower Design, the <span style="color:blue;">Project List </span> page will cover all areas of the screen in more depth.</p>
<p>The main things is to note that the screen is split in to two major parts- the list of projects in the middle of the screen and the projects details section at the bottom which includes a preview of the structure selected in the list. This section also has controls for each project - from generation quotes, glass and hardware repots to modifying the job cost and sending order directly to your supplier.</p>
<hr >
<h2 style="text-align:center;">Projects List Screen</h2>
<hr >
<img src="img/help_images/indexpage.jpg" style="width:100%;">
<hr >
<h2 style="text-align:center;">Starting a New Project</h2>
<hr >
<ol>
<li>
To start designing a structure click <b>New Shower</b> at the bottom of the screen, then <b>Measurement</b> which will open Shower Type selection in a new window

<img src="img/help_images/new_shower.jpg" style="width:100%;">
</li>
<ol>
</div>
</div>
<div style="width:100%; height:5%;border:1px solid #e4dede;text-align:right;">

<button style="height: 25px;width: 64px;" onclick="closeMe();">Close</button>


</div>

</div>
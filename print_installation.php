<?php 


ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
ob_start();
 
 if(isset($_POST["create_pdf"]))  
 {  
     require_once('tcpdf/tcpdf.php');  
      $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
      $obj_pdf->SetCreator(PDF_CREATOR);  
      $obj_pdf->SetTitle("installation_report");  
      $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
      $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
      $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
      $obj_pdf->SetDefaultMonospacedFont('helvetica');  
      $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
      $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
      $obj_pdf->setPrintHeader(false);  
      $obj_pdf->setPrintFooter(false);  
      $obj_pdf->SetAutoPageBreak(TRUE, 10);  
      $obj_pdf->SetFont('helvetica', '', 12);  
      $obj_pdf->AddPage();  
      $content = '';  
      $content .= '  
     
      <table  cellspacing="0" >  
           <tr>  
                <th width="100%">
				
				</th>  
                
           </tr>  
      ';  
	  
	  $content .= ' 

	<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
	<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.6.min.js"></script>


 ';	  
	  
  include('includes/config.php');

	$job_no=$_GET['job_no'];
	$get_all_data="SELECT * FROM job_details WHERE job_id='".$job_no."'";
	$exe_data=mysqli_query($con,$get_all_data);
	$get_data=mysqli_fetch_array($exe_data);
	$customer_idss=$get_data['customer_id'];
	$customer_namess=$get_data['customer_name'];
	$descriptionss=$get_data['description'];
	$mobiless=$get_data['mobile'];
	$emailss=$get_data['email'];
	$addressss=$get_data['address'];
	$job_name=$get_data['job_name'];
	$work=$get_data['work'];
	$site=$get_data['site'];
	$quantity=$get_data['quantity'];



	$shower_typess=$get_data['shower_type'];
	$door_typess=$get_data['door_type'];
	$hinges_typess=$get_data['hinges_type'];
	$shower_locationss=$get_data['shower_location'];
	$panel_qtyss=$get_data['panel_qty'];
	$glass_typess=$get_data['glass_type'];
	$glass_supplierss=$get_data['glass_supplier'];
	$lengthAss=$get_data['lengthA'];
	$lengthBss=$get_data['lengthB'];
	$lengthCss=$get_data['lengthC'];
	$lengthDss=$get_data['lengthD'];
	$lengthEss=$get_data['lengthE'];
	$lengthFss=$get_data['lengthF'];
	$lengthGss=$get_data['lengthG'];
	$lengthHss=$get_data['lengthH'];
	$lengthIss=$get_data['lengthI'];
	$angle1ss=$get_data['angle2'];
	$angle2ss=$get_data['angle2'];
	$angle3ss=$get_data['angle3'];
	$angle4ss=$get_data['angle4'];
	$angle5ss=$get_data['angle5'];
	$angle6ss=$get_data['angle6'];
	$paneltype1ss=$get_data['paneltype1'];
	$paneltype2ss=$get_data['paneltype2'];
	$paneltype3ss=$get_data['paneltype3'];
	$paneltype4ss=$get_data['paneltype4'];
	$paneltype5ss=$get_data['paneltype5'];
	$paneltype6ss=$get_data['paneltype6'];
	$glass_thickness1ss=$get_data['glass_thickness1'];
	$glass_thickness2ss=$get_data['glass_thickness2'];
	$glass_thickness3ss=$get_data['glass_thickness3'];
	$glass_thickness4ss=$get_data['glass_thickness4'];
	$glass_thickness5ss=$get_data['glass_thickness5'];
	$glass_thickness6ss=$get_data['glass_thickness6'];
	$glass_imgss=$get_data['glass_img'];

	$installation_comment=$get_data['installation_comment'];
	$glass_order_comment=$get_data['glass_order_comment'];
	$information_warnings_comment=$get_data['information_warnings_comment'];


	$query_glassthickness="SELECT * FROM glassthickness WHERE `job_no` ='$job_no'";
	$exe_glassthickness=mysqli_query($con,$query_glassthickness);
	while($get_glassthickness=mysqli_fetch_array($exe_glassthickness))
	{
		$pan_no=$get_glassthickness['panel_no'];
		${"glass_thickness$pan_no"}=$get_glassthickness['value'];
	}
    
     $valA=$valB=$valC=$valD=$valE=$valF=$valG=$valH=$valI=0;
	$valueA=$valueB=$valueC=$valueD=$valueE=$valueF=$valueG=$valueH=$valueI=0;
	
	
      $content .= '<tr>  
                          <td>
						  


	<div class="print-glassorder-main">


	<div style="border: 1px solid #000; width:100%;height:20%;border-radius:20px;">
	<table>
	<tr>
	<td style="width:45%"><b style="font-size:15px;">INSTALLATION SHEET </b></td>
	<td style="width:30%"><b>Job Name: '.$job_name .'</b>
	</td>
	<td style="width:25%"><b>Date: '.date('d/m/Y') .'</b>


	</td>

	</tr>
	</table>

	</div>';		

	 $content .= '
	 
	 

	<table border="1">
	<tr>
	<td rowspan="2">
	<img src="date_save/images/panel_drawing/'.$job_no .'/01-01.png" >
	</td>
	<td>


	<div style="height:200px;">
	<p>Customer Name: '.$customer_namess.'<br />
	Location: '.$shower_locationss.'<br />
	Qauntity: '.$quantity.'<br />
	PO #: WHITEHORN</p>


	</div>
	</td></tr>
	<tr>
	<td>
	<div style="height:200px;">
	<b>OUT OF PLUMB/LEVEL CONDITIONS</b>
	<div style="width:70%;">
	';

	$content_svg='';
	$content_svg .= '<svg height="520" width="510">
	<line x1="280" y1="140" x2="280" y2="245" style="stroke:rgb(0,0,0);stroke-width:1" />
	<line x1="280" y1="245" x2="480" y2="245" style="stroke:rgb(0,0,0);stroke-width:1" />
	<line x1="480" y1="245" x2="480" y2="140" style="stroke:rgb(0,0,0);stroke-width:1" />
	</svg>';

	$content .= '
	<br /><br /><br /><br /><br /><br /><br />
	</div>
	</div>
	</td>
	</tr>
	</table>
	 ';
$heightttt=$panel_qtyss*450;
	$content .= '';

	$widthhh=(($panel_qtyss*80)+60);




	$first_x=50;
	$sec_x=130;
	$first_y=150;
	$sec_y=270;
	

for($p=1; $p<=$panel_qtyss; $p++)
{
		${"content_svg$p"}='';
			$query_get_panwlwise_data="SELECT * FROM panelwise_dimension WHERE job_no='$job_no' AND panel_no='$p'";

			$exe_get_panwlwise_data=mysqli_query($con,$query_get_panwlwise_data);
			$get_panwlwise_data=mysqli_fetch_array($exe_get_panwlwise_data);
			$values=$get_panwlwise_data['values'];
			
			$first_part=explode(">>",$values);
			//print_r(count($first_part));echo'<br />';
			$char='A';
			$noss=count($first_part);
			$nossdd=$noss-1;
			
			$chararray=array();
			for($charn=0;$charn<$nossdd;$charn++)
			{
			$arr=explode("-",$first_part[$charn]);
			${"val$char"}=$arr[1];
			${"value$char"}=$arr[1];
			
			$chararray[]=$char;
			$char++;	
			}


		$prev_panel_no=$p-1;
		$next_panel_no=$p+1;	
		$paneltitle='paneltype'.$p.'';
		$query_panel_type="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitle'";
		$exe_panel_type=mysqli_query($con,$query_panel_type);
		$get_panel_type=mysqli_fetch_array($exe_panel_type);
		$panel_typess=$get_panel_type['value'];

		$paneltitlenext='paneltype'.$next_panel_no.'';
		$query_panel_typenext="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitlenext'";
		$exe_panel_typenext=mysqli_query($con,$query_panel_typenext);
		$get_panel_typenext=mysqli_fetch_array($exe_panel_typenext);
		$panel_typessnext=$get_panel_typenext['value'];
		
		
		$paneltitleprev='paneltype'.$prev_panel_no.'';
		$query_panel_typeprev="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitleprev'";
		$exe_panel_typeprev=mysqli_query($con,$query_panel_typeprev);
		$get_panel_typeprev=mysqli_fetch_array($exe_panel_typeprev);
		$panel_typessprev=$get_panel_typeprev['value'];

			
			
			if($panel_typess=='Side-Nib-Notch'||$panel_typess=='Side-Notch'||$panel_typess=='Return-Notch'||$panel_typess=='Return-Nib-Notch')
			{
			
			
	
			
	${"content_svg$p"} .='
		 	<svg height="500" width="350">
		  <line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="420" x2="80" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="90" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="100" y1="420" x2="150" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		   <line x1="160" y1="400" x2="160" y2="390" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="160" y="385" fill="#FE8D28">'.$valueC.'"</text>
		   <line x1="160" y1="375" x2="160" y2="350" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		   <line x1="150" y1="365" x2="190" y2="365" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="200" y="365" fill="#A00082">'.$valueD.'"</text>
		   <line x1="220" y1="365" x2="250" y2="365" style="stroke:rgb(160,0,130);stroke-width:2" />
		   
		   
		  
		  
		  <line x1="270" y1="50" x2="270" y2="190" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <text x="270" y="200" fill="#007D7D">'.$valueE.'"</text>
		  <line x1="270" y1="205" x2="270" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(125,0,0);stroke-width:2" />
		  <text x="135" y="10" fill="#7D0000">'.$valueF.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="400" x2="150" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <line x1="150" y1="400" x2="150" y2="350" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="150" y1="350" x2="250" y2="350" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(125,0,0);stroke-width:2" />
		</svg>';

			
			}
			elseif($panel_typess=='Side-Notch-Double'||$panel_typess=='Return-Notch-Double')
			{
			
			
			
	${"content_svg$p"} .='

			<svg height="500" width="350">
		 <line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="420" x2="80" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="80" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="100" y1="420" x2="117" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		   <line x1="125" y1="400" x2="125" y2="390" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="125" y="385" fill="#FE8D28">'.$valueC.'"</text>
		   <line x1="125" y1="375" x2="125" y2="360" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		   <line x1="117" y1="370" x2="140" y2="370" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="140" y="372" fill="#A00082">'.$valueD.'"</text>
		   <line x1="160" y1="370" x2="183" y2="370" style="stroke:rgb(160,0,130);stroke-width:2" />
		   
		   
		  
		  
		  <line x1="195" y1="360" x2="195" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <text x="195" y="350" fill="#007D7D">'.$valueE.'"</text>
		  <line x1="195" y1="340" x2="195" y2="320" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="183" y1="335" x2="210" y2="335" style="stroke:rgb(125,0,0);stroke-width:2" />
		  <text x="210" y="335" fill="#7D0000">'.$valueF.'"</text>
		  <line x1="220" y1="335" x2="250" y2="335" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  
		  <line x1="270" y1="50" x2="270" y2="190" style="stroke:rgb(0,0,100);stroke-width:2" />
		  <text x="270" y="200" fill="#000064">'.$valueG.'"</text>
		  <line x1="270" y1="205" x2="270" y2="320" style="stroke:rgb(0,0,100);stroke-width:2" />
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(200,40,40);stroke-width:2" />
		  <text x="135" y="10" fill="#C82828">'.$valueH.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(200,40,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="400" x2="117" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="117" y1="400" x2="117" y2="360" style="stroke:rgb(254,141,0);stroke-width:2" />
		  <line x1="117" y1="360" x2="183" y2="360" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="183" y1="360" x2="183" y2="320" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <line x1="183" y1="320" x2="250" y2="320" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  
		  
		  <line x1="250" y1="50" x2="250" y2="320" style="stroke:rgb(0,0,100);stroke-width:2" />
		  
		  
		   <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(200,40,40);stroke-width:2" />
		  
		</svg>';	
			
			}
			else
			{
			if($panel_typess=='Door-Hinge-Left')
			{
			
			
			
	${"content_svg$p"} .='
	
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28">'.$valueC.'"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082">'.$valueD.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="80" x2="80" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="80" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="110" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="110" x2="50" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="340" x2="80" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="340" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="370" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="370" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		 <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>';
			
			}
			elseif($panel_typess=='Door-Hinge-Right'){

			
			
		
	${"content_svg$p"} .='

		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28">'.$valueC.'"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082">'.$valueD.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="80" x2="220" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="80" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="110" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="110" x2="250" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  <line x1="250" y1="340" x2="220" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="340" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="370" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  

		<line x1="250" y1="370" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>';
			
			
			}
			else{	
		if($panel_typess=='Side')
		{
				
			
			if($panel_typessnext=='Door-Hinge-Left')
			{

			
			
	${"content_svg$p"} .='

			<svg height="500" width="350">
		
			<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28">'.$valueC.'"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082">'.$valueD.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="80" x2="220" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="80" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="110" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="110" x2="250" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  <line x1="250" y1="340" x2="220" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="340" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="370" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  

			<line x1="250" y1="370" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
			</svg>';

					}
			elseif($panel_typessprev=='Door-Hinge-Right')
			{
			
			
	${"content_svg$p"} .='

			<svg height="500" width="350">
		
			<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28">'.$valueC.'"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082">'.$valueD.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="80" x2="80" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="80" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="110" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="110" x2="50" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="340" x2="80" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="340" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="370" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="370" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		 <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
			</svg>';

			
			}
			else
			{
			
			
			
	${"content_svg$p"} .='

			<svg height="500" width="350">
			
			<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28">'.$valueC.'"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082">'.$valueD.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
			
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
			</svg>';

			}
			
		}
			
			else{	
			
		
			
				
	${"content_svg$p"} .='
			<svg height="500" width="350">
			
			
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">'.$valueA.'"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28">'.$valueB.'"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28">'.$valueC.'"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082">'.$valueD.'"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>';

			
			}
			
			
	}
	}
		


}
	 $content .= '</div></td></tr>';  
		  

	$obj_pdf->ImageSVG('@' . $content_svg, $x=15, $y=30, $w='', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false);

$obj_pdf->writeHTML($content, true, false, true, false, '');  


$qwe = 10;

    for($p=1; $p<=$panel_qtyss; $p++)
	{
		if ($p==1) {
			$obj_pdf->ImageSVG('@' . ${"content_svg$p"}, $x=30, $y=110, $w='200', $h='400', $link='', $align='N', $palign='C', $border=0, $fitonpage=true);
		}else{
      		$obj_pdf->ImageSVG('@' . ${"content_svg$p"}, $x=30, $y=$qwe, $w='200', $h='400', $link='', $align='N', $palign='C', $border=0, $fitonpage=true);
  		}
      		$qwe = $qwe+100;
    }
			$obj_pdf->Ln(200);





	$content1='';

	$content1 .= '<div class="print-glassorder-head">
	<div class="glassorderhead1"><h2><u>Hardware Details</u></h2></div>
	<div class="glassorderhead2"></div>
	</div>

	</div>	';		

	 $content1 .= '

	<div>
	<table border="1">
	<tr class="hardwaredetail-table-thead">
	<th style="width:10%;"><b>Qty </b></th>
	<th style="width:25%;"><b>Description</b></th>
	<th style="width:35%;"><b>Finish</b></th>
	<th style="width:13%;"><b>Unit Cost</b></th>
	<th style="width:13%;"><b>Subtotal</b></th>
	</tr>';
	$total=0;
	$query_hardware_added="SELECT DISTINCT hardware_id, cate_type_name, hardware_name, description, model_name FROM added_hardware_for_order WHERE job_no='$job_no' AND customer_id='$customer_idss'";
	$exe_hardware_added=mysqli_query($con,$query_hardware_added);
	while($get_hardware_added=mysqli_fetch_array($exe_hardware_added))
	{
	//$hardware_qty=$get_hardware_added[''];
	$hardware_id=$get_hardware_added['hardware_id'];
	$cate_type_name=$get_hardware_added['cate_type_name'];
	$hardware_name=$get_hardware_added['hardware_name'];
	$description=$get_hardware_added['description'];
	$model_name=$get_hardware_added['model_name'];

	$query_get_qty="SELECT COUNT( id ) as qty FROM added_hardware_for_order WHERE hardware_id ='$hardware_id' AND job_no ='$job_no'";
	$exe_get_qty=mysqli_query($con,$query_get_qty);
	$get_qty=mysqli_fetch_array($exe_get_qty);
	$qty=$get_qty['qty'];

	$query_get_image="SELECT * FROM hardware WHERE id='$hardware_id'";
	$exe_get_image=mysqli_query($con,$query_get_image);
	$get_image=mysqli_fetch_array($exe_get_image);
	$img=$get_image['img'];
	$price=$get_image['price'];
	$subtotal=$price*$qty.'.00';
	$total=$total+$subtotal.'.00';

	$content1 .= '
	<tr>
	<td>'.$qty.' 
	 </td>
	<td>'.$description.'</td>
	<td>'.$hardware_name.'</td>
	<td>'.$price.'</td>
	<td>'.$subtotal.'</td>
	</tr>

	';
	}


	$content1 .= '

	<tr>
	<td colspan="4" align="right"><b style="font-size:15px;border-top: 1px solid black;">Total: </b></td>
	<td><b style="font-size:15px;border-top: 1px solid black;">'.$total .'</b></td>
	</tr>


	</table>
	<br /><br />
	</div>
	';

	$content1 .= '<div class="print-glassorder-head">
	<div class="glassorderhead1"><h2><u>Installation Comment</u></h2></div>
	<div class="glassorderhead2" style="border:1px solid black;">

	'.$installation_comment.'

	<br /><br /><br /><br /><br /><br />
	</div>
	</div>
	';
		  
	      $obj_pdf->writeHTML($content1);  
		  
		  $filename="pdf_data/27/test.pdf";

	      $obj_pdf->Output('installation_report.pdf','D');

	 }









	echo'
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
	<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.6.min.js"></script>

	';
	  include('includes/config.php');
	$job_no=$_GET['job_no'];
	$get_all_data="SELECT * FROM job_details WHERE job_id='".$_GET['job_no']."'";
	$exe_data=mysqli_query($con,$get_all_data);
	$get_data=mysqli_fetch_array($exe_data);
	$customer_idss=$get_data['customer_id'];
	$customer_namess=$get_data['customer_name'];
	$descriptionss=$get_data['description'];
	$mobiless=$get_data['mobile'];
	$emailss=$get_data['email'];
	$addressss=$get_data['address'];
	$job_name=$get_data['job_name'];

	$shower_typess=$get_data['shower_type'];
	$door_typess=$get_data['door_type'];
	$hinges_typess=$get_data['hinges_type'];
	$shower_locationss=$get_data['shower_location'];
	$panel_qtyss=$get_data['panel_qty'];
	$glass_typess=$get_data['glass_type'];
	$glass_supplierss=$get_data['glass_supplier'];

	$glass_imgss=$get_data['glass_img'];

	?>


	<script src="pdfjs/jquery.min.js"></script>
	<script src="pdfjs/dom-to-image.min.js"
	    integrity="sha256-c9vxcXyAG4paArQG3xk6DjyW/9aHxai2ef9RpMWO44A=" crossorigin="anonymous"></script>
	<script src="pdfjs/jspdf.min.js"></script>



	<div id="content" style="background: #fff;border-bottom: 1px solid #ffffff;">


	<div style="width: 100%;border: 1px solid #000;">
	 <div style="float: left; width: 50%;"><h2>&nbsp;INSTALLATION SHEET</h2></div>
	 <div style="float: left; width: 25%;"><br /><b style="font-size:13px;">Job Name: <?php echo$job_name ?></b></div>
	 <div style="float: left; width: 25%;"><br /><b style="font-size:13px;"><?php echo date('d/m/yy'); ?></b></div>
	 <br style="clear: left;" />
	</div>
	<div style="width: 100%;height:400px;border-right: 1px solid #000;border-left: 1px solid #000;border-bottom: 1px solid #000;">
	<div style="width:60%;float:left;height:400px;">

	<img src="date_save/images/panel_drawing/<?php echo$job_no  ?>/01-01.png" style="width:100%;">
	</div>


	<div style="width:39%;float:left;height:400px;border-left:1px solid black;">
	<div style="height:200px;border-bottom: 1px solid #000;"></div>


	<div style="height:200px;">
	<b>OUT OF PLUMB/LEVEL CONDITIONS</b>
	<div style="padding:5px;">
	<svg height="160" width="300">
	<line x1="0" y1="0" x2="0" y2="155" style="stroke:rgb(0,0,0);stroke-width:2" />
	<line x1="0" y1="155" x2="200" y2="155" style="stroke:rgb(0,0,0);stroke-width:2" />
	<line x1="200" y1="155" x2="200" y2="0" style="stroke:rgb(0,0,0);stroke-width:2" />
	</svg>
	</div>
	</div></div>

	</div>




	<div> 
	
<!-- Notch end-->
			<?php
			for($p=1; $p<=$panel_qtyss; $p++)
			{
			$prev_panel_no=$p-1;
		$next_panel_no=$p+1;
		$paneltitle='paneltype'.$p.'';
		$query_panel_type="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitle'";
		$exe_panel_type=mysqli_query($con,$query_panel_type);
		$get_panel_type=mysqli_fetch_array($exe_panel_type);
		$panel_typess=$get_panel_type['value'];
		
		$paneltitlenext='paneltype'.$next_panel_no.'';
		$query_panel_typenext="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitlenext'";
		$exe_panel_typenext=mysqli_query($con,$query_panel_typenext);
		$get_panel_typenext=mysqli_fetch_array($exe_panel_typenext);
		$panel_typessnext=$get_panel_typenext['value'];
		
		
		$paneltitleprev='paneltype'.$prev_panel_no.'';
		$query_panel_typeprev="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitleprev'";
		$exe_panel_typeprev=mysqli_query($con,$query_panel_typeprev);
		$get_panel_typeprev=mysqli_fetch_array($exe_panel_typeprev);
		$panel_typessprev=$get_panel_typeprev['value'];
		
		
		$query_get_panwlwise_data="SELECT * FROM panelwise_dimension WHERE job_no='$job_no' AND panel_no='$p'";
		$exe_get_panwlwise_data=mysqli_query($con,$query_get_panwlwise_data);
		$get_panwlwise_data=mysqli_fetch_array($exe_get_panwlwise_data);
		$values=$get_panwlwise_data['values'];
			
		$first_part=explode(">>",$values);
		//print_r(count($first_part));echo'<br />';
		$char='A';
		$noss=count($first_part);
		$nossdd=$noss-1;
			
		$chararray=array();
		for($charn=0;$charn<$nossdd;$charn++)
		{
		$arr=explode("-",$first_part[$charn]);
		//print_r($arr);echo'<br />';
		${"val$char"}=$arr[1];
		${"value$char"}=$arr[1];
			
		$chararray[]=$char;
		$char++;	
		}
			
			
			echo'
			<hr />
			<h2 style="text-align:center;">Panel- '.$p.'</h2>
			<hr />
			';
			
			if($panel_typess=='Side-Nib-Notch'||$panel_typess=='Side-Notch'||$panel_typess=='Return-Notch'||$panel_typess=='Return-Nib-Notch')
			{
			
			
			
			?>
			
			<!--  Notch Start-->
		 	<svg height="500" width="350">
		  <line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="420" x2="80" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="90" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="100" y1="420" x2="150" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		   <line x1="160" y1="400" x2="160" y2="390" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="160" y="385" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		   <line x1="160" y1="375" x2="160" y2="350" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		   <line x1="150" y1="365" x2="190" y2="365" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="200" y="365" fill="#A00082"><?php echo$valueD; ?>"</text>
		   <line x1="220" y1="365" x2="250" y2="365" style="stroke:rgb(160,0,130);stroke-width:2" />
		   
		   
		  
		  
		  <line x1="270" y1="50" x2="270" y2="190" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <text x="270" y="200" fill="#007D7D"><?php echo$valueE; ?>"</text>
		  <line x1="270" y1="205" x2="270" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(125,0,0);stroke-width:2" />
		  <text x="135" y="10" fill="#7D0000"><?php echo$valueF; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="400" x2="150" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <line x1="150" y1="400" x2="150" y2="350" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="150" y1="350" x2="250" y2="350" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(125,0,0);stroke-width:2" />
		</svg>
			<!-- Notch end-->
			
			
			<?php
			}
			elseif($panel_typess=='Side-Notch-Double'||$panel_typess=='Return-Notch-Double')
			{
			?>
			
			
		<!--  Notch-Double Start-->
			<svg height="500" width="350">
		 <line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="420" x2="80" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="80" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="100" y1="420" x2="117" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		   <line x1="125" y1="400" x2="125" y2="390" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="125" y="385" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		   <line x1="125" y1="375" x2="125" y2="360" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		   <line x1="117" y1="370" x2="140" y2="370" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="140" y="372" fill="#A00082"><?php echo$valueD; ?>"</text>
		   <line x1="160" y1="370" x2="183" y2="370" style="stroke:rgb(160,0,130);stroke-width:2" />
		   
		   
		  
		  
		  <line x1="195" y1="360" x2="195" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <text x="195" y="350" fill="#007D7D"><?php echo$valueE; ?>"</text>
		  <line x1="195" y1="340" x2="195" y2="320" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="183" y1="335" x2="210" y2="335" style="stroke:rgb(125,0,0);stroke-width:2" />
		  <text x="210" y="335" fill="#7D0000"><?php echo$valueF; ?>"</text>
		  <line x1="220" y1="335" x2="250" y2="335" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  
		  <line x1="270" y1="50" x2="270" y2="190" style="stroke:rgb(0,0,100);stroke-width:2" />
		  <text x="270" y="200" fill="#000064"><?php echo$valueG; ?>"</text>
		  <line x1="270" y1="205" x2="270" y2="320" style="stroke:rgb(0,0,100);stroke-width:2" />
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(200,40,40);stroke-width:2" />
		  <text x="135" y="10" fill="#C82828"><?php echo$valueH; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(200,40,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="400" x2="117" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="117" y1="400" x2="117" y2="360" style="stroke:rgb(254,141,0);stroke-width:2" />
		  <line x1="117" y1="360" x2="183" y2="360" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="183" y1="360" x2="183" y2="320" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <line x1="183" y1="320" x2="250" y2="320" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  
		  
		  <line x1="250" y1="50" x2="250" y2="320" style="stroke:rgb(0,0,100);stroke-width:2" />
		  
		  
		   <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(200,40,40);stroke-width:2" />
		  
		</svg>
			<!-- Notch-Double end-->	
			
			<?php
			}
			else
			{
			if($panel_typess=='Door-Hinge-Left')
			{
				
			?>
			
			
		<!-- hinge left start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="80" x2="80" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="80" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="110" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="110" x2="50" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="340" x2="80" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="340" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="370" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="370" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		 <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
		
		<!-- hinge left end-->
			
			
			<?php
			}
			elseif($panel_typess=='Door-Hinge-Right'){
			?>
			
			
		
			<!-- hinge right start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="80" x2="220" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="80" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="110" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="110" x2="250" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  <line x1="250" y1="340" x2="220" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="340" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="370" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  

		<line x1="250" y1="370" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>

		<!-- hinge right end-->
			
			
			<?php
			}
			else{	
			if($panel_typess=='Side')
			{
				
			?>
			
			<?php
			
			if($panel_typessnext=='Door-Hinge-Left')
			{
			?>
			
			
			<!-- hinge right start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="80" x2="220" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="80" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="110" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="110" x2="250" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  <line x1="250" y1="340" x2="220" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="340" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="370" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  

		<line x1="250" y1="370" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>

		<!-- hinge right end-->
			
			<?php
			}
			elseif($panel_typessprev=='Door-Hinge-Right')
			{
			?>
			
		<!-- hinge left start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="80" x2="80" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="80" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="110" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="110" x2="50" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="340" x2="80" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="340" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="370" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="370" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		 <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
		
		<!-- hinge left end-->
			
			<?php
			}
			else
			{
			?>
			
			
			<!-- plane start-->
			<svg height="500" width="350">
			
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
			
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
			<?php
			}
			
			}
			
			else{	
			
			?>
			
			
			
				
			<!-- plane start-->
			<svg height="500" width="350">
			
			
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
			<!-- plane end-->
			
			<?php
			}
			
			
			}
			}
			}
			?>
			
		

	</div>
<div class="print-glassorder-head" style="padding-top:20%;">
<div class="glassorderhead1"><h2><u>Hardware Details</u></h2></div>
<div class="glassorderhead2"></div>
</div>

<div style="border:1px solid black;">
<style>
thead tr th{border-bottom: 1px solid black;
border-top: 1px solid black;  
  border-collapse:separate; 
  border-spacing:10px 10px;} 
</style>
<table class="hardwaredetail-table">
<thead>
<tr class="hardwaredetail-table-thead" style="text-align:center;">
<th style="width:10%;">Qty</th>
<th style="width:20%;">Part No.</th>
<th style="width:50%;">Description</th>
<th style="width:20%;">Finish</th>
<tr>
</thead>

<tbody>
<?php  
$total=0;
$query_hardware_added="SELECT DISTINCT hardware_id, cate_type_name, hardware_name, description, model_name FROM added_hardware_for_order WHERE job_no='$job_no' AND customer_id='$customer_idss'";
$exe_hardware_added=mysqli_query($con,$query_hardware_added);
while($get_hardware_added=mysqli_fetch_array($exe_hardware_added))
{
//$hardware_qty=$get_hardware_added[''];
$hardware_id=$get_hardware_added['hardware_id'];
$cate_type_name=$get_hardware_added['cate_type_name'];
$hardware_name=$get_hardware_added['hardware_name'];
$description=$get_hardware_added['description'];
$model_name=$get_hardware_added['model_name'];

$query_get_qty="SELECT COUNT( id ) as qty FROM added_hardware_for_order WHERE hardware_id ='$hardware_id' AND job_no ='$job_no'";
$exe_get_qty=mysqli_query($con,$query_get_qty);
$get_qty=mysqli_fetch_array($exe_get_qty);
$qty=$get_qty['qty'];

$query_get_image="SELECT * FROM hardware WHERE id='$hardware_id'";
$exe_get_image=mysqli_query($con,$query_get_image);
$get_image=mysqli_fetch_array($exe_get_image);
$img=$get_image['img'];
$price=$get_image['price'];
$subtotal=$price*$qty.'.0000';
$total=$total+$subtotal.'.0000';
echo'<tr style="text-align:center;">
<td>'.$qty.'</td>
<td>'.$model_name.'</td>
<td>'.$description.'</td>
<td>'.$hardware_name.'</td>
</tr>';
}
?>

<!--
<tr>
<td colspan="4" align="right"><b style="font-size:15px;border-top: 1px solid black;">Total: </b></td>
<td><b style="font-size:15px;border-top: 1px solid black;"><?php echo$total ?></b></td>
</tr>-->
</tbody>

</table>
</div>
<div style="border:1px solid black;">
<div class="print-glassorder-head" style="padding:2%;">
<div class="glassorderhead1"><h2><u>Installation Comment</u></h2></div>
<div class="glassorderhead2"></div>
</div>
<div style="padding:4%;">
<p></p>
</div>

</div>






<div id="editor"></div>




</div>




</div>
<div class="glass-order-footerbutton">

<table style="width:100%">
<tr>
<td style="width:10%;"><input type="text" name="copy" id="copy" value="1" style="width:100%;"></td>
<td style="width:10%;"> 
<button name="email" value="Email" id="email" ><a href="email_repots.php?job_no=<?php echo$_GET['job_no']; ?>&report_name=Installation Report" onclick=" return email_repots(this, 'notes')">Email</a></button></td>
<td style="width:10%;"><form method="post">  
<input type="submit" name="create_pdf" class="btn btn-danger" value="Save PDF" />  
 </form> </td>
<td style="width:10%;"><input type="button" name="advance_print" onclick="window.print()" value="Advanced Print" id="advance_print"></td>
<td style="width:10%;"><input type="button" name="print" onclick="window.print()" value="Print" id="print"></td>
<td style="width:10%;"><input type="button" name="fax" value="Fax" id="fax"> </td>
<td style="width:40%;"><input type="button" name="close" value="Close" id="close" onclick="closeMe();"> </td>

</tr>
</table>


</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script> -->
<!-- <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>  -->
<!-- // dependency for Kendo UI API -->
<!--  <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
 <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script> -->

<script>
function pdf(){ 

html2canvas(document.body).then(function(canvas) {
    // Export canvas as a blob 
    canvas.toBlob(function(blob) {
        // Generate file download
        window.saveAs(blob, "yourwebsite_screenshot.png");
    });
});

// kendo.drawing
//     .drawDOM("#content", 
//     { 
//         paperSize: "A4",
//         margin: { top: "1cm", bottom: "1cm" },
//         scale: 0.8,
//         height: 500
//     })
//         .then(function(group){
//         kendo.drawing.pdf.saveAs(group, "Exported.pdf")
//     });
}


function email_repots(mylink, windowname){

	if (! window.focus)return true; var href; if (typeof(mylink) == 'string') href=mylink; else href=mylink.href; window.open(href, windowname, 'width=700,height=600,scrollbars=yes'); return false; 

	
}

// function pdf(){
// 	let doc = new jsPDF('p','pt','a4');
// 	doc.addHTML(document.body,function() {
//     doc.save('Installation.pdf');
// });
// }

// function pdf() {
// 		const filename  = 'ThisIsYourPDFFilename.pdf';

// 		html2canvas(document.querySelector('#content')).then(canvas => {
// 			let pdf = new jsPDF('p', 'pt', 'a4');
// 			pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 298);
// 			pdf.save(filename);
// 		});
// 	}
// var doc = new jsPDF();
// var specialElementHandlers = {
//     '#editor': function (element, renderer) {
//         return true;
//     }
// };

// $('#cmd').click(function () {
//     doc.fromHTML($('#content').html(), 15, 15, {
//         'width': 170,
//             'elementHandlers': specialElementHandlers
//     });
//     doc.save('sample-file.pdf');
// });

</script>


<style>
.viewLengthA{top:53%; left:37%;position: absolute;}
.viewLengthB{top:73%; left:51%;position: absolute;}
.viewLengthC{top:68%; left:61%;position: absolute;}
.viewLengthD{top:47%; left:70%;position: absolute;}
</style>

<script>
function closeMe()
{
    window.opener = self;
    window.close();
}
</script>

<!--
<table class="print-glassorder-main">
<tr class="print-glassorder-head">
<td class="glassorderhead1"><h1>Glass Order</h1></td>
<td class="glassorderhead2">Printed On <?php echo date('d/m/Y') ?></td>
</tr>
</table>
-->

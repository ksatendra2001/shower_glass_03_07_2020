<?php
	include('includes/header.php');
	$job_no = $_GET['job_no'];
	if(isset($_GET['panel_no']))
	{
	$panel_no=$_GET['panel_no'];
	}
	else{
	$panel_no=1;	
	}
	
	$sql = "SELECT * FROM job_details where job_id =".$job_no;
	$result = mysqli_fetch_array(mysqli_query($con,$sql));
	
	$panel_qty=$result['panel_qty'];
	
	$valA=$valB=$valC=$valD=$valE=$valF=$valG=$valH=$valI=0;
	$valueA=$valueB=$valueC=$valueD=$valueE=$valueF=$valueG=$valueH=$valueI=0;
?>

<!--<link href="css/fontawesome.min.css" rel="stylesheet" type="text/css">-->
<script type="text/javascript" src="css/all.js"></script>
<style type="text/css">
	html *
	{
	    color: #000;
	    font-family: Verdana;
	    font-size: 11px;
	}
	.main{
	/*	width: fit-content;
		height: fit-content;*/
	}.section1{
		width: 40%;
		height: fit-content;
		border-right: 1px solid grey;
		float: left;
	}.section2{
		width: 59%;
		height: 604px;
		border: 1px solid grey;
		float: left;
	}.inpt{
		background: #EBEBEB;
	    border: 1px solid #CACACA;
	}table{
		width: 100%;
	    border-collapse: collapse;
	}.first_first{
	    border-bottom:1px solid lightgrey;
	}#glass_btn:focus{
	    background:white !important;
	}#order_notes_btn:focus{
	    background:white;
	}
</style>

<script type="text/javascript">
	 function order_notes_btn() {
		document.getElementById('glass').style.display = 'none';
		document.getElementById('order_notes_details').style.display = 'block';
		document.getElementById('glass_btn').style.background = '#EBEBEB';
	  }
	  function glass_btn() {
		document.getElementById('glass').style.display = 'block';
		document.getElementById('order_notes_details').style.display = 'none';
		document.getElementById('glass_btn').style.background = 'white';
	  }
	  function hide_panel_measurement(){
	  	document.getElementById('panel_measurement').style.display = 'none';
	  	document.getElementById('hide_measurement').style.display = 'none';
	  	document.getElementById('show_measurement').style.display = 'block';
	  	document.getElementById('panel_measurement_parent_div').style.height = '30px';
	  	document.getElementById('panel_edges_parent_div').style.height = '372px';
	  }
	  function show_panel_measurement(){
	  	document.getElementById('panel_measurement').style.display = 'block';
	  	document.getElementById('show_measurement').style.display = 'none';
	  	document.getElementById('hide_measurement').style.display = 'block';
	  	document.getElementById('panel_measurement_parent_div').style.height = 'auto';
	  	document.getElementById('panel_edges_parent_div').style.height = '210px';
	  }
</script>

<body>
<div class="main">
	<div class="section1">
		<table>
			<tr class="first_first">
				<td style="width: 30%; text-align: right; padding-bottom: 5px;">Shower Type <input class="inpt" style="width: 35%;" type="text" name="shower_type" value="<?php echo $result["shower_type"];?>"></td>
				<td style="width: 25%; text-align: right; padding-bottom: 5px;">Panel Count <input class="inpt" style="width: 20%;" type="text" name="panel_count" value="<?php echo $result["panel_qty"];?>"></td>
				<td style="width: 110px; text-align: right; padding-bottom: 5px;">Supplier 
					<select style="width: 65%;" class="inpt">
						<option class="inpt"><?php echo $result["glass_supplier"];?></option>
					</select>
				</td>
			</tr>
		</table>
		<?php
		$prev_panel_no=$panel_no-1;
		$next_panel_no=$panel_no+1;
		$paneltitle='paneltype'.$panel_no.'';
		$query_panel_type="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitle'";
		$exe_panel_type=mysqli_query($con,$query_panel_type);
		$get_panel_type=mysqli_fetch_array($exe_panel_type);
		$panel_typess=$get_panel_type['value'];
		
		$paneltitlenext='paneltype'.$next_panel_no.'';
		$query_panel_typenext="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitlenext'";
		$exe_panel_typenext=mysqli_query($con,$query_panel_typenext);
		$get_panel_typenext=mysqli_fetch_array($exe_panel_typenext);
		$panel_typessnext=$get_panel_typenext['value'];
		
		
		$paneltitleprev='paneltype'.$prev_panel_no.'';
		$query_panel_typeprev="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitleprev'";
		$exe_panel_typeprev=mysqli_query($con,$query_panel_typeprev);
		$get_panel_typeprev=mysqli_fetch_array($exe_panel_typeprev);
		$panel_typessprev=$get_panel_typeprev['value'];
		
		
		
		?>
		
		
		<div style="width: fit-content; height: 156px;">
			<div style="float: left;">
			<?php
			if($prev_panel_no==0)
			{
			?>
			<button disabled style="height: 156px;padding: 0px;"  id="prev" onclick="prev_figure();"><i class="fa fa-arrow-left fa-3x"></i><br>Prev</button>	
			<?php
			}
			else
			{
			?>	
			<a href="panel_details_popup.php?job_no=<?php echo$job_no; ?>&panel_no=<?php echo$prev_panel_no; ?>"><button style="height: 156px;padding: 0px;"  id="prev" onclick="prev_figure();"><i class="fa fa-arrow-left fa-3x"></i><br>Prev</button></a>
				
			
			<?php
			}
			
			?>	
				
			</div>

			<div style="float: left; width: 87%;height: 156px;">
				<div style="float: left;">					
					<button style="padding: 9px; background: white; border: 0;" onclick="glass_btn()" id="glass_btn">Glass</button>					
				</div>
				<div >					
					<button style="padding: 8px;" class="inpt" onclick="order_notes_btn()" id="order_notes_btn">Order Notes</button>					
				</div>
				<div style="border: 1px solid grey;height: 79%;" id="glass">
					<label for="p" style="margin-left: 5%;padding-right: 1%;">Panel(P1)</label>
            		<input style="width: 25%;" class="inpt" type="text" id="p" value="<?php echo$panel_typess; ?>">

            		<label for="g" style="margin-left: 3%;padding-right: 1%;">Glass</label>
            		<input style="width: 8%;" type="text" id="g" value="10">

            		<select style="width: 22%;padding-right: 1%;"> 
            			<option><?php echo $result["glass_type"];?></option>
            		</select>

            		<button style="padding: 5px; margin-left: 5.5%;" class="inpt">...</button>	

					<div style="margin-top: 0.8%;">
						<label for="p" style="margin-left: 3%;padding-right: 1%;">Glass Style</label>
	            		<select style="width: 81.5%;"> 
            				<option>Toughened</option>
            			</select>	
					</div>
					<div style="margin-top: 1.2%;">
						<label for="p" style="margin-left: 6.5%;padding-right: 1%;">Supplier</label>
	            		<select style="width: 81.4%;"> 
            				<option><?php echo $result["glass_supplier"];?></option>
            			</select>	
					</div>
					<div style="margin-top: 1.2%;">
						<label for="p" style="margin-left: 8.7%;padding-right: 1%;">Stamp</label>
	            		<select style="width: 39.5%;"> 
            				<option>TFA</option>
            			</select>
            			<select style="width: 40.8%;"> 
            				<option>Bottom Left</option>
            			</select>	
					</div>
					<div style="margin-top: 1.2%; margin-bottom: 2%;">
						<label for="p" style="margin-left: 1%;padding-right: 1%;">Panel Source</label>
	            		<select style="width: 81%;"> 
            				<option>Custom Ordered</option>
            			</select>	
					</div>
				</div>
				<!-- Hidden -->
				<div style="border: 1px solid grey; display: none;width: 467px; height: 122px;" id="order_notes_details">

					<textarea style="margin: 0px;width: 458px;height: 117px;"><?php echo $result["private_note"];?></textarea>

				</div>
				<!-- End Hidden				 -->
			</div>
			
			<div style="float: left;">	

	<?php
			if($panel_no==$panel_qty)
			{
			?>
				<button disabled style="height: 156px;padding: 0px;" id="next" onclick="next_figure();"><i class="fa fa-arrow-right fa-3x"></i><br>Next</button>	
			<?php
			}
			else
			{
			?>	
			
				<a href="panel_details_popup.php?job_no=<?php echo$job_no; ?>&panel_no=<?php echo$next_panel_no; ?>"><button style="height: 156px;padding: 0px;" id="next" onclick="next_figure();"><i class="fa fa-arrow-right fa-3x"></i><br>Next</button></a>
			
			<?php
			}
			
			?>

			
				


				
			</div>
		</div>

		<div style="height:212px;margin-top: 10px;border: 1px solid lightgrey;" id="panel_measurement_parent_div">
				<div style="margin-top: 4px;">
					<hr width="2%" style="float: left;"><span style="float:left; padding: 0px 4px 0px 4px;">Panel Measurement</span>
					<hr width="45.5%" style="float: left;">
					<button style="padding: 0px 10px 2px 10px;margin: 0px 3px 0px 3px; float: left;" onclick="hide_panel_measurement();" id="hide_measurement">Hide <u>M</u>easurements</button>
					<button style="padding: 0px 10px 2px 10px;margin: 0px 3px 0px 3px; float: left; display: none;" onclick="show_panel_measurement();" id="show_measurement">Show <u>M</u>easurements</button>
					<hr width="2%" style="float: left;">					
				</div>

<?php
			$query_get_panwlwise_data="SELECT * FROM panelwise_dimension WHERE job_no='$job_no' AND panel_no='$panel_no'";
			$exe_get_panwlwise_data=mysqli_query($con,$query_get_panwlwise_data);
			$get_panwlwise_data=mysqli_fetch_array($exe_get_panwlwise_data);
			$values=$get_panwlwise_data['values'];
			
			$first_part=explode(">>",$values);
			//print_r(count($first_part));echo'<br />';
			$char='A';
			$noss=count($first_part);
			$nossdd=$noss-1;
			
			$chararray=array();
			for($charn=0;$charn<$nossdd;$charn++)
			{
			$arr=explode("-",$first_part[$charn]);
			//print_r($arr);echo'<br />';
			${"val$char"}=$arr[1];
			${"value$char"}=$arr[1];
			
			$chararray[]=$char;
			$char++;	
			}
			//print_r($chararray);

?>


					

				<div style="height: 186px;display: block;" id="panel_measurement">
					<div style="width:100%;height:10%;">
					
					
					<div style="width:100%;">
					<div style="width:30%;float:left;text-align:right;">
					<label><b>Edge </b></label>
					</div>
					<div style="width:70%;float:left;">
					
					
					
					<table >
					<tr>
					
					
					<?php
					$cound_char=count($chararray);
					$width_textt=(100/$cound_char);
					foreach($chararray as $valuess){
						echo'<td>';
						
						echo'<input type="text" disabled value="'.$valuess.'" class="text'.$valuess.'" style="width:100%;">';
					//echo'<td><input style="width: 8%;text-align: center;color: #4F43DD" class="text'.$valuess.'" type="text" id="p" value="'.$valuess.'" disabled=""></td>';	
					echo'</td>';	
					}
					?>
					
					</tr>
					</table>
					</div>
					
					</div>
					</div>
					
					
					<div style="width:100%;height:10%;">
					
					
					<div style="width:100%;">
					<div style="width:30%;float:left;text-align:right;">
					<label><b>Length </b></label>
					</div>
					
					<div style="width:70%;float:left;">
					
					
					
					<table >
					<tr>
					
					
					<?php
					$cound_char=count($chararray);
					$width_textt=(100/$cound_char);
					foreach($chararray as $valuess){
						echo'<td>';
						
						echo'<input type="text" disabled value="'.${"val$valuess"}.'" class="text'.$valuess.'" style="width:100%;">';
					
					echo'</td>';	
					}
					?>
					
					</tr>
					</table>
					</div>
					
					</div>
					</div>
					
					
					
					
					<div style="width:100%;height:10%;">
					
					
					<div style="width:100%;">
					<div style="width:30%;float:left;text-align:right;">
					<label><b><span id="change_plubname">Out of Plumb/Level </span></b></label>
					</div>
					
					<div style="width:70%;float:left;">
					
					
					
					<table >
					<tr>
					
					
					<?php
					$cound_char=count($chararray);
					$width_textt=(100/$cound_char);
					foreach($chararray as $valuess){
						echo'<td>';
						
						echo'<input type="text" disabled value="0" class="text'.$valuess.'" style="width:100%;">';
					
					echo'</td>';	
					}
					?>
					
					</tr>
					</table>
					</div>
					
					</div>
					</div>
					
					
					
					<div style="width:100%;height:10%;display:none;" id="elbow_length_div">
					
					
					<div style="width:100%;">
					<div style="width:30%;float:left;text-align:right;">
					<label><b>Elbow Length </b></label>
					</div>
					
					<div style="width:70%;float:left;">
					
					
					
					<table >
					<tr>
					
					
					<?php
					$cound_char=count($chararray);
					$width_textt=(100/$cound_char);
					foreach($chararray as $valuess){
						echo'<td>';
						
						echo'<input type="text" disabled value="0" class="text'.$valuess.'" style="width:100%;">';
					
					echo'</td>';	
					}
					?>
					
					</tr>
					</table>
					</div>
					
					</div>
					</div>
					
					
					<div style="width:100%;height:10%;display:none;" id="outage_elbows_div">
					
					
					<div style="width:100%;">
					<div style="width:30%;float:left;text-align:right;">
					<label><b>Outage to elbow </b></label>
					</div>
					
					<div style="width:70%;float:left;">
					
					
					
					<table >
					<tr>
					
					
					<?php
					$cound_char=count($chararray);
					$width_textt=(100/$cound_char);
					foreach($chararray as $valuess){
						echo'<td>';
						
						echo'<input type="text" disabled value="0" class="text'.$valuess.'" style="width:100%;">';
					
					echo'</td>';	
					}
					?>
					
					</tr>
					</table>
					</div>
					
					</div>
					</div>
					
					
					
					<div style="width:100%;height:20%;">
					
					
					<div style="width:100%;">
					<div style="width:30%;float:left;text-align:right;">
					<label><b>Define Outage
					<br><button style="margin-left: 8%;padding:0px 8px 0px 8px;" onclick="show_elbows_textbox(this.form);" id="show_elbows_textbox">Show Elbows</button>
					<button style="margin-left: 8%;padding:0px 8px 0px 8px;display:none;" onclick="hide_elbows_textbox(this.form);" id="hide_elbows_textbox">Hide Elbows</button>
					</b></label>
					</div>
					
					<div style="width:70%;float:left;">
					
					
					
					<table >
					<tr>
					
					
					<?php
					$cound_char=count($chararray);
					$width_textt=(100/$cound_char);
					$nunn=0;
					foreach($chararray as $valuess){
						echo'<td>';
						
						echo'<img src="img/outage/level/middle_select.jpg" style="width:100%;">';	
			
						echo'</select>
						';
					
					echo'</td>';	
					$nunn++;
					}
					?>
					
					</tr>
					</table>
					</div>
					
					</div>
					</div>
					
					
					<div style="width:100%;height:20%;padding-top: 27px;">
					<div style="width:49%;float:left;">
					<fieldset>
					<legend>Clearance to Previous Panel</legend>
					<div style="width:100%;">
					<div style="width:49%;float:left;">
					Left <input type="text" style="width:40%;" disabled>
					
					</div>
					<div style="width:49%;float:left;">
					Up <input type="text" style="width:40%;" disabled>
					</div>
					</div>
					
					</fieldset>
					</div>
					<div style="width:49%;float:left;">
					<fieldset>
					<legend>Clearance to Next Panel</legend>
					<div style="width:100%;">
					<div style="width:49%;float:left;">
					Right <input type="text" style="width:40%;" disabled>
					
					</div>
					<div style="width:49%;float:left;">
					Up <input type="text" style="width:40%;" disabled>
					</div>
					</div>
					
					</fieldset>
					</div>
					</div>


				</div>	
		</div>
		
	
	
	
<script src="js/select/select222.js"></script>
<script type="text/javascript">
    function custom_template(obj){
            var data = $(obj.element).data();
            if(data && data['img_src']){
                img_src = data['img_src'];
                template = $("<img src=\"" + img_src + "\" style=\"width:100%;height:70px;\"/>");
                return template;
            }
        }
    var options = {
        'templateSelection': custom_template,
        'templateResult': custom_template,
    }
	<?php
	$nunn=0;
	foreach($chararray as $valuess){
	echo"$('#id_select2_example".$nunn."').select2(options);";	
	$nunn++;
	}
		
		?>
    
  

    $('.select2-container--default .select2-selection--single').css({'height': '50px'});
    //$('.select2-container--default .select2-selection--single').css({'display': 'none;'});
 
</script>
	
		<script type="text/javascript">
    	
function show_elbows_textbox(form){
	$("#outage_elbows_div").css("display","");
	$("#elbow_length_div").css("display","");
	$('#hide_elbows_textbox').css("display","");
	$('#show_elbows_textbox').css("display","none");
	$("#change_plubname").text('Outage past elbows');
}

function hide_elbows_textbox(form){
	
	$("#outage_elbows_div").css("display","none");
	$("#elbow_length_div").css("display","none");
	$('#hide_elbows_textbox').css("display","none");
	$('#show_elbows_textbox').css("display","");
	$("#change_plubname").text('Out of Plumb');
	
}

		</script>

		<div style="margin-top: 10px;height: 210px;border: 1px solid lightgrey;overflow: auto;" id="panel_edges_parent_div">
			<div>
				<hr width="2%" style="float: left;"><span style="float:left; padding: 0px 4px 0px 4px;">Panel Edges</span>
				<hr width="82%" style="float: left;">					
			</div>
			<div>
			
			
				<?php
			if($panel_typess=='Side-Nib-Notch'||$panel_typess=='Side-Notch'||$panel_typess=='Return-Notch'||$panel_typess=='Return-Nib-Notch')
			{
			
			?>
			
			
			
			<div style="border: 1px solid #989090;margin-top: 20px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #4F43DD">Left Edge</label>
					<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>

				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #78C780">Bottom Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>

				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #FEA255">Right Notch Vertical Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #B43C9D">Right Notch Horizontal Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				
				
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #007D7D">Right Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #7D0000">Top Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
			
			<?php
			}
			elseif($panel_typess=='Side-Notch-Double'||$panel_typess=='Return-Notch-Double')
			{
			?>
			
			
			
			
			<div style="border: 1px solid #989090;margin-top: 20px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #4F43DD">Left Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>

				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #78C780">Bottom Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>

				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #FEA255">Right Notch Vertical2 Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #B43C9D">Right Notch Horizontal2 Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				
				
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #007D7D">Right Notch Vertical Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #7D0000">Right Notch Horizontal Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
			
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #000064">Right Edge</label>
						<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>
						
						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #C82828">Top Edge</label>
					<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
			
			
			<?php
			}
			else
			{
			?>
			
			<div style="border: 1px solid #989090;margin-top: 20px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #4F43DD">Left Edge</label>
					<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>

				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #78C780">Bottom Edge</label>
					<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>

				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #FEA255">Right Edge</label>
					<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
				
				<div style="border: 1px solid #989090;margin-top: 10px;padding: 5px;padding-left: 24px;">
					<div style="margin-bottom: 5px;">
						<label for="left_edge_inpt" style="color: #B43C9D">Top Edge</label>
					<select name="left_edge_inpt"  style="width: 79%;">
						<?php   
					$get_album="SELECT * FROM shower_style ORDER BY style_id ASC";
					$exe_album=mysqli_query($con,$get_album);
					$sr_no=1;
					while($get_album_detail=mysqli_fetch_array($exe_album))
					{
					$style_name=$get_album_detail['shower_style'];	
					if($style_name=='Bracket - MFGS Elite')	
					{
					echo'<option value="'.$get_album_detail['style_id'].'" selected>'.$get_album_detail['shower_style'].'</option>';	

					}
					else{
					echo'<option value="'.$get_album_detail['style_id'].'">'.$get_album_detail['shower_style'].'</option>';	
					}
					}
					?>
					</select>						<button style="" class="inpt">...</button>
					</div>
					<div style="margin-bottom: 5px;width: 47%;float: left; ">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Tight Gap</label>
						<input type="text" name="" value="6" style="width: 18%;margin-left: 10px;">
						<br>
						<label for="checkbox" style="margin-left: 20px;" >Cut Style</label>
						<select style="width: 100px;margin-top: 5px;    margin-left: 10px;">
							<option>None</option>
						</select>
					</div>
					<div style="">
						<input type="checkbox" name="checkbox" value="">
						<label for="checkbox" style="">Override Cut Method</label>
						<select style="width: 100px;margin-left: 10px;">
							<option>Square</option>
						</select>
						<br>
						<label>No Mitre</label>
						<input type="checkbox" name="">
						<label style="margin-left: 42px;">Finish</label>
						<select style="margin-top: 5px;width: 100px;">
							<option>Polished Chrome</option>
							<option>Satin Nickel</option>
							<option>Robbed Bronze</option>
						</select>
					</div>
				</div>
			
			
			<?php
			}
			?>
			
				
				
				
				
				
				
				
			</div>
		</div>
	</div>


			
<script>
// hide_elbows_textbox
// outage_elbows_div
// elbow_length_div
// show_elbows_textbox
// change_plubname


function show_elbows_textbox(form){
	$("#outage_elbows_div").css("display","");
	$("#elbow_length_div").css("display","");
	$('#hide_elbows_textbox').css("display","");
	$('#show_elbows_textbox').css("display","none");
	$("#change_plubname").text('Outage past elbows');
}

function hide_elbows_textbox(form){
	
	$("#outage_elbows_div").css("display","none");
	$("#elbow_length_div").css("display","none");
	$('#hide_elbows_textbox').css("display","none");
	$('#show_elbows_textbox').css("display","");
	$("#change_plubname").text('Out of Plumb');
	
}


</script>

	<div class="section2">
		<div style="padding: 5px; border-bottom: 1px solid grey;height: 560px;">
			<div style="margin-left: 25%; padding-top:5%;" id="">
			
			
			<!--  Notch Start
		 	<svg height="500" width="350">
			<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC">A</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28">B</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28">C</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082">D</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  
		  
			<line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
		-->
		
			<!-- Notch end-->
			<?php
			
			
			if($panel_typess=='Side-Nib-Notch'||$panel_typess=='Side-Notch'||$panel_typess=='Return-Notch'||$panel_typess=='Return-Nib-Notch')
			{
			
			
			
			?>
			
			<!--  Notch Start-->
		 	<svg height="500" width="350">
		  <line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="420" x2="80" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="90" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="100" y1="420" x2="150" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		   <line x1="160" y1="400" x2="160" y2="390" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="160" y="385" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		   <line x1="160" y1="375" x2="160" y2="350" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		   <line x1="150" y1="365" x2="190" y2="365" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="200" y="365" fill="#A00082"><?php echo$valueD; ?>"</text>
		   <line x1="220" y1="365" x2="250" y2="365" style="stroke:rgb(160,0,130);stroke-width:2" />
		   
		   
		  
		  
		  <line x1="270" y1="50" x2="270" y2="190" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <text x="270" y="200" fill="#007D7D"><?php echo$valueE; ?>"</text>
		  <line x1="270" y1="205" x2="270" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(125,0,0);stroke-width:2" />
		  <text x="135" y="10" fill="#7D0000"><?php echo$valueF; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="400" x2="150" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <line x1="150" y1="400" x2="150" y2="350" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="150" y1="350" x2="250" y2="350" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(125,0,0);stroke-width:2" />
		</svg>
			<!-- Notch end-->
			
			
			<?php
			}
			elseif($panel_typess=='Side-Notch-Double'||$panel_typess=='Return-Notch-Double')
			{
			?>
			
			
		<!--  Notch-Double Start-->
			<svg height="500" width="350">
		 <line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="420" x2="80" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="80" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="100" y1="420" x2="117" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		   <line x1="125" y1="400" x2="125" y2="390" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="125" y="385" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		   <line x1="125" y1="375" x2="125" y2="360" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		   <line x1="117" y1="370" x2="140" y2="370" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="140" y="372" fill="#A00082"><?php echo$valueD; ?>"</text>
		   <line x1="160" y1="370" x2="183" y2="370" style="stroke:rgb(160,0,130);stroke-width:2" />
		   
		   
		  
		  
		  <line x1="195" y1="360" x2="195" y2="350" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <text x="195" y="350" fill="#007D7D"><?php echo$valueE; ?>"</text>
		  <line x1="195" y1="340" x2="195" y2="320" style="stroke:rgb(0,125,125);stroke-width:2" />
		  
		  <line x1="183" y1="335" x2="210" y2="335" style="stroke:rgb(125,0,0);stroke-width:2" />
		  <text x="210" y="335" fill="#7D0000"><?php echo$valueF; ?>"</text>
		  <line x1="220" y1="335" x2="250" y2="335" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  
		  <line x1="270" y1="50" x2="270" y2="190" style="stroke:rgb(0,0,100);stroke-width:2" />
		  <text x="270" y="200" fill="#000064"><?php echo$valueG; ?>"</text>
		  <line x1="270" y1="205" x2="270" y2="320" style="stroke:rgb(0,0,100);stroke-width:2" />
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(200,40,40);stroke-width:2" />
		  <text x="135" y="10" fill="#C82828"><?php echo$valueH; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(200,40,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="400" x2="117" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="117" y1="400" x2="117" y2="360" style="stroke:rgb(254,141,0);stroke-width:2" />
		  <line x1="117" y1="360" x2="183" y2="360" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="183" y1="360" x2="183" y2="320" style="stroke:rgb(0,125,125);stroke-width:2" />
		  <line x1="183" y1="320" x2="250" y2="320" style="stroke:rgb(125,0,0);stroke-width:2" />
		  
		  
		  
		  <line x1="250" y1="50" x2="250" y2="320" style="stroke:rgb(0,0,100);stroke-width:2" />
		  
		  
		   <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(200,40,40);stroke-width:2" />
		  
		</svg>
			<!-- Notch-Double end-->	
			
			<?php
			}
			else
			{
			if($panel_typess=='Door-Hinge-Left')
			{
				
			?>
			
			
		<!-- hinge left start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="80" x2="80" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="80" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="110" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="110" x2="50" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="340" x2="80" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="340" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="370" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="370" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		 <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
		
		<!-- hinge left end-->
			
			
			<?php
			}
			elseif($panel_typess=='Door-Hinge-Right'){
			?>
			
			
		
			<!-- hinge right start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="80" x2="220" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="80" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="110" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="110" x2="250" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  <line x1="250" y1="340" x2="220" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="340" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="370" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  

		<line x1="250" y1="370" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>

		<!-- hinge right end-->
			
			
			<?php
			}
			else{	
			if($panel_typess=='Side')
			{
				
			?>
			
			<?php
			
			if($panel_typessnext=='Door-Hinge-Left')
			{
			?>
			
			
			<!-- hinge right start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="80" x2="220" y2="80" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="80" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="110" x2="220" y2="110" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  <line x1="250" y1="110" x2="250" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  <line x1="250" y1="340" x2="220" y2="340" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="220" y1="340" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <line x1="250" y1="370" x2="220" y2="370" style="stroke:rgb(254,141,40);stroke-width:2" />
		  

		<line x1="250" y1="370" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>

		<!-- hinge right end-->
			
			<?php
			}
			elseif($panel_typessprev=='Door-Hinge-Right')
			{
			?>
			
		<!-- hinge left start-->
			
		<svg height="500" width="350">
		
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="50" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="80" x2="80" y2="80" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="80" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="110" x2="80" y2="110" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="110" x2="50" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="340" x2="80" y2="340" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="80" y1="340" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="370" x2="80" y2="370" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  <line x1="50" y1="370" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		 <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
		
		<!-- hinge left end-->
			
			<?php
			}
			else
			{
			?>
			
			
			<!-- plane start-->
			<svg height="500" width="350">
			
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
			
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
			<?php
			}
			
			}
			
			else{	
			
			?>
			
			
			
				
			<!-- plane start-->
			<svg height="500" width="350">
			
			
		<line x1="0" y1="50" x2="0" y2="190" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <text x="0" y="200" fill="#2828DC"><?php echo$valueA; ?>"</text>
		  <line x1="0" y1="205" x2="0" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="420" x2="130" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  <text x="135" y="420" fill="#28FF28"><?php echo$valueB; ?>"</text>
		  <line x1="170" y1="420" x2="250" y2="420" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  
		  
		  <line x1="290" y1="50" x2="290" y2="190" style="stroke:rgb(254,141,40);stroke-width:2" />
		  <text x="290" y="200" fill="#FE8D28"><?php echo$valueC; ?>"</text>
		  <line x1="290" y1="205" x2="290" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		  
		  
		  
		  <line x1="50" y1="10" x2="130" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <text x="135" y="10" fill="#A00082"><?php echo$valueD; ?>"</text>
		  <line x1="170" y1="10" x2="250" y2="10" style="stroke:rgb(160,0,130);stroke-width:2" />
		  
		  <line x1="50" y1="50" x2="250" y2="50" style="stroke:rgb(160,0,130);stroke-width:2" />
		  <line x1="50" y1="50" x2="50" y2="400" style="stroke:rgb(40,40,220);stroke-width:2" />
		  <line x1="50" y1="400" x2="250" y2="400" style="stroke:rgb(40,255,40);stroke-width:2" />
		  
		  <line x1="250" y1="50" x2="250" y2="400" style="stroke:rgb(254,141,40);stroke-width:2" />
		</svg>
			<!-- plane end-->
			
			<?php
			}
			
			
			}
			}
			
			?>
			
		
		
		
			
		

		
		
		
			
			
			
			
			
		
		
		
			</div>
		</div>
		<div style="margin-top: 5px; ">
			<input type="checkbox" name="panel_is_complex">
			<label for="panel_is_complex">Panel is complex    </label>
			<button><a href="fitting_popups.php?job_no=<?php echo$job_no ?>">Fittings and Brackets</a></button>
		</div>
	</div>
</div>
<script type="text/javascript">
	var i =1;
	$(document).ready(function(){
    	$("#show_figure").load('figure/Glass1.php');	
	});
	function next_figure(){
		if (i<=3){
	    i++;
	    $("#show_figure").load('figure/Glass'+i+'.php');
	    } 
	}

	function prev_figure(){
		if (i<=3){
		i--;
	    $("#show_figure").load('figure/Glass'+i+'.php');
	    }     
	}
</script>


<style>
.textA{color: blue; fill: blue;}
.textB{color: green; fill: green;}
.textB1{color: green; fill: green;}
.textB2{color: green; fill: green;}
.textB3{color: green; fill: green;}
.textB4{color: green; fill: green;}
.textB5{color: green; fill: green;}
.textB6{color: green; fill: green;}
.textC{color: coral; fill: coral;}
.textC1{color: coral; fill: coral;}
.textC2{color: coral; fill: coral;}
.textC3{color: coral; fill: coral;}
.textC4{color: coral; fill: coral;}
.textC5{color: coral; fill: coral;}
.textC6{color: coral; fill: coral;}
.textD{color: magenta; fill: magenta;}
.textD1{color: magenta; fill: magenta;}
.textD2{color: magenta; fill: magenta;}
.textD3{color: magenta; fill: magenta;}
.textD4{color: magenta; fill: magenta;}
.textD5{color: magenta; fill: magenta;}
.textE{color: #64c2f4; fill: #64c2f4;}
.textE1{color: #64c2f4; fill: #64c2f4;}
.textE2{color: #64c2f4; fill: #64c2f4;}
.textE3{color: #64c2f4; fill: #64c2f4;}
.textE4{color: #64c2f4; fill: #64c2f4;}
.textE5{color: #64c2f4; fill: #64c2f4;}
.textF{pcolor: #721b1b; fill: #721b1b;}
.textF1{pcolor: #721b1b; fill: #721b1b;}
.textF2{pcolor: #721b1b; fill: #721b1b;}
.textF3{pcolor: #721b1b; fill: #721b1b;}
.textF4{pcolor: #721b1b; fill: #721b1b;}
.textF6{pcolor: #721b1b; fill: #721b1b;}
.textG{color: #081bad; fill: #081bad;}
.textG1{color: #081bad; fill: #081bad;}
.textG2{color: #081bad; fill: #081bad;}
.textG3{color: #081bad; fill: #081bad;}
.textG4{color: #081bad; fill: #081bad;}
.textG5{color: #081bad; fill: #081bad;}
.textH{color: #081bad; fill: #081bad;}
.textH1{color: #081bad; fill: #081bad;}
.textH2{color: #081bad; fill: #081bad;}
.textH3{color: #081bad; fill: #081bad;}
.textH4{color: #081bad; fill: #081bad;}
.textH5{color: #081bad; fill: #081bad;}
.textI{color: #081bad; fill: #081bad;}
.textJ{color: #081bad; fill: #081bad;}
.textK{color: #081bad; fill: #081bad;}
.textL{color: #081bad; fill: #081bad;}
.textM{color: #081bad; fill: #081bad;}
.textN{color: #081bad; fill: #081bad;}
.textO{color: #081bad; fill: #081bad;}
.textP{color: #081bad; fill: #081bad;}
.textQ{color: #081bad; fill: #081bad;}
.textR{color: #081bad; fill: #081bad;}
.textS{color: #081bad; fill: #081bad;}
.textT{color: #081bad; fill: #081bad;}
.textU{color: #081bad; fill: #081bad;}
.textV{color: #081bad; fill: #081bad;}
.textW{color: #081bad; fill: #081bad;}
.textX{color: #081bad; fill: #081bad;}
.textY{color: #081bad; fill: #081bad;}
.textZ{color: #081bad; fill: #081bad;}


.textAA{color: #081bad; fill: #081bad;}
.textAB{color: #081bad; fill: #081bad;}
.textAC{color: #081bad; fill: #081bad;}
.textAD{color: #081bad; fill: #081bad;}
.textAE{color: #081bad; fill: #081bad;}
.textAF{color: #081bad; fill: #081bad;}
.textAG{color: #081bad; fill: #081bad;}
.textAH{color: #081bad; fill: #081bad;}
.textAI{color: #081bad; fill: #081bad;}
.textAJ{color: #081bad; fill: #081bad;}
.textAK{color: #081bad; fill: #081bad;}
.textAL{color: #081bad; fill: #081bad;}
</style>
</body>
</html>
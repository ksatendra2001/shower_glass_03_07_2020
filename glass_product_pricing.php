<?
require_once('includes/config.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Simple Notes</title>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
body { 
   margin-left: 0px;
 font-family: Verdana;
   margin-top: 0px;
   font-size: 11px;
}
.main_container{
/*    width: 948px;
    height: 592px;*/
}.top button{
    margin-left: -4px;
    border-top-width: 1px;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-left-width: 1px;
    padding-left: 8px;
    padding-right: 8px;
    padding-top: 6px;
    padding-bottom: 6px
}.middle_1{
    height: 540px;
    border: 1px solid grey;
}.middle_1 th{
        /*float: left;*/
    margin-top: 0;
    margin-bottom: 0;
    background: lightgrey;
    padding-top: 2px;
    padding-bottom: 2px;
    /*padding-right: 128.2px;*/
    /*width: 33%;*/
    border: 1px solid grey;
}.middle_2{
    height: 540px;
    border: 1px solid grey;
}.p th{
        /*float: left;*/
    margin-top: 0;
    margin-bottom: 0;
    background: lightgrey;
    padding-top: 2px;
    padding-bottom: 2px;
    /*padding-right: 128.2px;*/
    /*width: 33%;*/
    border: 1px solid grey;
}
</style>
<body>
     <?php

    $fetch_data_glass_product_type = "SELECT * FROM `glass_product_type`";
    $res_glass_product_type=mysqli_query($con,$fetch_data_glass_product_type);

    $fetch_data_supplier = "SELECT supplier_name FROM `supplier`";
    $res_supplier=mysqli_query($con,$fetch_data_supplier);
    
    ?>   
<div class="main_container">
    <div class="top">
        <button style="margin-left:0px; background: white;" onclick="SP();" id="SP">Selling Prices</button>
        <button onclick="SC();" id="SC">Supplier Costs</button>
    </div>

    <div class="middle_1" style="display: block;">
        <div style="float: left;margin-right: 100px;margin-left: 25px;margin-top: 10px;">
            <label for="FGP">Filter by Glass Product</label>
            <br>
            <select name="FGP" style="width: 100%;">
                <?php
                    while ($result11=mysqli_fetch_array($res_glass_product_type)) {
                        echo "<option>".$result11['glass_product_name']."</option>";
                    }
                ?>
            </select>
        </div>
        <div style="margin-top: 10px;margin-bottom: 10px;">
            <label for="FPM">Filter by Pricing Method </label>
            <br>
            <select name="FPM" style="width: 14%;">
                <option>ALL</option>
                <option>Cut to size</option>
                <option>Glazing Charge</option>
                <option>IGU Annealed</option>
                <option>IGU Toughened</option>
                <option>Stock Sheet</option>
                <option>Toughened</option>
            </select>
        </div>
        <div style="border: 1px solid grey;height: 476px;">
            <table style="width:100%;">
                <thead>
                    <tr>
                        <th>Glass Product Name</th>
                        <th>Pricing Method</th>
                        <th>Price Per Square M</th>
                    </tr>
                </thead>
                <tbody class="tbody_1">
            <?php
                    $tabindex = 'selling_prices';

                $fetch_data = "SELECT * FROM `glass_product_pricing` WHERE category='".$tabindex."'";
                $res=mysqli_query($con,$fetch_data);

                while ($result=mysqli_fetch_array($res)) {

                    echo '<tr '.$result['id'].'>
                        <td><select name="GPN" style="width:100%;" id="11">
                                <option>'.$result['glass_product_name'].'</option>
                            </select>
                        </td>
                        <td><select name="PM" style="width:100%;" id="11">
                                <option>'.$result['price_method'].'</option>
                            </select>
                        </td>
                        <td><input type="text" name="PPSM" style="width:100%;" id="11" value="'.$result['price_per_square_m'].'"></td>
                    </tr>'; 
                }
                ?>
                </tbody>
            </table>
            <!-- <h4 style="margin-top: 225px;text-align: center;color: lightgrey;">&lt;No data to display&gt;</h4> -->
        </div>
    </div>

    <div class="middle_2" style="display: none;">
        <div style="float: left;margin-left: 25px;margin-top: 10px;">
            <label for="FGP">Filter by Glass Product</label>
            <br>
            <select name="FGP" style="width: 100%;">
                <?php
                    $fetch_data_glass_product_type = "SELECT * FROM `glass_product_type`";
                    $res_glass_product_type=mysqli_query($con,$fetch_data_glass_product_type);
                    while ($result111=mysqli_fetch_array($res_glass_product_type)) {
                        echo "<option>".$result111['glass_product_name']."</option>";
                    }
                ?>
            </select>
        </div>
        <div style="float: left;margin-left: 25px;margin-top: 10px;">
            <label for="FS">Filter by Supplier</label>
            <br>
            <select name="FS" style="width: 100%;">
                <?php
                    while ($result22=mysqli_fetch_array($res_supplier)) {
                        echo "<option>".$result22['supplier_name']."</option>";
                    }
                ?>
            </select>
        </div>
        <div style="float: left;margin-left: 25px;margin-top: 10px; margin-right: 10px;">
            <label for="FPM">Filter by Pricing Method </label>
            <br>
            <select name="FPM" style="width: 99%;">
                <option>ALL</option>
                <option>Cut to size</option>
                <option>Glazing Charge</option>
                <option>IGU Annealed</option>
                <option>IGU Toughened</option>
                <option>Stock Sheet</option>
                <option>Toughened</option>
            </select>
        </div>
        <div style="margin-top: 10px;margin-bottom: 15px;">
            <p>You must slecet a supplier before<br> proceeding.</p>
        </div>
        <div style="border: 1px solid grey;height: 476px;" class="p">
            <table style="width:100%;">
                <thead>
                    <tr>
                        <th>Glass Product Name</th>
                        <th>Supplier</th>
                        <th>Pricing Method</th>
                        <th>Cost Per Square M</th>
                    </tr>
                </thead>
                <tbody class="tbody_2">
                                <?php
                    $tabindex = 'supplier_costs';

                $fetch_data = "SELECT * FROM `glass_product_pricing` WHERE category='".$tabindex."'";
                $res=mysqli_query($con,$fetch_data);
                while ($result=mysqli_fetch_array($res)) {

                    echo '<tr '.$result['id'].'>
                    <td><select name="GPN" style="width:100%;" id="11">
                                <option>'.$result['glass_product_name'].'</option>
                            </select>
                    </td>
                    <td><select name="S" style="width:100%;" id="11">
                                <option>'.$result['supplier'].'</option>
                            </select>
                        </td>
                    <td><select name="PM" style="width:100%;" id="11">
                                <option>'.$result['price_method'].'</option>
                            </select>
                    </td>    
                    <td><input type="text" name="CPSM" style="width:100%;" value="'.$result['cost_per_square_m'].'"></td>
                </tr>'; 
                }
                ?>
                </tbody>
            </table>
            <!-- <h4 style="margin-top: 225px;text-align: center;color: lightgrey;">&lt;No data to display&gt;</h4> -->
        </div>
    </div>

    <div class="bottom_1" style="margin-top: 3px;float: right;">
            <!-- <button class="delete_1"><u>D</u>elete</button> -->
            <button onclick="new_glass_price_1();" class="new_1"><u>N</u>ew Glass Price</button>
            <button onclick="save_1();" class="save_1" style="display: none;"><u>S</u>ave</button>
            <button onclick="window.close()"><u>C</u>lose</button>
    </div>

    <div class="bottom_2" style="margin-top: 3px;float: right;display: none;">
            <!-- <button class="delete_2"><u>D</u>elete</button> -->
            <button onclick="new_glass_price_2();" class="new_2"><u>N</u>ew Glass Price</button>
            <button onclick="save_2();" class="save_2" style="display: none;"><u>S</u>ave</button>
            <button onclick="window.close()"><u>C</u>lose</button>
    </div>
</div>
<script> 
function SP() {
    $('.middle_1').show();
    $('.middle_2').hide();
    $('.bottom_1').show();
    $('.bottom_2').hide();
    document.getElementById('SP').style.background = 'white';
    document.getElementById('SC').style.background = '#d3d3d3';
}

function SC() {
    $('.middle_2').show();
    $('.middle_1').hide();
    $('.bottom_2').show();
    $('.bottom_1').hide();
    document.getElementById('SC').style.background = 'white';
    document.getElementById('SP').style.background = '#d3d3d3';
}

function new_glass_price_1() {
    <?php
    $fetch_data_glass_product_type = "SELECT * FROM `glass_product_type`";
    $res=mysqli_query($con,$fetch_data_glass_product_type);
    ?>
    var item = '<tr>'+
                    '<td><select name="GPN" class="GPN" style="width:100%;">'+
                                '<?php
                                     while ($result=mysqli_fetch_array($res)) {echo '<option>'.$result["glass_product_name"].'</option>';}
                                ?>'+
                            '</select>'+
                        '</td>'+
                        '<td><select name="PM" class="PM" style="width:100%;">'+
                                '<option>ALL</option>'+
                                '<option>Cut to size</option>'+
                                '<option>Glazing Charge</option>'+
                                '<option>IGU Annealed</option>'+
                                '<option>IGU Toughened</option>'+
                                '<option>Stock Sheet</option>'+
                                '<option>Toughened</option>'+
                            '</select>'+
                        '</td>'+
                    '<td><input type="text" name="PPSM" class="PPSM" style="width:100%;"></td>'+
                '</tr>';
    $('.tbody_1').append(item);
    $('.new_1').hide();
    $('.save_1').show();
}

function new_glass_price_2() {
    <?php
    $fetch_data_glass_product_type = "SELECT * FROM `glass_product_type`";
    $res=mysqli_query($con,$fetch_data_glass_product_type);

    $fetch_data_supplier = "SELECT supplier_name FROM `supplier`";
    $res_supplier=mysqli_query($con,$fetch_data_supplier);
    ?>
    var item = '<tr>'+
                    '<td><select name="GPN" class="GPN_2" style="width:100%;">'+
                                '<?php
                                     while ($result=mysqli_fetch_array($res)) {echo '<option>'.$result["glass_product_name"].'</option>';}
                                ?>'+
                            '</select>'+
                        '</td>'+
                    '<td><select name="S" class="S" style="width:100%;">'+
                                '<?php
                                     while ($resultt=mysqli_fetch_array($res_supplier)) {echo '<option>'.$resultt["supplier_name"].'</option>';}
                                ?>'+
                            '</select>'+
                        '</td>'+
                    '<td><select name="PM_2" class="PM_2" style="width:100%;">'+
                                '<option>ALL</option>'+
                                '<option>Cut to size</option>'+
                                '<option>Glazing Charge</option>'+
                                '<option>IGU Annealed</option>'+
                                '<option>IGU Toughened</option>'+
                                '<option>Stock Sheet</option>'+
                                '<option>Toughened</option>'+
                            '</select>'+
                        '</td>'+
                    '<td><input type="text" name="CPSM" class="CPSM" style="width:100%;" ></td>'+
                '</tr>';
    $('.tbody_2').append(item);
    $('.new_2').hide();
    $('.save_2').show();
}

function save_1() {
    $('.new_1').show();
    $('.save_1').hide();
    var category1 = 'selling_prices';
    var glass_product_name = $('.GPN').val();
    var pricing_method = $('.PM').val();
    var price_per_square_m = $('.PPSM').val();
    $.ajax({
        type: 'post',
        url:"date_save/save_glass_product_pricing.php",
        data:{category:category1,glass_product_name:glass_product_name,pricing_method:pricing_method,price_per_square_m:price_per_square_m},
        success: function(data){

            }
    });
}

function save_2() {
    $('.new_2').show();
    $('.save_2').hide();

    var category2 = 'supplier_costs';
    var glass_product_name = $('.GPN_2').val();
    var supplier = $('.S').val();
    var pricing_method = $('.PM_2').val();
    var cost_per_square_m = $('.CPSM').val();
    $.ajax({
        type: 'post',
        url:"date_save/save_glass_product_pricing.php",
        data:{category:category2,glass_product_name:glass_product_name,supplier:supplier,pricing_method:pricing_method,cost_per_square_m:cost_per_square_m},
        success: function(data){

            }
    });
}

</script>
</body>
</html>
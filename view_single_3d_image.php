<?php
include('includes/header.php');
		$job_id = $_GET['job_id'];
		$glass_type = $_GET['glass_type'];
        $panel_qty = $_GET['panel_qty'];
        $job_no = $_GET['job_no'];
        $shower_type = $_GET['shower_type'];
        $panel_types = explode(",", $_GET['panel_types']);
        $angles = explode(",", $_GET['angles']);
        $shower_finish = $_GET['shower_finish'];
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>ADM Shower Glass</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<style>
.view_360{
    float: right;
    padding: 8px;
    border: none;
    border-radius: 4px;
    background: #b5d9ff;
    font-weight: bold;
    cursor: pointer;
   }

.ajax-loader {
  visibility: hidden;
  background-color: rgba(255,255,255,0.7);
  position: fixed;
  z-index: +100 !important;
  width: 100%;
  height:100%;
}

.ajax-loader img {
  position: relative;
  top:50%;
  left:50%;
}   
</style>
</head>
<body>
<span id="check_error"></span>
<div class="ajax-loader">
  <img src="img/loading-thickbox.gif" class="img-responsive" />
</div>

<div id="contentContainer">
    <center>
        <img src="date_save/images/panel_drawing_3d_view/<?php echo$job_id ?>/1.png">
    </center>
</div>

<input class="view_360" type="button" name="360_view" value="360" id="view">

<script>
    $('#view').click(function(){
//create blank array variable........
        var angles = [];
        var panel_types = [];
//convert php array variable data javascript array variable using json_encode...............
        var angles_json = <?php echo json_encode($angles) ?>;
        var panel_types_json = <?php echo json_encode($panel_types) ?>;
//put json_variable value in javascript array vairable
        $.each(angles_json,function(index,value) {
             angles.push(value);
        });

        $.each(panel_types_json,function(index,value) {
             panel_types.push(value);
        });
     $.ajax({
        type:'post',
        url:"date_save/draw_3d_image_drawing_3D_View.php",
        data:{
            panel_qty:'<?php echo $panel_qty ?>',
            job_no:'<?php echo $job_no ?>',
            shower_type:'<?php echo $shower_type ?>',
            panel_types:panel_types,
            angles:angles,
            glass_type:'<?php echo $glass_type ?>',
            shower_finish:'<?php echo $shower_finish ?>'
        },
        beforeSend: function(){
            $('.ajax-loader').css("visibility", "visible");
        },
        success: function(data){
            $('.ajax-loader').css("visibility", "hidden");            
			$("#check_error").html(data); 
        }
    }).done(function(){
        console.log("done");
         $.ajax({
             type:'post',
             url:"date_save/create_animatio_door.php",
             data:{job_no:<?php echo $job_no ?>},
             beforeSend: function(){
             $('.ajax-loader').css("visibility", "visible");
             },
             success: function(data){
                window.location.href="view_3d_image.php?job_id="+<?php echo$job_id ?>+"&glass_type="+'<?php echo$glass_type ?>'+"";  
             $('.ajax-loader').css("visibility", "hidden");
            }
         });
        });
})
</script>

</body>
</html>
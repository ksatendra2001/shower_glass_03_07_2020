<?php

include('includes/header.php');
//session_start();
$user_id=$_SESSION["user_id"];
$prev=$_SESSION['priv'];


if(isset($_GET['new_offset_ruleset']))
{
$new_offset_ruleset=$_GET['new_offset_ruleset'];
}
else{
	$new_offset_ruleset="No";
}


?>	


<div style="width:100%;">
<div style="width:100%; height:5%;border:1px solid #e4dede;padding:5px;">
Structure Type <select style="height: 22px;">
<option>All</option>
<option>Balustrade</option>
<option>Job</option>
<option>Shopfront</option>
<option selected>Shower</option>
<option>Splashback</option>
</select>

</div>

<div style="width:100%;height:88%;border:1px solid #e4dede;overflow:auto">
<div style="width:100%;">
<div style="width:30%; float:left;border:1px solid #e4dede;height:98%;padding:5px;">

<div class="fittingcategory" style="width: 100%;height:100%;">
<div class="fitting-haed" style="padding:5px;"><b>Fitting Category</b></div>
<div>

<select size="32" style="width:100%;" onchange="show_cat_type(this.form);show_hardware_detail(this.form);" name="category_name" id="category_name">
<?php
$query_get_cat="SELECT * FROM  `fitting_category`";
$exe_get_cat=mysqli_query($con,$query_get_cat);
while($result_cat=mysqli_fetch_array($exe_get_cat))

{
	
	if($result_cat['cate_id']==1)
	{
	echo'<option value="'.$result_cat['cate_id'].'" selected>'.$result_cat['fitting_category'].'</option>';
	}
	else{
		echo'<option value="'.$result_cat['cate_id'].'">'.$result_cat['fitting_category'].'</option>';

	}
	
}
?>
</select>

</div>
</div>

</div>
<div style="width:68%; float:left;">
<div style="padding-left:32px;">
<div style="width: 100%;">
 <div style="float: left; width: 40%;border:1px solid #e4dede;">
 <div class="hardwaretype" style="width: 100%;">
<div class="hardwaretype-head" style="width: 96.5%;padding:5px;"><b><span id="cattype11">Frameless Styles</span> List</b></div>

<div>
<select size="33" style="width:100%;" name="category_type" id="category_type"  class="field-select" required>

<?php 
$get_cat_type = "SELECT * FROM fitting_category_type WHERE fitting_category_id = '1' ORDER BY cat_type_id DESC";
   $exe_cat_type = mysqli_query($con,$get_cat_type);
   $k=0;
   while($fetch_cat_type = mysqli_fetch_array($exe_cat_type))
   {
	   $k=$k=1;
	   if($k==1)
	   {
	   echo'<option value="'.$fetch_cat_type['cat_type_id'].'" selected>'.$fetch_cat_type['fitting_category_type'].'</option>';
	   }
	   else{
	   echo'<option value="'.$fetch_cat_type['cat_type_id'].'">'.$fetch_cat_type['fitting_category_type'].'</option>';
	   }
	   
   }


?>

</select>

</div>
</div></div>
 <div style="float: left; width: 15%;padding-left:20px;">
 
 <div style="margin: auto;padding-top: 150%;">
 <button style="width:50%;height:40px;" onclick="add_preferred_fitting(this.form);"><b style="font-size:25px;">></b></button><br /><br />
 <button style="width:50%;height:40px;" onclick="remove_preferred_fitting(this.form);"><b style="font-size:25px;"><</b></button>
 
 </div>
 
 </div>
 <div style="float: left; width: 40%;border:1px solid #e4dede;height:98%;overflow:auto">
 <div style="">
 <div class="hardwaretype-head" style="width: 96.5%;padding:5px;color:#0e6c8a;"><b>Preferred Fittings</b></div>
 <div>
 <?php 
	
 ?>
 
<select size="33" style="width:100%;" name="preferred_fittings" id="preferred_fittings"  required>

<?php 
$get_cat_type = "SELECT * FROM hardware_preferred_fittings WHERE customer_id='$user_id' ORDER BY preferred_id DESC";
   $exe_cat_type = mysqli_query($con,$get_cat_type);
   $k=0;
   while($fetch_cat_type = mysqli_fetch_array($exe_cat_type))
   {
	   $k=$k=1;
	   if($k==1)
	   {
	   echo'<option value="'.$fetch_cat_type['preferred_id'].'" selected>'.$fetch_cat_type['cate_type_name'].'</option>';
	   }
	   else{
	   echo'<option value="'.$fetch_cat_type['preferred_id'].'">'.$fetch_cat_type['cate_type_name'].'</option>';
	   }
	   
   }


?>

</select>
 </div>
 
 </div>
 
 </div>
 <br style="clear: left;" />
</div>

</div>


</div>
</div>


</div>


<div style="width:100%; height:5%;border:1px solid #e4dede;text-align:right;">

<button style="height: 25px;width: 64px;" onclick="closeMe();">Close</button>
</div>

</div>
<script>

function remove_preferred_fitting(form){
	var added_id=$("input[name='hardware_id_re']:checked").val();
	
	var preferred_fittings=$('#preferred_fittings').val();
		if(preferred_fittings=='0'){
			alert('please select hardware for remove');
		}
		
		else{
			$.ajax({
      type:'post',
      url:"date_save/remove_preferred_fitting.php",
      data:{preferred_fittings:preferred_fittings},
      success: function(data){
		  window.location.reload(true);
       //$("#added_hardware_detail").html(data);
                
	}
      });
		}

}

function add_preferred_fitting(form){
		
		var cate_id=$('#category_name').val();
		var cate_name=$('#category_name :selected').text();
		var customer_id='<?php echo$user_id ?>';
		var cate_type_id=$('#category_type').val();
		var cate_type_name=$('#category_type :selected').text();
		

		
		
		
		if(cate_type_id>0){
		$.ajax({
      type:'post',
      url:"date_save/add_preferred_fitting.php",
      data:{cate_id:cate_id,cate_name:cate_name,cate_type_id:cate_type_id,cate_type_name:cate_type_name,customer_id:customer_id},
      success: function(data){
		  window.location.reload(true);
		  //alert(data);
       //$("#added_hardware_detail").html(data);
                
	}
      });
		}
		
		else{
			alert('please select hardware color');
		}
		
		
	
}



function show_hardware_detail(form){
    
        
		var cat_color_id=$('#category_type').val();
		var cat_color=$('#category_type :selected').text();
	
		 $.ajax({
      type:'post',
      url:"date_save/get_cate_color.php",
      data:{cat_color_id:cat_color_id},
      success: function(data){
		  //window.location.reload(true);
       $("#hardware_detail").html(data);
                
	}
      });
		
		
		$("#cattype").text(cat_color);
    
}





function show_cat_type(form){
    
        
		var cattype_val=$('#category_name').val();
		var cattype=$('#category_name :selected').text();
	
		 $.ajax({
      type:'post',
      url:"date_save/get_cate_type.php",
      data:{cat_id:cattype_val},
      success: function(data){
		  //window.location.reload(true);
       $("#category_type").html(data);
                
	}
      });
		
		
		$("#cattype11").text(cattype);
    
}

function increase_maximum(n) {
	var y = x = 0;
    var test = document.getElementById("test");
	 x = parseInt($('#test').val());
	 y = parseInt($('#test22').val());
	
	 x = x + n;
	 y = y + n;
	 
	
	var round =Math.round(x);
	var round2 =Math.round(y);
	
	//alert(round2);
	var x_name=''+x+'mm';
	var y_name=''+y+'mm';
	

	
	$('#test').val(x); 
	$('#test22').val(y); 
	
	
	//document.getElementById("stage").style.WebkitTransform = "rotateX(60deg)";
	//document.getElementById("stage").style.WebkitTransform = "rotateY("+y+"deg)";
};
window.onclick;
function closeMe()
{
    window.opener = self;
    window.close();
}





</script>


<html>
<head>
<title>ADM Glass Shower</title>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.6.min.js"></script>
<!--<script type="text/javascript" src="date_save/save_date.js"></script>-->
</head>
<STYLE>A {text-decoration: none; color:black;} </STYLE>
<body class="body">


<style>
body{font-size:15px;}
select{font-size:12px;}
</style>
<div class="main-measurement">
<div class="measurement-head">
<div class="measurement-head1">Shower Configuration<br />

<div >
  <div style="float:left;">
  
  <div style="padding:2px;">
  <select class="copyfromselect">
    <option value="">Copy From</option>
    <option value="Existing">Existing....</option>
    <option value="Templates">Templates....</option>
    <option value="Library">Library....</option>

</select>
&nbsp;&nbsp;
<input type="checkbox">Use Extended Panel Type</input>
</div>

<div style="padding:2px;">
<div style="float:left;">
Shower Type
<select>
<option value="InLine" selected>In Line</option><option value="Sqaure">Sqaure</option><option value="Quadrant">Quadrant</option>    
</select>
</div>
<div>
<form method="get" id="panel_form" onsubmit="return validateForm()">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Panel Count <input type="text" pattern="\d*" name="panel_qty" id="panel_qty" value="20" onchange="change_pane_qty(this.form)" style="width:25px;">


<input type="hidden" name="shower_type" value="InLine">
<input type="hidden" name="door_type" value="Double">
<input type="hidden" name="door_location" value="2">
<input type="hidden" name="hinges_type" value="Right">
<input type="hidden" name="order_no" value="1">
<input type="hidden" name="customer_id" value="2">
<input type="hidden" name="job_no" value="29">
<input type="hidden" name="diff_angle_no" value="0">
<input type="hidden" name="position1" value="0">
<input type="hidden" name="position2" value="0">
<input type="hidden" name="panel_typess1" value="Side">
<input type="hidden" name="panel_typess2" value="Door-Hing-Left">
<input type="hidden" name="panel_typess3" value="Door-Hing-Right">
<input type="hidden" name="panel_typess4" value="Side">
<input type="hidden" name="panel_typess5" value="">
<input type="hidden" name="panel_typess6" value="">
<input type="hidden" name="img" value="f319eace7f33056e2917ca28be1cba47.jpg">
</form>
</div>
</div>

</div>
<script>

function validateForm() {
  var panel_qty=$("#panel_qty").val();
 // alert(panel_qty);

    var regex = /^[0-9\_]+$/
    //alert(regex.test(panel_qty));
	if(regex.test(panel_qty)==false)
	{
		 alert("Panel Quantity should be 1 to 40");
		  return false;
	}
	else{
	if(panel_qty>40) {
    alert("A Shower cannot contain more than 40 panels");
    return false;
  }
	}
}
function change_pane_qty(form){
	var panel_qty=$("#panel_qty").val();
	if(panel_qty>40)
	{
		//alert('A Shower cannot contain more than 40 panels');
		//return false;
	}
	else{
		//window.location.reload(true);
	$('#panel_form').submit();
	
	}
	//window.location.reload(true);
}
</script>

  <div style="float:right;width: 19%;">
<select style="width: 100%;">
    <option value="">Image</option>
    <option value="Existing">Image1....</option>
    <option value="Templates">Image2....</option>
    <option value="Library">Image3....</option>

</select>
&nbsp;
</div>
</div>




</div>
<div class="measurement-head2">&nbsp;&nbsp;Shower Style
<div style="margin:5px;">Style <select name="shower_style" id="shower_style">
<option value="1">Custom Frameless</option><option value="2">Bracket - MFGS Elite</option><option value="3">Channel - MFGS Elite</option><option value="4">Dias Contour 2050</option><option value="5">Dias Contour Square Cut 2051</option><option value="6">Dias Eden Square Cut 3041</option><option value="7">Dias Mirage 3050</option><option value="8">Dias Mirage 3050 INT & EXT Corner Stakes</option><option value="9">Dias Mirage Square Cut 2051</option><option value="10">Dias Nautilus 4010</option><option value="11">Dias Profile 2040</option><option value="12">Dias Profile Square Cut 2041</option><option value="13">Dias Series 7400 - Corner - No Seal</option><option value="14">Dias Series 7400 - Corner - Seal</option><option value="16">Dias Series 7500 - Inline</option><option value="17">Dias Vogue 5050</option><option value="18">Dias Vogue Square Cut 5051</option><option value="19">Fethers ES & CH18</option><option value="20">Fethers ES & SB10 SQ</option><option value="21">Fethers ES & SB50 SQ</option><option value="22">Pivotech Dimension</option><option value="23">Pivotech Dimension - Matrix Adapter</option><option value="24">Pivotech Eclipse</option><option value="25">Pivotech Evolution</option><option value="26">Pivotech Momentum</option>
</select></div>
<div>Finish <select name="shower_finish" id="shower_finish" style="width:76%;">
<option>Polished Chrome</option>
<option>Satin Nickel</option>
<option>Robbed Bronze</option>
</select></div>
</div>
<div class="measurement-head3">&nbsp;&nbsp;Shower Head Support and Glass


<div >




  <div style="float:left;">
  <div style="padding:2px;">&nbsp;
Head Support <select style="width: 20%">
			<option>None</option>
			<option>Overpanel</option>
			<option>Header</option>
			<option>Support Bar</option>
			<option>Clamp</option>
			<option>Channel - Slide</option>
			<option>Channel - Lif and Drop</option>
			</select>
			
			<input type="checkbox"> To Ceiling
			&nbsp;&nbsp;&nbsp;
			Glass Type <select style="width: 24%;" name="glass_type" id="glass_type" onchange="change_glass_type(this.form)">
			
			<option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>			</select>
			</div>
			
			  <div style="padding:2px;">&nbsp;
Glass Stamp &nbsp;&nbsp;<select style="width: 20%" name="glass_stamp" id="glass_stamp">
			<option>None</option>
			<option>Overpanel</option>
			<option>Header</option>
			<option>Support Bar</option>
			<option>Clamp</option>
			<option>Channel - Slide</option>
			<option>Channel - Lif and Drop</option>
			</select>
			
			&nbsp;&nbsp;&nbsp;
			Supplier <select style="width: 42%;" name="supplier" id="supplier">
			<option value="1">Fethers</option><option value="2">G James</option><option value="3">Viridian Glass</option><option value="4">Jeldwen</option><option value="5" selected>National Glass</option><option value="6">AGG</option><option value="7">Logan Glass</option><option value="8">Classic Frosted Glass</option><option value="9">Watson Glass</option><option value="10">Tough Glass</option><option value="11">Glasstech</option><option value="12">Glass Processing USA</option><option value="13">Superkote Glass</option><option value="14">Glass Outlet</option>			</select>
			</div>
			
</div>


  <div style="float:left;">

</div>
</div>

</div>
</div>

<div class="measurement-panel">
<div class="measurement-panel1">



<!--<div style="overflow-x: scroll;width:100%;">-->
<div class="pnls" >
<div class="panel11">
<div class="pan1"></div>
<div class="pan2"></div>
</div>



	
<div class="panel12">
<div class="panl1">Panel 1</div>
<div class="panl2">
<select id="panle201" class="paneltypee1 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19" selected>Return-Notch-Double</option><option value="20">Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness1" id="glass_thickness1" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype1" id="gltype1"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel21">
<div class="pan201" onclick="change_angle_angle1(this.form);" data-toggle="modal" data-target="#myModal1"><input type="text" name="pan201" id="pan201" disabled value="180&deg;"  placeholder="180" class="angle1 angles"></div>
</div>
<style>
div .panel11{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel12{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow1 ul{
list-style-type: none;
position: relative;
}
.panel-drow1 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
	
<div class="panel22">
<div class="panl1">Panel 2</div>
<div class="panl2">
<select id="panle202" class="paneltypee2 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness2" id="glass_thickness2" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype2" id="gltype2"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel31">
<div class="pan202" onclick="change_angle_angle2(this.form);" data-toggle="modal" data-target="#myModal2"><input type="text" name="pan202" id="pan202" disabled value="180&deg;"  placeholder="180" class="angle2 angles"></div>
</div>
<style>
div .panel21{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel22{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow2 ul{
list-style-type: none;
position: relative;
}
.panel-drow2 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel32">
<div class="panl1">Panel 3</div>
<div class="panl2">
<select id="panle203" class="paneltypee3 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness3" id="glass_thickness3" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype3" id="gltype3"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel41">
<div class="pan203" onclick="change_angle_angle3(this.form);" data-toggle="modal" data-target="#myModal3"><input type="text" name="pan203" id="pan203" disabled value="180&deg;"  placeholder="180" class="angle3 angles"></div>
</div>
<style>
div .panel31{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel32{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow3 ul{
list-style-type: none;
position: relative;
}
.panel-drow3 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel42">
<div class="panl1">Panel 4</div>
<div class="panl2">
<select id="panle204" class="paneltypee4 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness4" id="glass_thickness4" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype4" id="gltype4"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel51">
<div class="pan204" onclick="change_angle_angle4(this.form);" data-toggle="modal" data-target="#myModal4"><input type="text" name="pan204" id="pan204" disabled value="180&deg;"  placeholder="180" class="angle4 angles"></div>
</div>
<style>
div .panel41{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel42{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow4 ul{
list-style-type: none;
position: relative;
}
.panel-drow4 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel52">
<div class="panl1">Panel 5</div>
<div class="panl2">
<select id="panle205" class="paneltypee5 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness5" id="glass_thickness5" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype5" id="gltype5"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel61">
<div class="pan205" onclick="change_angle_angle5(this.form);" data-toggle="modal" data-target="#myModal5"><input type="text" name="pan205" id="pan205" disabled value="180&deg;"  placeholder="180" class="angle5 angles"></div>
</div>
<style>
div .panel51{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel52{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow5 ul{
list-style-type: none;
position: relative;
}
.panel-drow5 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel62">
<div class="panl1">Panel 6</div>
<div class="panl2">
<select id="panle206" class="paneltypee6 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness6" id="glass_thickness6" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype6" id="gltype6"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel71">
<div class="pan206" onclick="change_angle_angle6(this.form);" data-toggle="modal" data-target="#myModal6"><input type="text" name="pan206" id="pan206" disabled value="180&deg;"  placeholder="180" class="angle6 angles"></div>
</div>
<style>
div .panel61{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel62{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow6 ul{
list-style-type: none;
position: relative;
}
.panel-drow6 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel72">
<div class="panl1">Panel 7</div>
<div class="panl2">
<select id="panle207" class="paneltypee7 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness7" id="glass_thickness7" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype7" id="gltype7"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel81">
<div class="pan207" onclick="change_angle_angle7(this.form);" data-toggle="modal" data-target="#myModal7"><input type="text" name="pan207" id="pan207" disabled value="180&deg;"  placeholder="180" class="angle7 angles"></div>
</div>
<style>
div .panel71{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel72{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow7 ul{
list-style-type: none;
position: relative;
}
.panel-drow7 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel82">
<div class="panl1">Panel 8</div>
<div class="panl2">
<select id="panle208" class="paneltypee8 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness8" id="glass_thickness8" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype8" id="gltype8"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel91">
<div class="pan208" onclick="change_angle_angle8(this.form);" data-toggle="modal" data-target="#myModal8"><input type="text" name="pan208" id="pan208" disabled value="180&deg;"  placeholder="180" class="angle8 angles"></div>
</div>
<style>
div .panel81{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel82{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow8 ul{
list-style-type: none;
position: relative;
}
.panel-drow8 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel92">
<div class="panl1">Panel 9</div>
<div class="panl2">
<select id="panle209" class="paneltypee9 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness9" id="glass_thickness9" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype9" id="gltype9"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel101">
<div class="pan209" onclick="change_angle_angle9(this.form);" data-toggle="modal" data-target="#myModal9"><input type="text" name="pan209" id="pan209" disabled value="180&deg;"  placeholder="180" class="angle9 angles"></div>
</div>
<style>
div .panel91{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel92{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow9 ul{
list-style-type: none;
position: relative;
}
.panel-drow9 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel102">
<div class="panl1">Panel 10</div>
<div class="panl2">
<select id="panle2010" class="paneltypee10 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness10" id="glass_thickness10" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype10" id="gltype10"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel111">
<div class="pan2010" onclick="change_angle_angle10(this.form);" data-toggle="modal" data-target="#myModal10"><input type="text" name="pan2010" id="pan2010" disabled value="180&deg;"  placeholder="180" class="angle10 angles"></div>
</div>
<style>
div .panel101{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel102{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow10 ul{
list-style-type: none;
position: relative;
}
.panel-drow10 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel112">
<div class="panl1">Panel 11</div>
<div class="panl2">
<select id="panle2011" class="paneltypee11 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness11" id="glass_thickness11" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype11" id="gltype11"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel121">
<div class="pan2011" onclick="change_angle_angle11(this.form);" data-toggle="modal" data-target="#myModal11"><input type="text" name="pan2011" id="pan2011" disabled value="180&deg;"  placeholder="180" class="angle11 angles"></div>
</div>
<style>
div .panel111{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel112{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow11 ul{
list-style-type: none;
position: relative;
}
.panel-drow11 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel122">
<div class="panl1">Panel 12</div>
<div class="panl2">
<select id="panle2012" class="paneltypee12 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness12" id="glass_thickness12" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype12" id="gltype12"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel131">
<div class="pan2012" onclick="change_angle_angle12(this.form);" data-toggle="modal" data-target="#myModal12"><input type="text" name="pan2012" id="pan2012" disabled value="180&deg;"  placeholder="180" class="angle12 angles"></div>
</div>
<style>
div .panel121{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel122{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow12 ul{
list-style-type: none;
position: relative;
}
.panel-drow12 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel132">
<div class="panl1">Panel 13</div>
<div class="panl2">
<select id="panle2013" class="paneltypee13 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness13" id="glass_thickness13" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype13" id="gltype13"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel141">
<div class="pan2013" onclick="change_angle_angle13(this.form);" data-toggle="modal" data-target="#myModal13"><input type="text" name="pan2013" id="pan2013" disabled value="180&deg;"  placeholder="180" class="angle13 angles"></div>
</div>
<style>
div .panel131{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel132{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow13 ul{
list-style-type: none;
position: relative;
}
.panel-drow13 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel142">
<div class="panl1">Panel 14</div>
<div class="panl2">
<select id="panle2014" class="paneltypee14 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness14" id="glass_thickness14" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype14" id="gltype14"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel151">
<div class="pan2014" onclick="change_angle_angle14(this.form);" data-toggle="modal" data-target="#myModal14"><input type="text" name="pan2014" id="pan2014" disabled value="180&deg;"  placeholder="180" class="angle14 angles"></div>
</div>
<style>
div .panel141{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel142{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow14 ul{
list-style-type: none;
position: relative;
}
.panel-drow14 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2013{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2013 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel152">
<div class="panl1">Panel 15</div>
<div class="panl2">
<select id="panle2015" class="paneltypee15 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness15" id="glass_thickness15" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype15" id="gltype15"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel161">
<div class="pan2015" onclick="change_angle_angle15(this.form);" data-toggle="modal" data-target="#myModal15"><input type="text" name="pan2015" id="pan2015" disabled value="180&deg;"  placeholder="180" class="angle15 angles"></div>
</div>
<style>
div .panel151{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel152{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow15 ul{
list-style-type: none;
position: relative;
}
.panel-drow15 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2013{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2013 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2014{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2014 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel162">
<div class="panl1">Panel 16</div>
<div class="panl2">
<select id="panle2016" class="paneltypee16 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness16" id="glass_thickness16" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype16" id="gltype16"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel171">
<div class="pan2016" onclick="change_angle_angle16(this.form);" data-toggle="modal" data-target="#myModal16"><input type="text" name="pan2016" id="pan2016" disabled value="180&deg;"  placeholder="180" class="angle16 angles"></div>
</div>
<style>
div .panel161{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel162{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow16 ul{
list-style-type: none;
position: relative;
}
.panel-drow16 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2013{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2013 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2014{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2014 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2015{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2015 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel172">
<div class="panl1">Panel 17</div>
<div class="panl2">
<select id="panle2017" class="paneltypee17 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness17" id="glass_thickness17" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype17" id="gltype17"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel181">
<div class="pan2017" onclick="change_angle_angle17(this.form);" data-toggle="modal" data-target="#myModal17"><input type="text" name="pan2017" id="pan2017" disabled value="180&deg;"  placeholder="180" class="angle17 angles"></div>
</div>
<style>
div .panel171{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel172{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow17 ul{
list-style-type: none;
position: relative;
}
.panel-drow17 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2013{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2013 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2014{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2014 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2015{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2015 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2016{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2016 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel182">
<div class="panl1">Panel 18</div>
<div class="panl2">
<select id="panle2018" class="paneltypee18 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness18" id="glass_thickness18" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype18" id="gltype18"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel191">
<div class="pan2018" onclick="change_angle_angle18(this.form);" data-toggle="modal" data-target="#myModal18"><input type="text" name="pan2018" id="pan2018" disabled value="180&deg;"  placeholder="180" class="angle18 angles"></div>
</div>
<style>
div .panel181{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel182{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow18 ul{
list-style-type: none;
position: relative;
}
.panel-drow18 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2013{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2013 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2014{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2014 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2015{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2015 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2016{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2016 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2017{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2017 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel192">
<div class="panl1">Panel 19</div>
<div class="panl2">
<select id="panle2019" class="paneltypee19 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness19" id="glass_thickness19" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype19" id="gltype19"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>
<div class="panel201">
<div class="pan2019" onclick="change_angle_angle19(this.form);" data-toggle="modal" data-target="#myModal19"><input type="text" name="pan2019" id="pan2019" disabled value="180&deg;"  placeholder="180" class="angle19 angles"></div>
</div>
<style>
div .panel191{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel192{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow19 ul{
list-style-type: none;
position: relative;
}
.panel-drow19 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2013{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2013 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2014{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2014 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2015{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2015 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2016{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2016 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2017{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2017 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2018{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2018 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

	
<div class="panel202">
<div class="panl1">Panel 20</div>
<div class="panl2">
<select id="panle2020" class="paneltypee20 panel_types" onchange="change_panle(this.form); calculate_all_data(this.form);"><option value="8">Curved-Door-Hinge-Left</option><option value="9">Curved-Door-Hinge-Right</option><option value="10">Curved-Return</option><option value="11">Door-Hinge-Left</option><option value="14">Door-Hinge-Right</option><option value="13">Door-Slide-Inner</option><option value="12">Door-Slide-Outer</option><option value="3">Gap</option><option value="15">Return</option><option value="16">Return-Nib</option><option value="17">Return-Nib-Notch</option><option value="18">Return-Notch</option><option value="19">Return-Notch-Double</option><option value="20" selected>Side</option><option value="4">Side-Nib</option><option value="5">Side-Nib-Notch</option><option value="6">Side-Notch</option><option value="7">Side-Notch-Double</option>
</select>

</div>
<div class="panl3">
<div style="float:left; width:35%"><input type="text" class="glassthicknesss" name="glass_thickness20" id="glass_thickness20" onchange="change_glass_thickness(this.form);"  value="10"></div>
<div style="float:left; width:65%"><select name="gltype20" id="gltype20"><option value="17">Acid Etched</option><option value="18">Acoustic Clear Laminate</option><option value="19">Acoustic Grey Laminate</option><option value="20">Amber Oceanic</option><option value="21">Antique Mirror</option><option value="22">Aqualite Amber</option><option value="23">Aqualite Amber Laminate</option><option value="24">Aqualite Blue</option><option value="25">Aqualite Blue Laminate</option><option value="26">Aqualite Clear</option><option value="27">Aqualite Clear Laminate</option><option value="28">Aqualite Green</option><option value="29">Aqualite Green Laminate	</option><option value="2">Artic</option><option value="3">Artico</option><option value="4">Auto Clear</option><option value="5">Auto Green</option><option value="6">Black Mirror</option><option value="7">Blue</option><option value="8">Blue Artico</option><option value="10">Broad Reeded</option><option value="11">Broadline</option><option value="12">Bronze</option><option value="13">Bronze Laminate</option><option value="14">Bronze Mirror</option><option value="15">Bronze Stippolite</option><option value="16">Cathedral</option><option value="30">Ceramic White Laminate</option><option value="31">Classic Black Vinyl Backed</option><option value="9" selected>Clear</option><option value="32">Clear (Low E)</option><option value="33">Clear (Low Iron)</option><option value="34">Clear (Low Iron) Colour-Backed</option><option value="35">Clear Colour-Backed	</option><option value="36">Clear Diffused</option><option value="37">Clear Etchlite</option><option value="38">Clear Hush Laminate</option><option value="39">Clear Laminate</option><option value="40">Clear Vision Low Iron</option><option value="41">Climashield Clear</option><option value="42">Climashield Neutral</option><option value="43">Comfortplus Grey Laminate</option><option value="44">Comfortplus Neutral Laminate</option><option value="45">Cotswald </option><option value="46">Cotswold </option><option value="47">Dark blue</option><option value="49">Dark Grey	Spots</option><option value="48">Dark Grey</option><option value="50">DecorSatin</option><option value="51">Desert Sand</option><option value="52">Diamond Frost</option><option value="53">Energy Tech Clear</option><option value="54">Energy Tech Green</option><option value="55">Energy Tech Grey</option><option value="56">Energy Tech SuperGreen</option><option value="57">Etchlite</option><option value="58">Evantage Blue Green</option><option value="59">Evantage Bronze</option><option value="60">Evantage Clear</option><option value="61">Evantage Grey</option><option value="62">Evantage SuperBlue</option><option value="63">Evantage SuperGreen</option><option value="64">Evergreen</option><option value="65">Fire Place Glass</option><option value="67">Flemish</option><option value="68">Georgian Wire Cast (Obscure)</option><option value="69">Georgian Wired </option><option value="70">Glue Chip</option><option value="71">Green</option><option value="72">Green Artico</option><option value="73">Green Laminate</option><option value="74">Green Laminate (Low E)</option><option value="75">Grey</option><option value="76">Grey Acid Etched</option><option value="77">Grey Laminate</option><option value="78">Grey Laminate (Low E)</option><option value="79">Grey Mirror</option><option value="80">Grey Satina</option><option value="81">Grey Showertex Laminate</option><option value="82">Grey Stippolite</option><option value="83">Grey Translucent Laminate</option><option value="84">Hammered</option><option value="85">IntruderGuard Clear Laminate</option><option value="86">Kosciusko</option><option value="89">Laminate (Low Iron)</option><option value="91">Laminate Bronze</option><option value="92">Laminate Cathlite</option><option value="93">Laminate Clear</option><option value="94">Laminate Green</option><option value="95">Laminate Low E</option><option value="96">Laminate Sound Control</option><option value="97">Laminate White</option><option value="98">Laminated Bronze</option><option value="99">Laminated Clear</option><option value="100">Laminated Green</option><option value="102">Laminated Grey</option><option value="103">Laminated Mirror</option><option value="104">Laminated Opalucent</option><option value="105">Laminated Soundstop PVB</option><option value="106">Laminated Sunergy Clear</option><option value="87">Lecobel Cool White</option><option value="88">Lecobel Deep Black</option><option value="107">Lightbridge (Low E)</option><option value="108">Louvre Clear</option><option value="109">Louvre Dark Grey</option><option value="110">Louvre Grey</option>
</select></div>


</div>

</div>

<style>
div .panel201{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
div .panel202{float:left;border:1px solid #b5b5b5;height:10%;text-align:center;}
.panel-drow20 ul{
list-style-type: none;
position: relative;
}
.panel-drow20 ul li{    list-style: none;
    display: inline-block;
    margin: 2px 9px;
    float: left;}	

</style>
<style>.pan201{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan201 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan202{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan202 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan203{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan203 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan204{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan204 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan205{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan205 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan206{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan206 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan207{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan207 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan208{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan208 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan209{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan209 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2010{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2010 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2011{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2011 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2012{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2012 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2013{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2013 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2014{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2014 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2015{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2015 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2016{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2016 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2017{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2017 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2018{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2018 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>

<style>.pan2019{background-color:#b5d9ff;height:100%;width:30px;border:1px solid #b5b5b5;border-radius:3px;color:black;line-height:64px;text-align:center;}

.pan2019 input[type=text] {height:105%;width: 112%;background-color: #b5d9ff;border-radius: 3px;}
</style>
<!--
<style>
.panl6 select{width:10% !important}
.paneltypee3{width:10%;}
</style>
-->
<div class="panel11">
<div class="pan1"></div>
<div class="pan2"></div>
</div>

</div>




<div class="panel-drow">



<div id="dimension_data"></div>

</div>

<span id="tesssst"></span>
<!--</div>-->
</div>


<script>
document.querySelector('input').addEventListener('input', function(){
  document.querySelector('.measurement-panel1').scrollLeft = this.value;
})
</script>
<style>
.measurement-panel1{
  height: 450px;
  overflow: scroll;
  direction: ltl;  /*  the trick */
}

.measurement-panel1::before{ content:''; display:block; width:400%; height:1px; }
</style>


<div class="measurement-panel2">
ewfrfw
</div>

</div>




<div class="dimension-tab">
<div class="dimension-div1">

<div class="tab11" >
  <button class="tablinks active"  onclick="openDetails(event, 'Dimensions')">Dimensions</button>
  <button class="tablinks" onclick="openDetails(event, 'Fittings')">Fittings</button>
</div>
<div id="Dimensions" class="tabcontentss" style="display: block;" >
<div style="overflow-x: scroll;width:100%;">
<div id="text_fields"></div>
</div>
<table id="dimention-last">
<tr>
<td width="17%" align="right">Door Glass Width</td>
<td width="8%"><input type="text" name="door_glass_width" id="door_glass_width" value="650" onchange="save_measurement(this.form);"></td>
<td width="17%" align="right">Gap under door</td>
<td width="8%"><input type="text" name="gap_under_door" id="gap_under_door" value="Default"></td>
<td width="17%" align="right">Tray Offset</td>
<td width="8%"><input type="text" name="tray_offset" id="tray_offset" value="0"></td>
<td width="5%" align="right">From</td>
<td width="20%"><select>
<option>Centreline Glass</option>
<option>Exterior Glass</option>
<option>Exterior Metal</option>
<option>Interior Metal</option>
</select></td>
</tr>

</table>
</div>


<div id="Fittings" class="tabcontentss">

<div class="hardeware">
<div class="fittingcategory">
<div class="fitting-haed">Fitting Category</div>
<div>

<select size="12" style="width:100%;" onchange="show_cat_type(this.form);show_hardware_detail(this.form);" name="category_name" id="category_name">
<option value="1" selected>Frameless Styles</option><option value="2">Brackets</option><option value="3">Channels</option><option value="4">Dias Semi Frameless</option><option value="5">Door Seals</option><option value="6">Door Stops</option><option value="7">Frameless Styles</option><option value="8">Handles</option><option value="9">Headers</option><option value="10">Hinges</option><option value="11">Miscellaneous</option><option value="12">Shelf Brackets</option><option value="13">Sliding Door Components</option><option value="14">Sliding Door Kits</option><option value="15">Support Bars</option><option value="16">U-Channel</option></select>

</div>
</div>
<div class="hardwaretype">
<div class="hardwaretype-head" ><span id="cattype11">Frameless Styles</span> List</div>

<div>
<select size="12" style="width:100%;" name="category_type" id="category_type" onchange="show_hardware_detail(this.form)" class="field-select" required>

<option value="10" selected>Fethers ES & SB10 SQ</option><option value="9" selected>Fethers ES & CH18</option><option value="7" selected>Channel - MFGS Elite</option><option value="6" selected>Bracket - MFGS Elite</option>
</select>

</div>
</div>
<div class="hardwarename">
<div class="hardwaretype-head" ><span id="cattype">Frameless Styles</span> List</div>
<!--<div class="category-type">-->
<div>

<select size="12" style="width:100%;" name="hardware_detail" id="hardware_detail">
<option value="22" selected>Polished Chrome</option><option value="1" selected>Polished Chrome</option>
</select>
</div>
</div>
<div class="addhardware" style="">
<div class="hardwareadd-head" >Quantity</div>

<div class="hardwareadd-button">

<input type="text" name="qty" placeholder="Calculated" id="quantity" value=""  style="margin-top:6px;width:80%;"><br /><br />
<button style="width:80%;" onclick="add_hardware(this.form)"><b style="font-size:20px;">></b></button><br /><br />
<button style="width:80%;" onclick="remove_hardware(this.form)"><b style="font-size:20px;"><</b></button>

</div>
</div>
<div class="hardwaredetail" style="overflow:auto;">
<div class="hardwaredetail-head" >
<div class="hardwaredetail1">Qty</div>
<div class="hardwaredetail2">Ftiing</div>
<div class="hardwaredetail3">ss</div>
</div>
<script>

</script>
<style>

</style>
<div class="hardware-details" id="added_hardware_detail" >
<table  cellspacing="1" width="100%" id="table1" class="table--highlight">

	

	<tr class="hardwaredd2"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">BB12 Polished Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="13">Gold Plated</option><option value="14">Polished Chrome</option><option value="15">Satin Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="2" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="2" id="added_id">


	
	

	<tr class="hardwaredd28"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">BB12 Polished Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="13">Gold Plated</option><option value="14">Polished Chrome</option><option value="15">Satin Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="28" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="28" id="added_id">


	
	

	<tr class="hardwaredd29"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">Fethers ES & CH18 Satin Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="3">Polished Chrome</option><option value="4">Satin Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="29" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="29" id="added_id">


	
	

	<tr class="hardwaredd30"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">Fethers ES & CH18 Satin Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="3">Polished Chrome</option><option value="4">Satin Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="30" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="30" id="added_id">


	
	

	<tr class="hardwaredd31"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">BB11 Polished Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="10">Gold Plated</option><option value="11">Polished Chrome</option><option value="12">Satin Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="31" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="31" id="added_id">


	
	

	<tr class="hardwaredd34"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">AC2 Polished Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="17">Polished Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="34" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="34" id="added_id">


	
	

	<tr class="hardwaredd35"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">Fethers ES & CH18 Satin Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="3">Polished Chrome</option><option value="4">Satin Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="35" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="35" id="added_id">


	
	

	<tr class="hardwaredd38"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">Fethers ES & CH18 Polished Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="3">Polished Chrome</option><option value="4">Satin Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="38" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="38" id="added_id">


	
	

	<tr class="hardwaredd39"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">Channel - MFGS Elite Polished Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="2">Polished Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="39" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="39" id="added_id">


	
	

	<tr class="hardwaredd40"  onclick="change_hardware_img(this.form)">
	<td class="harddetail1">1</td>
<td class="harddetail2">Bracket - MFGS Elite Polished Chrome</td>
<td class="harddetail3">
<select  name="hardware_color" id="hardware_color" onchange="update_hardware_color(this.fom)" style="width: 100%;"><option></option><option value="1">Polished Chrome</option><option value="22">Polished Chrome</option></select><input type="radio" name="hardware_id_re" id="hardware_id_re" 
  value="40" style="display:none;">
</td>
</tr>

<input type="hidden" name="added_id" value="40" id="added_id">


	<input type="radio" checked name="hardware_id_re" id="hardware_id_re" 
  value="0" style="display:none;">
</table>
</div>

<script>
function change_hardware_img(form){
	
	var added_id=$("input[name='hardware_id_re']:checked").val();
	//alert(added_id);
	if(added_id=='0'){
			//alert('please select hardware for remove');
		}
		
		else{
			$.ajax({
      type:'post',
      url:"date_save/show_hardware_img.php",
      data:{added_id:added_id},
      success: function(data){
		  //window.location.reload(true);
       $("#change_fitting_drawing").html(data);
        //alert(data);      
                
	}
      });
		}
	
	
}
 $(function() {
var $tableRows = $('.table--highlight tr');
$tableRows.click(function() {
  var self = $(this);
  var siblings = self.siblings();
  self.addClass('selectedhardware');
  siblings.removeClass('selectedhardware');
  self.find('input:radio').prop('checked',true);
  
});
});
</script>

</div>
</div>

</div>

</div>



</div>
<style>
.selectedhardware{background-color:#b5d9ff;}
.selectedhardware select{background-color:#b5d9ff;}
</style>
<div class="dimension-div2">
<div class="upper-button">
<button class="up-botton1"><u>B</u>ase Widths..</button> 
<button class="up-botton2"><u>H</u>ead Slop..</button> 
<button class="up-botton3"><u>F</u>ittin<u>g</u>s</button> 
<button class="up-botton4"><u>P</u>anels</button> 
<button class="up-botton5" onclick="view_3D_image()">3D</button></div>
<div class="preview-image">
<div class="tabdiv2">
  <button class="tablinks"  onclick="openDimesion(event, 'Comment')">Comments and Warnings</button>
  <button class="tablinks active" onclick="openDimesion(event, '3D View')">3D View</button>
  <button class="tablinks" onclick="openDimesion(event, 'Fitting Drawing')">Fitting Drawing</button>
</div>

<div id="Comment" class="tabcontentdiv2" style="display: none;">
<div class="intall_commet1">
<div style="float:left; margin:5px;"><span>Installation Comment</span> </div>
<div  style="float:right;">
<select class="installation_comment_list" id="installation_comment_list" onchange="add_installation_comment(this.form);">
<option value="">Append Standard</option>
<option value="C/L OF BATH LIP">C/L OF BATH LIP</option>
<option value="C/L OF CHASE UP WALL">C/L OF CHASE UP WALL</option>
<option value="C/L OF GRANT LINE ON FLOOR">C/L OF GRANT LINE ON FLOOR</option>
<option value="C/L OF UPSTAND">C/L OF UPSTAND</option>
<option value="CHANNEL FLUSH INSIDE OF STEP">CHANNEL FLUSH INSIDE OF STEP</option>
<option value="CHANNEL FLUSH INSIDE OF UPSTAND">CHANNEL FLUSH INSIDE OF UPSTAND</option>
<option value="CHANNEL FLUSH OUTSIDE OF STEP">CHANNEL FLUSH OUTSIDE OF STEP</option>
<option value="CHANNEL FLUSH OUTSIDE OF UPSTAND">CHANNEL FLUSH OUTSIDE OF UPSTAND</option>

</select></div>
</div>
<div class="intall_commet2"><textarea name="installation_comment" id="installation_comment"></textarea></div>


<div class="glass_order_commet1">
<div style="float:left; margin:5px;"><span>Glass Order Comment</span> </div>
<div  style="float:right;">
<select class="glass_order_comment_list" id="glass_order_comment_list" onchange="glass_order_comment(this.form);">
<option value="">Append Standard</option>
<option value="C/L OF BATH LIP">C/L OF BATH LIP</option>
<option value="C/L OF CHASE UP WALL">C/L OF CHASE UP WALL</option>
<option value="C/L OF GRANT LINE ON FLOOR">C/L OF GRANT LINE ON FLOOR</option>
<option value="C/L OF UPSTAND">C/L OF UPSTAND</option>
<option value="CHANNEL FLUSH INSIDE OF STEP">CHANNEL FLUSH INSIDE OF STEP</option>
<option value="CHANNEL FLUSH INSIDE OF UPSTAND">CHANNEL FLUSH INSIDE OF UPSTAND</option>
<option value="CHANNEL FLUSH OUTSIDE OF STEP">CHANNEL FLUSH OUTSIDE OF STEP</option>
<option value="CHANNEL FLUSH OUTSIDE OF UPSTAND">CHANNEL FLUSH OUTSIDE OF UPSTAND</option>

</select></div>
</div>
<div class="glass_order_commet2"><textarea name="glass_order_comment" id="glass_order_comment"></textarea></div>



<div class="inforation_warnings_commet1">
<div style="float:left; margin:5px;"><span>Shower Information and Warnings</span> </div>

</div>
<div class="inforation_warnings_commet2"><input type="text" name="inforation_warnings_comment" id="inforation_warnings_comment"></div>








</div>
<div id="3D View" class="tabcontentdiv2" style="display: block;">
<div class="img-div">
<img src="adm_shower/images/template/f319eace7f33056e2917ca28be1cba47.jpg">
</div>
</div>
<div id="Fitting Drawing" class="tabcontentdiv2" style="display: none;text-align: center;" >
<span id="change_fitting_drawing"><img src="adm_shower/images/hardware/handle.jpg"></span></div>
</div>

</div>



</div>

<div class="button_bottom">
<div class="button_bottom11">
<button>Edit Hardware</button>
<button>C<u>a</u>lculate</button>
<button>C<u>l</u>ear Shower</button>
<button onclick="save_all_data(this.form)"><u>S</u>ave</button>
<button onclick="closeWebsite()"><u>C</u>ancel</button>
</div>
</div>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>

</style>


 <div class="modal fade" id="myModal1" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle1(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 1<br /><input type="text" id="panelss11" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 2<br /><input type="text" id="panelss12" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle1" id="newangle1" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection1" id="intersection1">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 1 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre1" value="0" id="rightmitre1">&deg;</p>
		 
		 <p><select name="rightmitre1_status" id="rightmitre1_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 2 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre1" value="0" id="leftmitre1">&deg;</p>
		 
		 <p><select name="leftmitre1_status" id="leftmitre1_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle1" onclick="update_angle1(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle1(form){
				var pann_qty="20";
				
				var paneltype1=$("#panle201 :selected").text();
				var paneltype2=$("#panle202 :selected").text();
				
				$("#panelss11").val(paneltype1);
				$("#panelss12").val(paneltype2);
				//alert(paneltype1);
				//alert(paneltype2);
}


function update_angle1(form){
	var newangle1=$("#newangle1").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee1 = 0;
var panel_no="1";


var anglee1 = newangle1;

var val_anglle=anglee1
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee1:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan21").val(""+newangle1+""+ascii(176)+"");
 $("#pan31").val(""+newangle1+""+ascii(176)+"");
 $("#pan41").val(""+newangle1+""+ascii(176)+"");
 $("#pan51").val(""+newangle1+""+ascii(176)+"");
 $("#pan61").val(""+newangle1+""+ascii(176)+"");
 $("#pan71").val(""+newangle1+""+ascii(176)+"");
 $("#pan81").val(""+newangle1+""+ascii(176)+"");
 $("#pan91").val(""+newangle1+""+ascii(176)+"");
 $("#pan101").val(""+newangle1+""+ascii(176)+"");
 $("#pan111").val(""+newangle1+""+ascii(176)+"");
 $("#pan121").val(""+newangle1+""+ascii(176)+"");
 $("#pan131").val(""+newangle1+""+ascii(176)+"");
 $("#pan141").val(""+newangle1+""+ascii(176)+"");
 $("#pan151").val(""+newangle1+""+ascii(176)+"");
 $("#pan161").val(""+newangle1+""+ascii(176)+"");
 $("#pan171").val(""+newangle1+""+ascii(176)+"");
 $("#pan181").val(""+newangle1+""+ascii(176)+"");
 $("#pan191").val(""+newangle1+""+ascii(176)+"");

$("#pan21").attr("placeholder",anglee1);	
	
	
	
}
  </script> <div class="modal fade" id="myModal2" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle2(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 2<br /><input type="text" id="panelss21" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 3<br /><input type="text" id="panelss22" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle2" id="newangle2" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection2" id="intersection2">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 2 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre2" value="0" id="rightmitre2">&deg;</p>
		 
		 <p><select name="rightmitre2_status" id="rightmitre2_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 3 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre2" value="0" id="leftmitre2">&deg;</p>
		 
		 <p><select name="leftmitre2_status" id="leftmitre2_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle2" onclick="update_angle2(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle2(form){
				var pann_qty="20";
				
				var paneltype2=$("#panle202 :selected").text();
				var paneltype3=$("#panle203 :selected").text();
				
				$("#panelss21").val(paneltype2);
				$("#panelss22").val(paneltype3);
				//alert(paneltype2);
				//alert(paneltype3);
}


function update_angle2(form){
	var newangle2=$("#newangle2").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee2 = 0;
var panel_no="2";


var anglee2 = newangle2;

var val_anglle=anglee2
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee2:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan32").val(""+newangle2+""+ascii(176)+"");
 $("#pan42").val(""+newangle2+""+ascii(176)+"");
 $("#pan52").val(""+newangle2+""+ascii(176)+"");
 $("#pan62").val(""+newangle2+""+ascii(176)+"");
 $("#pan72").val(""+newangle2+""+ascii(176)+"");
 $("#pan82").val(""+newangle2+""+ascii(176)+"");
 $("#pan92").val(""+newangle2+""+ascii(176)+"");
 $("#pan102").val(""+newangle2+""+ascii(176)+"");
 $("#pan112").val(""+newangle2+""+ascii(176)+"");
 $("#pan122").val(""+newangle2+""+ascii(176)+"");
 $("#pan132").val(""+newangle2+""+ascii(176)+"");
 $("#pan142").val(""+newangle2+""+ascii(176)+"");
 $("#pan152").val(""+newangle2+""+ascii(176)+"");
 $("#pan162").val(""+newangle2+""+ascii(176)+"");
 $("#pan172").val(""+newangle2+""+ascii(176)+"");
 $("#pan182").val(""+newangle2+""+ascii(176)+"");
 $("#pan192").val(""+newangle2+""+ascii(176)+"");

$("#pan32").attr("placeholder",anglee2);	
	
	
	
}
  </script> <div class="modal fade" id="myModal3" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle3(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 3<br /><input type="text" id="panelss31" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 4<br /><input type="text" id="panelss32" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle3" id="newangle3" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection3" id="intersection3">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 3 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre3" value="0" id="rightmitre3">&deg;</p>
		 
		 <p><select name="rightmitre3_status" id="rightmitre3_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 4 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre3" value="0" id="leftmitre3">&deg;</p>
		 
		 <p><select name="leftmitre3_status" id="leftmitre3_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle3" onclick="update_angle3(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle3(form){
				var pann_qty="20";
				
				var paneltype3=$("#panle203 :selected").text();
				var paneltype4=$("#panle204 :selected").text();
				
				$("#panelss31").val(paneltype3);
				$("#panelss32").val(paneltype4);
				//alert(paneltype3);
				//alert(paneltype4);
}


function update_angle3(form){
	var newangle3=$("#newangle3").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee3 = 0;
var panel_no="3";


var anglee3 = newangle3;

var val_anglle=anglee3
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee3:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan43").val(""+newangle3+""+ascii(176)+"");
 $("#pan53").val(""+newangle3+""+ascii(176)+"");
 $("#pan63").val(""+newangle3+""+ascii(176)+"");
 $("#pan73").val(""+newangle3+""+ascii(176)+"");
 $("#pan83").val(""+newangle3+""+ascii(176)+"");
 $("#pan93").val(""+newangle3+""+ascii(176)+"");
 $("#pan103").val(""+newangle3+""+ascii(176)+"");
 $("#pan113").val(""+newangle3+""+ascii(176)+"");
 $("#pan123").val(""+newangle3+""+ascii(176)+"");
 $("#pan133").val(""+newangle3+""+ascii(176)+"");
 $("#pan143").val(""+newangle3+""+ascii(176)+"");
 $("#pan153").val(""+newangle3+""+ascii(176)+"");
 $("#pan163").val(""+newangle3+""+ascii(176)+"");
 $("#pan173").val(""+newangle3+""+ascii(176)+"");
 $("#pan183").val(""+newangle3+""+ascii(176)+"");
 $("#pan193").val(""+newangle3+""+ascii(176)+"");

$("#pan43").attr("placeholder",anglee3);	
	
	
	
}
  </script> <div class="modal fade" id="myModal4" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle4(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 4<br /><input type="text" id="panelss41" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 5<br /><input type="text" id="panelss42" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle4" id="newangle4" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection4" id="intersection4">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 4 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre4" value="0" id="rightmitre4">&deg;</p>
		 
		 <p><select name="rightmitre4_status" id="rightmitre4_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 5 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre4" value="0" id="leftmitre4">&deg;</p>
		 
		 <p><select name="leftmitre4_status" id="leftmitre4_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle4" onclick="update_angle4(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle4(form){
				var pann_qty="20";
				
				var paneltype4=$("#panle204 :selected").text();
				var paneltype5=$("#panle205 :selected").text();
				
				$("#panelss41").val(paneltype4);
				$("#panelss42").val(paneltype5);
				//alert(paneltype4);
				//alert(paneltype5);
}


function update_angle4(form){
	var newangle4=$("#newangle4").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee4 = 0;
var panel_no="4";


var anglee4 = newangle4;

var val_anglle=anglee4
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee4:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan54").val(""+newangle4+""+ascii(176)+"");
 $("#pan64").val(""+newangle4+""+ascii(176)+"");
 $("#pan74").val(""+newangle4+""+ascii(176)+"");
 $("#pan84").val(""+newangle4+""+ascii(176)+"");
 $("#pan94").val(""+newangle4+""+ascii(176)+"");
 $("#pan104").val(""+newangle4+""+ascii(176)+"");
 $("#pan114").val(""+newangle4+""+ascii(176)+"");
 $("#pan124").val(""+newangle4+""+ascii(176)+"");
 $("#pan134").val(""+newangle4+""+ascii(176)+"");
 $("#pan144").val(""+newangle4+""+ascii(176)+"");
 $("#pan154").val(""+newangle4+""+ascii(176)+"");
 $("#pan164").val(""+newangle4+""+ascii(176)+"");
 $("#pan174").val(""+newangle4+""+ascii(176)+"");
 $("#pan184").val(""+newangle4+""+ascii(176)+"");
 $("#pan194").val(""+newangle4+""+ascii(176)+"");

$("#pan54").attr("placeholder",anglee4);	
	
	
	
}
  </script> <div class="modal fade" id="myModal5" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle5(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 5<br /><input type="text" id="panelss51" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 6<br /><input type="text" id="panelss52" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle5" id="newangle5" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection5" id="intersection5">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 5 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre5" value="0" id="rightmitre5">&deg;</p>
		 
		 <p><select name="rightmitre5_status" id="rightmitre5_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 6 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre5" value="0" id="leftmitre5">&deg;</p>
		 
		 <p><select name="leftmitre5_status" id="leftmitre5_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle5" onclick="update_angle5(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle5(form){
				var pann_qty="20";
				
				var paneltype5=$("#panle205 :selected").text();
				var paneltype6=$("#panle206 :selected").text();
				
				$("#panelss51").val(paneltype5);
				$("#panelss52").val(paneltype6);
				//alert(paneltype5);
				//alert(paneltype6);
}


function update_angle5(form){
	var newangle5=$("#newangle5").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee5 = 0;
var panel_no="5";


var anglee5 = newangle5;

var val_anglle=anglee5
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee5:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan65").val(""+newangle5+""+ascii(176)+"");
 $("#pan75").val(""+newangle5+""+ascii(176)+"");
 $("#pan85").val(""+newangle5+""+ascii(176)+"");
 $("#pan95").val(""+newangle5+""+ascii(176)+"");
 $("#pan105").val(""+newangle5+""+ascii(176)+"");
 $("#pan115").val(""+newangle5+""+ascii(176)+"");
 $("#pan125").val(""+newangle5+""+ascii(176)+"");
 $("#pan135").val(""+newangle5+""+ascii(176)+"");
 $("#pan145").val(""+newangle5+""+ascii(176)+"");
 $("#pan155").val(""+newangle5+""+ascii(176)+"");
 $("#pan165").val(""+newangle5+""+ascii(176)+"");
 $("#pan175").val(""+newangle5+""+ascii(176)+"");
 $("#pan185").val(""+newangle5+""+ascii(176)+"");
 $("#pan195").val(""+newangle5+""+ascii(176)+"");

$("#pan65").attr("placeholder",anglee5);	
	
	
	
}
  </script> <div class="modal fade" id="myModal6" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle6(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 6<br /><input type="text" id="panelss61" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 7<br /><input type="text" id="panelss62" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle6" id="newangle6" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection6" id="intersection6">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 6 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre6" value="0" id="rightmitre6">&deg;</p>
		 
		 <p><select name="rightmitre6_status" id="rightmitre6_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 7 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre6" value="0" id="leftmitre6">&deg;</p>
		 
		 <p><select name="leftmitre6_status" id="leftmitre6_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle6" onclick="update_angle6(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle6(form){
				var pann_qty="20";
				
				var paneltype6=$("#panle206 :selected").text();
				var paneltype7=$("#panle207 :selected").text();
				
				$("#panelss61").val(paneltype6);
				$("#panelss62").val(paneltype7);
				//alert(paneltype6);
				//alert(paneltype7);
}


function update_angle6(form){
	var newangle6=$("#newangle6").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee6 = 0;
var panel_no="6";


var anglee6 = newangle6;

var val_anglle=anglee6
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee6:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan76").val(""+newangle6+""+ascii(176)+"");
 $("#pan86").val(""+newangle6+""+ascii(176)+"");
 $("#pan96").val(""+newangle6+""+ascii(176)+"");
 $("#pan106").val(""+newangle6+""+ascii(176)+"");
 $("#pan116").val(""+newangle6+""+ascii(176)+"");
 $("#pan126").val(""+newangle6+""+ascii(176)+"");
 $("#pan136").val(""+newangle6+""+ascii(176)+"");
 $("#pan146").val(""+newangle6+""+ascii(176)+"");
 $("#pan156").val(""+newangle6+""+ascii(176)+"");
 $("#pan166").val(""+newangle6+""+ascii(176)+"");
 $("#pan176").val(""+newangle6+""+ascii(176)+"");
 $("#pan186").val(""+newangle6+""+ascii(176)+"");
 $("#pan196").val(""+newangle6+""+ascii(176)+"");

$("#pan76").attr("placeholder",anglee6);	
	
	
	
}
  </script> <div class="modal fade" id="myModal7" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle7(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 7<br /><input type="text" id="panelss71" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 8<br /><input type="text" id="panelss72" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle7" id="newangle7" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection7" id="intersection7">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 7 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre7" value="0" id="rightmitre7">&deg;</p>
		 
		 <p><select name="rightmitre7_status" id="rightmitre7_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 8 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre7" value="0" id="leftmitre7">&deg;</p>
		 
		 <p><select name="leftmitre7_status" id="leftmitre7_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle7" onclick="update_angle7(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle7(form){
				var pann_qty="20";
				
				var paneltype7=$("#panle207 :selected").text();
				var paneltype8=$("#panle208 :selected").text();
				
				$("#panelss71").val(paneltype7);
				$("#panelss72").val(paneltype8);
				//alert(paneltype7);
				//alert(paneltype8);
}


function update_angle7(form){
	var newangle7=$("#newangle7").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee7 = 0;
var panel_no="7";


var anglee7 = newangle7;

var val_anglle=anglee7
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee7:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan87").val(""+newangle7+""+ascii(176)+"");
 $("#pan97").val(""+newangle7+""+ascii(176)+"");
 $("#pan107").val(""+newangle7+""+ascii(176)+"");
 $("#pan117").val(""+newangle7+""+ascii(176)+"");
 $("#pan127").val(""+newangle7+""+ascii(176)+"");
 $("#pan137").val(""+newangle7+""+ascii(176)+"");
 $("#pan147").val(""+newangle7+""+ascii(176)+"");
 $("#pan157").val(""+newangle7+""+ascii(176)+"");
 $("#pan167").val(""+newangle7+""+ascii(176)+"");
 $("#pan177").val(""+newangle7+""+ascii(176)+"");
 $("#pan187").val(""+newangle7+""+ascii(176)+"");
 $("#pan197").val(""+newangle7+""+ascii(176)+"");

$("#pan87").attr("placeholder",anglee7);	
	
	
	
}
  </script> <div class="modal fade" id="myModal8" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle8(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 8<br /><input type="text" id="panelss81" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 9<br /><input type="text" id="panelss82" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle8" id="newangle8" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection8" id="intersection8">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 8 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre8" value="0" id="rightmitre8">&deg;</p>
		 
		 <p><select name="rightmitre8_status" id="rightmitre8_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 9 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre8" value="0" id="leftmitre8">&deg;</p>
		 
		 <p><select name="leftmitre8_status" id="leftmitre8_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle8" onclick="update_angle8(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle8(form){
				var pann_qty="20";
				
				var paneltype8=$("#panle208 :selected").text();
				var paneltype9=$("#panle209 :selected").text();
				
				$("#panelss81").val(paneltype8);
				$("#panelss82").val(paneltype9);
				//alert(paneltype8);
				//alert(paneltype9);
}


function update_angle8(form){
	var newangle8=$("#newangle8").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee8 = 0;
var panel_no="8";


var anglee8 = newangle8;

var val_anglle=anglee8
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee8:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan98").val(""+newangle8+""+ascii(176)+"");
 $("#pan108").val(""+newangle8+""+ascii(176)+"");
 $("#pan118").val(""+newangle8+""+ascii(176)+"");
 $("#pan128").val(""+newangle8+""+ascii(176)+"");
 $("#pan138").val(""+newangle8+""+ascii(176)+"");
 $("#pan148").val(""+newangle8+""+ascii(176)+"");
 $("#pan158").val(""+newangle8+""+ascii(176)+"");
 $("#pan168").val(""+newangle8+""+ascii(176)+"");
 $("#pan178").val(""+newangle8+""+ascii(176)+"");
 $("#pan188").val(""+newangle8+""+ascii(176)+"");
 $("#pan198").val(""+newangle8+""+ascii(176)+"");

$("#pan98").attr("placeholder",anglee8);	
	
	
	
}
  </script> <div class="modal fade" id="myModal9" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle9(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 9<br /><input type="text" id="panelss91" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 10<br /><input type="text" id="panelss92" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle9" id="newangle9" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection9" id="intersection9">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 9 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre9" value="0" id="rightmitre9">&deg;</p>
		 
		 <p><select name="rightmitre9_status" id="rightmitre9_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 10 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre9" value="0" id="leftmitre9">&deg;</p>
		 
		 <p><select name="leftmitre9_status" id="leftmitre9_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle9" onclick="update_angle9(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle9(form){
				var pann_qty="20";
				
				var paneltype9=$("#panle209 :selected").text();
				var paneltype10=$("#panle2010 :selected").text();
				
				$("#panelss91").val(paneltype9);
				$("#panelss92").val(paneltype10);
				//alert(paneltype9);
				//alert(paneltype10);
}


function update_angle9(form){
	var newangle9=$("#newangle9").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee9 = 0;
var panel_no="9";


var anglee9 = newangle9;

var val_anglle=anglee9
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee9:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan109").val(""+newangle9+""+ascii(176)+"");
 $("#pan119").val(""+newangle9+""+ascii(176)+"");
 $("#pan129").val(""+newangle9+""+ascii(176)+"");
 $("#pan139").val(""+newangle9+""+ascii(176)+"");
 $("#pan149").val(""+newangle9+""+ascii(176)+"");
 $("#pan159").val(""+newangle9+""+ascii(176)+"");
 $("#pan169").val(""+newangle9+""+ascii(176)+"");
 $("#pan179").val(""+newangle9+""+ascii(176)+"");
 $("#pan189").val(""+newangle9+""+ascii(176)+"");
 $("#pan199").val(""+newangle9+""+ascii(176)+"");

$("#pan109").attr("placeholder",anglee9);	
	
	
	
}
  </script> <div class="modal fade" id="myModal10" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle10(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 10<br /><input type="text" id="panelss101" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 11<br /><input type="text" id="panelss102" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle10" id="newangle10" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection10" id="intersection10">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 10 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre10" value="0" id="rightmitre10">&deg;</p>
		 
		 <p><select name="rightmitre10_status" id="rightmitre10_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 11 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre10" value="0" id="leftmitre10">&deg;</p>
		 
		 <p><select name="leftmitre10_status" id="leftmitre10_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle10" onclick="update_angle10(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle10(form){
				var pann_qty="20";
				
				var paneltype10=$("#panle2010 :selected").text();
				var paneltype11=$("#panle2011 :selected").text();
				
				$("#panelss101").val(paneltype10);
				$("#panelss102").val(paneltype11);
				//alert(paneltype10);
				//alert(paneltype11);
}


function update_angle10(form){
	var newangle10=$("#newangle10").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee10 = 0;
var panel_no="10";


var anglee10 = newangle10;

var val_anglle=anglee10
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee10:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1110").val(""+newangle10+""+ascii(176)+"");
 $("#pan1210").val(""+newangle10+""+ascii(176)+"");
 $("#pan1310").val(""+newangle10+""+ascii(176)+"");
 $("#pan1410").val(""+newangle10+""+ascii(176)+"");
 $("#pan1510").val(""+newangle10+""+ascii(176)+"");
 $("#pan1610").val(""+newangle10+""+ascii(176)+"");
 $("#pan1710").val(""+newangle10+""+ascii(176)+"");
 $("#pan1810").val(""+newangle10+""+ascii(176)+"");
 $("#pan1910").val(""+newangle10+""+ascii(176)+"");

$("#pan1110").attr("placeholder",anglee10);	
	
	
	
}
  </script> <div class="modal fade" id="myModal11" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle11(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 11<br /><input type="text" id="panelss111" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 12<br /><input type="text" id="panelss112" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle11" id="newangle11" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection11" id="intersection11">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 11 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre11" value="0" id="rightmitre11">&deg;</p>
		 
		 <p><select name="rightmitre11_status" id="rightmitre11_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 12 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre11" value="0" id="leftmitre11">&deg;</p>
		 
		 <p><select name="leftmitre11_status" id="leftmitre11_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle11" onclick="update_angle11(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle11(form){
				var pann_qty="20";
				
				var paneltype11=$("#panle2011 :selected").text();
				var paneltype12=$("#panle2012 :selected").text();
				
				$("#panelss111").val(paneltype11);
				$("#panelss112").val(paneltype12);
				//alert(paneltype11);
				//alert(paneltype12);
}


function update_angle11(form){
	var newangle11=$("#newangle11").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee11 = 0;
var panel_no="11";


var anglee11 = newangle11;

var val_anglle=anglee11
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee11:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1211").val(""+newangle11+""+ascii(176)+"");
 $("#pan1311").val(""+newangle11+""+ascii(176)+"");
 $("#pan1411").val(""+newangle11+""+ascii(176)+"");
 $("#pan1511").val(""+newangle11+""+ascii(176)+"");
 $("#pan1611").val(""+newangle11+""+ascii(176)+"");
 $("#pan1711").val(""+newangle11+""+ascii(176)+"");
 $("#pan1811").val(""+newangle11+""+ascii(176)+"");
 $("#pan1911").val(""+newangle11+""+ascii(176)+"");

$("#pan1211").attr("placeholder",anglee11);	
	
	
	
}
  </script> <div class="modal fade" id="myModal12" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle12(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 12<br /><input type="text" id="panelss121" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 13<br /><input type="text" id="panelss122" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle12" id="newangle12" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection12" id="intersection12">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 12 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre12" value="0" id="rightmitre12">&deg;</p>
		 
		 <p><select name="rightmitre12_status" id="rightmitre12_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 13 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre12" value="0" id="leftmitre12">&deg;</p>
		 
		 <p><select name="leftmitre12_status" id="leftmitre12_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle12" onclick="update_angle12(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle12(form){
				var pann_qty="20";
				
				var paneltype12=$("#panle2012 :selected").text();
				var paneltype13=$("#panle2013 :selected").text();
				
				$("#panelss121").val(paneltype12);
				$("#panelss122").val(paneltype13);
				//alert(paneltype12);
				//alert(paneltype13);
}


function update_angle12(form){
	var newangle12=$("#newangle12").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee12 = 0;
var panel_no="12";


var anglee12 = newangle12;

var val_anglle=anglee12
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee12:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1312").val(""+newangle12+""+ascii(176)+"");
 $("#pan1412").val(""+newangle12+""+ascii(176)+"");
 $("#pan1512").val(""+newangle12+""+ascii(176)+"");
 $("#pan1612").val(""+newangle12+""+ascii(176)+"");
 $("#pan1712").val(""+newangle12+""+ascii(176)+"");
 $("#pan1812").val(""+newangle12+""+ascii(176)+"");
 $("#pan1912").val(""+newangle12+""+ascii(176)+"");

$("#pan1312").attr("placeholder",anglee12);	
	
	
	
}
  </script> <div class="modal fade" id="myModal13" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle13(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 13<br /><input type="text" id="panelss131" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 14<br /><input type="text" id="panelss132" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle13" id="newangle13" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection13" id="intersection13">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 13 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre13" value="0" id="rightmitre13">&deg;</p>
		 
		 <p><select name="rightmitre13_status" id="rightmitre13_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 14 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre13" value="0" id="leftmitre13">&deg;</p>
		 
		 <p><select name="leftmitre13_status" id="leftmitre13_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle13" onclick="update_angle13(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle13(form){
				var pann_qty="20";
				
				var paneltype13=$("#panle2013 :selected").text();
				var paneltype14=$("#panle2014 :selected").text();
				
				$("#panelss131").val(paneltype13);
				$("#panelss132").val(paneltype14);
				//alert(paneltype13);
				//alert(paneltype14);
}


function update_angle13(form){
	var newangle13=$("#newangle13").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee13 = 0;
var panel_no="13";


var anglee13 = newangle13;

var val_anglle=anglee13
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee13:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1413").val(""+newangle13+""+ascii(176)+"");
 $("#pan1513").val(""+newangle13+""+ascii(176)+"");
 $("#pan1613").val(""+newangle13+""+ascii(176)+"");
 $("#pan1713").val(""+newangle13+""+ascii(176)+"");
 $("#pan1813").val(""+newangle13+""+ascii(176)+"");
 $("#pan1913").val(""+newangle13+""+ascii(176)+"");

$("#pan1413").attr("placeholder",anglee13);	
	
	
	
}
  </script> <div class="modal fade" id="myModal14" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle14(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 14<br /><input type="text" id="panelss141" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 15<br /><input type="text" id="panelss142" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle14" id="newangle14" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection14" id="intersection14">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 14 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre14" value="0" id="rightmitre14">&deg;</p>
		 
		 <p><select name="rightmitre14_status" id="rightmitre14_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 15 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre14" value="0" id="leftmitre14">&deg;</p>
		 
		 <p><select name="leftmitre14_status" id="leftmitre14_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle14" onclick="update_angle14(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle14(form){
				var pann_qty="20";
				
				var paneltype14=$("#panle2014 :selected").text();
				var paneltype15=$("#panle2015 :selected").text();
				
				$("#panelss141").val(paneltype14);
				$("#panelss142").val(paneltype15);
				//alert(paneltype14);
				//alert(paneltype15);
}


function update_angle14(form){
	var newangle14=$("#newangle14").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee14 = 0;
var panel_no="14";


var anglee14 = newangle14;

var val_anglle=anglee14
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee14:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1514").val(""+newangle14+""+ascii(176)+"");
 $("#pan1614").val(""+newangle14+""+ascii(176)+"");
 $("#pan1714").val(""+newangle14+""+ascii(176)+"");
 $("#pan1814").val(""+newangle14+""+ascii(176)+"");
 $("#pan1914").val(""+newangle14+""+ascii(176)+"");

$("#pan1514").attr("placeholder",anglee14);	
	
	
	
}
  </script> <div class="modal fade" id="myModal15" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle15(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 15<br /><input type="text" id="panelss151" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 16<br /><input type="text" id="panelss152" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle15" id="newangle15" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection15" id="intersection15">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 15 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre15" value="0" id="rightmitre15">&deg;</p>
		 
		 <p><select name="rightmitre15_status" id="rightmitre15_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 16 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre15" value="0" id="leftmitre15">&deg;</p>
		 
		 <p><select name="leftmitre15_status" id="leftmitre15_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle15" onclick="update_angle15(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle15(form){
				var pann_qty="20";
				
				var paneltype15=$("#panle2015 :selected").text();
				var paneltype16=$("#panle2016 :selected").text();
				
				$("#panelss151").val(paneltype15);
				$("#panelss152").val(paneltype16);
				//alert(paneltype15);
				//alert(paneltype16);
}


function update_angle15(form){
	var newangle15=$("#newangle15").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee15 = 0;
var panel_no="15";


var anglee15 = newangle15;

var val_anglle=anglee15
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee15:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1615").val(""+newangle15+""+ascii(176)+"");
 $("#pan1715").val(""+newangle15+""+ascii(176)+"");
 $("#pan1815").val(""+newangle15+""+ascii(176)+"");
 $("#pan1915").val(""+newangle15+""+ascii(176)+"");

$("#pan1615").attr("placeholder",anglee15);	
	
	
	
}
  </script> <div class="modal fade" id="myModal16" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle16(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 16<br /><input type="text" id="panelss161" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 17<br /><input type="text" id="panelss162" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle16" id="newangle16" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection16" id="intersection16">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 16 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre16" value="0" id="rightmitre16">&deg;</p>
		 
		 <p><select name="rightmitre16_status" id="rightmitre16_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 17 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre16" value="0" id="leftmitre16">&deg;</p>
		 
		 <p><select name="leftmitre16_status" id="leftmitre16_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle16" onclick="update_angle16(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle16(form){
				var pann_qty="20";
				
				var paneltype16=$("#panle2016 :selected").text();
				var paneltype17=$("#panle2017 :selected").text();
				
				$("#panelss161").val(paneltype16);
				$("#panelss162").val(paneltype17);
				//alert(paneltype16);
				//alert(paneltype17);
}


function update_angle16(form){
	var newangle16=$("#newangle16").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee16 = 0;
var panel_no="16";


var anglee16 = newangle16;

var val_anglle=anglee16
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee16:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1716").val(""+newangle16+""+ascii(176)+"");
 $("#pan1816").val(""+newangle16+""+ascii(176)+"");
 $("#pan1916").val(""+newangle16+""+ascii(176)+"");

$("#pan1716").attr("placeholder",anglee16);	
	
	
	
}
  </script> <div class="modal fade" id="myModal17" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle17(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 17<br /><input type="text" id="panelss171" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 18<br /><input type="text" id="panelss172" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle17" id="newangle17" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection17" id="intersection17">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 17 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre17" value="0" id="rightmitre17">&deg;</p>
		 
		 <p><select name="rightmitre17_status" id="rightmitre17_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 18 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre17" value="0" id="leftmitre17">&deg;</p>
		 
		 <p><select name="leftmitre17_status" id="leftmitre17_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle17" onclick="update_angle17(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle17(form){
				var pann_qty="20";
				
				var paneltype17=$("#panle2017 :selected").text();
				var paneltype18=$("#panle2018 :selected").text();
				
				$("#panelss171").val(paneltype17);
				$("#panelss172").val(paneltype18);
				//alert(paneltype17);
				//alert(paneltype18);
}


function update_angle17(form){
	var newangle17=$("#newangle17").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee17 = 0;
var panel_no="17";


var anglee17 = newangle17;

var val_anglle=anglee17
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee17:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1817").val(""+newangle17+""+ascii(176)+"");
 $("#pan1917").val(""+newangle17+""+ascii(176)+"");

$("#pan1817").attr("placeholder",anglee17);	
	
	
	
}
  </script> <div class="modal fade" id="myModal18" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle18(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 18<br /><input type="text" id="panelss181" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 19<br /><input type="text" id="panelss182" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle18" id="newangle18" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection18" id="intersection18">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 18 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre18" value="0" id="rightmitre18">&deg;</p>
		 
		 <p><select name="rightmitre18_status" id="rightmitre18_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 19 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre18" value="0" id="leftmitre18">&deg;</p>
		 
		 <p><select name="leftmitre18_status" id="leftmitre18_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle18" onclick="update_angle18(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle18(form){
				var pann_qty="20";
				
				var paneltype18=$("#panle2018 :selected").text();
				var paneltype19=$("#panle2019 :selected").text();
				
				$("#panelss181").val(paneltype18);
				$("#panelss182").val(paneltype19);
				//alert(paneltype18);
				//alert(paneltype19);
}


function update_angle18(form){
	var newangle18=$("#newangle18").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee18 = 0;
var panel_no="18";


var anglee18 = newangle18;

var val_anglle=anglee18
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee18:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
  $("#pan1918").val(""+newangle18+""+ascii(176)+"");

$("#pan1918").attr("placeholder",anglee18);	
	
	
	
}
  </script> <div class="modal fade" id="myModal19" role="dialog" >
    <div class="modal-dialog">
 
      <div class="modal-content" align="center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="update_angle19(this.form)">&times;</button>
          <h6 class="modal-title">Shower Junction between</h6>
        </div>
        <div class="modal-body">
         <div class="div1st" align="center" >
		 <div class="div1st1">Panel 19<br /><input type="text" id="panelss191" value="" disabled ></div>
		 <div class="div1st2">and</div>
		 <div class="div1st3">Panel 20<br /><input type="text" id="panelss192" value="" disabled ></div>
		 </div>
		 <div class="div2nd">
		 <div class="div2nd1">Junction Angle <input type="text" name="newangle19" id="newangle19" value="180" >&deg;</div>
		 <div class="div2nd2">Intersection <select name="intersection19" id="intersection19">
		 <option>No Overlap - Mitre</option>
		 </select>
		 </div>
		 </div>
		
		 <div class="div3rd" >
		 <div class="div3rd1">
		 <p>Panel 19 </p>
		 <p>Right Mitre</p>
		 <p><input type="text" name="rightmitre19" value="0" id="rightmitre19">&deg;</p>
		 
		 <p><select name="rightmitre19_status" id="rightmitre19_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 <div class="div3rd2">
		 <h6 align="left">&nbsp; Interior<h6/>
		 <div class="panel1st"></div>
		 <div class="panel2nd"></div>
		 <br />
		 <br />
		 <br />
		 <br />
		 <br />
		 <h6 align="left">&nbsp; Exterior<h6/>
		 </div>
		 <div class="div3rd3">
		  <p>Panel 20 </p>
		 <p>Left Mitre</p>
		 <p><input type="text" name="leftmitre19" value="0" id="leftmitre19">&deg;</p>
		 
		 <p><select name="leftmitre19_status" id="leftmitre19_status">
		 <option>Calculated</option>
		 <option>Locked</option>
		 </select></p>
		 </div>
		 </div>
		 
		 <div class="div4th">
		 <input type="checkbox" >Split Dimensions At This Junction
		 </div>
		 
		 

		 
        </div>
        <div class="modal-footer">
          Enter the angle between the panels meeting at this junction <button type="button" class="btn btn-default" data-dismiss="modal" id="close_angle19" onclick="update_angle19(this.form)">Close</button>
        </div>
      </div>
      
	  

    </div>
  </div>
  
  <script>
  
  
  

function change_angle_angle19(form){
				var pann_qty="20";
				
				var paneltype19=$("#panle2019 :selected").text();
				var paneltype20=$("#panle2020 :selected").text();
				
				$("#panelss191").val(paneltype19);
				$("#panelss192").val(paneltype20);
				//alert(paneltype19);
				//alert(paneltype20);
}


function update_angle19(form){
	var newangle19=$("#newangle19").val();
	//var res = pan42.replace("180Â°", "111");
var pann_qty="20";
var job_no="29";
var anglee19 = 0;
var panel_no="19";


var anglee19 = newangle19;

var val_anglle=anglee19
var angleee=""+val_anglle+"Â°";
//90Ã‚Â°
//var res = angleee.replace(""+val_anglle+"Ã‚Â°", ""+val_anglle+"Â°");
//alert(res);

$.ajax({
      type:"post",
      url:"date_save/save_angle.php",
      data:{job_no:job_no,anglee19:val_anglle,panel_no:panel_no},
      success: function(data){
		 window.location.reload(true);
       //alert(data);
                
	}
      });
 
$("#pan2019").attr("placeholder",anglee19);	
	
	
	
}
  </script>  
  
<script>

function change_glass_thickness(form){
	var pann_qty='20';
	var job_no='29';
	var glass_thickness1= glass_thickness1= glass_thickness2= glass_thickness3= glass_thickness4= glass_thickness5= glass_thickness6="";
	 glass_thickness1 =$("#glass_thickness1").val();
	 glass_thickness2 =$("#glass_thickness2").val();
	 glass_thickness3 =$("#glass_thickness3").val();
	 glass_thickness4 =$("#glass_thickness4").val();
	 glass_thickness5 =$("#glass_thickness5").val();
	 glass_thickness6 =$("#glass_thickness6").val();
	 $.ajax({
      type:'post',
      url:"date_save/update_glass_thickness.php",
      data:{pann_qty:pann_qty,job_no:job_no,glass_thickness1:glass_thickness1,glass_thickness2:glass_thickness2,glass_thickness3:glass_thickness3,glass_thickness4:glass_thickness4,glass_thickness5:glass_thickness5,glass_thickness6:glass_thickness6},
      success: function(data){
		  //window.location.reload(true);
        //alert(data);      
       //$("#added_hardware_detail").html(data);
                
	}
      });

}


function change_glass_type(form){
	var glass_type=$('#glass_type :selected').text();
	var glass_type_id=$('#glass_type :selected').val();
	//alert(glass_type);
	//alert(glass_type_id);
	$("#gltype1 option").filter(function() {
	return this.text == glass_type; 
	}).attr('selected', true);
	
	$("#gltype2 option").filter(function() {
	return this.text == glass_type; 
	}).attr('selected', true);
	
	$("#gltype3 option").filter(function() {
	return this.text == glass_type; 
	}).attr('selected', true);
	
	$("#gltype4 option").filter(function() {
	return this.text == glass_type; 
	}).attr('selected', true);
	
	$("#gltype5 option").filter(function() {
	return this.text == glass_type; 
	}).attr('selected', true);
	
	$("#gltype6 option").filter(function() {
	return this.text == glass_type; 
	}).attr('selected', true);
	
	$("#gltype7 option").filter(function() {
	return this.text == glass_type; 
	}).attr('selected', true);
	//save_all_data();
	
	
}

function save_all_data(form){
	
	//alert(lengthE)
	//var job_id=$("#job_number_id").val();
	//alert(job_id);
	
	var lastcahr=$("#lastchar").val();
	
	
	
	//alert(lengthE);
	var panel_qty='20';
	var customer_id=2;
	var order_no=1;
	//alert(customer_id);
	//alert(order_no);
	var panel_count=$("#panel_count").val();
	var supplier_id=$('#supplier :selected').val();
	var glass_supplier=$('#supplier :selected').text();
	var glass_type_id=$('#glass_type :selected').val();
	var glass_type=$('#glass_type :selected').text();
	
	
	var job_id='29';
	//alert(job_id);
	var shower_type='InLine';
	var door_type='Double';
	var hinges_type='Right';
	var img='f319eace7f33056e2917ca28be1cba47.jpg';
	var door_glass_width=$("#door_glass_width").val();
//for textboxes
	var lengthtextbox = [];   
	$(".lengthtextbox").each(function() {
		   lengthtextbox.push($(this).val());
	 });
	//alert(lengthtextbox);

	//for select
	var panel_types = [];    
    $(".panel_types :selected").each(function(){
        panel_types.push($(this).text()); 
    });
   // alert(panel_types);
    //return false;
//for angle	
	var angles = [];   
	$(".angles").each(function() {
		   angles.push($(this).attr('placeholder'));
	 });
	//alert(angles);
	
//for outofplumbtextbox
	var outofplumbtextbox = [];   
	$(".outofplumbtextbox").each(function() {
		   outofplumbtextbox.push($(this).val());
	 });
	//alert(outofplumbtextbox);
	
//for glassthicknesss
	var glassthicknesss = [];   
	$(".glassthicknesss").each(function() {
		   glassthicknesss.push($(this).val());
	 });
	//alert(glassthicknesss);
				
	var checkzerolength = lengthtextbox.includes("0");
	if(checkzerolength==true)
	{
	alert('Dimensions Needs value!');
	}
	else{

	$.ajax({
      type:'post',
      url:"date_save/save_quotes_data.php",
      data:{customer_id:customer_id,panel_count:panel_count,supplier_id:supplier_id,glass_type_id:glass_type_id,glass_type:glass_type,glass_supplier:glass_supplier,job_id:job_id,order_no:order_no,shower_type:shower_type,door_type:door_type,hinges_type:hinges_type,img:img,panel_qty:panel_qty,door_glass_width:door_glass_width,lastcahr:lastcahr,lengthtextbox:lengthtextbox,panel_types:panel_types,angles:angles,outofplumbtextbox:outofplumbtextbox,glassthicknesss:glassthicknesss},
      success: function(data){
		  //window.location.reload(true);
        //alert(data);      
       //$("#tesssst").html(data);
                
	}
      });
}
	
}
function view_3D_image() {
	var lengthA=$("#lengthA").val();
	var lengthB=$("#lengthB").val();
	var lengthC=$("#lengthC").val();
	var lengthD=$("#lengthD").val();
	var job_id='29';
	 var x = screen.width/2 - 700/2;
    var y = screen.height/2 - 650/2;
	if(lengthA=='0'||lengthB=='0'||lengthC=='0'||lengthD=='0')
	{
		alert('please fill all length');
	}
	else{ 
	var myWindow = window.open("view_3d_image.php?img=f319eace7f33056e2917ca28be1cba47.jpg&lengthA="+lengthA+"&lengthB="+lengthB+"&lengthC="+lengthC+"&lengthD="+lengthD+"", "", "width=800,height=600,left="+x+",top="+y+",");
	}
   
	
	
}
</script>


<script>


function calculate_all_data(form){
	var panlettyp1=panlettyp2=panlettyp3=panlettyp4=panlettyp5=panlettyp6=panlettyp7=panlettyp8=panlettyp9=panlettyp10=panlettyp11=panlettyp12=panlettyp13=panlettyp14=panlettyp15=panlettyp16=panlettyp17=panlettyp18=panlettyp19=panlettyp20=panlettyp21=panlettyp22=panlettyp23=panlettyp24=panlettyp25=panlettyp26=panlettyp27=panlettyp28=panlettyp29=panlettyp30=panlettyp31=panlettyp32=panlettyp34=panlettyp35=panlettyp36=panlettyp37=panlettyp38=panlettyp39=panlettyp40='';
		
		var angle1=angle2=angle3=angle4=angle5=angle6=angle7=angle8=angle9=angle10=angle11=angle12=angle13=angle14=angle15=angle16=angle17=angle18=angle19=angle20=angle21=angle22=angle23=angle24=angle25=angle26=angle27=angle28=angle29=angle30=angle31=angle32=angle33=angle34=angle35=angle36=angle37=angle38=angle39=angle40='0';
		
		
		var shower_type='InLine';
		var panel_qty='20';
		var job_no='29';
		
		var lengthtextbox = []; 
		var outofplumbtextbox = [];
		var lastcahr=$("#lastchar").val();
		var lastcahr2=$("#lengthA").val();
		var panel_qtyss=$("#panel_qty").val();
		//alert(panel_qtyss);
		var lengthA=11;var outofplumbA=12;lengthtextbox.push(lengthA);outofplumbtextbox.push(outofplumbA);var lengthB=21;var outofplumbB=22;lengthtextbox.push(lengthB);outofplumbtextbox.push(outofplumbB);var lengthC=31;var outofplumbC=32;lengthtextbox.push(lengthC);outofplumbtextbox.push(outofplumbC);var lengthD=41;var outofplumbD=42;lengthtextbox.push(lengthD);outofplumbtextbox.push(outofplumbD);var lengthE=51;var outofplumbE=52;lengthtextbox.push(lengthE);outofplumbtextbox.push(outofplumbE);var lengthF=61;var outofplumbF=62;lengthtextbox.push(lengthF);outofplumbtextbox.push(outofplumbF);var lengthG=71;var outofplumbG=72;lengthtextbox.push(lengthG);outofplumbtextbox.push(outofplumbG);var lengthH=81;var outofplumbH=82;lengthtextbox.push(lengthH);outofplumbtextbox.push(outofplumbH);var lengthI=91;var outofplumbI=92;lengthtextbox.push(lengthI);outofplumbtextbox.push(outofplumbI);var lengthJ=101;var outofplumbJ=102;lengthtextbox.push(lengthJ);outofplumbtextbox.push(outofplumbJ);var lengthK=111;var outofplumbK=112;lengthtextbox.push(lengthK);outofplumbtextbox.push(outofplumbK);var lengthL=121;var outofplumbL=122;lengthtextbox.push(lengthL);outofplumbtextbox.push(outofplumbL);var lengthM=131;var outofplumbM=132;lengthtextbox.push(lengthM);outofplumbtextbox.push(outofplumbM);var lengthN=141;var outofplumbN=142;lengthtextbox.push(lengthN);outofplumbtextbox.push(outofplumbN);var lengthO=151;var outofplumbO=152;lengthtextbox.push(lengthO);outofplumbtextbox.push(outofplumbO);var lengthP=161;var outofplumbP=162;lengthtextbox.push(lengthP);outofplumbtextbox.push(outofplumbP);var lengthQ=171;var outofplumbQ=172;lengthtextbox.push(lengthQ);outofplumbtextbox.push(outofplumbQ);var lengthR=181;var outofplumbR=182;lengthtextbox.push(lengthR);outofplumbtextbox.push(outofplumbR);var lengthS=191;var outofplumbS=192;lengthtextbox.push(lengthS);outofplumbtextbox.push(outofplumbS);var lengthT=201;var outofplumbT=202;lengthtextbox.push(lengthT);outofplumbtextbox.push(outofplumbT);var lengthU=211;var outofplumbU=212;lengthtextbox.push(lengthU);outofplumbtextbox.push(outofplumbU);var lengthV=221;var outofplumbV=222;lengthtextbox.push(lengthV);outofplumbtextbox.push(outofplumbV);var lengthW=231;var outofplumbW=232;lengthtextbox.push(lengthW);outofplumbtextbox.push(outofplumbW);var lengthX=241;var outofplumbX=242;lengthtextbox.push(lengthX);outofplumbtextbox.push(outofplumbX);var lengthY=251;var outofplumbY=252;lengthtextbox.push(lengthY);outofplumbtextbox.push(outofplumbY);var lengthZ=261;var outofplumbZ=262;lengthtextbox.push(lengthZ);outofplumbtextbox.push(outofplumbZ);var lengthAA=271;var outofplumbAA=272;lengthtextbox.push(lengthAA);outofplumbtextbox.push(outofplumbAA);var lengthAB=281;var outofplumbAB=282;lengthtextbox.push(lengthAB);outofplumbtextbox.push(outofplumbAB);var lengthAC=0;var outofplumbAC=0;lengthtextbox.push(lengthAC);outofplumbtextbox.push(outofplumbAC);var lengthAD=0;var outofplumbAD=0;lengthtextbox.push(lengthAD);outofplumbtextbox.push(outofplumbAD);var lengthAE=0;var outofplumbAE=0;lengthtextbox.push(lengthAE);outofplumbtextbox.push(outofplumbAE);var lengthAF=0;var outofplumbAF=0;lengthtextbox.push(lengthAF);outofplumbtextbox.push(outofplumbAF);var lengthAG=0;var outofplumbAG=0;lengthtextbox.push(lengthAG);outofplumbtextbox.push(outofplumbAG);var lengthAH=0;var outofplumbAH=0;lengthtextbox.push(lengthAH);outofplumbtextbox.push(outofplumbAH);var lengthAI=0;var outofplumbAI=0;lengthtextbox.push(lengthAI);outofplumbtextbox.push(outofplumbAI);var lengthAJ=0;var outofplumbAJ=0;lengthtextbox.push(lengthAJ);outofplumbtextbox.push(outofplumbAJ);var lengthAK=0;var outofplumbAK=0;lengthtextbox.push(lengthAK);outofplumbtextbox.push(outofplumbAK);var lengthAL=0;var outofplumbAL=0;lengthtextbox.push(lengthAL);outofplumbtextbox.push(outofplumbAL);var lengthAM=0;var outofplumbAM=0;lengthtextbox.push(lengthAM);outofplumbtextbox.push(outofplumbAM);var lengthAN=0;var outofplumbAN=0;lengthtextbox.push(lengthAN);outofplumbtextbox.push(outofplumbAN);var lengthAO=0;var outofplumbAO=0;lengthtextbox.push(lengthAO);outofplumbtextbox.push(outofplumbAO);var lengthAP=0;var outofplumbAP=0;lengthtextbox.push(lengthAP);outofplumbtextbox.push(outofplumbAP);var lengthAQ=0;var outofplumbAQ=0;lengthtextbox.push(lengthAQ);outofplumbtextbox.push(outofplumbAQ);var lengthAR=0;var outofplumbAR=0;lengthtextbox.push(lengthAR);outofplumbtextbox.push(outofplumbAR);var lengthAS=0;var outofplumbAS=0;lengthtextbox.push(lengthAS);outofplumbtextbox.push(outofplumbAS);var lengthAT=0;var outofplumbAT=0;lengthtextbox.push(lengthAT);outofplumbtextbox.push(outofplumbAT);var lengthAU=0;var outofplumbAU=0;lengthtextbox.push(lengthAU);outofplumbtextbox.push(outofplumbAU);var lengthAV=0;var outofplumbAV=0;lengthtextbox.push(lengthAV);outofplumbtextbox.push(outofplumbAV);var lengthAW=0;var outofplumbAW=0;lengthtextbox.push(lengthAW);outofplumbtextbox.push(outofplumbAW);var lengthAX=0;var outofplumbAX=0;lengthtextbox.push(lengthAX);outofplumbtextbox.push(outofplumbAX);var lengthAY=0;var outofplumbAY=0;lengthtextbox.push(lengthAY);outofplumbtextbox.push(outofplumbAY);var lengthAZ=0;var outofplumbAZ=0;lengthtextbox.push(lengthAZ);outofplumbtextbox.push(outofplumbAZ);var lengthBA=0;var outofplumbBA=0;lengthtextbox.push(lengthBA);outofplumbtextbox.push(outofplumbBA);var lengthBB=0;var outofplumbBB=0;lengthtextbox.push(lengthBB);outofplumbtextbox.push(outofplumbBB);var lengthBC=0;var outofplumbBC=0;lengthtextbox.push(lengthBC);outofplumbtextbox.push(outofplumbBC);var lengthBD=0;var outofplumbBD=0;lengthtextbox.push(lengthBD);outofplumbtextbox.push(outofplumbBD);var lengthBE=0;var outofplumbBE=0;lengthtextbox.push(lengthBE);outofplumbtextbox.push(outofplumbBE);var lengthBF=0;var outofplumbBF=0;lengthtextbox.push(lengthBF);outofplumbtextbox.push(outofplumbBF);var lengthBG=0;var outofplumbBG=0;lengthtextbox.push(lengthBG);outofplumbtextbox.push(outofplumbBG);var lengthBH=0;var outofplumbBH=0;lengthtextbox.push(lengthBH);outofplumbtextbox.push(outofplumbBH);var lengthBI=0;var outofplumbBI=0;lengthtextbox.push(lengthBI);outofplumbtextbox.push(outofplumbBI);var lengthBJ=0;var outofplumbBJ=0;lengthtextbox.push(lengthBJ);outofplumbtextbox.push(outofplumbBJ);var lengthBK=0;var outofplumbBK=0;lengthtextbox.push(lengthBK);outofplumbtextbox.push(outofplumbBK);var lengthBL=0;var outofplumbBL=0;lengthtextbox.push(lengthBL);outofplumbtextbox.push(outofplumbBL);var lengthBM=0;var outofplumbBM=0;lengthtextbox.push(lengthBM);outofplumbtextbox.push(outofplumbBM);var lengthBN=0;var outofplumbBN=0;lengthtextbox.push(lengthBN);outofplumbtextbox.push(outofplumbBN);var lengthBO=0;var outofplumbBO=0;lengthtextbox.push(lengthBO);outofplumbtextbox.push(outofplumbBO);var lengthBP=0;var outofplumbBP=0;lengthtextbox.push(lengthBP);outofplumbtextbox.push(outofplumbBP);var lengthBQ=0;var outofplumbBQ=0;lengthtextbox.push(lengthBQ);outofplumbtextbox.push(outofplumbBQ);var lengthBR=0;var outofplumbBR=0;lengthtextbox.push(lengthBR);outofplumbtextbox.push(outofplumbBR);var lengthBS=0;var outofplumbBS=0;lengthtextbox.push(lengthBS);outofplumbtextbox.push(outofplumbBS);var lengthBT=0;var outofplumbBT=0;lengthtextbox.push(lengthBT);outofplumbtextbox.push(outofplumbBT);var lengthBU=0;var outofplumbBU=0;lengthtextbox.push(lengthBU);outofplumbtextbox.push(outofplumbBU);var lengthBV=0;var outofplumbBV=0;lengthtextbox.push(lengthBV);outofplumbtextbox.push(outofplumbBV);var lengthBW=0;var outofplumbBW=0;lengthtextbox.push(lengthBW);outofplumbtextbox.push(outofplumbBW);var lengthBX=0;var outofplumbBX=0;lengthtextbox.push(lengthBX);outofplumbtextbox.push(outofplumbBX);var lengthBY=0;var outofplumbBY=0;lengthtextbox.push(lengthBY);outofplumbtextbox.push(outofplumbBY);var lengthBZ=0;var outofplumbBZ=0;lengthtextbox.push(lengthBZ);outofplumbtextbox.push(outofplumbBZ);var lengthCA=0;var outofplumbCA=0;lengthtextbox.push(lengthCA);outofplumbtextbox.push(outofplumbCA);var lengthCB=0;var outofplumbCB=0;lengthtextbox.push(lengthCB);outofplumbtextbox.push(outofplumbCB);var lengthCC=0;var outofplumbCC=0;lengthtextbox.push(lengthCC);outofplumbtextbox.push(outofplumbCC);var lengthCD=0;var outofplumbCD=0;lengthtextbox.push(lengthCD);outofplumbtextbox.push(outofplumbCD);var lengthCE=0;var outofplumbCE=0;lengthtextbox.push(lengthCE);outofplumbtextbox.push(outofplumbCE);var lengthCF=0;var outofplumbCF=0;lengthtextbox.push(lengthCF);outofplumbtextbox.push(outofplumbCF);var lengthCG=0;var outofplumbCG=0;lengthtextbox.push(lengthCG);outofplumbtextbox.push(outofplumbCG);var lengthCH=0;var outofplumbCH=0;lengthtextbox.push(lengthCH);outofplumbtextbox.push(outofplumbCH);var lengthCI=0;var outofplumbCI=0;lengthtextbox.push(lengthCI);outofplumbtextbox.push(outofplumbCI);var lengthCJ=0;var outofplumbCJ=0;lengthtextbox.push(lengthCJ);outofplumbtextbox.push(outofplumbCJ);var lengthCK=0;var outofplumbCK=0;lengthtextbox.push(lengthCK);outofplumbtextbox.push(outofplumbCK);var lengthCL=0;var outofplumbCL=0;lengthtextbox.push(lengthCL);outofplumbtextbox.push(outofplumbCL);var lengthCM=0;var outofplumbCM=0;lengthtextbox.push(lengthCM);outofplumbtextbox.push(outofplumbCM);var lengthCN=0;var outofplumbCN=0;lengthtextbox.push(lengthCN);outofplumbtextbox.push(outofplumbCN);var lengthCO=0;var outofplumbCO=0;lengthtextbox.push(lengthCO);outofplumbtextbox.push(outofplumbCO);var lengthCP=0;var outofplumbCP=0;lengthtextbox.push(lengthCP);outofplumbtextbox.push(outofplumbCP);var lengthCQ=0;var outofplumbCQ=0;lengthtextbox.push(lengthCQ);outofplumbtextbox.push(outofplumbCQ);var lengthCR=0;var outofplumbCR=0;lengthtextbox.push(lengthCR);outofplumbtextbox.push(outofplumbCR);var lengthCS=0;var outofplumbCS=0;lengthtextbox.push(lengthCS);outofplumbtextbox.push(outofplumbCS);var lengthCT=0;var outofplumbCT=0;lengthtextbox.push(lengthCT);outofplumbtextbox.push(outofplumbCT);var lengthCU=0;var outofplumbCU=0;lengthtextbox.push(lengthCU);outofplumbtextbox.push(outofplumbCU);var lengthCV=0;var outofplumbCV=0;lengthtextbox.push(lengthCV);outofplumbtextbox.push(outofplumbCV);var lengthCW=0;var outofplumbCW=0;lengthtextbox.push(lengthCW);outofplumbtextbox.push(outofplumbCW);var lengthCX=0;var outofplumbCX=0;lengthtextbox.push(lengthCX);outofplumbtextbox.push(outofplumbCX);var lengthCY=0;var outofplumbCY=0;lengthtextbox.push(lengthCY);outofplumbtextbox.push(outofplumbCY);var lengthCZ=0;var outofplumbCZ=0;lengthtextbox.push(lengthCZ);outofplumbtextbox.push(outofplumbCZ);var lengthDA=0;var outofplumbDA=0;lengthtextbox.push(lengthDA);outofplumbtextbox.push(outofplumbDA);var lengthDB=0;var outofplumbDB=0;lengthtextbox.push(lengthDB);outofplumbtextbox.push(outofplumbDB);var lengthDC=0;var outofplumbDC=0;lengthtextbox.push(lengthDC);outofplumbtextbox.push(outofplumbDC);var lengthDD=0;var outofplumbDD=0;lengthtextbox.push(lengthDD);outofplumbtextbox.push(outofplumbDD);var lengthDE=0;var outofplumbDE=0;lengthtextbox.push(lengthDE);outofplumbtextbox.push(outofplumbDE);var lengthDF=0;var outofplumbDF=0;lengthtextbox.push(lengthDF);outofplumbtextbox.push(outofplumbDF);var lengthDG=0;var outofplumbDG=0;lengthtextbox.push(lengthDG);outofplumbtextbox.push(outofplumbDG);var lengthDH=0;var outofplumbDH=0;lengthtextbox.push(lengthDH);outofplumbtextbox.push(outofplumbDH);var lengthDI=0;var outofplumbDI=0;lengthtextbox.push(lengthDI);outofplumbtextbox.push(outofplumbDI);var lengthDJ=0;var outofplumbDJ=0;lengthtextbox.push(lengthDJ);outofplumbtextbox.push(outofplumbDJ);var lengthDK=0;var outofplumbDK=0;lengthtextbox.push(lengthDK);outofplumbtextbox.push(outofplumbDK);var lengthDL=0;var outofplumbDL=0;lengthtextbox.push(lengthDL);outofplumbtextbox.push(outofplumbDL);var lengthDM=0;var outofplumbDM=0;lengthtextbox.push(lengthDM);outofplumbtextbox.push(outofplumbDM);var lengthDN=0;var outofplumbDN=0;lengthtextbox.push(lengthDN);outofplumbtextbox.push(outofplumbDN);var lengthDO=0;var outofplumbDO=0;lengthtextbox.push(lengthDO);outofplumbtextbox.push(outofplumbDO);var lengthDP=0;var outofplumbDP=0;lengthtextbox.push(lengthDP);outofplumbtextbox.push(outofplumbDP);var lengthDQ=0;var outofplumbDQ=0;lengthtextbox.push(lengthDQ);outofplumbtextbox.push(outofplumbDQ);var lengthDR=0;var outofplumbDR=0;lengthtextbox.push(lengthDR);outofplumbtextbox.push(outofplumbDR);var lengthDS=0;var outofplumbDS=0;lengthtextbox.push(lengthDS);outofplumbtextbox.push(outofplumbDS);var lengthDT=0;var outofplumbDT=0;lengthtextbox.push(lengthDT);outofplumbtextbox.push(outofplumbDT);var lengthDU=0;var outofplumbDU=0;lengthtextbox.push(lengthDU);outofplumbtextbox.push(outofplumbDU);var lengthDV=0;var outofplumbDV=0;lengthtextbox.push(lengthDV);outofplumbtextbox.push(outofplumbDV);var lengthDW=0;var outofplumbDW=0;lengthtextbox.push(lengthDW);outofplumbtextbox.push(outofplumbDW);var lengthDX=0;var outofplumbDX=0;lengthtextbox.push(lengthDX);outofplumbtextbox.push(outofplumbDX);var lengthDY=0;var outofplumbDY=0;lengthtextbox.push(lengthDY);outofplumbtextbox.push(outofplumbDY);var lengthDZ=0;var outofplumbDZ=0;lengthtextbox.push(lengthDZ);outofplumbtextbox.push(outofplumbDZ);var lengthEA=0;var outofplumbEA=0;lengthtextbox.push(lengthEA);outofplumbtextbox.push(outofplumbEA);var lengthEB=0;var outofplumbEB=0;lengthtextbox.push(lengthEB);outofplumbtextbox.push(outofplumbEB);var lengthEC=0;var outofplumbEC=0;lengthtextbox.push(lengthEC);outofplumbtextbox.push(outofplumbEC);var lengthED=0;var outofplumbED=0;lengthtextbox.push(lengthED);outofplumbtextbox.push(outofplumbED);var lengthEE=0;var outofplumbEE=0;lengthtextbox.push(lengthEE);outofplumbtextbox.push(outofplumbEE);var lengthEF=0;var outofplumbEF=0;lengthtextbox.push(lengthEF);outofplumbtextbox.push(outofplumbEF);var lengthEG=0;var outofplumbEG=0;lengthtextbox.push(lengthEG);outofplumbtextbox.push(outofplumbEG);var lengthEH=0;var outofplumbEH=0;lengthtextbox.push(lengthEH);outofplumbtextbox.push(outofplumbEH);var lengthEI=0;var outofplumbEI=0;lengthtextbox.push(lengthEI);outofplumbtextbox.push(outofplumbEI);var lengthEJ=0;var outofplumbEJ=0;lengthtextbox.push(lengthEJ);outofplumbtextbox.push(outofplumbEJ);var lengthEK=0;var outofplumbEK=0;lengthtextbox.push(lengthEK);outofplumbtextbox.push(outofplumbEK);var lengthEL=0;var outofplumbEL=0;lengthtextbox.push(lengthEL);outofplumbtextbox.push(outofplumbEL);var lengthEM=0;var outofplumbEM=0;lengthtextbox.push(lengthEM);outofplumbtextbox.push(outofplumbEM);var lengthEN=0;var outofplumbEN=0;lengthtextbox.push(lengthEN);outofplumbtextbox.push(outofplumbEN);var lengthEO=0;var outofplumbEO=0;lengthtextbox.push(lengthEO);outofplumbtextbox.push(outofplumbEO);var lengthEP=0;var outofplumbEP=0;lengthtextbox.push(lengthEP);outofplumbtextbox.push(outofplumbEP);var lengthEQ=0;var outofplumbEQ=0;lengthtextbox.push(lengthEQ);outofplumbtextbox.push(outofplumbEQ);var lengthER=0;var outofplumbER=0;lengthtextbox.push(lengthER);outofplumbtextbox.push(outofplumbER);var lengthES=0;var outofplumbES=0;lengthtextbox.push(lengthES);outofplumbtextbox.push(outofplumbES);var lengthET=0;var outofplumbET=0;lengthtextbox.push(lengthET);outofplumbtextbox.push(outofplumbET);var lengthEU=0;var outofplumbEU=0;lengthtextbox.push(lengthEU);outofplumbtextbox.push(outofplumbEU);var lengthEV=0;var outofplumbEV=0;lengthtextbox.push(lengthEV);outofplumbtextbox.push(outofplumbEV);var lengthEW=0;var outofplumbEW=0;lengthtextbox.push(lengthEW);outofplumbtextbox.push(outofplumbEW);var lengthEX=0;var outofplumbEX=0;lengthtextbox.push(lengthEX);outofplumbtextbox.push(outofplumbEX);var lengthEY=0;var outofplumbEY=0;lengthtextbox.push(lengthEY);outofplumbtextbox.push(outofplumbEY);var lengthEZ=0;var outofplumbEZ=0;lengthtextbox.push(lengthEZ);outofplumbtextbox.push(outofplumbEZ);var lengthFA=0;var outofplumbFA=0;lengthtextbox.push(lengthFA);outofplumbtextbox.push(outofplumbFA);var lengthFB=0;var outofplumbFB=0;lengthtextbox.push(lengthFB);outofplumbtextbox.push(outofplumbFB);var lengthFC=0;var outofplumbFC=0;lengthtextbox.push(lengthFC);outofplumbtextbox.push(outofplumbFC);var lengthFD=0;var outofplumbFD=0;lengthtextbox.push(lengthFD);outofplumbtextbox.push(outofplumbFD);var lengthFE=0;var outofplumbFE=0;lengthtextbox.push(lengthFE);outofplumbtextbox.push(outofplumbFE);var lengthFF=0;var outofplumbFF=0;lengthtextbox.push(lengthFF);outofplumbtextbox.push(outofplumbFF);var lengthFG=0;var outofplumbFG=0;lengthtextbox.push(lengthFG);outofplumbtextbox.push(outofplumbFG);var lengthFH=0;var outofplumbFH=0;lengthtextbox.push(lengthFH);outofplumbtextbox.push(outofplumbFH);var lengthFI=0;var outofplumbFI=0;lengthtextbox.push(lengthFI);outofplumbtextbox.push(outofplumbFI);var lengthFJ=0;var outofplumbFJ=0;lengthtextbox.push(lengthFJ);outofplumbtextbox.push(outofplumbFJ);var lengthFK=0;var outofplumbFK=0;lengthtextbox.push(lengthFK);outofplumbtextbox.push(outofplumbFK);var lengthFL=0;var outofplumbFL=0;lengthtextbox.push(lengthFL);outofplumbtextbox.push(outofplumbFL);var lengthFM=0;var outofplumbFM=0;lengthtextbox.push(lengthFM);outofplumbtextbox.push(outofplumbFM);var lengthFN=0;var outofplumbFN=0;lengthtextbox.push(lengthFN);outofplumbtextbox.push(outofplumbFN);var lengthFO=0;var outofplumbFO=0;lengthtextbox.push(lengthFO);outofplumbtextbox.push(outofplumbFO);var lengthFP=0;var outofplumbFP=0;lengthtextbox.push(lengthFP);outofplumbtextbox.push(outofplumbFP);var lengthFQ=0;var outofplumbFQ=0;lengthtextbox.push(lengthFQ);outofplumbtextbox.push(outofplumbFQ);var lengthFR=0;var outofplumbFR=0;lengthtextbox.push(lengthFR);outofplumbtextbox.push(outofplumbFR);var lengthFS=0;var outofplumbFS=0;lengthtextbox.push(lengthFS);outofplumbtextbox.push(outofplumbFS);var lengthFT=0;var outofplumbFT=0;lengthtextbox.push(lengthFT);outofplumbtextbox.push(outofplumbFT);var lengthFU=0;var outofplumbFU=0;lengthtextbox.push(lengthFU);outofplumbtextbox.push(outofplumbFU);var lengthFV=0;var outofplumbFV=0;lengthtextbox.push(lengthFV);outofplumbtextbox.push(outofplumbFV);var lengthFW=0;var outofplumbFW=0;lengthtextbox.push(lengthFW);outofplumbtextbox.push(outofplumbFW);var lengthFX=0;var outofplumbFX=0;lengthtextbox.push(lengthFX);outofplumbtextbox.push(outofplumbFX);var lengthFY=0;var outofplumbFY=0;lengthtextbox.push(lengthFY);outofplumbtextbox.push(outofplumbFY);var lengthFZ=0;var outofplumbFZ=0;lengthtextbox.push(lengthFZ);outofplumbtextbox.push(outofplumbFZ);var lengthGA=0;var outofplumbGA=0;lengthtextbox.push(lengthGA);outofplumbtextbox.push(outofplumbGA);var lengthGB=0;var outofplumbGB=0;lengthtextbox.push(lengthGB);outofplumbtextbox.push(outofplumbGB);var lengthGC=0;var outofplumbGC=0;lengthtextbox.push(lengthGC);outofplumbtextbox.push(outofplumbGC);var lengthGD=0;var outofplumbGD=0;lengthtextbox.push(lengthGD);outofplumbtextbox.push(outofplumbGD);var lengthGE=0;var outofplumbGE=0;lengthtextbox.push(lengthGE);outofplumbtextbox.push(outofplumbGE);var lengthGF=0;var outofplumbGF=0;lengthtextbox.push(lengthGF);outofplumbtextbox.push(outofplumbGF);var lengthGG=0;var outofplumbGG=0;lengthtextbox.push(lengthGG);outofplumbtextbox.push(outofplumbGG);var lengthGH=0;var outofplumbGH=0;lengthtextbox.push(lengthGH);outofplumbtextbox.push(outofplumbGH);var lengthGI=0;var outofplumbGI=0;lengthtextbox.push(lengthGI);outofplumbtextbox.push(outofplumbGI);var lengthGJ=0;var outofplumbGJ=0;lengthtextbox.push(lengthGJ);outofplumbtextbox.push(outofplumbGJ);var lengthGK=0;var outofplumbGK=0;lengthtextbox.push(lengthGK);outofplumbtextbox.push(outofplumbGK);var lengthGL=0;var outofplumbGL=0;lengthtextbox.push(lengthGL);outofplumbtextbox.push(outofplumbGL);var lengthGM=0;var outofplumbGM=0;lengthtextbox.push(lengthGM);outofplumbtextbox.push(outofplumbGM);var lengthGN=0;var outofplumbGN=0;lengthtextbox.push(lengthGN);outofplumbtextbox.push(outofplumbGN);var lengthGO=0;var outofplumbGO=0;lengthtextbox.push(lengthGO);outofplumbtextbox.push(outofplumbGO);var lengthGP=0;var outofplumbGP=0;lengthtextbox.push(lengthGP);outofplumbtextbox.push(outofplumbGP);var lengthGQ=0;var outofplumbGQ=0;lengthtextbox.push(lengthGQ);outofplumbtextbox.push(outofplumbGQ);var lengthGR=0;var outofplumbGR=0;lengthtextbox.push(lengthGR);outofplumbtextbox.push(outofplumbGR);var lengthGS=0;var outofplumbGS=0;lengthtextbox.push(lengthGS);outofplumbtextbox.push(outofplumbGS);var lengthGT=0;var outofplumbGT=0;lengthtextbox.push(lengthGT);outofplumbtextbox.push(outofplumbGT);var lengthGU=0;var outofplumbGU=0;lengthtextbox.push(lengthGU);outofplumbtextbox.push(outofplumbGU);var lengthGV=0;var outofplumbGV=0;lengthtextbox.push(lengthGV);outofplumbtextbox.push(outofplumbGV);var lengthGW=0;var outofplumbGW=0;lengthtextbox.push(lengthGW);outofplumbtextbox.push(outofplumbGW);var lengthGX=0;var outofplumbGX=0;lengthtextbox.push(lengthGX);outofplumbtextbox.push(outofplumbGX);var lengthGY=0;var outofplumbGY=0;lengthtextbox.push(lengthGY);outofplumbtextbox.push(outofplumbGY);var lengthGZ=0;var outofplumbGZ=0;lengthtextbox.push(lengthGZ);outofplumbtextbox.push(outofplumbGZ);var lengthHA=0;var outofplumbHA=0;lengthtextbox.push(lengthHA);outofplumbtextbox.push(outofplumbHA);var lengthHB=0;var outofplumbHB=0;lengthtextbox.push(lengthHB);outofplumbtextbox.push(outofplumbHB);var lengthHC=0;var outofplumbHC=0;lengthtextbox.push(lengthHC);outofplumbtextbox.push(outofplumbHC);var lengthHD=0;var outofplumbHD=0;lengthtextbox.push(lengthHD);outofplumbtextbox.push(outofplumbHD);var lengthHE=0;var outofplumbHE=0;lengthtextbox.push(lengthHE);outofplumbtextbox.push(outofplumbHE);var lengthHF=0;var outofplumbHF=0;lengthtextbox.push(lengthHF);outofplumbtextbox.push(outofplumbHF);var lengthHG=0;var outofplumbHG=0;lengthtextbox.push(lengthHG);outofplumbtextbox.push(outofplumbHG);var lengthHH=0;var outofplumbHH=0;lengthtextbox.push(lengthHH);outofplumbtextbox.push(outofplumbHH);var lengthHI=0;var outofplumbHI=0;lengthtextbox.push(lengthHI);outofplumbtextbox.push(outofplumbHI);var lengthHJ=0;var outofplumbHJ=0;lengthtextbox.push(lengthHJ);outofplumbtextbox.push(outofplumbHJ);var lengthHK=0;var outofplumbHK=0;lengthtextbox.push(lengthHK);outofplumbtextbox.push(outofplumbHK);var lengthHL=0;var outofplumbHL=0;lengthtextbox.push(lengthHL);outofplumbtextbox.push(outofplumbHL);var lengthHM=0;var outofplumbHM=0;lengthtextbox.push(lengthHM);outofplumbtextbox.push(outofplumbHM);var lengthHN=0;var outofplumbHN=0;lengthtextbox.push(lengthHN);outofplumbtextbox.push(outofplumbHN);var lengthHO=0;var outofplumbHO=0;lengthtextbox.push(lengthHO);outofplumbtextbox.push(outofplumbHO);var lengthHP=0;var outofplumbHP=0;lengthtextbox.push(lengthHP);outofplumbtextbox.push(outofplumbHP);var lengthHQ=0;var outofplumbHQ=0;lengthtextbox.push(lengthHQ);outofplumbtextbox.push(outofplumbHQ);var lengthHR=0;var outofplumbHR=0;lengthtextbox.push(lengthHR);outofplumbtextbox.push(outofplumbHR);var lengthHS=0;var outofplumbHS=0;lengthtextbox.push(lengthHS);outofplumbtextbox.push(outofplumbHS);var lengthHT=0;var outofplumbHT=0;lengthtextbox.push(lengthHT);outofplumbtextbox.push(outofplumbHT);var lengthHU=0;var outofplumbHU=0;lengthtextbox.push(lengthHU);outofplumbtextbox.push(outofplumbHU);var lengthHV=0;var outofplumbHV=0;lengthtextbox.push(lengthHV);outofplumbtextbox.push(outofplumbHV);var lengthHW=0;var outofplumbHW=0;lengthtextbox.push(lengthHW);outofplumbtextbox.push(outofplumbHW);var lengthHX=0;var outofplumbHX=0;lengthtextbox.push(lengthHX);outofplumbtextbox.push(outofplumbHX);var lengthHY=0;var outofplumbHY=0;lengthtextbox.push(lengthHY);outofplumbtextbox.push(outofplumbHY);var lengthHZ=0;var outofplumbHZ=0;lengthtextbox.push(lengthHZ);outofplumbtextbox.push(outofplumbHZ);var lengthIA=0;var outofplumbIA=0;lengthtextbox.push(lengthIA);outofplumbtextbox.push(outofplumbIA);var lengthIB=0;var outofplumbIB=0;lengthtextbox.push(lengthIB);outofplumbtextbox.push(outofplumbIB);var lengthIC=0;var outofplumbIC=0;lengthtextbox.push(lengthIC);outofplumbtextbox.push(outofplumbIC);var lengthID=0;var outofplumbID=0;lengthtextbox.push(lengthID);outofplumbtextbox.push(outofplumbID);var lengthIE=0;var outofplumbIE=0;lengthtextbox.push(lengthIE);outofplumbtextbox.push(outofplumbIE);var lengthIF=0;var outofplumbIF=0;lengthtextbox.push(lengthIF);outofplumbtextbox.push(outofplumbIF);var lengthIG=0;var outofplumbIG=0;lengthtextbox.push(lengthIG);outofplumbtextbox.push(outofplumbIG);var lengthIH=0;var outofplumbIH=0;lengthtextbox.push(lengthIH);outofplumbtextbox.push(outofplumbIH);var lengthII=0;var outofplumbII=0;lengthtextbox.push(lengthII);outofplumbtextbox.push(outofplumbII);var lengthIJ=0;var outofplumbIJ=0;lengthtextbox.push(lengthIJ);outofplumbtextbox.push(outofplumbIJ);var lengthIK=0;var outofplumbIK=0;lengthtextbox.push(lengthIK);outofplumbtextbox.push(outofplumbIK);var lengthIL=0;var outofplumbIL=0;lengthtextbox.push(lengthIL);outofplumbtextbox.push(outofplumbIL);var lengthIM=0;var outofplumbIM=0;lengthtextbox.push(lengthIM);outofplumbtextbox.push(outofplumbIM);var lengthIN=0;var outofplumbIN=0;lengthtextbox.push(lengthIN);outofplumbtextbox.push(outofplumbIN);var lengthIO=0;var outofplumbIO=0;lengthtextbox.push(lengthIO);outofplumbtextbox.push(outofplumbIO);var lengthIP=0;var outofplumbIP=0;lengthtextbox.push(lengthIP);outofplumbtextbox.push(outofplumbIP);var lengthIQ=0;var outofplumbIQ=0;lengthtextbox.push(lengthIQ);outofplumbtextbox.push(outofplumbIQ);var lengthIR=0;var outofplumbIR=0;lengthtextbox.push(lengthIR);outofplumbtextbox.push(outofplumbIR);var lengthIS=0;var outofplumbIS=0;lengthtextbox.push(lengthIS);outofplumbtextbox.push(outofplumbIS);var lengthIT=0;var outofplumbIT=0;lengthtextbox.push(lengthIT);outofplumbtextbox.push(outofplumbIT);var lengthIU=0;var outofplumbIU=0;lengthtextbox.push(lengthIU);outofplumbtextbox.push(outofplumbIU);var lengthIV=0;var outofplumbIV=0;lengthtextbox.push(lengthIV);outofplumbtextbox.push(outofplumbIV);var lengthIW=0;var outofplumbIW=0;lengthtextbox.push(lengthIW);outofplumbtextbox.push(outofplumbIW);var lengthIX=0;var outofplumbIX=0;lengthtextbox.push(lengthIX);outofplumbtextbox.push(outofplumbIX);var lengthIY=0;var outofplumbIY=0;lengthtextbox.push(lengthIY);outofplumbtextbox.push(outofplumbIY);var lengthIZ=0;var outofplumbIZ=0;lengthtextbox.push(lengthIZ);outofplumbtextbox.push(outofplumbIZ);var lengthJA=0;var outofplumbJA=0;lengthtextbox.push(lengthJA);outofplumbtextbox.push(outofplumbJA);var lengthJB=0;var outofplumbJB=0;lengthtextbox.push(lengthJB);outofplumbtextbox.push(outofplumbJB);var lengthJC=0;var outofplumbJC=0;lengthtextbox.push(lengthJC);outofplumbtextbox.push(outofplumbJC);var lengthJD=0;var outofplumbJD=0;lengthtextbox.push(lengthJD);outofplumbtextbox.push(outofplumbJD);var lengthJE=0;var outofplumbJE=0;lengthtextbox.push(lengthJE);outofplumbtextbox.push(outofplumbJE);var lengthJF=0;var outofplumbJF=0;lengthtextbox.push(lengthJF);outofplumbtextbox.push(outofplumbJF);var lengthJG=0;var outofplumbJG=0;lengthtextbox.push(lengthJG);outofplumbtextbox.push(outofplumbJG);var lengthJH=0;var outofplumbJH=0;lengthtextbox.push(lengthJH);outofplumbtextbox.push(outofplumbJH);var lengthJI=0;var outofplumbJI=0;lengthtextbox.push(lengthJI);outofplumbtextbox.push(outofplumbJI);var lengthJJ=0;var outofplumbJJ=0;lengthtextbox.push(lengthJJ);outofplumbtextbox.push(outofplumbJJ);var lengthJK=0;var outofplumbJK=0;lengthtextbox.push(lengthJK);outofplumbtextbox.push(outofplumbJK);var lengthJL=0;var outofplumbJL=0;lengthtextbox.push(lengthJL);outofplumbtextbox.push(outofplumbJL);var lengthJM=0;var outofplumbJM=0;lengthtextbox.push(lengthJM);outofplumbtextbox.push(outofplumbJM);var lengthJN=0;var outofplumbJN=0;lengthtextbox.push(lengthJN);outofplumbtextbox.push(outofplumbJN);var lengthJO=0;var outofplumbJO=0;lengthtextbox.push(lengthJO);outofplumbtextbox.push(outofplumbJO);var lengthJP=0;var outofplumbJP=0;lengthtextbox.push(lengthJP);outofplumbtextbox.push(outofplumbJP);var lengthJQ=0;var outofplumbJQ=0;lengthtextbox.push(lengthJQ);outofplumbtextbox.push(outofplumbJQ);var lengthJR=0;var outofplumbJR=0;lengthtextbox.push(lengthJR);outofplumbtextbox.push(outofplumbJR);var lengthJS=0;var outofplumbJS=0;lengthtextbox.push(lengthJS);outofplumbtextbox.push(outofplumbJS);var lengthJT=0;var outofplumbJT=0;lengthtextbox.push(lengthJT);outofplumbtextbox.push(outofplumbJT);var lengthJU=0;var outofplumbJU=0;lengthtextbox.push(lengthJU);outofplumbtextbox.push(outofplumbJU);var lengthJV=0;var outofplumbJV=0;lengthtextbox.push(lengthJV);outofplumbtextbox.push(outofplumbJV);var lengthJW=0;var outofplumbJW=0;lengthtextbox.push(lengthJW);outofplumbtextbox.push(outofplumbJW);var lengthJX=0;var outofplumbJX=0;lengthtextbox.push(lengthJX);outofplumbtextbox.push(outofplumbJX);var lengthJY=0;var outofplumbJY=0;lengthtextbox.push(lengthJY);outofplumbtextbox.push(outofplumbJY);var lengthJZ=0;var outofplumbJZ=0;lengthtextbox.push(lengthJZ);outofplumbtextbox.push(outofplumbJZ);var lengthKA=0;var outofplumbKA=0;lengthtextbox.push(lengthKA);outofplumbtextbox.push(outofplumbKA);var lengthKB=0;var outofplumbKB=0;lengthtextbox.push(lengthKB);outofplumbtextbox.push(outofplumbKB);var lengthKC=0;var outofplumbKC=0;lengthtextbox.push(lengthKC);outofplumbtextbox.push(outofplumbKC);var lengthKD=0;var outofplumbKD=0;lengthtextbox.push(lengthKD);outofplumbtextbox.push(outofplumbKD);var lengthKE=0;var outofplumbKE=0;lengthtextbox.push(lengthKE);outofplumbtextbox.push(outofplumbKE);var lengthKF=0;var outofplumbKF=0;lengthtextbox.push(lengthKF);outofplumbtextbox.push(outofplumbKF);var lengthKG=0;var outofplumbKG=0;lengthtextbox.push(lengthKG);outofplumbtextbox.push(outofplumbKG);var lengthKH=0;var outofplumbKH=0;lengthtextbox.push(lengthKH);outofplumbtextbox.push(outofplumbKH);var lengthKI=0;var outofplumbKI=0;lengthtextbox.push(lengthKI);outofplumbtextbox.push(outofplumbKI);var lengthKJ=0;var outofplumbKJ=0;lengthtextbox.push(lengthKJ);outofplumbtextbox.push(outofplumbKJ);var lengthKK=0;var outofplumbKK=0;lengthtextbox.push(lengthKK);outofplumbtextbox.push(outofplumbKK);var lengthKL=0;var outofplumbKL=0;lengthtextbox.push(lengthKL);outofplumbtextbox.push(outofplumbKL);var lengthKM=0;var outofplumbKM=0;lengthtextbox.push(lengthKM);outofplumbtextbox.push(outofplumbKM);var lengthKN=0;var outofplumbKN=0;lengthtextbox.push(lengthKN);outofplumbtextbox.push(outofplumbKN);var lengthKO=0;var outofplumbKO=0;lengthtextbox.push(lengthKO);outofplumbtextbox.push(outofplumbKO);var lengthKP=0;var outofplumbKP=0;lengthtextbox.push(lengthKP);outofplumbtextbox.push(outofplumbKP);var lengthKQ=0;var outofplumbKQ=0;lengthtextbox.push(lengthKQ);outofplumbtextbox.push(outofplumbKQ);var lengthKR=0;var outofplumbKR=0;lengthtextbox.push(lengthKR);outofplumbtextbox.push(outofplumbKR);var lengthKS=0;var outofplumbKS=0;lengthtextbox.push(lengthKS);outofplumbtextbox.push(outofplumbKS);var lengthKT=0;var outofplumbKT=0;lengthtextbox.push(lengthKT);outofplumbtextbox.push(outofplumbKT);var lengthKU=0;var outofplumbKU=0;lengthtextbox.push(lengthKU);outofplumbtextbox.push(outofplumbKU);var lengthKV=0;var outofplumbKV=0;lengthtextbox.push(lengthKV);outofplumbtextbox.push(outofplumbKV);var lengthKW=0;var outofplumbKW=0;lengthtextbox.push(lengthKW);outofplumbtextbox.push(outofplumbKW);var lengthKX=0;var outofplumbKX=0;lengthtextbox.push(lengthKX);outofplumbtextbox.push(outofplumbKX);var lengthKY=0;var outofplumbKY=0;lengthtextbox.push(lengthKY);outofplumbtextbox.push(outofplumbKY);var lengthKZ=0;var outofplumbKZ=0;lengthtextbox.push(lengthKZ);outofplumbtextbox.push(outofplumbKZ);var lengthLA=0;var outofplumbLA=0;lengthtextbox.push(lengthLA);outofplumbtextbox.push(outofplumbLA);var lengthLB=0;var outofplumbLB=0;lengthtextbox.push(lengthLB);outofplumbtextbox.push(outofplumbLB);var lengthLC=0;var outofplumbLC=0;lengthtextbox.push(lengthLC);outofplumbtextbox.push(outofplumbLC);var lengthLD=0;var outofplumbLD=0;lengthtextbox.push(lengthLD);outofplumbtextbox.push(outofplumbLD);var lengthLE=0;var outofplumbLE=0;lengthtextbox.push(lengthLE);outofplumbtextbox.push(outofplumbLE);var lengthLF=0;var outofplumbLF=0;lengthtextbox.push(lengthLF);outofplumbtextbox.push(outofplumbLF);var lengthLG=0;var outofplumbLG=0;lengthtextbox.push(lengthLG);outofplumbtextbox.push(outofplumbLG);var lengthLH=0;var outofplumbLH=0;lengthtextbox.push(lengthLH);outofplumbtextbox.push(outofplumbLH);var lengthLI=0;var outofplumbLI=0;lengthtextbox.push(lengthLI);outofplumbtextbox.push(outofplumbLI);var lengthLJ=0;var outofplumbLJ=0;lengthtextbox.push(lengthLJ);outofplumbtextbox.push(outofplumbLJ);var lengthLK=0;var outofplumbLK=0;lengthtextbox.push(lengthLK);outofplumbtextbox.push(outofplumbLK);var lengthLL=0;var outofplumbLL=0;lengthtextbox.push(lengthLL);outofplumbtextbox.push(outofplumbLL);var lengthLM=0;var outofplumbLM=0;lengthtextbox.push(lengthLM);outofplumbtextbox.push(outofplumbLM);var lengthLN=0;var outofplumbLN=0;lengthtextbox.push(lengthLN);outofplumbtextbox.push(outofplumbLN);var lengthLO=0;var outofplumbLO=0;lengthtextbox.push(lengthLO);outofplumbtextbox.push(outofplumbLO);var lengthLP=0;var outofplumbLP=0;lengthtextbox.push(lengthLP);outofplumbtextbox.push(outofplumbLP);var lengthLQ=0;var outofplumbLQ=0;lengthtextbox.push(lengthLQ);outofplumbtextbox.push(outofplumbLQ);var lengthLR=0;var outofplumbLR=0;lengthtextbox.push(lengthLR);outofplumbtextbox.push(outofplumbLR);var lengthLS=0;var outofplumbLS=0;lengthtextbox.push(lengthLS);outofplumbtextbox.push(outofplumbLS);var lengthLT=0;var outofplumbLT=0;lengthtextbox.push(lengthLT);outofplumbtextbox.push(outofplumbLT);var lengthLU=0;var outofplumbLU=0;lengthtextbox.push(lengthLU);outofplumbtextbox.push(outofplumbLU);var lengthLV=0;var outofplumbLV=0;lengthtextbox.push(lengthLV);outofplumbtextbox.push(outofplumbLV);var lengthLW=0;var outofplumbLW=0;lengthtextbox.push(lengthLW);outofplumbtextbox.push(outofplumbLW);var lengthLX=0;var outofplumbLX=0;lengthtextbox.push(lengthLX);outofplumbtextbox.push(outofplumbLX);var lengthLY=0;var outofplumbLY=0;lengthtextbox.push(lengthLY);outofplumbtextbox.push(outofplumbLY);var lengthLZ=0;var outofplumbLZ=0;lengthtextbox.push(lengthLZ);outofplumbtextbox.push(outofplumbLZ);var lengthMA=0;var outofplumbMA=0;lengthtextbox.push(lengthMA);outofplumbtextbox.push(outofplumbMA);var lengthMB=0;var outofplumbMB=0;lengthtextbox.push(lengthMB);outofplumbtextbox.push(outofplumbMB);var lengthMC=0;var outofplumbMC=0;lengthtextbox.push(lengthMC);outofplumbtextbox.push(outofplumbMC);var lengthMD=0;var outofplumbMD=0;lengthtextbox.push(lengthMD);outofplumbtextbox.push(outofplumbMD);var lengthME=0;var outofplumbME=0;lengthtextbox.push(lengthME);outofplumbtextbox.push(outofplumbME);var lengthMF=0;var outofplumbMF=0;lengthtextbox.push(lengthMF);outofplumbtextbox.push(outofplumbMF);var lengthMG=0;var outofplumbMG=0;lengthtextbox.push(lengthMG);outofplumbtextbox.push(outofplumbMG);var lengthMH=0;var outofplumbMH=0;lengthtextbox.push(lengthMH);outofplumbtextbox.push(outofplumbMH);var lengthMI=0;var outofplumbMI=0;lengthtextbox.push(lengthMI);outofplumbtextbox.push(outofplumbMI);var lengthMJ=0;var outofplumbMJ=0;lengthtextbox.push(lengthMJ);outofplumbtextbox.push(outofplumbMJ);var lengthMK=0;var outofplumbMK=0;lengthtextbox.push(lengthMK);outofplumbtextbox.push(outofplumbMK);var lengthML=0;var outofplumbML=0;lengthtextbox.push(lengthML);outofplumbtextbox.push(outofplumbML);var lengthMM=0;var outofplumbMM=0;lengthtextbox.push(lengthMM);outofplumbtextbox.push(outofplumbMM);var lengthMN=0;var outofplumbMN=0;lengthtextbox.push(lengthMN);outofplumbtextbox.push(outofplumbMN);var lengthMO=0;var outofplumbMO=0;lengthtextbox.push(lengthMO);outofplumbtextbox.push(outofplumbMO);var lengthMP=0;var outofplumbMP=0;lengthtextbox.push(lengthMP);outofplumbtextbox.push(outofplumbMP);var lengthMQ=0;var outofplumbMQ=0;lengthtextbox.push(lengthMQ);outofplumbtextbox.push(outofplumbMQ);var lengthMR=0;var outofplumbMR=0;lengthtextbox.push(lengthMR);outofplumbtextbox.push(outofplumbMR);var lengthMS=0;var outofplumbMS=0;lengthtextbox.push(lengthMS);outofplumbtextbox.push(outofplumbMS);var lengthMT=0;var outofplumbMT=0;lengthtextbox.push(lengthMT);outofplumbtextbox.push(outofplumbMT);var lengthMU=0;var outofplumbMU=0;lengthtextbox.push(lengthMU);outofplumbtextbox.push(outofplumbMU);var lengthMV=0;var outofplumbMV=0;lengthtextbox.push(lengthMV);outofplumbtextbox.push(outofplumbMV);var lengthMW=0;var outofplumbMW=0;lengthtextbox.push(lengthMW);outofplumbtextbox.push(outofplumbMW);var lengthMX=0;var outofplumbMX=0;lengthtextbox.push(lengthMX);outofplumbtextbox.push(outofplumbMX);var lengthMY=0;var outofplumbMY=0;lengthtextbox.push(lengthMY);outofplumbtextbox.push(outofplumbMY);var lengthMZ=0;var outofplumbMZ=0;lengthtextbox.push(lengthMZ);outofplumbtextbox.push(outofplumbMZ);var lengthNA=0;var outofplumbNA=0;lengthtextbox.push(lengthNA);outofplumbtextbox.push(outofplumbNA);var lengthNB=0;var outofplumbNB=0;lengthtextbox.push(lengthNB);outofplumbtextbox.push(outofplumbNB);var lengthNC=0;var outofplumbNC=0;lengthtextbox.push(lengthNC);outofplumbtextbox.push(outofplumbNC);var lengthND=0;var outofplumbND=0;lengthtextbox.push(lengthND);outofplumbtextbox.push(outofplumbND);var lengthNE=0;var outofplumbNE=0;lengthtextbox.push(lengthNE);outofplumbtextbox.push(outofplumbNE);var lengthNF=0;var outofplumbNF=0;lengthtextbox.push(lengthNF);outofplumbtextbox.push(outofplumbNF);var lengthNG=0;var outofplumbNG=0;lengthtextbox.push(lengthNG);outofplumbtextbox.push(outofplumbNG);var lengthNH=0;var outofplumbNH=0;lengthtextbox.push(lengthNH);outofplumbtextbox.push(outofplumbNH);var lengthNI=0;var outofplumbNI=0;lengthtextbox.push(lengthNI);outofplumbtextbox.push(outofplumbNI);var lengthNJ=0;var outofplumbNJ=0;lengthtextbox.push(lengthNJ);outofplumbtextbox.push(outofplumbNJ);var lengthNK=0;var outofplumbNK=0;lengthtextbox.push(lengthNK);outofplumbtextbox.push(outofplumbNK);var lengthNL=0;var outofplumbNL=0;lengthtextbox.push(lengthNL);outofplumbtextbox.push(outofplumbNL);var lengthNM=0;var outofplumbNM=0;lengthtextbox.push(lengthNM);outofplumbtextbox.push(outofplumbNM);var lengthNN=0;var outofplumbNN=0;lengthtextbox.push(lengthNN);outofplumbtextbox.push(outofplumbNN);var lengthNO=0;var outofplumbNO=0;lengthtextbox.push(lengthNO);outofplumbtextbox.push(outofplumbNO);var lengthNP=0;var outofplumbNP=0;lengthtextbox.push(lengthNP);outofplumbtextbox.push(outofplumbNP);var lengthNQ=0;var outofplumbNQ=0;lengthtextbox.push(lengthNQ);outofplumbtextbox.push(outofplumbNQ);var lengthNR=0;var outofplumbNR=0;lengthtextbox.push(lengthNR);outofplumbtextbox.push(outofplumbNR);var lengthNS=0;var outofplumbNS=0;lengthtextbox.push(lengthNS);outofplumbtextbox.push(outofplumbNS);var lengthNT=0;var outofplumbNT=0;lengthtextbox.push(lengthNT);outofplumbtextbox.push(outofplumbNT);var lengthNU=0;var outofplumbNU=0;lengthtextbox.push(lengthNU);outofplumbtextbox.push(outofplumbNU);var lengthNV=0;var outofplumbNV=0;lengthtextbox.push(lengthNV);outofplumbtextbox.push(outofplumbNV);var lengthNW=0;var outofplumbNW=0;lengthtextbox.push(lengthNW);outofplumbtextbox.push(outofplumbNW);var lengthNX=0;var outofplumbNX=0;lengthtextbox.push(lengthNX);outofplumbtextbox.push(outofplumbNX);var lengthNY=0;var outofplumbNY=0;lengthtextbox.push(lengthNY);outofplumbtextbox.push(outofplumbNY);var lengthNZ=0;var outofplumbNZ=0;lengthtextbox.push(lengthNZ);outofplumbtextbox.push(outofplumbNZ);var lengthOA=0;var outofplumbOA=0;lengthtextbox.push(lengthOA);outofplumbtextbox.push(outofplumbOA);var lengthOB=0;var outofplumbOB=0;lengthtextbox.push(lengthOB);outofplumbtextbox.push(outofplumbOB);var lengthOC=0;var outofplumbOC=0;lengthtextbox.push(lengthOC);outofplumbtextbox.push(outofplumbOC);var lengthOD=0;var outofplumbOD=0;lengthtextbox.push(lengthOD);outofplumbtextbox.push(outofplumbOD);var lengthOE=0;var outofplumbOE=0;lengthtextbox.push(lengthOE);outofplumbtextbox.push(outofplumbOE);var lengthOF=0;var outofplumbOF=0;lengthtextbox.push(lengthOF);outofplumbtextbox.push(outofplumbOF);var lengthOG=0;var outofplumbOG=0;lengthtextbox.push(lengthOG);outofplumbtextbox.push(outofplumbOG);var lengthOH=0;var outofplumbOH=0;lengthtextbox.push(lengthOH);outofplumbtextbox.push(outofplumbOH);var lengthOI=0;var outofplumbOI=0;lengthtextbox.push(lengthOI);outofplumbtextbox.push(outofplumbOI);var lengthOJ=0;var outofplumbOJ=0;lengthtextbox.push(lengthOJ);outofplumbtextbox.push(outofplumbOJ);var lengthOK=0;var outofplumbOK=0;lengthtextbox.push(lengthOK);outofplumbtextbox.push(outofplumbOK);var lengthOL=0;var outofplumbOL=0;lengthtextbox.push(lengthOL);outofplumbtextbox.push(outofplumbOL);var lengthOM=0;var outofplumbOM=0;lengthtextbox.push(lengthOM);outofplumbtextbox.push(outofplumbOM);var lengthON=0;var outofplumbON=0;lengthtextbox.push(lengthON);outofplumbtextbox.push(outofplumbON);var lengthOO=0;var outofplumbOO=0;lengthtextbox.push(lengthOO);outofplumbtextbox.push(outofplumbOO);var lengthOP=0;var outofplumbOP=0;lengthtextbox.push(lengthOP);outofplumbtextbox.push(outofplumbOP);var lengthOQ=0;var outofplumbOQ=0;lengthtextbox.push(lengthOQ);outofplumbtextbox.push(outofplumbOQ);var lengthOR=0;var outofplumbOR=0;lengthtextbox.push(lengthOR);outofplumbtextbox.push(outofplumbOR);var lengthOS=0;var outofplumbOS=0;lengthtextbox.push(lengthOS);outofplumbtextbox.push(outofplumbOS);var lengthOT=0;var outofplumbOT=0;lengthtextbox.push(lengthOT);outofplumbtextbox.push(outofplumbOT);var lengthOU=0;var outofplumbOU=0;lengthtextbox.push(lengthOU);outofplumbtextbox.push(outofplumbOU);var lengthOV=0;var outofplumbOV=0;lengthtextbox.push(lengthOV);outofplumbtextbox.push(outofplumbOV);var lengthOW=0;var outofplumbOW=0;lengthtextbox.push(lengthOW);outofplumbtextbox.push(outofplumbOW);var lengthOX=0;var outofplumbOX=0;lengthtextbox.push(lengthOX);outofplumbtextbox.push(outofplumbOX);var lengthOY=0;var outofplumbOY=0;lengthtextbox.push(lengthOY);outofplumbtextbox.push(outofplumbOY);var lengthOZ=0;var outofplumbOZ=0;lengthtextbox.push(lengthOZ);outofplumbtextbox.push(outofplumbOZ);var lengthPA=0;var outofplumbPA=0;lengthtextbox.push(lengthPA);outofplumbtextbox.push(outofplumbPA);var lengthPB=0;var outofplumbPB=0;lengthtextbox.push(lengthPB);outofplumbtextbox.push(outofplumbPB);var lengthPC=0;var outofplumbPC=0;lengthtextbox.push(lengthPC);outofplumbtextbox.push(outofplumbPC);var lengthPD=0;var outofplumbPD=0;lengthtextbox.push(lengthPD);outofplumbtextbox.push(outofplumbPD);var lengthPE=0;var outofplumbPE=0;lengthtextbox.push(lengthPE);outofplumbtextbox.push(outofplumbPE);var lengthPF=0;var outofplumbPF=0;lengthtextbox.push(lengthPF);outofplumbtextbox.push(outofplumbPF);var lengthPG=0;var outofplumbPG=0;lengthtextbox.push(lengthPG);outofplumbtextbox.push(outofplumbPG);var lengthPH=0;var outofplumbPH=0;lengthtextbox.push(lengthPH);outofplumbtextbox.push(outofplumbPH);var lengthPI=0;var outofplumbPI=0;lengthtextbox.push(lengthPI);outofplumbtextbox.push(outofplumbPI);var lengthPJ=0;var outofplumbPJ=0;lengthtextbox.push(lengthPJ);outofplumbtextbox.push(outofplumbPJ);var lengthPK=0;var outofplumbPK=0;lengthtextbox.push(lengthPK);outofplumbtextbox.push(outofplumbPK);var lengthPL=0;var outofplumbPL=0;lengthtextbox.push(lengthPL);outofplumbtextbox.push(outofplumbPL);var lengthPM=0;var outofplumbPM=0;lengthtextbox.push(lengthPM);outofplumbtextbox.push(outofplumbPM);var lengthPN=0;var outofplumbPN=0;lengthtextbox.push(lengthPN);outofplumbtextbox.push(outofplumbPN);var lengthPO=0;var outofplumbPO=0;lengthtextbox.push(lengthPO);outofplumbtextbox.push(outofplumbPO);var lengthPP=0;var outofplumbPP=0;lengthtextbox.push(lengthPP);outofplumbtextbox.push(outofplumbPP);var lengthPQ=0;var outofplumbPQ=0;lengthtextbox.push(lengthPQ);outofplumbtextbox.push(outofplumbPQ);var lengthPR=0;var outofplumbPR=0;lengthtextbox.push(lengthPR);outofplumbtextbox.push(outofplumbPR);var lengthPS=0;var outofplumbPS=0;lengthtextbox.push(lengthPS);outofplumbtextbox.push(outofplumbPS);var lengthPT=0;var outofplumbPT=0;lengthtextbox.push(lengthPT);outofplumbtextbox.push(outofplumbPT);var lengthPU=0;var outofplumbPU=0;lengthtextbox.push(lengthPU);outofplumbtextbox.push(outofplumbPU);var lengthPV=0;var outofplumbPV=0;lengthtextbox.push(lengthPV);outofplumbtextbox.push(outofplumbPV);var lengthPW=0;var outofplumbPW=0;lengthtextbox.push(lengthPW);outofplumbtextbox.push(outofplumbPW);var lengthPX=0;var outofplumbPX=0;lengthtextbox.push(lengthPX);outofplumbtextbox.push(outofplumbPX);var lengthPY=0;var outofplumbPY=0;lengthtextbox.push(lengthPY);outofplumbtextbox.push(outofplumbPY);var lengthPZ=0;var outofplumbPZ=0;lengthtextbox.push(lengthPZ);outofplumbtextbox.push(outofplumbPZ);var lengthQA=0;var outofplumbQA=0;lengthtextbox.push(lengthQA);outofplumbtextbox.push(outofplumbQA);var lengthQB=0;var outofplumbQB=0;lengthtextbox.push(lengthQB);outofplumbtextbox.push(outofplumbQB);var lengthQC=0;var outofplumbQC=0;lengthtextbox.push(lengthQC);outofplumbtextbox.push(outofplumbQC);var lengthQD=0;var outofplumbQD=0;lengthtextbox.push(lengthQD);outofplumbtextbox.push(outofplumbQD);var lengthQE=0;var outofplumbQE=0;lengthtextbox.push(lengthQE);outofplumbtextbox.push(outofplumbQE);var lengthQF=0;var outofplumbQF=0;lengthtextbox.push(lengthQF);outofplumbtextbox.push(outofplumbQF);var lengthQG=0;var outofplumbQG=0;lengthtextbox.push(lengthQG);outofplumbtextbox.push(outofplumbQG);var lengthQH=0;var outofplumbQH=0;lengthtextbox.push(lengthQH);outofplumbtextbox.push(outofplumbQH);var lengthQI=0;var outofplumbQI=0;lengthtextbox.push(lengthQI);outofplumbtextbox.push(outofplumbQI);var lengthQJ=0;var outofplumbQJ=0;lengthtextbox.push(lengthQJ);outofplumbtextbox.push(outofplumbQJ);var lengthQK=0;var outofplumbQK=0;lengthtextbox.push(lengthQK);outofplumbtextbox.push(outofplumbQK);var lengthQL=0;var outofplumbQL=0;lengthtextbox.push(lengthQL);outofplumbtextbox.push(outofplumbQL);var lengthQM=0;var outofplumbQM=0;lengthtextbox.push(lengthQM);outofplumbtextbox.push(outofplumbQM);var lengthQN=0;var outofplumbQN=0;lengthtextbox.push(lengthQN);outofplumbtextbox.push(outofplumbQN);var lengthQO=0;var outofplumbQO=0;lengthtextbox.push(lengthQO);outofplumbtextbox.push(outofplumbQO);var lengthQP=0;var outofplumbQP=0;lengthtextbox.push(lengthQP);outofplumbtextbox.push(outofplumbQP);var lengthQQ=0;var outofplumbQQ=0;lengthtextbox.push(lengthQQ);outofplumbtextbox.push(outofplumbQQ);var lengthQR=0;var outofplumbQR=0;lengthtextbox.push(lengthQR);outofplumbtextbox.push(outofplumbQR);var lengthQS=0;var outofplumbQS=0;lengthtextbox.push(lengthQS);outofplumbtextbox.push(outofplumbQS);var lengthQT=0;var outofplumbQT=0;lengthtextbox.push(lengthQT);outofplumbtextbox.push(outofplumbQT);var lengthQU=0;var outofplumbQU=0;lengthtextbox.push(lengthQU);outofplumbtextbox.push(outofplumbQU);var lengthQV=0;var outofplumbQV=0;lengthtextbox.push(lengthQV);outofplumbtextbox.push(outofplumbQV);var lengthQW=0;var outofplumbQW=0;lengthtextbox.push(lengthQW);outofplumbtextbox.push(outofplumbQW);var lengthQX=0;var outofplumbQX=0;lengthtextbox.push(lengthQX);outofplumbtextbox.push(outofplumbQX);var lengthQY=0;var outofplumbQY=0;lengthtextbox.push(lengthQY);outofplumbtextbox.push(outofplumbQY);var lengthQZ=0;var outofplumbQZ=0;lengthtextbox.push(lengthQZ);outofplumbtextbox.push(outofplumbQZ);var lengthRA=0;var outofplumbRA=0;lengthtextbox.push(lengthRA);outofplumbtextbox.push(outofplumbRA);var lengthRB=0;var outofplumbRB=0;lengthtextbox.push(lengthRB);outofplumbtextbox.push(outofplumbRB);var lengthRC=0;var outofplumbRC=0;lengthtextbox.push(lengthRC);outofplumbtextbox.push(outofplumbRC);var lengthRD=0;var outofplumbRD=0;lengthtextbox.push(lengthRD);outofplumbtextbox.push(outofplumbRD);var lengthRE=0;var outofplumbRE=0;lengthtextbox.push(lengthRE);outofplumbtextbox.push(outofplumbRE);var lengthRF=0;var outofplumbRF=0;lengthtextbox.push(lengthRF);outofplumbtextbox.push(outofplumbRF);var lengthRG=0;var outofplumbRG=0;lengthtextbox.push(lengthRG);outofplumbtextbox.push(outofplumbRG);var lengthRH=0;var outofplumbRH=0;lengthtextbox.push(lengthRH);outofplumbtextbox.push(outofplumbRH);var lengthRI=0;var outofplumbRI=0;lengthtextbox.push(lengthRI);outofplumbtextbox.push(outofplumbRI);var lengthRJ=0;var outofplumbRJ=0;lengthtextbox.push(lengthRJ);outofplumbtextbox.push(outofplumbRJ);var lengthRK=0;var outofplumbRK=0;lengthtextbox.push(lengthRK);outofplumbtextbox.push(outofplumbRK);var lengthRL=0;var outofplumbRL=0;lengthtextbox.push(lengthRL);outofplumbtextbox.push(outofplumbRL);var lengthRM=0;var outofplumbRM=0;lengthtextbox.push(lengthRM);outofplumbtextbox.push(outofplumbRM);var lengthRN=0;var outofplumbRN=0;lengthtextbox.push(lengthRN);outofplumbtextbox.push(outofplumbRN);var lengthRO=0;var outofplumbRO=0;lengthtextbox.push(lengthRO);outofplumbtextbox.push(outofplumbRO);var lengthRP=0;var outofplumbRP=0;lengthtextbox.push(lengthRP);outofplumbtextbox.push(outofplumbRP);var lengthRQ=0;var outofplumbRQ=0;lengthtextbox.push(lengthRQ);outofplumbtextbox.push(outofplumbRQ);var lengthRR=0;var outofplumbRR=0;lengthtextbox.push(lengthRR);outofplumbtextbox.push(outofplumbRR);var lengthRS=0;var outofplumbRS=0;lengthtextbox.push(lengthRS);outofplumbtextbox.push(outofplumbRS);var lengthRT=0;var outofplumbRT=0;lengthtextbox.push(lengthRT);outofplumbtextbox.push(outofplumbRT);var lengthRU=0;var outofplumbRU=0;lengthtextbox.push(lengthRU);outofplumbtextbox.push(outofplumbRU);var lengthRV=0;var outofplumbRV=0;lengthtextbox.push(lengthRV);outofplumbtextbox.push(outofplumbRV);var lengthRW=0;var outofplumbRW=0;lengthtextbox.push(lengthRW);outofplumbtextbox.push(outofplumbRW);var lengthRX=0;var outofplumbRX=0;lengthtextbox.push(lengthRX);outofplumbtextbox.push(outofplumbRX);var lengthRY=0;var outofplumbRY=0;lengthtextbox.push(lengthRY);outofplumbtextbox.push(outofplumbRY);var lengthRZ=0;var outofplumbRZ=0;lengthtextbox.push(lengthRZ);outofplumbtextbox.push(outofplumbRZ);var lengthSA=0;var outofplumbSA=0;lengthtextbox.push(lengthSA);outofplumbtextbox.push(outofplumbSA);var lengthSB=0;var outofplumbSB=0;lengthtextbox.push(lengthSB);outofplumbtextbox.push(outofplumbSB);var lengthSC=0;var outofplumbSC=0;lengthtextbox.push(lengthSC);outofplumbtextbox.push(outofplumbSC);var lengthSD=0;var outofplumbSD=0;lengthtextbox.push(lengthSD);outofplumbtextbox.push(outofplumbSD);var lengthSE=0;var outofplumbSE=0;lengthtextbox.push(lengthSE);outofplumbtextbox.push(outofplumbSE);var lengthSF=0;var outofplumbSF=0;lengthtextbox.push(lengthSF);outofplumbtextbox.push(outofplumbSF);var lengthSG=0;var outofplumbSG=0;lengthtextbox.push(lengthSG);outofplumbtextbox.push(outofplumbSG);var lengthSH=0;var outofplumbSH=0;lengthtextbox.push(lengthSH);outofplumbtextbox.push(outofplumbSH);var lengthSI=0;var outofplumbSI=0;lengthtextbox.push(lengthSI);outofplumbtextbox.push(outofplumbSI);var lengthSJ=0;var outofplumbSJ=0;lengthtextbox.push(lengthSJ);outofplumbtextbox.push(outofplumbSJ);var lengthSK=0;var outofplumbSK=0;lengthtextbox.push(lengthSK);outofplumbtextbox.push(outofplumbSK);var lengthSL=0;var outofplumbSL=0;lengthtextbox.push(lengthSL);outofplumbtextbox.push(outofplumbSL);var lengthSM=0;var outofplumbSM=0;lengthtextbox.push(lengthSM);outofplumbtextbox.push(outofplumbSM);var lengthSN=0;var outofplumbSN=0;lengthtextbox.push(lengthSN);outofplumbtextbox.push(outofplumbSN);var lengthSO=0;var outofplumbSO=0;lengthtextbox.push(lengthSO);outofplumbtextbox.push(outofplumbSO);var lengthSP=0;var outofplumbSP=0;lengthtextbox.push(lengthSP);outofplumbtextbox.push(outofplumbSP);var lengthSQ=0;var outofplumbSQ=0;lengthtextbox.push(lengthSQ);outofplumbtextbox.push(outofplumbSQ);var lengthSR=0;var outofplumbSR=0;lengthtextbox.push(lengthSR);outofplumbtextbox.push(outofplumbSR);var lengthSS=0;var outofplumbSS=0;lengthtextbox.push(lengthSS);outofplumbtextbox.push(outofplumbSS);var lengthST=0;var outofplumbST=0;lengthtextbox.push(lengthST);outofplumbtextbox.push(outofplumbST);var lengthSU=0;var outofplumbSU=0;lengthtextbox.push(lengthSU);outofplumbtextbox.push(outofplumbSU);var lengthSV=0;var outofplumbSV=0;lengthtextbox.push(lengthSV);outofplumbtextbox.push(outofplumbSV);var lengthSW=0;var outofplumbSW=0;lengthtextbox.push(lengthSW);outofplumbtextbox.push(outofplumbSW);var lengthSX=0;var outofplumbSX=0;lengthtextbox.push(lengthSX);outofplumbtextbox.push(outofplumbSX);var lengthSY=0;var outofplumbSY=0;lengthtextbox.push(lengthSY);outofplumbtextbox.push(outofplumbSY);var lengthSZ=0;var outofplumbSZ=0;lengthtextbox.push(lengthSZ);outofplumbtextbox.push(outofplumbSZ);var lengthTA=0;var outofplumbTA=0;lengthtextbox.push(lengthTA);outofplumbtextbox.push(outofplumbTA);var lengthTB=0;var outofplumbTB=0;lengthtextbox.push(lengthTB);outofplumbtextbox.push(outofplumbTB);var lengthTC=0;var outofplumbTC=0;lengthtextbox.push(lengthTC);outofplumbtextbox.push(outofplumbTC);var lengthTD=0;var outofplumbTD=0;lengthtextbox.push(lengthTD);outofplumbtextbox.push(outofplumbTD);var lengthTE=0;var outofplumbTE=0;lengthtextbox.push(lengthTE);outofplumbtextbox.push(outofplumbTE);var lengthTF=0;var outofplumbTF=0;lengthtextbox.push(lengthTF);outofplumbtextbox.push(outofplumbTF);var lengthTG=0;var outofplumbTG=0;lengthtextbox.push(lengthTG);outofplumbtextbox.push(outofplumbTG);var lengthTH=0;var outofplumbTH=0;lengthtextbox.push(lengthTH);outofplumbtextbox.push(outofplumbTH);var lengthTI=0;var outofplumbTI=0;lengthtextbox.push(lengthTI);outofplumbtextbox.push(outofplumbTI);var lengthTJ=0;var outofplumbTJ=0;lengthtextbox.push(lengthTJ);outofplumbtextbox.push(outofplumbTJ);var lengthTK=0;var outofplumbTK=0;lengthtextbox.push(lengthTK);outofplumbtextbox.push(outofplumbTK);var lengthTL=0;var outofplumbTL=0;lengthtextbox.push(lengthTL);outofplumbtextbox.push(outofplumbTL);var lengthTM=0;var outofplumbTM=0;lengthtextbox.push(lengthTM);outofplumbtextbox.push(outofplumbTM);var lengthTN=0;var outofplumbTN=0;lengthtextbox.push(lengthTN);outofplumbtextbox.push(outofplumbTN);var lengthTO=0;var outofplumbTO=0;lengthtextbox.push(lengthTO);outofplumbtextbox.push(outofplumbTO);var lengthTP=0;var outofplumbTP=0;lengthtextbox.push(lengthTP);outofplumbtextbox.push(outofplumbTP);var lengthTQ=0;var outofplumbTQ=0;lengthtextbox.push(lengthTQ);outofplumbtextbox.push(outofplumbTQ);var lengthTR=0;var outofplumbTR=0;lengthtextbox.push(lengthTR);outofplumbtextbox.push(outofplumbTR);var lengthTS=0;var outofplumbTS=0;lengthtextbox.push(lengthTS);outofplumbtextbox.push(outofplumbTS);var lengthTT=0;var outofplumbTT=0;lengthtextbox.push(lengthTT);outofplumbtextbox.push(outofplumbTT);var lengthTU=0;var outofplumbTU=0;lengthtextbox.push(lengthTU);outofplumbtextbox.push(outofplumbTU);var lengthTV=0;var outofplumbTV=0;lengthtextbox.push(lengthTV);outofplumbtextbox.push(outofplumbTV);var lengthTW=0;var outofplumbTW=0;lengthtextbox.push(lengthTW);outofplumbtextbox.push(outofplumbTW);var lengthTX=0;var outofplumbTX=0;lengthtextbox.push(lengthTX);outofplumbtextbox.push(outofplumbTX);var lengthTY=0;var outofplumbTY=0;lengthtextbox.push(lengthTY);outofplumbtextbox.push(outofplumbTY);var lengthTZ=0;var outofplumbTZ=0;lengthtextbox.push(lengthTZ);outofplumbtextbox.push(outofplumbTZ);var lengthUA=0;var outofplumbUA=0;lengthtextbox.push(lengthUA);outofplumbtextbox.push(outofplumbUA);var lengthUB=0;var outofplumbUB=0;lengthtextbox.push(lengthUB);outofplumbtextbox.push(outofplumbUB);var lengthUC=0;var outofplumbUC=0;lengthtextbox.push(lengthUC);outofplumbtextbox.push(outofplumbUC);var lengthUD=0;var outofplumbUD=0;lengthtextbox.push(lengthUD);outofplumbtextbox.push(outofplumbUD);var lengthUE=0;var outofplumbUE=0;lengthtextbox.push(lengthUE);outofplumbtextbox.push(outofplumbUE);var lengthUF=0;var outofplumbUF=0;lengthtextbox.push(lengthUF);outofplumbtextbox.push(outofplumbUF);var lengthUG=0;var outofplumbUG=0;lengthtextbox.push(lengthUG);outofplumbtextbox.push(outofplumbUG);var lengthUH=0;var outofplumbUH=0;lengthtextbox.push(lengthUH);outofplumbtextbox.push(outofplumbUH);var lengthUI=0;var outofplumbUI=0;lengthtextbox.push(lengthUI);outofplumbtextbox.push(outofplumbUI);var lengthUJ=0;var outofplumbUJ=0;lengthtextbox.push(lengthUJ);outofplumbtextbox.push(outofplumbUJ);var lengthUK=0;var outofplumbUK=0;lengthtextbox.push(lengthUK);outofplumbtextbox.push(outofplumbUK);var lengthUL=0;var outofplumbUL=0;lengthtextbox.push(lengthUL);outofplumbtextbox.push(outofplumbUL);var lengthUM=0;var outofplumbUM=0;lengthtextbox.push(lengthUM);outofplumbtextbox.push(outofplumbUM);var lengthUN=0;var outofplumbUN=0;lengthtextbox.push(lengthUN);outofplumbtextbox.push(outofplumbUN);var lengthUO=0;var outofplumbUO=0;lengthtextbox.push(lengthUO);outofplumbtextbox.push(outofplumbUO);var lengthUP=0;var outofplumbUP=0;lengthtextbox.push(lengthUP);outofplumbtextbox.push(outofplumbUP);var lengthUQ=0;var outofplumbUQ=0;lengthtextbox.push(lengthUQ);outofplumbtextbox.push(outofplumbUQ);var lengthUR=0;var outofplumbUR=0;lengthtextbox.push(lengthUR);outofplumbtextbox.push(outofplumbUR);var lengthUS=0;var outofplumbUS=0;lengthtextbox.push(lengthUS);outofplumbtextbox.push(outofplumbUS);var lengthUT=0;var outofplumbUT=0;lengthtextbox.push(lengthUT);outofplumbtextbox.push(outofplumbUT);var lengthUU=0;var outofplumbUU=0;lengthtextbox.push(lengthUU);outofplumbtextbox.push(outofplumbUU);var lengthUV=0;var outofplumbUV=0;lengthtextbox.push(lengthUV);outofplumbtextbox.push(outofplumbUV);var lengthUW=0;var outofplumbUW=0;lengthtextbox.push(lengthUW);outofplumbtextbox.push(outofplumbUW);var lengthUX=0;var outofplumbUX=0;lengthtextbox.push(lengthUX);outofplumbtextbox.push(outofplumbUX);var lengthUY=0;var outofplumbUY=0;lengthtextbox.push(lengthUY);outofplumbtextbox.push(outofplumbUY);var lengthUZ=0;var outofplumbUZ=0;lengthtextbox.push(lengthUZ);outofplumbtextbox.push(outofplumbUZ);var lengthVA=0;var outofplumbVA=0;lengthtextbox.push(lengthVA);outofplumbtextbox.push(outofplumbVA);var lengthVB=0;var outofplumbVB=0;lengthtextbox.push(lengthVB);outofplumbtextbox.push(outofplumbVB);var lengthVC=0;var outofplumbVC=0;lengthtextbox.push(lengthVC);outofplumbtextbox.push(outofplumbVC);var lengthVD=0;var outofplumbVD=0;lengthtextbox.push(lengthVD);outofplumbtextbox.push(outofplumbVD);var lengthVE=0;var outofplumbVE=0;lengthtextbox.push(lengthVE);outofplumbtextbox.push(outofplumbVE);var lengthVF=0;var outofplumbVF=0;lengthtextbox.push(lengthVF);outofplumbtextbox.push(outofplumbVF);var lengthVG=0;var outofplumbVG=0;lengthtextbox.push(lengthVG);outofplumbtextbox.push(outofplumbVG);var lengthVH=0;var outofplumbVH=0;lengthtextbox.push(lengthVH);outofplumbtextbox.push(outofplumbVH);var lengthVI=0;var outofplumbVI=0;lengthtextbox.push(lengthVI);outofplumbtextbox.push(outofplumbVI);var lengthVJ=0;var outofplumbVJ=0;lengthtextbox.push(lengthVJ);outofplumbtextbox.push(outofplumbVJ);var lengthVK=0;var outofplumbVK=0;lengthtextbox.push(lengthVK);outofplumbtextbox.push(outofplumbVK);var lengthVL=0;var outofplumbVL=0;lengthtextbox.push(lengthVL);outofplumbtextbox.push(outofplumbVL);var lengthVM=0;var outofplumbVM=0;lengthtextbox.push(lengthVM);outofplumbtextbox.push(outofplumbVM);var lengthVN=0;var outofplumbVN=0;lengthtextbox.push(lengthVN);outofplumbtextbox.push(outofplumbVN);var lengthVO=0;var outofplumbVO=0;lengthtextbox.push(lengthVO);outofplumbtextbox.push(outofplumbVO);var lengthVP=0;var outofplumbVP=0;lengthtextbox.push(lengthVP);outofplumbtextbox.push(outofplumbVP);var lengthVQ=0;var outofplumbVQ=0;lengthtextbox.push(lengthVQ);outofplumbtextbox.push(outofplumbVQ);var lengthVR=0;var outofplumbVR=0;lengthtextbox.push(lengthVR);outofplumbtextbox.push(outofplumbVR);var lengthVS=0;var outofplumbVS=0;lengthtextbox.push(lengthVS);outofplumbtextbox.push(outofplumbVS);var lengthVT=0;var outofplumbVT=0;lengthtextbox.push(lengthVT);outofplumbtextbox.push(outofplumbVT);var lengthVU=0;var outofplumbVU=0;lengthtextbox.push(lengthVU);outofplumbtextbox.push(outofplumbVU);var lengthVV=0;var outofplumbVV=0;lengthtextbox.push(lengthVV);outofplumbtextbox.push(outofplumbVV);var lengthVW=0;var outofplumbVW=0;lengthtextbox.push(lengthVW);outofplumbtextbox.push(outofplumbVW);var lengthVX=0;var outofplumbVX=0;lengthtextbox.push(lengthVX);outofplumbtextbox.push(outofplumbVX);var lengthVY=0;var outofplumbVY=0;lengthtextbox.push(lengthVY);outofplumbtextbox.push(outofplumbVY);var lengthVZ=0;var outofplumbVZ=0;lengthtextbox.push(lengthVZ);outofplumbtextbox.push(outofplumbVZ);var lengthWA=0;var outofplumbWA=0;lengthtextbox.push(lengthWA);outofplumbtextbox.push(outofplumbWA);var lengthWB=0;var outofplumbWB=0;lengthtextbox.push(lengthWB);outofplumbtextbox.push(outofplumbWB);var lengthWC=0;var outofplumbWC=0;lengthtextbox.push(lengthWC);outofplumbtextbox.push(outofplumbWC);var lengthWD=0;var outofplumbWD=0;lengthtextbox.push(lengthWD);outofplumbtextbox.push(outofplumbWD);var lengthWE=0;var outofplumbWE=0;lengthtextbox.push(lengthWE);outofplumbtextbox.push(outofplumbWE);var lengthWF=0;var outofplumbWF=0;lengthtextbox.push(lengthWF);outofplumbtextbox.push(outofplumbWF);var lengthWG=0;var outofplumbWG=0;lengthtextbox.push(lengthWG);outofplumbtextbox.push(outofplumbWG);var lengthWH=0;var outofplumbWH=0;lengthtextbox.push(lengthWH);outofplumbtextbox.push(outofplumbWH);var lengthWI=0;var outofplumbWI=0;lengthtextbox.push(lengthWI);outofplumbtextbox.push(outofplumbWI);var lengthWJ=0;var outofplumbWJ=0;lengthtextbox.push(lengthWJ);outofplumbtextbox.push(outofplumbWJ);var lengthWK=0;var outofplumbWK=0;lengthtextbox.push(lengthWK);outofplumbtextbox.push(outofplumbWK);var lengthWL=0;var outofplumbWL=0;lengthtextbox.push(lengthWL);outofplumbtextbox.push(outofplumbWL);var lengthWM=0;var outofplumbWM=0;lengthtextbox.push(lengthWM);outofplumbtextbox.push(outofplumbWM);var lengthWN=0;var outofplumbWN=0;lengthtextbox.push(lengthWN);outofplumbtextbox.push(outofplumbWN);var lengthWO=0;var outofplumbWO=0;lengthtextbox.push(lengthWO);outofplumbtextbox.push(outofplumbWO);var lengthWP=0;var outofplumbWP=0;lengthtextbox.push(lengthWP);outofplumbtextbox.push(outofplumbWP);var lengthWQ=0;var outofplumbWQ=0;lengthtextbox.push(lengthWQ);outofplumbtextbox.push(outofplumbWQ);var lengthWR=0;var outofplumbWR=0;lengthtextbox.push(lengthWR);outofplumbtextbox.push(outofplumbWR);var lengthWS=0;var outofplumbWS=0;lengthtextbox.push(lengthWS);outofplumbtextbox.push(outofplumbWS);var lengthWT=0;var outofplumbWT=0;lengthtextbox.push(lengthWT);outofplumbtextbox.push(outofplumbWT);var lengthWU=0;var outofplumbWU=0;lengthtextbox.push(lengthWU);outofplumbtextbox.push(outofplumbWU);var lengthWV=0;var outofplumbWV=0;lengthtextbox.push(lengthWV);outofplumbtextbox.push(outofplumbWV);var lengthWW=0;var outofplumbWW=0;lengthtextbox.push(lengthWW);outofplumbtextbox.push(outofplumbWW);var lengthWX=0;var outofplumbWX=0;lengthtextbox.push(lengthWX);outofplumbtextbox.push(outofplumbWX);var lengthWY=0;var outofplumbWY=0;lengthtextbox.push(lengthWY);outofplumbtextbox.push(outofplumbWY);var lengthWZ=0;var outofplumbWZ=0;lengthtextbox.push(lengthWZ);outofplumbtextbox.push(outofplumbWZ);var lengthXA=0;var outofplumbXA=0;lengthtextbox.push(lengthXA);outofplumbtextbox.push(outofplumbXA);var lengthXB=0;var outofplumbXB=0;lengthtextbox.push(lengthXB);outofplumbtextbox.push(outofplumbXB);var lengthXC=0;var outofplumbXC=0;lengthtextbox.push(lengthXC);outofplumbtextbox.push(outofplumbXC);var lengthXD=0;var outofplumbXD=0;lengthtextbox.push(lengthXD);outofplumbtextbox.push(outofplumbXD);var lengthXE=0;var outofplumbXE=0;lengthtextbox.push(lengthXE);outofplumbtextbox.push(outofplumbXE);var lengthXF=0;var outofplumbXF=0;lengthtextbox.push(lengthXF);outofplumbtextbox.push(outofplumbXF);var lengthXG=0;var outofplumbXG=0;lengthtextbox.push(lengthXG);outofplumbtextbox.push(outofplumbXG);var lengthXH=0;var outofplumbXH=0;lengthtextbox.push(lengthXH);outofplumbtextbox.push(outofplumbXH);var lengthXI=0;var outofplumbXI=0;lengthtextbox.push(lengthXI);outofplumbtextbox.push(outofplumbXI);var lengthXJ=0;var outofplumbXJ=0;lengthtextbox.push(lengthXJ);outofplumbtextbox.push(outofplumbXJ);var lengthXK=0;var outofplumbXK=0;lengthtextbox.push(lengthXK);outofplumbtextbox.push(outofplumbXK);var lengthXL=0;var outofplumbXL=0;lengthtextbox.push(lengthXL);outofplumbtextbox.push(outofplumbXL);var lengthXM=0;var outofplumbXM=0;lengthtextbox.push(lengthXM);outofplumbtextbox.push(outofplumbXM);var lengthXN=0;var outofplumbXN=0;lengthtextbox.push(lengthXN);outofplumbtextbox.push(outofplumbXN);var lengthXO=0;var outofplumbXO=0;lengthtextbox.push(lengthXO);outofplumbtextbox.push(outofplumbXO);var lengthXP=0;var outofplumbXP=0;lengthtextbox.push(lengthXP);outofplumbtextbox.push(outofplumbXP);var lengthXQ=0;var outofplumbXQ=0;lengthtextbox.push(lengthXQ);outofplumbtextbox.push(outofplumbXQ);var lengthXR=0;var outofplumbXR=0;lengthtextbox.push(lengthXR);outofplumbtextbox.push(outofplumbXR);var lengthXS=0;var outofplumbXS=0;lengthtextbox.push(lengthXS);outofplumbtextbox.push(outofplumbXS);var lengthXT=0;var outofplumbXT=0;lengthtextbox.push(lengthXT);outofplumbtextbox.push(outofplumbXT);var lengthXU=0;var outofplumbXU=0;lengthtextbox.push(lengthXU);outofplumbtextbox.push(outofplumbXU);var lengthXV=0;var outofplumbXV=0;lengthtextbox.push(lengthXV);outofplumbtextbox.push(outofplumbXV);var lengthXW=0;var outofplumbXW=0;lengthtextbox.push(lengthXW);outofplumbtextbox.push(outofplumbXW);var lengthXX=0;var outofplumbXX=0;lengthtextbox.push(lengthXX);outofplumbtextbox.push(outofplumbXX);var lengthXY=0;var outofplumbXY=0;lengthtextbox.push(lengthXY);outofplumbtextbox.push(outofplumbXY);var lengthXZ=0;var outofplumbXZ=0;lengthtextbox.push(lengthXZ);outofplumbtextbox.push(outofplumbXZ);var lengthYA=0;var outofplumbYA=0;lengthtextbox.push(lengthYA);outofplumbtextbox.push(outofplumbYA);var lengthYB=0;var outofplumbYB=0;lengthtextbox.push(lengthYB);outofplumbtextbox.push(outofplumbYB);var lengthYC=0;var outofplumbYC=0;lengthtextbox.push(lengthYC);outofplumbtextbox.push(outofplumbYC);var lengthYD=0;var outofplumbYD=0;lengthtextbox.push(lengthYD);outofplumbtextbox.push(outofplumbYD);var lengthYE=0;var outofplumbYE=0;lengthtextbox.push(lengthYE);outofplumbtextbox.push(outofplumbYE);var lengthYF=0;var outofplumbYF=0;lengthtextbox.push(lengthYF);outofplumbtextbox.push(outofplumbYF);var lengthYG=0;var outofplumbYG=0;lengthtextbox.push(lengthYG);outofplumbtextbox.push(outofplumbYG);var lengthYH=0;var outofplumbYH=0;lengthtextbox.push(lengthYH);outofplumbtextbox.push(outofplumbYH);var lengthYI=0;var outofplumbYI=0;lengthtextbox.push(lengthYI);outofplumbtextbox.push(outofplumbYI);var lengthYJ=0;var outofplumbYJ=0;lengthtextbox.push(lengthYJ);outofplumbtextbox.push(outofplumbYJ);var lengthYK=0;var outofplumbYK=0;lengthtextbox.push(lengthYK);outofplumbtextbox.push(outofplumbYK);var lengthYL=0;var outofplumbYL=0;lengthtextbox.push(lengthYL);outofplumbtextbox.push(outofplumbYL);var lengthYM=0;var outofplumbYM=0;lengthtextbox.push(lengthYM);outofplumbtextbox.push(outofplumbYM);var lengthYN=0;var outofplumbYN=0;lengthtextbox.push(lengthYN);outofplumbtextbox.push(outofplumbYN);var lengthYO=0;var outofplumbYO=0;lengthtextbox.push(lengthYO);outofplumbtextbox.push(outofplumbYO);var lengthYP=0;var outofplumbYP=0;lengthtextbox.push(lengthYP);outofplumbtextbox.push(outofplumbYP);var lengthYQ=0;var outofplumbYQ=0;lengthtextbox.push(lengthYQ);outofplumbtextbox.push(outofplumbYQ);var lengthYR=0;var outofplumbYR=0;lengthtextbox.push(lengthYR);outofplumbtextbox.push(outofplumbYR);var lengthYS=0;var outofplumbYS=0;lengthtextbox.push(lengthYS);outofplumbtextbox.push(outofplumbYS);var lengthYT=0;var outofplumbYT=0;lengthtextbox.push(lengthYT);outofplumbtextbox.push(outofplumbYT);var lengthYU=0;var outofplumbYU=0;lengthtextbox.push(lengthYU);outofplumbtextbox.push(outofplumbYU);var lengthYV=0;var outofplumbYV=0;lengthtextbox.push(lengthYV);outofplumbtextbox.push(outofplumbYV);var lengthYW=0;var outofplumbYW=0;lengthtextbox.push(lengthYW);outofplumbtextbox.push(outofplumbYW);var lengthYX=0;var outofplumbYX=0;lengthtextbox.push(lengthYX);outofplumbtextbox.push(outofplumbYX);var lengthYY=0;var outofplumbYY=0;lengthtextbox.push(lengthYY);outofplumbtextbox.push(outofplumbYY);var lengthYZ=0;var outofplumbYZ=0;lengthtextbox.push(lengthYZ);outofplumbtextbox.push(outofplumbYZ);		
		//for 	
	   
	
		
		//alert();
		//for textboxes
	  
	
	//alert(lengthtextbox);
		
		/*
		*/
		
		
	//var lastcahr=$("#lastchar").val();	
		//alert(lastcahr2);	
	
	
	
	//for select
	var panel_types = [];    
    $(".panel_types :selected").each(function(){
        panel_types.push($(this).text()); 
    });
   // alert(panel_types);
    //return false;
//for angle	
	var angles = [];
	if(panel_qty==1)
	{
	 angles.push('180');	
	}
	else{
	$(".angles").each(function() {
		   angles.push($(this).attr('placeholder'));
	 });
	}
	//alert(angles);
	

	//alert(outofplumbtextbox);
		
		
		
	 //alert(ccc);	
	var countpsnl=panel_types.length;
	//alert(countpsnl);
	if(countpsnl > 0)
		{	
		
		
		$.ajax({
      type:'post',
      url:"date_save/change_text_field.php",
      data:{panel_qty:panel_qty,job_no:job_no,shower_type:shower_type,lastcahr:lastcahr,lengthtextbox:lengthtextbox,panel_types:panel_types,angles:angles,outofplumbtextbox:outofplumbtextbox},
      success: function(data){
		//window.location.reload(true);
       // alert(data);      
        //$("#added_hardware_detail").html(data);
         $("#text_fields").html(data);       
	}
      });
		}				
}

$(document).ready(function() {
	//window.location.reload(2);
	history.go(2);
	var panel_qty=$("#panel_qty").val();
	//alert(panel_qty);
	//if(panel_qty>40)
	//{
		
	//}
	//else{
	change_panle(this.form);
	
	calculate_all_data(this.form);
	//}
	
});

function change_panle(form){
	
		//var panel_types='Side';
		var panlettyp1=panlettyp2=panlettyp3=panlettyp4=panlettyp5=panlettyp6=panlettyp7=panlettyp8=panlettyp9=panlettyp10=panlettyp11=panlettyp12=panlettyp13=panlettyp14=panlettyp15=panlettyp16=panlettyp17=panlettyp18=panlettyp19=panlettyp20=panlettyp21=panlettyp22=panlettyp23=panlettyp24=panlettyp25=panlettyp26=panlettyp27=panlettyp28=panlettyp29=panlettyp30=panlettyp31=panlettyp32=panlettyp34=panlettyp35=panlettyp36=panlettyp37=panlettyp38=panlettyp39=panlettyp40='';
		
		var angle1=angle2=angle3=angle4=angle5=angle6=angle7=angle8=angle9=angle10=angle11=angle12=angle13=angle14=angle15=angle16=angle17=angle18=angle19=angle20=angle21=angle22=angle23=angle24=angle25=angle26=angle27=angle28=angle29=angle30=angle31=angle32=angle33=angle34=angle35=angle36=angle37=angle38=angle39=angle40='0';
		
		
		var shower_type='InLine';
		var panel_qty='20';
		var job_no='29';
		
		
	//for angle	
	var angles = [];
	if(panel_qty==1)
	{
	 angles.push('180');	
	}
	else{
	$(".angles").each(function() {
		   angles.push($(this).attr('placeholder'));
	 });
	}
	//alert(angles);
	
	
	//for paneltype
	var panel_types = [];    
    $(".panel_types :selected").each(function(){
        panel_types.push($(this).text()); 
    });
   // alert(panel_types);
    //return false;
	 	
		
		var countpsnl=panel_types.length;
	//alert(countpsnl);
	if(countpsnl > 0)
		{	
		$.ajax({
      type:'post',
      url:"date_save/update_panel_type.php",
      data:{panel_qty:panel_qty,job_no:job_no,shower_type:shower_type,panel_types:panel_types,angles:angles},
      success: function(data){
		//  window.location.reload(true);
       // alert(data);      
       //$("#added_hardware_detail").html(data);
         $("#dimension_data").html(data);       
	}
      });
}	
		
}






function closeWebsite(){
	var resultConfirm = confirm("Are you sure you want to cancel creation of this Shower?");
	if (resultConfirm == true) {
		console.log("closing window...");
		window.close();
	}
}
function openDimesion(evt, detailName) {
  var i, tabcontentdiv2, tablinks;
  tabcontentdiv2 = document.getElementsByClassName("tabcontentdiv2");
  for (i = 0; i < tabcontentdiv2.length; i++) {
    tabcontentdiv2[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(detailName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>





<script>

function glass_order_comment(form){
		var intall_commet_val=$('#glass_order_comment_list').val();
		var intall_commet=$('#glass_order_comment_list :selected').text();
	//alert(intall_commet_val);
	//alert(intall_commet);
	$("#glass_order_comment").val(intall_commet_val); 
}



function add_installation_comment(form){
		var intall_commet_val=$('#installation_comment_list').val();
		var intall_commet=$('#installation_comment_list :selected').text();
	//alert(intall_commet_val);
	//alert(intall_commet);
	$("#installation_comment").val(intall_commet_val); 
}

function save_measurement(form){
	
	
	
	var panlettyp1=panlettyp2=panlettyp3=panlettyp4=panlettyp5=panlettyp6=panlettyp7=panlettyp8=panlettyp9=panlettyp10=panlettyp11=panlettyp12=panlettyp13=panlettyp14=panlettyp15=panlettyp16=panlettyp17=panlettyp18=panlettyp19=panlettyp20=panlettyp21=panlettyp22=panlettyp23=panlettyp24=panlettyp25=panlettyp26=panlettyp27=panlettyp28=panlettyp29=panlettyp30=panlettyp31=panlettyp32=panlettyp34=panlettyp35=panlettyp36=panlettyp37=panlettyp38=panlettyp39=panlettyp40='';
		
	var angle1=angle2=angle3=angle4=angle5=angle6=angle7=angle8=angle9=angle10=angle11=angle12=angle13=angle14=angle15=angle16=angle17=angle18=angle19=angle20=angle21=angle22=angle23=angle24=angle25=angle26=angle27=angle28=angle29=angle30=angle31=angle32=angle33=angle34=angle35=angle36=angle37=angle38=angle39=angle40='0';
	
	
	
		var lengthA=0;var lengthB=0;var lengthC=0;var lengthD=0;var lengthE=0;var lengthF=0;var lengthG=0;var lengthH=0;var lengthI=0;var lengthJ=0;var lengthK=0;var lengthL=0;var lengthM=0;var lengthN=0;var lengthO=0;var lengthP=0;var lengthQ=0;var lengthR=0;var lengthS=0;var lengthT=0;var lengthU=0;var lengthV=0;var lengthW=0;var lengthX=0;var lengthY=0;var lengthZ=0;var lengthAA=0;var lengthAB=0;var lengthAC=0;var lengthAD=0;var lengthAE=0;var lengthAF=0;var lengthAG=0;var lengthAH=0;var lengthAI=0;var lengthAJ=0;var lengthAK=0;var lengthAL=0;var lengthAM=0;var lengthAN=0;var lengthAO=0;var lengthAP=0;var lengthAQ=0;var lengthAR=0;var lengthAS=0;var lengthAT=0;var lengthAU=0;var lengthAV=0;var lengthAW=0;var lengthAX=0;var lengthAY=0;var lengthAZ=0;var lengthBA=0;var lengthBB=0;var lengthBC=0;var lengthBD=0;var lengthBE=0;var lengthBF=0;var lengthBG=0;var lengthBH=0;var lengthBI=0;var lengthBJ=0;var lengthBK=0;var lengthBL=0;var lengthBM=0;var lengthBN=0;var lengthBO=0;var lengthBP=0;var lengthBQ=0;var lengthBR=0;var lengthBS=0;var lengthBT=0;var lengthBU=0;var lengthBV=0;var lengthBW=0;var lengthBX=0;var lengthBY=0;var lengthBZ=0;var lengthCA=0;var lengthCB=0;var lengthCC=0;var lengthCD=0;var lengthCE=0;var lengthCF=0;var lengthCG=0;var lengthCH=0;var lengthCI=0;var lengthCJ=0;var lengthCK=0;var lengthCL=0;var lengthCM=0;var lengthCN=0;var lengthCO=0;var lengthCP=0;var lengthCQ=0;var lengthCR=0;var lengthCS=0;var lengthCT=0;var lengthCU=0;var lengthCV=0;var lengthCW=0;var lengthCX=0;var lengthCY=0;var lengthCZ=0;var lengthDA=0;var lengthDB=0;var lengthDC=0;var lengthDD=0;var lengthDE=0;var lengthDF=0;var lengthDG=0;var lengthDH=0;var lengthDI=0;var lengthDJ=0;var lengthDK=0;var lengthDL=0;var lengthDM=0;var lengthDN=0;var lengthDO=0;var lengthDP=0;var lengthDQ=0;var lengthDR=0;var lengthDS=0;var lengthDT=0;var lengthDU=0;var lengthDV=0;var lengthDW=0;var lengthDX=0;var lengthDY=0;var lengthDZ=0;var lengthEA=0;var lengthEB=0;var lengthEC=0;var lengthED=0;var lengthEE=0;var lengthEF=0;var lengthEG=0;var lengthEH=0;var lengthEI=0;var lengthEJ=0;var lengthEK=0;var lengthEL=0;var lengthEM=0;var lengthEN=0;var lengthEO=0;var lengthEP=0;var lengthEQ=0;var lengthER=0;var lengthES=0;var lengthET=0;var lengthEU=0;var lengthEV=0;var lengthEW=0;var lengthEX=0;var lengthEY=0;var lengthEZ=0;var lengthFA=0;var lengthFB=0;var lengthFC=0;var lengthFD=0;var lengthFE=0;var lengthFF=0;var lengthFG=0;var lengthFH=0;var lengthFI=0;var lengthFJ=0;var lengthFK=0;var lengthFL=0;var lengthFM=0;var lengthFN=0;var lengthFO=0;var lengthFP=0;var lengthFQ=0;var lengthFR=0;var lengthFS=0;var lengthFT=0;var lengthFU=0;var lengthFV=0;var lengthFW=0;var lengthFX=0;var lengthFY=0;var lengthFZ=0;var lengthGA=0;var lengthGB=0;var lengthGC=0;var lengthGD=0;var lengthGE=0;var lengthGF=0;var lengthGG=0;var lengthGH=0;var lengthGI=0;var lengthGJ=0;var lengthGK=0;var lengthGL=0;var lengthGM=0;var lengthGN=0;var lengthGO=0;var lengthGP=0;var lengthGQ=0;var lengthGR=0;var lengthGS=0;var lengthGT=0;var lengthGU=0;var lengthGV=0;var lengthGW=0;var lengthGX=0;var lengthGY=0;var lengthGZ=0;var lengthHA=0;var lengthHB=0;var lengthHC=0;var lengthHD=0;var lengthHE=0;var lengthHF=0;var lengthHG=0;var lengthHH=0;var lengthHI=0;var lengthHJ=0;var lengthHK=0;var lengthHL=0;var lengthHM=0;var lengthHN=0;var lengthHO=0;var lengthHP=0;var lengthHQ=0;var lengthHR=0;var lengthHS=0;var lengthHT=0;var lengthHU=0;var lengthHV=0;var lengthHW=0;var lengthHX=0;var lengthHY=0;var lengthHZ=0;var lengthIA=0;var lengthIB=0;var lengthIC=0;var lengthID=0;var lengthIE=0;var lengthIF=0;var lengthIG=0;var lengthIH=0;var lengthII=0;var lengthIJ=0;var lengthIK=0;var lengthIL=0;var lengthIM=0;var lengthIN=0;var lengthIO=0;var lengthIP=0;var lengthIQ=0;var lengthIR=0;var lengthIS=0;var lengthIT=0;var lengthIU=0;var lengthIV=0;var lengthIW=0;var lengthIX=0;var lengthIY=0;var lengthIZ=0;var lengthJA=0;var lengthJB=0;var lengthJC=0;var lengthJD=0;var lengthJE=0;var lengthJF=0;var lengthJG=0;var lengthJH=0;var lengthJI=0;var lengthJJ=0;var lengthJK=0;var lengthJL=0;var lengthJM=0;var lengthJN=0;var lengthJO=0;var lengthJP=0;var lengthJQ=0;var lengthJR=0;var lengthJS=0;var lengthJT=0;var lengthJU=0;var lengthJV=0;var lengthJW=0;var lengthJX=0;var lengthJY=0;var lengthJZ=0;var lengthKA=0;var lengthKB=0;var lengthKC=0;var lengthKD=0;var lengthKE=0;var lengthKF=0;var lengthKG=0;var lengthKH=0;var lengthKI=0;var lengthKJ=0;var lengthKK=0;var lengthKL=0;var lengthKM=0;var lengthKN=0;var lengthKO=0;var lengthKP=0;var lengthKQ=0;var lengthKR=0;var lengthKS=0;var lengthKT=0;var lengthKU=0;var lengthKV=0;var lengthKW=0;var lengthKX=0;var lengthKY=0;var lengthKZ=0;var lengthLA=0;var lengthLB=0;var lengthLC=0;var lengthLD=0;var lengthLE=0;var lengthLF=0;var lengthLG=0;var lengthLH=0;var lengthLI=0;var lengthLJ=0;var lengthLK=0;var lengthLL=0;var lengthLM=0;var lengthLN=0;var lengthLO=0;var lengthLP=0;var lengthLQ=0;var lengthLR=0;var lengthLS=0;var lengthLT=0;var lengthLU=0;var lengthLV=0;var lengthLW=0;var lengthLX=0;var lengthLY=0;var lengthLZ=0;var lengthMA=0;var lengthMB=0;var lengthMC=0;var lengthMD=0;var lengthME=0;var lengthMF=0;var lengthMG=0;var lengthMH=0;var lengthMI=0;var lengthMJ=0;var lengthMK=0;var lengthML=0;var lengthMM=0;var lengthMN=0;var lengthMO=0;var lengthMP=0;var lengthMQ=0;var lengthMR=0;var lengthMS=0;var lengthMT=0;var lengthMU=0;var lengthMV=0;var lengthMW=0;var lengthMX=0;var lengthMY=0;var lengthMZ=0;var lengthNA=0;var lengthNB=0;var lengthNC=0;var lengthND=0;var lengthNE=0;var lengthNF=0;var lengthNG=0;var lengthNH=0;var lengthNI=0;var lengthNJ=0;var lengthNK=0;var lengthNL=0;var lengthNM=0;var lengthNN=0;var lengthNO=0;var lengthNP=0;var lengthNQ=0;var lengthNR=0;var lengthNS=0;var lengthNT=0;var lengthNU=0;var lengthNV=0;var lengthNW=0;var lengthNX=0;var lengthNY=0;var lengthNZ=0;var lengthOA=0;var lengthOB=0;var lengthOC=0;var lengthOD=0;var lengthOE=0;var lengthOF=0;var lengthOG=0;var lengthOH=0;var lengthOI=0;var lengthOJ=0;var lengthOK=0;var lengthOL=0;var lengthOM=0;var lengthON=0;var lengthOO=0;var lengthOP=0;var lengthOQ=0;var lengthOR=0;var lengthOS=0;var lengthOT=0;var lengthOU=0;var lengthOV=0;var lengthOW=0;var lengthOX=0;var lengthOY=0;var lengthOZ=0;var lengthPA=0;var lengthPB=0;var lengthPC=0;var lengthPD=0;var lengthPE=0;var lengthPF=0;var lengthPG=0;var lengthPH=0;var lengthPI=0;var lengthPJ=0;var lengthPK=0;var lengthPL=0;var lengthPM=0;var lengthPN=0;var lengthPO=0;var lengthPP=0;var lengthPQ=0;var lengthPR=0;var lengthPS=0;var lengthPT=0;var lengthPU=0;var lengthPV=0;var lengthPW=0;var lengthPX=0;var lengthPY=0;var lengthPZ=0;var lengthQA=0;var lengthQB=0;var lengthQC=0;var lengthQD=0;var lengthQE=0;var lengthQF=0;var lengthQG=0;var lengthQH=0;var lengthQI=0;var lengthQJ=0;var lengthQK=0;var lengthQL=0;var lengthQM=0;var lengthQN=0;var lengthQO=0;var lengthQP=0;var lengthQQ=0;var lengthQR=0;var lengthQS=0;var lengthQT=0;var lengthQU=0;var lengthQV=0;var lengthQW=0;var lengthQX=0;var lengthQY=0;var lengthQZ=0;var lengthRA=0;var lengthRB=0;var lengthRC=0;var lengthRD=0;var lengthRE=0;var lengthRF=0;var lengthRG=0;var lengthRH=0;var lengthRI=0;var lengthRJ=0;var lengthRK=0;var lengthRL=0;var lengthRM=0;var lengthRN=0;var lengthRO=0;var lengthRP=0;var lengthRQ=0;var lengthRR=0;var lengthRS=0;var lengthRT=0;var lengthRU=0;var lengthRV=0;var lengthRW=0;var lengthRX=0;var lengthRY=0;var lengthRZ=0;var lengthSA=0;var lengthSB=0;var lengthSC=0;var lengthSD=0;var lengthSE=0;var lengthSF=0;var lengthSG=0;var lengthSH=0;var lengthSI=0;var lengthSJ=0;var lengthSK=0;var lengthSL=0;var lengthSM=0;var lengthSN=0;var lengthSO=0;var lengthSP=0;var lengthSQ=0;var lengthSR=0;var lengthSS=0;var lengthST=0;var lengthSU=0;var lengthSV=0;var lengthSW=0;var lengthSX=0;var lengthSY=0;var lengthSZ=0;var lengthTA=0;var lengthTB=0;var lengthTC=0;var lengthTD=0;var lengthTE=0;var lengthTF=0;var lengthTG=0;var lengthTH=0;var lengthTI=0;var lengthTJ=0;var lengthTK=0;var lengthTL=0;var lengthTM=0;var lengthTN=0;var lengthTO=0;var lengthTP=0;var lengthTQ=0;var lengthTR=0;var lengthTS=0;var lengthTT=0;var lengthTU=0;var lengthTV=0;var lengthTW=0;var lengthTX=0;var lengthTY=0;var lengthTZ=0;var lengthUA=0;var lengthUB=0;var lengthUC=0;var lengthUD=0;var lengthUE=0;var lengthUF=0;var lengthUG=0;var lengthUH=0;var lengthUI=0;var lengthUJ=0;var lengthUK=0;var lengthUL=0;var lengthUM=0;var lengthUN=0;var lengthUO=0;var lengthUP=0;var lengthUQ=0;var lengthUR=0;var lengthUS=0;var lengthUT=0;var lengthUU=0;var lengthUV=0;var lengthUW=0;var lengthUX=0;var lengthUY=0;var lengthUZ=0;var lengthVA=0;var lengthVB=0;var lengthVC=0;var lengthVD=0;var lengthVE=0;var lengthVF=0;var lengthVG=0;var lengthVH=0;var lengthVI=0;var lengthVJ=0;var lengthVK=0;var lengthVL=0;var lengthVM=0;var lengthVN=0;var lengthVO=0;var lengthVP=0;var lengthVQ=0;var lengthVR=0;var lengthVS=0;var lengthVT=0;var lengthVU=0;var lengthVV=0;var lengthVW=0;var lengthVX=0;var lengthVY=0;var lengthVZ=0;var lengthWA=0;var lengthWB=0;var lengthWC=0;var lengthWD=0;var lengthWE=0;var lengthWF=0;var lengthWG=0;var lengthWH=0;var lengthWI=0;var lengthWJ=0;var lengthWK=0;var lengthWL=0;var lengthWM=0;var lengthWN=0;var lengthWO=0;var lengthWP=0;var lengthWQ=0;var lengthWR=0;var lengthWS=0;var lengthWT=0;var lengthWU=0;var lengthWV=0;var lengthWW=0;var lengthWX=0;var lengthWY=0;var lengthWZ=0;var lengthXA=0;var lengthXB=0;var lengthXC=0;var lengthXD=0;var lengthXE=0;var lengthXF=0;var lengthXG=0;var lengthXH=0;var lengthXI=0;var lengthXJ=0;var lengthXK=0;var lengthXL=0;var lengthXM=0;var lengthXN=0;var lengthXO=0;var lengthXP=0;var lengthXQ=0;var lengthXR=0;var lengthXS=0;var lengthXT=0;var lengthXU=0;var lengthXV=0;var lengthXW=0;var lengthXX=0;var lengthXY=0;var lengthXZ=0;var lengthYA=0;var lengthYB=0;var lengthYC=0;var lengthYD=0;var lengthYE=0;var lengthYF=0;var lengthYG=0;var lengthYH=0;var lengthYI=0;var lengthYJ=0;var lengthYK=0;var lengthYL=0;var lengthYM=0;var lengthYN=0;var lengthYO=0;var lengthYP=0;var lengthYQ=0;var lengthYR=0;var lengthYS=0;var lengthYT=0;var lengthYU=0;var lengthYV=0;var lengthYW=0;var lengthYX=0;var lengthYY=0;var lengthYZ=0;	
	var lastcahr=$("#lastchar").val();
	
	
	
	  var shower_type = 'InLine';
	  var door_type = 'Double';
	  var hinges_type = 'Right';
	 var panel_qty='20';
	  
	/* var angleposition_array =array();
	 var angl1 ='180';
	 var angl2 ='180';
	 var angl3 ='180';
	 var angl4 ='180';
	 var angl5 ='180';
	 var angl6 ='180';*/
	 var door_location ='2';

	 
		
	//for textboxes
	var lengthtextbox = [];   
	$(".lengthtextbox").each(function() {
		   lengthtextbox.push($(this).val());
	 });
	//alert(lengthtextbox);
	var lengthA=$("#lengthA").val();;var lengthB=$("#lengthB").val();;var lengthC=$("#lengthC").val();;var lengthD=$("#lengthD").val();;var lengthE=$("#lengthE").val();;var lengthF=$("#lengthF").val();;var lengthG=$("#lengthG").val();;var lengthH=$("#lengthH").val();;var lengthI=$("#lengthI").val();;var lengthJ=$("#lengthJ").val();;var lengthK=$("#lengthK").val();;var lengthL=$("#lengthL").val();;var lengthM=$("#lengthM").val();;var lengthN=$("#lengthN").val();;var lengthO=$("#lengthO").val();;var lengthP=$("#lengthP").val();;var lengthQ=$("#lengthQ").val();;var lengthR=$("#lengthR").val();;var lengthS=$("#lengthS").val();;var lengthT=$("#lengthT").val();;var lengthU=$("#lengthU").val();;var lengthV=$("#lengthV").val();;var lengthW=$("#lengthW").val();;var lengthX=$("#lengthX").val();;var lengthY=$("#lengthY").val();;var lengthZ=$("#lengthZ").val();;var lengthAA=$("#lengthAA").val();;var lengthAB=$("#lengthAB").val();;var lengthAC=$("#lengthAC").val();;var lengthAD=$("#lengthAD").val();;var lengthAE=$("#lengthAE").val();;var lengthAF=$("#lengthAF").val();;var lengthAG=$("#lengthAG").val();;var lengthAH=$("#lengthAH").val();;var lengthAI=$("#lengthAI").val();;var lengthAJ=$("#lengthAJ").val();;var lengthAK=$("#lengthAK").val();;var lengthAL=$("#lengthAL").val();;var lengthAM=$("#lengthAM").val();;var lengthAN=$("#lengthAN").val();;var lengthAO=$("#lengthAO").val();;var lengthAP=$("#lengthAP").val();;var lengthAQ=$("#lengthAQ").val();;var lengthAR=$("#lengthAR").val();;var lengthAS=$("#lengthAS").val();;var lengthAT=$("#lengthAT").val();;var lengthAU=$("#lengthAU").val();;var lengthAV=$("#lengthAV").val();;var lengthAW=$("#lengthAW").val();;var lengthAX=$("#lengthAX").val();;var lengthAY=$("#lengthAY").val();;var lengthAZ=$("#lengthAZ").val();;var lengthBA=$("#lengthBA").val();;var lengthBB=$("#lengthBB").val();;var lengthBC=$("#lengthBC").val();;var lengthBD=$("#lengthBD").val();;var lengthBE=$("#lengthBE").val();;var lengthBF=$("#lengthBF").val();;var lengthBG=$("#lengthBG").val();;var lengthBH=$("#lengthBH").val();;var lengthBI=$("#lengthBI").val();;var lengthBJ=$("#lengthBJ").val();;var lengthBK=$("#lengthBK").val();;var lengthBL=$("#lengthBL").val();;var lengthBM=$("#lengthBM").val();;var lengthBN=$("#lengthBN").val();;var lengthBO=$("#lengthBO").val();;var lengthBP=$("#lengthBP").val();;var lengthBQ=$("#lengthBQ").val();;var lengthBR=$("#lengthBR").val();;var lengthBS=$("#lengthBS").val();;var lengthBT=$("#lengthBT").val();;var lengthBU=$("#lengthBU").val();;var lengthBV=$("#lengthBV").val();;var lengthBW=$("#lengthBW").val();;var lengthBX=$("#lengthBX").val();;var lengthBY=$("#lengthBY").val();;var lengthBZ=$("#lengthBZ").val();;var lengthCA=$("#lengthCA").val();;var lengthCB=$("#lengthCB").val();;var lengthCC=$("#lengthCC").val();;var lengthCD=$("#lengthCD").val();;var lengthCE=$("#lengthCE").val();;var lengthCF=$("#lengthCF").val();;var lengthCG=$("#lengthCG").val();;var lengthCH=$("#lengthCH").val();;var lengthCI=$("#lengthCI").val();;var lengthCJ=$("#lengthCJ").val();;var lengthCK=$("#lengthCK").val();;var lengthCL=$("#lengthCL").val();;var lengthCM=$("#lengthCM").val();;var lengthCN=$("#lengthCN").val();;var lengthCO=$("#lengthCO").val();;var lengthCP=$("#lengthCP").val();;var lengthCQ=$("#lengthCQ").val();;var lengthCR=$("#lengthCR").val();;var lengthCS=$("#lengthCS").val();;var lengthCT=$("#lengthCT").val();;var lengthCU=$("#lengthCU").val();;var lengthCV=$("#lengthCV").val();;var lengthCW=$("#lengthCW").val();;var lengthCX=$("#lengthCX").val();;var lengthCY=$("#lengthCY").val();;var lengthCZ=$("#lengthCZ").val();;var lengthDA=$("#lengthDA").val();;var lengthDB=$("#lengthDB").val();;var lengthDC=$("#lengthDC").val();;var lengthDD=$("#lengthDD").val();;var lengthDE=$("#lengthDE").val();;var lengthDF=$("#lengthDF").val();;var lengthDG=$("#lengthDG").val();;var lengthDH=$("#lengthDH").val();;var lengthDI=$("#lengthDI").val();;var lengthDJ=$("#lengthDJ").val();;var lengthDK=$("#lengthDK").val();;var lengthDL=$("#lengthDL").val();;var lengthDM=$("#lengthDM").val();;var lengthDN=$("#lengthDN").val();;var lengthDO=$("#lengthDO").val();;var lengthDP=$("#lengthDP").val();;var lengthDQ=$("#lengthDQ").val();;var lengthDR=$("#lengthDR").val();;var lengthDS=$("#lengthDS").val();;var lengthDT=$("#lengthDT").val();;var lengthDU=$("#lengthDU").val();;var lengthDV=$("#lengthDV").val();;var lengthDW=$("#lengthDW").val();;var lengthDX=$("#lengthDX").val();;var lengthDY=$("#lengthDY").val();;var lengthDZ=$("#lengthDZ").val();;var lengthEA=$("#lengthEA").val();;var lengthEB=$("#lengthEB").val();;var lengthEC=$("#lengthEC").val();;var lengthED=$("#lengthED").val();;var lengthEE=$("#lengthEE").val();;var lengthEF=$("#lengthEF").val();;var lengthEG=$("#lengthEG").val();;var lengthEH=$("#lengthEH").val();;var lengthEI=$("#lengthEI").val();;var lengthEJ=$("#lengthEJ").val();;var lengthEK=$("#lengthEK").val();;var lengthEL=$("#lengthEL").val();;var lengthEM=$("#lengthEM").val();;var lengthEN=$("#lengthEN").val();;var lengthEO=$("#lengthEO").val();;var lengthEP=$("#lengthEP").val();;var lengthEQ=$("#lengthEQ").val();;var lengthER=$("#lengthER").val();;var lengthES=$("#lengthES").val();;var lengthET=$("#lengthET").val();;var lengthEU=$("#lengthEU").val();;var lengthEV=$("#lengthEV").val();;var lengthEW=$("#lengthEW").val();;var lengthEX=$("#lengthEX").val();;var lengthEY=$("#lengthEY").val();;var lengthEZ=$("#lengthEZ").val();;var lengthFA=$("#lengthFA").val();;var lengthFB=$("#lengthFB").val();;var lengthFC=$("#lengthFC").val();;var lengthFD=$("#lengthFD").val();;var lengthFE=$("#lengthFE").val();;var lengthFF=$("#lengthFF").val();;var lengthFG=$("#lengthFG").val();;var lengthFH=$("#lengthFH").val();;var lengthFI=$("#lengthFI").val();;var lengthFJ=$("#lengthFJ").val();;var lengthFK=$("#lengthFK").val();;var lengthFL=$("#lengthFL").val();;var lengthFM=$("#lengthFM").val();;var lengthFN=$("#lengthFN").val();;var lengthFO=$("#lengthFO").val();;var lengthFP=$("#lengthFP").val();;var lengthFQ=$("#lengthFQ").val();;var lengthFR=$("#lengthFR").val();;var lengthFS=$("#lengthFS").val();;var lengthFT=$("#lengthFT").val();;var lengthFU=$("#lengthFU").val();;var lengthFV=$("#lengthFV").val();;var lengthFW=$("#lengthFW").val();;var lengthFX=$("#lengthFX").val();;var lengthFY=$("#lengthFY").val();;var lengthFZ=$("#lengthFZ").val();;var lengthGA=$("#lengthGA").val();;var lengthGB=$("#lengthGB").val();;var lengthGC=$("#lengthGC").val();;var lengthGD=$("#lengthGD").val();;var lengthGE=$("#lengthGE").val();;var lengthGF=$("#lengthGF").val();;var lengthGG=$("#lengthGG").val();;var lengthGH=$("#lengthGH").val();;var lengthGI=$("#lengthGI").val();;var lengthGJ=$("#lengthGJ").val();;var lengthGK=$("#lengthGK").val();;var lengthGL=$("#lengthGL").val();;var lengthGM=$("#lengthGM").val();;var lengthGN=$("#lengthGN").val();;var lengthGO=$("#lengthGO").val();;var lengthGP=$("#lengthGP").val();;var lengthGQ=$("#lengthGQ").val();;var lengthGR=$("#lengthGR").val();;var lengthGS=$("#lengthGS").val();;var lengthGT=$("#lengthGT").val();;var lengthGU=$("#lengthGU").val();;var lengthGV=$("#lengthGV").val();;var lengthGW=$("#lengthGW").val();;var lengthGX=$("#lengthGX").val();;var lengthGY=$("#lengthGY").val();;var lengthGZ=$("#lengthGZ").val();;var lengthHA=$("#lengthHA").val();;var lengthHB=$("#lengthHB").val();;var lengthHC=$("#lengthHC").val();;var lengthHD=$("#lengthHD").val();;var lengthHE=$("#lengthHE").val();;var lengthHF=$("#lengthHF").val();;var lengthHG=$("#lengthHG").val();;var lengthHH=$("#lengthHH").val();;var lengthHI=$("#lengthHI").val();;var lengthHJ=$("#lengthHJ").val();;var lengthHK=$("#lengthHK").val();;var lengthHL=$("#lengthHL").val();;var lengthHM=$("#lengthHM").val();;var lengthHN=$("#lengthHN").val();;var lengthHO=$("#lengthHO").val();;var lengthHP=$("#lengthHP").val();;var lengthHQ=$("#lengthHQ").val();;var lengthHR=$("#lengthHR").val();;var lengthHS=$("#lengthHS").val();;var lengthHT=$("#lengthHT").val();;var lengthHU=$("#lengthHU").val();;var lengthHV=$("#lengthHV").val();;var lengthHW=$("#lengthHW").val();;var lengthHX=$("#lengthHX").val();;var lengthHY=$("#lengthHY").val();;var lengthHZ=$("#lengthHZ").val();;var lengthIA=$("#lengthIA").val();;var lengthIB=$("#lengthIB").val();;var lengthIC=$("#lengthIC").val();;var lengthID=$("#lengthID").val();;var lengthIE=$("#lengthIE").val();;var lengthIF=$("#lengthIF").val();;var lengthIG=$("#lengthIG").val();;var lengthIH=$("#lengthIH").val();;var lengthII=$("#lengthII").val();;var lengthIJ=$("#lengthIJ").val();;var lengthIK=$("#lengthIK").val();;var lengthIL=$("#lengthIL").val();;var lengthIM=$("#lengthIM").val();;var lengthIN=$("#lengthIN").val();;var lengthIO=$("#lengthIO").val();;var lengthIP=$("#lengthIP").val();;var lengthIQ=$("#lengthIQ").val();;var lengthIR=$("#lengthIR").val();;var lengthIS=$("#lengthIS").val();;var lengthIT=$("#lengthIT").val();;var lengthIU=$("#lengthIU").val();;var lengthIV=$("#lengthIV").val();;var lengthIW=$("#lengthIW").val();;var lengthIX=$("#lengthIX").val();;var lengthIY=$("#lengthIY").val();;var lengthIZ=$("#lengthIZ").val();;var lengthJA=$("#lengthJA").val();;var lengthJB=$("#lengthJB").val();;var lengthJC=$("#lengthJC").val();;var lengthJD=$("#lengthJD").val();;var lengthJE=$("#lengthJE").val();;var lengthJF=$("#lengthJF").val();;var lengthJG=$("#lengthJG").val();;var lengthJH=$("#lengthJH").val();;var lengthJI=$("#lengthJI").val();;var lengthJJ=$("#lengthJJ").val();;var lengthJK=$("#lengthJK").val();;var lengthJL=$("#lengthJL").val();;var lengthJM=$("#lengthJM").val();;var lengthJN=$("#lengthJN").val();;var lengthJO=$("#lengthJO").val();;var lengthJP=$("#lengthJP").val();;var lengthJQ=$("#lengthJQ").val();;var lengthJR=$("#lengthJR").val();;var lengthJS=$("#lengthJS").val();;var lengthJT=$("#lengthJT").val();;var lengthJU=$("#lengthJU").val();;var lengthJV=$("#lengthJV").val();;var lengthJW=$("#lengthJW").val();;var lengthJX=$("#lengthJX").val();;var lengthJY=$("#lengthJY").val();;var lengthJZ=$("#lengthJZ").val();;var lengthKA=$("#lengthKA").val();;var lengthKB=$("#lengthKB").val();;var lengthKC=$("#lengthKC").val();;var lengthKD=$("#lengthKD").val();;var lengthKE=$("#lengthKE").val();;var lengthKF=$("#lengthKF").val();;var lengthKG=$("#lengthKG").val();;var lengthKH=$("#lengthKH").val();;var lengthKI=$("#lengthKI").val();;var lengthKJ=$("#lengthKJ").val();;var lengthKK=$("#lengthKK").val();;var lengthKL=$("#lengthKL").val();;var lengthKM=$("#lengthKM").val();;var lengthKN=$("#lengthKN").val();;var lengthKO=$("#lengthKO").val();;var lengthKP=$("#lengthKP").val();;var lengthKQ=$("#lengthKQ").val();;var lengthKR=$("#lengthKR").val();;var lengthKS=$("#lengthKS").val();;var lengthKT=$("#lengthKT").val();;var lengthKU=$("#lengthKU").val();;var lengthKV=$("#lengthKV").val();;var lengthKW=$("#lengthKW").val();;var lengthKX=$("#lengthKX").val();;var lengthKY=$("#lengthKY").val();;var lengthKZ=$("#lengthKZ").val();;var lengthLA=$("#lengthLA").val();;var lengthLB=$("#lengthLB").val();;var lengthLC=$("#lengthLC").val();;var lengthLD=$("#lengthLD").val();;var lengthLE=$("#lengthLE").val();;var lengthLF=$("#lengthLF").val();;var lengthLG=$("#lengthLG").val();;var lengthLH=$("#lengthLH").val();;var lengthLI=$("#lengthLI").val();;var lengthLJ=$("#lengthLJ").val();;var lengthLK=$("#lengthLK").val();;var lengthLL=$("#lengthLL").val();;var lengthLM=$("#lengthLM").val();;var lengthLN=$("#lengthLN").val();;var lengthLO=$("#lengthLO").val();;var lengthLP=$("#lengthLP").val();;var lengthLQ=$("#lengthLQ").val();;var lengthLR=$("#lengthLR").val();;var lengthLS=$("#lengthLS").val();;var lengthLT=$("#lengthLT").val();;var lengthLU=$("#lengthLU").val();;var lengthLV=$("#lengthLV").val();;var lengthLW=$("#lengthLW").val();;var lengthLX=$("#lengthLX").val();;var lengthLY=$("#lengthLY").val();;var lengthLZ=$("#lengthLZ").val();;var lengthMA=$("#lengthMA").val();;var lengthMB=$("#lengthMB").val();;var lengthMC=$("#lengthMC").val();;var lengthMD=$("#lengthMD").val();;var lengthME=$("#lengthME").val();;var lengthMF=$("#lengthMF").val();;var lengthMG=$("#lengthMG").val();;var lengthMH=$("#lengthMH").val();;var lengthMI=$("#lengthMI").val();;var lengthMJ=$("#lengthMJ").val();;var lengthMK=$("#lengthMK").val();;var lengthML=$("#lengthML").val();;var lengthMM=$("#lengthMM").val();;var lengthMN=$("#lengthMN").val();;var lengthMO=$("#lengthMO").val();;var lengthMP=$("#lengthMP").val();;var lengthMQ=$("#lengthMQ").val();;var lengthMR=$("#lengthMR").val();;var lengthMS=$("#lengthMS").val();;var lengthMT=$("#lengthMT").val();;var lengthMU=$("#lengthMU").val();;var lengthMV=$("#lengthMV").val();;var lengthMW=$("#lengthMW").val();;var lengthMX=$("#lengthMX").val();;var lengthMY=$("#lengthMY").val();;var lengthMZ=$("#lengthMZ").val();;var lengthNA=$("#lengthNA").val();;var lengthNB=$("#lengthNB").val();;var lengthNC=$("#lengthNC").val();;var lengthND=$("#lengthND").val();;var lengthNE=$("#lengthNE").val();;var lengthNF=$("#lengthNF").val();;var lengthNG=$("#lengthNG").val();;var lengthNH=$("#lengthNH").val();;var lengthNI=$("#lengthNI").val();;var lengthNJ=$("#lengthNJ").val();;var lengthNK=$("#lengthNK").val();;var lengthNL=$("#lengthNL").val();;var lengthNM=$("#lengthNM").val();;var lengthNN=$("#lengthNN").val();;var lengthNO=$("#lengthNO").val();;var lengthNP=$("#lengthNP").val();;var lengthNQ=$("#lengthNQ").val();;var lengthNR=$("#lengthNR").val();;var lengthNS=$("#lengthNS").val();;var lengthNT=$("#lengthNT").val();;var lengthNU=$("#lengthNU").val();;var lengthNV=$("#lengthNV").val();;var lengthNW=$("#lengthNW").val();;var lengthNX=$("#lengthNX").val();;var lengthNY=$("#lengthNY").val();;var lengthNZ=$("#lengthNZ").val();;var lengthOA=$("#lengthOA").val();;var lengthOB=$("#lengthOB").val();;var lengthOC=$("#lengthOC").val();;var lengthOD=$("#lengthOD").val();;var lengthOE=$("#lengthOE").val();;var lengthOF=$("#lengthOF").val();;var lengthOG=$("#lengthOG").val();;var lengthOH=$("#lengthOH").val();;var lengthOI=$("#lengthOI").val();;var lengthOJ=$("#lengthOJ").val();;var lengthOK=$("#lengthOK").val();;var lengthOL=$("#lengthOL").val();;var lengthOM=$("#lengthOM").val();;var lengthON=$("#lengthON").val();;var lengthOO=$("#lengthOO").val();;var lengthOP=$("#lengthOP").val();;var lengthOQ=$("#lengthOQ").val();;var lengthOR=$("#lengthOR").val();;var lengthOS=$("#lengthOS").val();;var lengthOT=$("#lengthOT").val();;var lengthOU=$("#lengthOU").val();;var lengthOV=$("#lengthOV").val();;var lengthOW=$("#lengthOW").val();;var lengthOX=$("#lengthOX").val();;var lengthOY=$("#lengthOY").val();;var lengthOZ=$("#lengthOZ").val();;var lengthPA=$("#lengthPA").val();;var lengthPB=$("#lengthPB").val();;var lengthPC=$("#lengthPC").val();;var lengthPD=$("#lengthPD").val();;var lengthPE=$("#lengthPE").val();;var lengthPF=$("#lengthPF").val();;var lengthPG=$("#lengthPG").val();;var lengthPH=$("#lengthPH").val();;var lengthPI=$("#lengthPI").val();;var lengthPJ=$("#lengthPJ").val();;var lengthPK=$("#lengthPK").val();;var lengthPL=$("#lengthPL").val();;var lengthPM=$("#lengthPM").val();;var lengthPN=$("#lengthPN").val();;var lengthPO=$("#lengthPO").val();;var lengthPP=$("#lengthPP").val();;var lengthPQ=$("#lengthPQ").val();;var lengthPR=$("#lengthPR").val();;var lengthPS=$("#lengthPS").val();;var lengthPT=$("#lengthPT").val();;var lengthPU=$("#lengthPU").val();;var lengthPV=$("#lengthPV").val();;var lengthPW=$("#lengthPW").val();;var lengthPX=$("#lengthPX").val();;var lengthPY=$("#lengthPY").val();;var lengthPZ=$("#lengthPZ").val();;var lengthQA=$("#lengthQA").val();;var lengthQB=$("#lengthQB").val();;var lengthQC=$("#lengthQC").val();;var lengthQD=$("#lengthQD").val();;var lengthQE=$("#lengthQE").val();;var lengthQF=$("#lengthQF").val();;var lengthQG=$("#lengthQG").val();;var lengthQH=$("#lengthQH").val();;var lengthQI=$("#lengthQI").val();;var lengthQJ=$("#lengthQJ").val();;var lengthQK=$("#lengthQK").val();;var lengthQL=$("#lengthQL").val();;var lengthQM=$("#lengthQM").val();;var lengthQN=$("#lengthQN").val();;var lengthQO=$("#lengthQO").val();;var lengthQP=$("#lengthQP").val();;var lengthQQ=$("#lengthQQ").val();;var lengthQR=$("#lengthQR").val();;var lengthQS=$("#lengthQS").val();;var lengthQT=$("#lengthQT").val();;var lengthQU=$("#lengthQU").val();;var lengthQV=$("#lengthQV").val();;var lengthQW=$("#lengthQW").val();;var lengthQX=$("#lengthQX").val();;var lengthQY=$("#lengthQY").val();;var lengthQZ=$("#lengthQZ").val();;var lengthRA=$("#lengthRA").val();;var lengthRB=$("#lengthRB").val();;var lengthRC=$("#lengthRC").val();;var lengthRD=$("#lengthRD").val();;var lengthRE=$("#lengthRE").val();;var lengthRF=$("#lengthRF").val();;var lengthRG=$("#lengthRG").val();;var lengthRH=$("#lengthRH").val();;var lengthRI=$("#lengthRI").val();;var lengthRJ=$("#lengthRJ").val();;var lengthRK=$("#lengthRK").val();;var lengthRL=$("#lengthRL").val();;var lengthRM=$("#lengthRM").val();;var lengthRN=$("#lengthRN").val();;var lengthRO=$("#lengthRO").val();;var lengthRP=$("#lengthRP").val();;var lengthRQ=$("#lengthRQ").val();;var lengthRR=$("#lengthRR").val();;var lengthRS=$("#lengthRS").val();;var lengthRT=$("#lengthRT").val();;var lengthRU=$("#lengthRU").val();;var lengthRV=$("#lengthRV").val();;var lengthRW=$("#lengthRW").val();;var lengthRX=$("#lengthRX").val();;var lengthRY=$("#lengthRY").val();;var lengthRZ=$("#lengthRZ").val();;var lengthSA=$("#lengthSA").val();;var lengthSB=$("#lengthSB").val();;var lengthSC=$("#lengthSC").val();;var lengthSD=$("#lengthSD").val();;var lengthSE=$("#lengthSE").val();;var lengthSF=$("#lengthSF").val();;var lengthSG=$("#lengthSG").val();;var lengthSH=$("#lengthSH").val();;var lengthSI=$("#lengthSI").val();;var lengthSJ=$("#lengthSJ").val();;var lengthSK=$("#lengthSK").val();;var lengthSL=$("#lengthSL").val();;var lengthSM=$("#lengthSM").val();;var lengthSN=$("#lengthSN").val();;var lengthSO=$("#lengthSO").val();;var lengthSP=$("#lengthSP").val();;var lengthSQ=$("#lengthSQ").val();;var lengthSR=$("#lengthSR").val();;var lengthSS=$("#lengthSS").val();;var lengthST=$("#lengthST").val();;var lengthSU=$("#lengthSU").val();;var lengthSV=$("#lengthSV").val();;var lengthSW=$("#lengthSW").val();;var lengthSX=$("#lengthSX").val();;var lengthSY=$("#lengthSY").val();;var lengthSZ=$("#lengthSZ").val();;var lengthTA=$("#lengthTA").val();;var lengthTB=$("#lengthTB").val();;var lengthTC=$("#lengthTC").val();;var lengthTD=$("#lengthTD").val();;var lengthTE=$("#lengthTE").val();;var lengthTF=$("#lengthTF").val();;var lengthTG=$("#lengthTG").val();;var lengthTH=$("#lengthTH").val();;var lengthTI=$("#lengthTI").val();;var lengthTJ=$("#lengthTJ").val();;var lengthTK=$("#lengthTK").val();;var lengthTL=$("#lengthTL").val();;var lengthTM=$("#lengthTM").val();;var lengthTN=$("#lengthTN").val();;var lengthTO=$("#lengthTO").val();;var lengthTP=$("#lengthTP").val();;var lengthTQ=$("#lengthTQ").val();;var lengthTR=$("#lengthTR").val();;var lengthTS=$("#lengthTS").val();;var lengthTT=$("#lengthTT").val();;var lengthTU=$("#lengthTU").val();;var lengthTV=$("#lengthTV").val();;var lengthTW=$("#lengthTW").val();;var lengthTX=$("#lengthTX").val();;var lengthTY=$("#lengthTY").val();;var lengthTZ=$("#lengthTZ").val();;var lengthUA=$("#lengthUA").val();;var lengthUB=$("#lengthUB").val();;var lengthUC=$("#lengthUC").val();;var lengthUD=$("#lengthUD").val();;var lengthUE=$("#lengthUE").val();;var lengthUF=$("#lengthUF").val();;var lengthUG=$("#lengthUG").val();;var lengthUH=$("#lengthUH").val();;var lengthUI=$("#lengthUI").val();;var lengthUJ=$("#lengthUJ").val();;var lengthUK=$("#lengthUK").val();;var lengthUL=$("#lengthUL").val();;var lengthUM=$("#lengthUM").val();;var lengthUN=$("#lengthUN").val();;var lengthUO=$("#lengthUO").val();;var lengthUP=$("#lengthUP").val();;var lengthUQ=$("#lengthUQ").val();;var lengthUR=$("#lengthUR").val();;var lengthUS=$("#lengthUS").val();;var lengthUT=$("#lengthUT").val();;var lengthUU=$("#lengthUU").val();;var lengthUV=$("#lengthUV").val();;var lengthUW=$("#lengthUW").val();;var lengthUX=$("#lengthUX").val();;var lengthUY=$("#lengthUY").val();;var lengthUZ=$("#lengthUZ").val();;var lengthVA=$("#lengthVA").val();;var lengthVB=$("#lengthVB").val();;var lengthVC=$("#lengthVC").val();;var lengthVD=$("#lengthVD").val();;var lengthVE=$("#lengthVE").val();;var lengthVF=$("#lengthVF").val();;var lengthVG=$("#lengthVG").val();;var lengthVH=$("#lengthVH").val();;var lengthVI=$("#lengthVI").val();;var lengthVJ=$("#lengthVJ").val();;var lengthVK=$("#lengthVK").val();;var lengthVL=$("#lengthVL").val();;var lengthVM=$("#lengthVM").val();;var lengthVN=$("#lengthVN").val();;var lengthVO=$("#lengthVO").val();;var lengthVP=$("#lengthVP").val();;var lengthVQ=$("#lengthVQ").val();;var lengthVR=$("#lengthVR").val();;var lengthVS=$("#lengthVS").val();;var lengthVT=$("#lengthVT").val();;var lengthVU=$("#lengthVU").val();;var lengthVV=$("#lengthVV").val();;var lengthVW=$("#lengthVW").val();;var lengthVX=$("#lengthVX").val();;var lengthVY=$("#lengthVY").val();;var lengthVZ=$("#lengthVZ").val();;var lengthWA=$("#lengthWA").val();;var lengthWB=$("#lengthWB").val();;var lengthWC=$("#lengthWC").val();;var lengthWD=$("#lengthWD").val();;var lengthWE=$("#lengthWE").val();;var lengthWF=$("#lengthWF").val();;var lengthWG=$("#lengthWG").val();;var lengthWH=$("#lengthWH").val();;var lengthWI=$("#lengthWI").val();;var lengthWJ=$("#lengthWJ").val();;var lengthWK=$("#lengthWK").val();;var lengthWL=$("#lengthWL").val();;var lengthWM=$("#lengthWM").val();;var lengthWN=$("#lengthWN").val();;var lengthWO=$("#lengthWO").val();;var lengthWP=$("#lengthWP").val();;var lengthWQ=$("#lengthWQ").val();;var lengthWR=$("#lengthWR").val();;var lengthWS=$("#lengthWS").val();;var lengthWT=$("#lengthWT").val();;var lengthWU=$("#lengthWU").val();;var lengthWV=$("#lengthWV").val();;var lengthWW=$("#lengthWW").val();;var lengthWX=$("#lengthWX").val();;var lengthWY=$("#lengthWY").val();;var lengthWZ=$("#lengthWZ").val();;var lengthXA=$("#lengthXA").val();;var lengthXB=$("#lengthXB").val();;var lengthXC=$("#lengthXC").val();;var lengthXD=$("#lengthXD").val();;var lengthXE=$("#lengthXE").val();;var lengthXF=$("#lengthXF").val();;var lengthXG=$("#lengthXG").val();;var lengthXH=$("#lengthXH").val();;var lengthXI=$("#lengthXI").val();;var lengthXJ=$("#lengthXJ").val();;var lengthXK=$("#lengthXK").val();;var lengthXL=$("#lengthXL").val();;var lengthXM=$("#lengthXM").val();;var lengthXN=$("#lengthXN").val();;var lengthXO=$("#lengthXO").val();;var lengthXP=$("#lengthXP").val();;var lengthXQ=$("#lengthXQ").val();;var lengthXR=$("#lengthXR").val();;var lengthXS=$("#lengthXS").val();;var lengthXT=$("#lengthXT").val();;var lengthXU=$("#lengthXU").val();;var lengthXV=$("#lengthXV").val();;var lengthXW=$("#lengthXW").val();;var lengthXX=$("#lengthXX").val();;var lengthXY=$("#lengthXY").val();;var lengthXZ=$("#lengthXZ").val();;var lengthYA=$("#lengthYA").val();;var lengthYB=$("#lengthYB").val();;var lengthYC=$("#lengthYC").val();;var lengthYD=$("#lengthYD").val();;var lengthYE=$("#lengthYE").val();;var lengthYF=$("#lengthYF").val();;var lengthYG=$("#lengthYG").val();;var lengthYH=$("#lengthYH").val();;var lengthYI=$("#lengthYI").val();;var lengthYJ=$("#lengthYJ").val();;var lengthYK=$("#lengthYK").val();;var lengthYL=$("#lengthYL").val();;var lengthYM=$("#lengthYM").val();;var lengthYN=$("#lengthYN").val();;var lengthYO=$("#lengthYO").val();;var lengthYP=$("#lengthYP").val();;var lengthYQ=$("#lengthYQ").val();;var lengthYR=$("#lengthYR").val();;var lengthYS=$("#lengthYS").val();;var lengthYT=$("#lengthYT").val();;var lengthYU=$("#lengthYU").val();;var lengthYV=$("#lengthYV").val();;var lengthYW=$("#lengthYW").val();;var lengthYX=$("#lengthYX").val();;var lengthYY=$("#lengthYY").val();;var lengthYZ=$("#lengthYZ").val();;		

	//for select
	var panel_types = [];    
    $(".panel_types :selected").each(function(){
        panel_types.push($(this).text()); 
    });
   // alert(panel_types);
    //return false;
//for angle	
	var angles = [];   
	$(".angles").each(function() {
		   angles.push($(this).attr('placeholder'));
	 });
	//alert(angles);
	
//for 	
	var outofplumbtextbox = [];   
	$(".outofplumbtextbox").each(function() {
		   outofplumbtextbox.push($(this).val());
	 });
	//alert(outofplumbtextbox);
		
	
	var job_no='29'; 
	
var door_width=$("#door_glass_width").val(); 
	 	$.ajax({
      type:'post',
      url:"date_save/partition_text_field.php",
      data:{panel_qty:panel_qty,job_no:job_no,shower_type:shower_type,lastcahr:lastcahr,lengthtextbox:lengthtextbox,panel_types:panel_types,angles:angles},
      success: function(data){
		  //window.location.reload(true);
        //alert(data);      
       //$("#added_hardware_detail").html(data);
         $("#check_dataaa").html(data);     
		 var mainarr=data;
		 var partarr = mainarr.split("/");
		 var noss=partarr.length;
		 
	//alert(partarr[0]);	
	//alert(partarr[1]);	
	//alert(partarr[2]);	
	//alert(partarr[3]);	
	//alert(partarr[4]);	
	//alert(partarr[5]);	
		
	 for(var art=0; art<noss; art++)
	 {
		var textvalss = partarr[art].split("-"); 
	//alert(textvalss[0]);	
	//alert(textvalss[1]);	
	//$("."+textvalss[0]+"").html(textvalss[1]);
	 }
	
	 	$(".textA").html(lengthA);$(".textB").html(lengthB);$(".textC").html(lengthC);$(".textD").html(lengthD);$(".textE").html(lengthE);$(".textF").html(lengthF);$(".textG").html(lengthG);$(".textH").html(lengthH);$(".textI").html(lengthI);$(".textJ").html(lengthJ);$(".textK").html(lengthK);$(".textL").html(lengthL);$(".textM").html(lengthM);$(".textN").html(lengthN);$(".textO").html(lengthO);$(".textP").html(lengthP);$(".textQ").html(lengthQ);$(".textR").html(lengthR);$(".textS").html(lengthS);$(".textT").html(lengthT);$(".textU").html(lengthU);$(".textV").html(lengthV);$(".textW").html(lengthW);$(".textX").html(lengthX);$(".textY").html(lengthY);$(".textZ").html(lengthZ);$(".textAA").html(lengthAA);$(".textAB").html(lengthAB);$(".textAC").html(lengthAC);$(".textAD").html(lengthAD);$(".textAE").html(lengthAE);$(".textAF").html(lengthAF);$(".textAG").html(lengthAG);$(".textAH").html(lengthAH);$(".textAI").html(lengthAI);$(".textAJ").html(lengthAJ);$(".textAK").html(lengthAK);$(".textAL").html(lengthAL);$(".textAM").html(lengthAM);$(".textAN").html(lengthAN);$(".textAO").html(lengthAO);$(".textAP").html(lengthAP);$(".textAQ").html(lengthAQ);$(".textAR").html(lengthAR);$(".textAS").html(lengthAS);$(".textAT").html(lengthAT);$(".textAU").html(lengthAU);$(".textAV").html(lengthAV);$(".textAW").html(lengthAW);$(".textAX").html(lengthAX);$(".textAY").html(lengthAY);$(".textAZ").html(lengthAZ);$(".textBA").html(lengthBA);$(".textBB").html(lengthBB);$(".textBC").html(lengthBC);$(".textBD").html(lengthBD);$(".textBE").html(lengthBE);$(".textBF").html(lengthBF);$(".textBG").html(lengthBG);$(".textBH").html(lengthBH);$(".textBI").html(lengthBI);$(".textBJ").html(lengthBJ);$(".textBK").html(lengthBK);$(".textBL").html(lengthBL);$(".textBM").html(lengthBM);$(".textBN").html(lengthBN);$(".textBO").html(lengthBO);$(".textBP").html(lengthBP);$(".textBQ").html(lengthBQ);$(".textBR").html(lengthBR);$(".textBS").html(lengthBS);$(".textBT").html(lengthBT);$(".textBU").html(lengthBU);$(".textBV").html(lengthBV);$(".textBW").html(lengthBW);$(".textBX").html(lengthBX);$(".textBY").html(lengthBY);$(".textBZ").html(lengthBZ);$(".textCA").html(lengthCA);$(".textCB").html(lengthCB);$(".textCC").html(lengthCC);$(".textCD").html(lengthCD);$(".textCE").html(lengthCE);$(".textCF").html(lengthCF);$(".textCG").html(lengthCG);$(".textCH").html(lengthCH);$(".textCI").html(lengthCI);$(".textCJ").html(lengthCJ);$(".textCK").html(lengthCK);$(".textCL").html(lengthCL);$(".textCM").html(lengthCM);$(".textCN").html(lengthCN);$(".textCO").html(lengthCO);$(".textCP").html(lengthCP);$(".textCQ").html(lengthCQ);$(".textCR").html(lengthCR);$(".textCS").html(lengthCS);$(".textCT").html(lengthCT);$(".textCU").html(lengthCU);$(".textCV").html(lengthCV);$(".textCW").html(lengthCW);$(".textCX").html(lengthCX);$(".textCY").html(lengthCY);$(".textCZ").html(lengthCZ);$(".textDA").html(lengthDA);$(".textDB").html(lengthDB);$(".textDC").html(lengthDC);$(".textDD").html(lengthDD);$(".textDE").html(lengthDE);$(".textDF").html(lengthDF);$(".textDG").html(lengthDG);$(".textDH").html(lengthDH);$(".textDI").html(lengthDI);$(".textDJ").html(lengthDJ);$(".textDK").html(lengthDK);$(".textDL").html(lengthDL);$(".textDM").html(lengthDM);$(".textDN").html(lengthDN);$(".textDO").html(lengthDO);$(".textDP").html(lengthDP);$(".textDQ").html(lengthDQ);$(".textDR").html(lengthDR);$(".textDS").html(lengthDS);$(".textDT").html(lengthDT);$(".textDU").html(lengthDU);$(".textDV").html(lengthDV);$(".textDW").html(lengthDW);$(".textDX").html(lengthDX);$(".textDY").html(lengthDY);$(".textDZ").html(lengthDZ);$(".textEA").html(lengthEA);$(".textEB").html(lengthEB);$(".textEC").html(lengthEC);$(".textED").html(lengthED);$(".textEE").html(lengthEE);$(".textEF").html(lengthEF);$(".textEG").html(lengthEG);$(".textEH").html(lengthEH);$(".textEI").html(lengthEI);$(".textEJ").html(lengthEJ);$(".textEK").html(lengthEK);$(".textEL").html(lengthEL);$(".textEM").html(lengthEM);$(".textEN").html(lengthEN);$(".textEO").html(lengthEO);$(".textEP").html(lengthEP);$(".textEQ").html(lengthEQ);$(".textER").html(lengthER);$(".textES").html(lengthES);$(".textET").html(lengthET);$(".textEU").html(lengthEU);$(".textEV").html(lengthEV);$(".textEW").html(lengthEW);$(".textEX").html(lengthEX);$(".textEY").html(lengthEY);$(".textEZ").html(lengthEZ);$(".textFA").html(lengthFA);$(".textFB").html(lengthFB);$(".textFC").html(lengthFC);$(".textFD").html(lengthFD);$(".textFE").html(lengthFE);$(".textFF").html(lengthFF);$(".textFG").html(lengthFG);$(".textFH").html(lengthFH);$(".textFI").html(lengthFI);$(".textFJ").html(lengthFJ);$(".textFK").html(lengthFK);$(".textFL").html(lengthFL);$(".textFM").html(lengthFM);$(".textFN").html(lengthFN);$(".textFO").html(lengthFO);$(".textFP").html(lengthFP);$(".textFQ").html(lengthFQ);$(".textFR").html(lengthFR);$(".textFS").html(lengthFS);$(".textFT").html(lengthFT);$(".textFU").html(lengthFU);$(".textFV").html(lengthFV);$(".textFW").html(lengthFW);$(".textFX").html(lengthFX);$(".textFY").html(lengthFY);$(".textFZ").html(lengthFZ);$(".textGA").html(lengthGA);$(".textGB").html(lengthGB);$(".textGC").html(lengthGC);$(".textGD").html(lengthGD);$(".textGE").html(lengthGE);$(".textGF").html(lengthGF);$(".textGG").html(lengthGG);$(".textGH").html(lengthGH);$(".textGI").html(lengthGI);$(".textGJ").html(lengthGJ);$(".textGK").html(lengthGK);$(".textGL").html(lengthGL);$(".textGM").html(lengthGM);$(".textGN").html(lengthGN);$(".textGO").html(lengthGO);$(".textGP").html(lengthGP);$(".textGQ").html(lengthGQ);$(".textGR").html(lengthGR);$(".textGS").html(lengthGS);$(".textGT").html(lengthGT);$(".textGU").html(lengthGU);$(".textGV").html(lengthGV);$(".textGW").html(lengthGW);$(".textGX").html(lengthGX);$(".textGY").html(lengthGY);$(".textGZ").html(lengthGZ);$(".textHA").html(lengthHA);$(".textHB").html(lengthHB);$(".textHC").html(lengthHC);$(".textHD").html(lengthHD);$(".textHE").html(lengthHE);$(".textHF").html(lengthHF);$(".textHG").html(lengthHG);$(".textHH").html(lengthHH);$(".textHI").html(lengthHI);$(".textHJ").html(lengthHJ);$(".textHK").html(lengthHK);$(".textHL").html(lengthHL);$(".textHM").html(lengthHM);$(".textHN").html(lengthHN);$(".textHO").html(lengthHO);$(".textHP").html(lengthHP);$(".textHQ").html(lengthHQ);$(".textHR").html(lengthHR);$(".textHS").html(lengthHS);$(".textHT").html(lengthHT);$(".textHU").html(lengthHU);$(".textHV").html(lengthHV);$(".textHW").html(lengthHW);$(".textHX").html(lengthHX);$(".textHY").html(lengthHY);$(".textHZ").html(lengthHZ);$(".textIA").html(lengthIA);$(".textIB").html(lengthIB);$(".textIC").html(lengthIC);$(".textID").html(lengthID);$(".textIE").html(lengthIE);$(".textIF").html(lengthIF);$(".textIG").html(lengthIG);$(".textIH").html(lengthIH);$(".textII").html(lengthII);$(".textIJ").html(lengthIJ);$(".textIK").html(lengthIK);$(".textIL").html(lengthIL);$(".textIM").html(lengthIM);$(".textIN").html(lengthIN);$(".textIO").html(lengthIO);$(".textIP").html(lengthIP);$(".textIQ").html(lengthIQ);$(".textIR").html(lengthIR);$(".textIS").html(lengthIS);$(".textIT").html(lengthIT);$(".textIU").html(lengthIU);$(".textIV").html(lengthIV);$(".textIW").html(lengthIW);$(".textIX").html(lengthIX);$(".textIY").html(lengthIY);$(".textIZ").html(lengthIZ);$(".textJA").html(lengthJA);$(".textJB").html(lengthJB);$(".textJC").html(lengthJC);$(".textJD").html(lengthJD);$(".textJE").html(lengthJE);$(".textJF").html(lengthJF);$(".textJG").html(lengthJG);$(".textJH").html(lengthJH);$(".textJI").html(lengthJI);$(".textJJ").html(lengthJJ);$(".textJK").html(lengthJK);$(".textJL").html(lengthJL);$(".textJM").html(lengthJM);$(".textJN").html(lengthJN);$(".textJO").html(lengthJO);$(".textJP").html(lengthJP);$(".textJQ").html(lengthJQ);$(".textJR").html(lengthJR);$(".textJS").html(lengthJS);$(".textJT").html(lengthJT);$(".textJU").html(lengthJU);$(".textJV").html(lengthJV);$(".textJW").html(lengthJW);$(".textJX").html(lengthJX);$(".textJY").html(lengthJY);$(".textJZ").html(lengthJZ);$(".textKA").html(lengthKA);$(".textKB").html(lengthKB);$(".textKC").html(lengthKC);$(".textKD").html(lengthKD);$(".textKE").html(lengthKE);$(".textKF").html(lengthKF);$(".textKG").html(lengthKG);$(".textKH").html(lengthKH);$(".textKI").html(lengthKI);$(".textKJ").html(lengthKJ);$(".textKK").html(lengthKK);$(".textKL").html(lengthKL);$(".textKM").html(lengthKM);$(".textKN").html(lengthKN);$(".textKO").html(lengthKO);$(".textKP").html(lengthKP);$(".textKQ").html(lengthKQ);$(".textKR").html(lengthKR);$(".textKS").html(lengthKS);$(".textKT").html(lengthKT);$(".textKU").html(lengthKU);$(".textKV").html(lengthKV);$(".textKW").html(lengthKW);$(".textKX").html(lengthKX);$(".textKY").html(lengthKY);$(".textKZ").html(lengthKZ);$(".textLA").html(lengthLA);$(".textLB").html(lengthLB);$(".textLC").html(lengthLC);$(".textLD").html(lengthLD);$(".textLE").html(lengthLE);$(".textLF").html(lengthLF);$(".textLG").html(lengthLG);$(".textLH").html(lengthLH);$(".textLI").html(lengthLI);$(".textLJ").html(lengthLJ);$(".textLK").html(lengthLK);$(".textLL").html(lengthLL);$(".textLM").html(lengthLM);$(".textLN").html(lengthLN);$(".textLO").html(lengthLO);$(".textLP").html(lengthLP);$(".textLQ").html(lengthLQ);$(".textLR").html(lengthLR);$(".textLS").html(lengthLS);$(".textLT").html(lengthLT);$(".textLU").html(lengthLU);$(".textLV").html(lengthLV);$(".textLW").html(lengthLW);$(".textLX").html(lengthLX);$(".textLY").html(lengthLY);$(".textLZ").html(lengthLZ);$(".textMA").html(lengthMA);$(".textMB").html(lengthMB);$(".textMC").html(lengthMC);$(".textMD").html(lengthMD);$(".textME").html(lengthME);$(".textMF").html(lengthMF);$(".textMG").html(lengthMG);$(".textMH").html(lengthMH);$(".textMI").html(lengthMI);$(".textMJ").html(lengthMJ);$(".textMK").html(lengthMK);$(".textML").html(lengthML);$(".textMM").html(lengthMM);$(".textMN").html(lengthMN);$(".textMO").html(lengthMO);$(".textMP").html(lengthMP);$(".textMQ").html(lengthMQ);$(".textMR").html(lengthMR);$(".textMS").html(lengthMS);$(".textMT").html(lengthMT);$(".textMU").html(lengthMU);$(".textMV").html(lengthMV);$(".textMW").html(lengthMW);$(".textMX").html(lengthMX);$(".textMY").html(lengthMY);$(".textMZ").html(lengthMZ);$(".textNA").html(lengthNA);$(".textNB").html(lengthNB);$(".textNC").html(lengthNC);$(".textND").html(lengthND);$(".textNE").html(lengthNE);$(".textNF").html(lengthNF);$(".textNG").html(lengthNG);$(".textNH").html(lengthNH);$(".textNI").html(lengthNI);$(".textNJ").html(lengthNJ);$(".textNK").html(lengthNK);$(".textNL").html(lengthNL);$(".textNM").html(lengthNM);$(".textNN").html(lengthNN);$(".textNO").html(lengthNO);$(".textNP").html(lengthNP);$(".textNQ").html(lengthNQ);$(".textNR").html(lengthNR);$(".textNS").html(lengthNS);$(".textNT").html(lengthNT);$(".textNU").html(lengthNU);$(".textNV").html(lengthNV);$(".textNW").html(lengthNW);$(".textNX").html(lengthNX);$(".textNY").html(lengthNY);$(".textNZ").html(lengthNZ);$(".textOA").html(lengthOA);$(".textOB").html(lengthOB);$(".textOC").html(lengthOC);$(".textOD").html(lengthOD);$(".textOE").html(lengthOE);$(".textOF").html(lengthOF);$(".textOG").html(lengthOG);$(".textOH").html(lengthOH);$(".textOI").html(lengthOI);$(".textOJ").html(lengthOJ);$(".textOK").html(lengthOK);$(".textOL").html(lengthOL);$(".textOM").html(lengthOM);$(".textON").html(lengthON);$(".textOO").html(lengthOO);$(".textOP").html(lengthOP);$(".textOQ").html(lengthOQ);$(".textOR").html(lengthOR);$(".textOS").html(lengthOS);$(".textOT").html(lengthOT);$(".textOU").html(lengthOU);$(".textOV").html(lengthOV);$(".textOW").html(lengthOW);$(".textOX").html(lengthOX);$(".textOY").html(lengthOY);$(".textOZ").html(lengthOZ);$(".textPA").html(lengthPA);$(".textPB").html(lengthPB);$(".textPC").html(lengthPC);$(".textPD").html(lengthPD);$(".textPE").html(lengthPE);$(".textPF").html(lengthPF);$(".textPG").html(lengthPG);$(".textPH").html(lengthPH);$(".textPI").html(lengthPI);$(".textPJ").html(lengthPJ);$(".textPK").html(lengthPK);$(".textPL").html(lengthPL);$(".textPM").html(lengthPM);$(".textPN").html(lengthPN);$(".textPO").html(lengthPO);$(".textPP").html(lengthPP);$(".textPQ").html(lengthPQ);$(".textPR").html(lengthPR);$(".textPS").html(lengthPS);$(".textPT").html(lengthPT);$(".textPU").html(lengthPU);$(".textPV").html(lengthPV);$(".textPW").html(lengthPW);$(".textPX").html(lengthPX);$(".textPY").html(lengthPY);$(".textPZ").html(lengthPZ);$(".textQA").html(lengthQA);$(".textQB").html(lengthQB);$(".textQC").html(lengthQC);$(".textQD").html(lengthQD);$(".textQE").html(lengthQE);$(".textQF").html(lengthQF);$(".textQG").html(lengthQG);$(".textQH").html(lengthQH);$(".textQI").html(lengthQI);$(".textQJ").html(lengthQJ);$(".textQK").html(lengthQK);$(".textQL").html(lengthQL);$(".textQM").html(lengthQM);$(".textQN").html(lengthQN);$(".textQO").html(lengthQO);$(".textQP").html(lengthQP);$(".textQQ").html(lengthQQ);$(".textQR").html(lengthQR);$(".textQS").html(lengthQS);$(".textQT").html(lengthQT);$(".textQU").html(lengthQU);$(".textQV").html(lengthQV);$(".textQW").html(lengthQW);$(".textQX").html(lengthQX);$(".textQY").html(lengthQY);$(".textQZ").html(lengthQZ);$(".textRA").html(lengthRA);$(".textRB").html(lengthRB);$(".textRC").html(lengthRC);$(".textRD").html(lengthRD);$(".textRE").html(lengthRE);$(".textRF").html(lengthRF);$(".textRG").html(lengthRG);$(".textRH").html(lengthRH);$(".textRI").html(lengthRI);$(".textRJ").html(lengthRJ);$(".textRK").html(lengthRK);$(".textRL").html(lengthRL);$(".textRM").html(lengthRM);$(".textRN").html(lengthRN);$(".textRO").html(lengthRO);$(".textRP").html(lengthRP);$(".textRQ").html(lengthRQ);$(".textRR").html(lengthRR);$(".textRS").html(lengthRS);$(".textRT").html(lengthRT);$(".textRU").html(lengthRU);$(".textRV").html(lengthRV);$(".textRW").html(lengthRW);$(".textRX").html(lengthRX);$(".textRY").html(lengthRY);$(".textRZ").html(lengthRZ);$(".textSA").html(lengthSA);$(".textSB").html(lengthSB);$(".textSC").html(lengthSC);$(".textSD").html(lengthSD);$(".textSE").html(lengthSE);$(".textSF").html(lengthSF);$(".textSG").html(lengthSG);$(".textSH").html(lengthSH);$(".textSI").html(lengthSI);$(".textSJ").html(lengthSJ);$(".textSK").html(lengthSK);$(".textSL").html(lengthSL);$(".textSM").html(lengthSM);$(".textSN").html(lengthSN);$(".textSO").html(lengthSO);$(".textSP").html(lengthSP);$(".textSQ").html(lengthSQ);$(".textSR").html(lengthSR);$(".textSS").html(lengthSS);$(".textST").html(lengthST);$(".textSU").html(lengthSU);$(".textSV").html(lengthSV);$(".textSW").html(lengthSW);$(".textSX").html(lengthSX);$(".textSY").html(lengthSY);$(".textSZ").html(lengthSZ);$(".textTA").html(lengthTA);$(".textTB").html(lengthTB);$(".textTC").html(lengthTC);$(".textTD").html(lengthTD);$(".textTE").html(lengthTE);$(".textTF").html(lengthTF);$(".textTG").html(lengthTG);$(".textTH").html(lengthTH);$(".textTI").html(lengthTI);$(".textTJ").html(lengthTJ);$(".textTK").html(lengthTK);$(".textTL").html(lengthTL);$(".textTM").html(lengthTM);$(".textTN").html(lengthTN);$(".textTO").html(lengthTO);$(".textTP").html(lengthTP);$(".textTQ").html(lengthTQ);$(".textTR").html(lengthTR);$(".textTS").html(lengthTS);$(".textTT").html(lengthTT);$(".textTU").html(lengthTU);$(".textTV").html(lengthTV);$(".textTW").html(lengthTW);$(".textTX").html(lengthTX);$(".textTY").html(lengthTY);$(".textTZ").html(lengthTZ);$(".textUA").html(lengthUA);$(".textUB").html(lengthUB);$(".textUC").html(lengthUC);$(".textUD").html(lengthUD);$(".textUE").html(lengthUE);$(".textUF").html(lengthUF);$(".textUG").html(lengthUG);$(".textUH").html(lengthUH);$(".textUI").html(lengthUI);$(".textUJ").html(lengthUJ);$(".textUK").html(lengthUK);$(".textUL").html(lengthUL);$(".textUM").html(lengthUM);$(".textUN").html(lengthUN);$(".textUO").html(lengthUO);$(".textUP").html(lengthUP);$(".textUQ").html(lengthUQ);$(".textUR").html(lengthUR);$(".textUS").html(lengthUS);$(".textUT").html(lengthUT);$(".textUU").html(lengthUU);$(".textUV").html(lengthUV);$(".textUW").html(lengthUW);$(".textUX").html(lengthUX);$(".textUY").html(lengthUY);$(".textUZ").html(lengthUZ);$(".textVA").html(lengthVA);$(".textVB").html(lengthVB);$(".textVC").html(lengthVC);$(".textVD").html(lengthVD);$(".textVE").html(lengthVE);$(".textVF").html(lengthVF);$(".textVG").html(lengthVG);$(".textVH").html(lengthVH);$(".textVI").html(lengthVI);$(".textVJ").html(lengthVJ);$(".textVK").html(lengthVK);$(".textVL").html(lengthVL);$(".textVM").html(lengthVM);$(".textVN").html(lengthVN);$(".textVO").html(lengthVO);$(".textVP").html(lengthVP);$(".textVQ").html(lengthVQ);$(".textVR").html(lengthVR);$(".textVS").html(lengthVS);$(".textVT").html(lengthVT);$(".textVU").html(lengthVU);$(".textVV").html(lengthVV);$(".textVW").html(lengthVW);$(".textVX").html(lengthVX);$(".textVY").html(lengthVY);$(".textVZ").html(lengthVZ);$(".textWA").html(lengthWA);$(".textWB").html(lengthWB);$(".textWC").html(lengthWC);$(".textWD").html(lengthWD);$(".textWE").html(lengthWE);$(".textWF").html(lengthWF);$(".textWG").html(lengthWG);$(".textWH").html(lengthWH);$(".textWI").html(lengthWI);$(".textWJ").html(lengthWJ);$(".textWK").html(lengthWK);$(".textWL").html(lengthWL);$(".textWM").html(lengthWM);$(".textWN").html(lengthWN);$(".textWO").html(lengthWO);$(".textWP").html(lengthWP);$(".textWQ").html(lengthWQ);$(".textWR").html(lengthWR);$(".textWS").html(lengthWS);$(".textWT").html(lengthWT);$(".textWU").html(lengthWU);$(".textWV").html(lengthWV);$(".textWW").html(lengthWW);$(".textWX").html(lengthWX);$(".textWY").html(lengthWY);$(".textWZ").html(lengthWZ);$(".textXA").html(lengthXA);$(".textXB").html(lengthXB);$(".textXC").html(lengthXC);$(".textXD").html(lengthXD);$(".textXE").html(lengthXE);$(".textXF").html(lengthXF);$(".textXG").html(lengthXG);$(".textXH").html(lengthXH);$(".textXI").html(lengthXI);$(".textXJ").html(lengthXJ);$(".textXK").html(lengthXK);$(".textXL").html(lengthXL);$(".textXM").html(lengthXM);$(".textXN").html(lengthXN);$(".textXO").html(lengthXO);$(".textXP").html(lengthXP);$(".textXQ").html(lengthXQ);$(".textXR").html(lengthXR);$(".textXS").html(lengthXS);$(".textXT").html(lengthXT);$(".textXU").html(lengthXU);$(".textXV").html(lengthXV);$(".textXW").html(lengthXW);$(".textXX").html(lengthXX);$(".textXY").html(lengthXY);$(".textXZ").html(lengthXZ);$(".textYA").html(lengthYA);$(".textYB").html(lengthYB);$(".textYC").html(lengthYC);$(".textYD").html(lengthYD);$(".textYE").html(lengthYE);$(".textYF").html(lengthYF);$(".textYG").html(lengthYG);$(".textYH").html(lengthYH);$(".textYI").html(lengthYI);$(".textYJ").html(lengthYJ);$(".textYK").html(lengthYK);$(".textYL").html(lengthYL);$(".textYM").html(lengthYM);$(".textYN").html(lengthYN);$(".textYO").html(lengthYO);$(".textYP").html(lengthYP);$(".textYQ").html(lengthYQ);$(".textYR").html(lengthYR);$(".textYS").html(lengthYS);$(".textYT").html(lengthYT);$(".textYU").html(lengthYU);$(".textYV").html(lengthYV);$(".textYW").html(lengthYW);$(".textYX").html(lengthYX);$(".textYY").html(lengthYY);$(".textYZ").html(lengthYZ);	 
	  
		

	}
      });
	 
	 
	 

	
			
	  
	
}

function update_hardware_color(form){
	
	var added_id=$("#added_id").val();
	var hardware_color_id=$('#hardware_color').val();
	var hardware_color_name=$('#hardware_color :selected').text();
	//alert(hardware_color_name);
	if(hardware_color_name=='')
	{
		window.location.reload(true);
	}
else{
	$.ajax({
      type:'post',
      url:"date_save/update_hardware_color_for_order.php",
      data:{added_id:added_id,hardware_color_id:hardware_color_id,hardware_color_name:hardware_color_name},
      success: function(data){
		  window.location.reload(true);
       //$("#added_hardware_detail").html(data);
       // alert(data);      
                
	}
      });
}
}
function remove_hardware(form){
	var added_id=$("input[name='hardware_id_re']:checked").val();
	//alert(hardware_id);
		if(added_id=='0'){
			alert('please select hardware for remove');
		}
		
		else{
			$.ajax({
      type:'post',
      url:"date_save/remove_hardware_order.php",
      data:{added_id:added_id},
      success: function(data){
		  window.location.reload(true);
       //$("#added_hardware_detail").html(data);
       // alert(data);      
                
	}
      });
		}

}

function add_hardware(form){
		
		var cate_id=$('#category_name').val();
		var cate_name=$('#category_name :selected').text();
		var customer_id='2';
		var cate_type_id=$('#category_type').val();
		var cate_type_name=$('#category_type :selected').text();
		
		var hardware_id=$('#hardware_detail').val();
		var hardware_name=$('#hardware_detail :selected').text();
		var order_no=1;
		var job_no=29;
		var quantity=$('#quantity').val();
		if(quantity=='')
		{
			var quantity='1';
		}
		
		//alert(cate_name);
		//alert(cate_type_id);
		//alert(hardware_id);
		
		if(hardware_id>0 && cate_type_id>0){
		$.ajax({
      type:'post',
      url:"date_save/add_hardware_for_order.php",
      data:{cate_id:cate_id,cate_name:cate_name,cate_type_id:cate_type_id,cate_type_name:cate_type_name,hardware_id:hardware_id,hardware_name:hardware_name,order_no:order_no,job_no:job_no,quantity:quantity,customer_id:customer_id},
      success: function(data){
		  window.location.reload(true);
       //$("#added_hardware_detail").html(data);
       // alert(data);      
                
	}
      });
	  //alert('yes');
		}
		
		else{
			alert('please select hardware color');
		}
		
		
	
}

function show_hardware_detail(form){
    
        
		var cat_color_id=$('#category_type').val();
		var cat_color=$('#category_type :selected').text();
		//var cattype_val=$(this).val();
	
		//alert(cat_color);
		 $.ajax({
      type:'post',
      url:"date_save/get_cate_color.php",
      data:{cat_color_id:cat_color_id},
      success: function(data){
		  //window.location.reload(true);
       $("#hardware_detail").html(data);
       // alert(data);      
                
	}
      });
		//document.getElementById("cattype").value = cattype;
		
		
		$("#cattype").text(cat_color);
    
}





function show_cat_type(form){
    
        
		var cattype_val=$('#category_name').val();
		var cattype=$('#category_name :selected').text();
		//var cattype_val=$(this).val();
	
		//alert(cattype);
		 $.ajax({
      type:'post',
      url:"date_save/get_cate_type.php",
      data:{cat_id:cattype_val},
      success: function(data){
		  //window.location.reload(true);
       $("#category_type").html(data);
       // alert(data);      
                
	}
      });
		//document.getElementById("cattype").value = cattype;
		
		
		$("#cattype11").text(cattype);
    
}

function openDetails(evt, detailName) {
  var i, tabcontentss, tablinks;
  tabcontentss = document.getElementsByClassName("tabcontentss");
  for (i = 0; i < tabcontentss.length; i++) {
    tabcontentss[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(detailName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>  


</div>

</body>
</html>
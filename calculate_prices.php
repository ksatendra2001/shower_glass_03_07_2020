<?php 

include('includes/header.php');

$material_price=$material_gross=$glass_price=$labour_per_fixed_based_panel_rate1=$labour_per_fixed_based_panel_rate=$labperfixbaspan_linecost=$labperfixbaspan_lineprice=$labour_per_door_rate=$labperdoor_linecost=$labperdoor_lineprice=$labour_per_glass_area_rate=$labperglasarea_linecost=$labperglasarea_lineprice=$labour_travel_time_rate=$labtravel_linecost=$labtravel_lineprice=$travell_expence_rate=$travelexpence_linecost=$travelexpence_lineprice=$accommondation_rate=$accommondation_linecost=$accommondation_lineprice=$other_rate1=$other_linecost=$other_lineprice=$linepricetotal=$discount_price=$linecosttotal=$grosstotal=$sub_total=$discount_amoun=$taxamount=$total_amount='0.00';

$labour_per_fixed_based_panel_qty='2';
$labour_per_door_qty='1';
$labour_per_glass_area_qty='2.14';
$labour_travel_time_qty=$travell_expence_qty=$accommondation_qty=$other_qty='0';


$job_no=$_GET['job_no'];
$get_all_data="SELECT * FROM quote_prices WHERE job_no='".$_GET['job_no']."'";
$exe_data=mysqli_query($con,$get_all_data);
$get_data=mysqli_fetch_array($exe_data);
if($get_data){
$material_qty=$get_data['material_qty'];
$material_price=$get_data['material_price'];
$material_gross=''.$material_price.'';
$glass_qty=$get_data['glass_qty'];
$glass_price=$get_data['glass_price'];
$glass_gross=''.$glass_price.'';


$labour_per_fixed_based_panel_qty=$get_data['labour_per_fixed_based_panel_qty'];
$labour_per_fixed_based_panel_rate=$get_data['labour_per_fixed_based_panel_rate'];
$labperfixbaspan_linecost=$labperfixbaspan_lineprice=''.($labour_per_fixed_based_panel_rate*$labour_per_fixed_based_panel_qty).'.00';

$labour_per_door_qty=$get_data['labour_per_door_qty'];
$labour_per_door_rate=$get_data['labour_per_door_rate'];
$labperdoor_linecost=$labperdoor_lineprice=''.($labour_per_door_rate*$labour_per_door_qty).'.00';

$labour_per_glass_area_qty=$get_data['labour_per_glass_area_qty'];
$labour_per_glass_area_rate=$get_data['labour_per_glass_area_rate'];
$labperglasarea_linecost=$labperglasarea_lineprice=''.($labour_per_glass_area_rate*$labour_per_glass_area_qty).'.00';

$labour_travel_time_qty=$get_data['labour_travel_time_qty'];
$labour_travel_time_rate=$get_data['labour_travel_time_rate'];
$labtravel_linecost=$labtravel_lineprice=''.($labour_travel_time_rate*$labour_travel_time_qty).'.00';

$travell_expence_qty=$get_data['travell_expence_qty'];
$travell_expence_rate=$get_data['travell_expence_rate'];
$travelexpence_linecost=$travelexpence_lineprice=''.($travell_expence_rate*$travell_expence_qty).'.00';

$accommondation_qty=$get_data['accommondation_qty'];
$accommondation_rate=$get_data['accommondation_rate'];
$accommondation_linecost=$accommondation_lineprice=''.($accommondation_rate*$accommondation_qty).'.00';

$other_qty=$get_data['other_qty'];
$other_rate=$get_data['other_rate'];
$other_linecost=$other_lineprice=''.($other_rate*$other_qty).'.00';

$linepricetotal=$get_data['linepricetotal'];

$discount_rate=$get_data['discount_rate'];
$discount_price=$get_data['discount_price'];


$linecosttotal=$get_data['linecosttotal'];
$total_markup_per=$get_data['total_markup_per'];

$grosstotal=$get_data['grosstotal'];
$marginpercent=$get_data['marginpercent'];
//$sub_total=$get_data['sub_total'];
//$tax_name=$get_data['tax_name'];
//$tax_rate=$get_data['tax_rate'];

$discount_amoun=$get_data['discount_amoun'];
//$taxamount=$get_data['taxamount'];
//$total_amount=$get_data['total_amount'];

}
else{
$material_qty=1;
$glass_gross=0;
$glass_qty=1;
$discount_rate=0;
$other_rate=0;
}



?>





<div class="calculate-price-main" id="calculate-price-main">
<table style="width:100%; " class="cost-table">
<tr style="background-color:#c5c5c5; border-top:1px solid #c5c5c5;text-align: left;">
<th style="width:30%;font-size:9px;">Item</th>
<th style="width:10%;font-size:9px;">Quantity</th>
<th style="width:10%;font-size:9px;display:none;" class="costs">Cost Rate</th>
<th style="width:10%;font-size:9px;display:none;" class="costs">Line Cost</th>
<th style="width:10%;font-size:9px;display:none;" class="costs">Mark-up</th>
<th style="width:10%;font-size:9px;display:none;" class="costs">Gross Profit</th>
<th style="width:10%;font-size:9px;display:none;" class="costs">Margin</th>
<th style="width:10%;font-size:9px;">Line Price</th>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Material</td>
<td style="width:10%;font-size:9px;"><span id="material_qty"><?php echo$material_qty ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="material_gross"><?php echo$material_gross ?><span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;"><input type="text"  name="material_line_price" id="material_line_price" value="$<?php echo$material_price ?>"  readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Glass</td>
<td style="width:10%;font-size:9px;"><span id="glass_qty"><?php echo$glass_qty ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="glass_gross"><?php echo$glass_gross ?><span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;"><input type="text" name="glass_line_price" id="glass_line_price" value="$<?php echo$glass_price ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Labour Per Fixed Based Panel</td>
<td style="width:10%;font-size:9px;"><span id="labour_per_fixed_based_panel_qty"><?php echo$labour_per_fixed_based_panel_qty ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"><input type="text" name="labour_per_fix_cost_rate" id="labour_per_fix_cost_rate" value="$<?php echo$labour_per_fixed_based_panel_rate ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="labour_per_fix_line_cost"><?php echo$labperfixbaspan_linecost ?><span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;" >$<span id="labour_per_fix_line_price"><?php echo$labperfixbaspan_lineprice ?><span></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Labour Per Door</td>
<td style="width:10%;font-size:9px;"><span id="labour_per_door_qty"><?php echo$labour_per_door_qty ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"><input type="text" name="labour_per_door_cost_rate" id="labour_per_door_cost_rate" value="$<?php echo$labour_per_door_rate ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="labour_per_door_line_cost"><?php echo$labperdoor_linecost ?><span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;">$<span id="labour_per_door_line_price"><?php echo$labperdoor_lineprice ?><span></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Labour Per Glass Area</td>
<td style="width:10%;font-size:9px;"><span id="labour_per_glass_area_qty"><?php echo$labour_per_glass_area_qty ?></span> ft<sup>2</sup></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"><input type="text" name="labour_per_glass_cost_rate" id="labour_per_glass_cost_rate" value="$<?php echo$labour_per_glass_area_rate ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="labour_per_glass_line_cost"><?php echo$labperglasarea_linecost ?><span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;">$<span id="labour_per_glass_line_price"><?php echo$labperglasarea_lineprice ?><span></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Labour In Travel Time</td>
<td style="width:10%;font-size:9px;"><input type="text" name="labour_travel_time_qty" id="labour_travel_time_qty" value="<?php echo$labour_travel_time_qty ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"><input type="text" name="labour_travel_time_cost_rate" id="labour_travel_time_cost_rate" value="$<?php echo$labour_travel_time_rate ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="labour_travel_time_line_cost"><?php echo$labtravel_linecost ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;">$<span id="labour_travel_time_line_price"><?php echo$labtravel_lineprice ?></span></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Travel Expenses</td>
<td style="width:10%;font-size:9px;"><input type="text" name="travelexpence_qty" id="travelexpence_qty" value="<?php echo$travell_expence_qty ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"><input type="text" name="travelexpence_cost_rate" id="travelexpence_cost_rate" value="$<?php echo$travell_expence_rate ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="travelexpence_line_cost"><?php echo$travelexpence_linecost ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;">$<span id="travelexpence_line_price"><?php echo$travelexpence_lineprice ?></span></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Accommodation</td>
<td style="width:10%;font-size:9px;"><input type="text" name="accommodation_qty" id="accommodation_qty" value="<?php echo$accommondation_qty ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"><input type="text" name="accommodation_cost_rate" id="accommodation_cost_rate" value="$<?php echo$accommondation_rate ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="accommodation_line_cost"><?php echo$accommondation_linecost ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;">$<span id="accommodation_line_price"><?php echo$accommondation_lineprice ?></span></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Other</td>
<td style="width:10%;font-size:9px;"><input type="text" name="other_qty" id="other_qty" value="<?php echo$other_qty ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs"><input type="text" name="other_cost_rate" id="other_cost_rate" value="$<?php echo$other_rate ?>" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$<span id="other_line_cost"><?php echo$other_linecost ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">0%</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">$0.00</td>
<td style="width:10%;font-size:9px;display:none;" class="costs"></td>
<td style="width:10%;font-size:9px;">$<span id="other_line_price"><?php echo$other_lineprice ?></span></td>
</tr>
<tr style="text-align: left;">
<td style="width:30%;font-size:9px;">Discount</td>
<td style="width:10%;font-size:9px;"><input type="text" name="discount_percent" id="discount_percent" value="<?php echo$discount_rate ?>%" readonly="true" ondblclick="this.readOnly='';" onchange="update_price_calculated(this.form)" /></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">-$<span id="dicount_amount_costrate"><?php echo$discount_price ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">N/A</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">N/A</td>
<td style="width:10%;font-size:9px;display:none;" class="costs">-$<span id="dicount_amount_gross"><?php echo$discount_price ?></span></td>
<td style="width:10%;font-size:9px;display:none;" class="costs">N/A</td>
<td style="width:10%;font-size:9px;">-$<span id="dicount_amount_line_price">$<?php echo$discount_price ?></span></td>
</tr>
</table>
</div>
<div class="calculate-price1">
<input type="text" class="costs" value="$<?php echo$linecosttotal ?>" id="linecosttotal" name="linecosttotal" style="width:13%;height: 8%;display:none;" />
<input type="text" class="costs" value="0%" id="total_markup_per" name="total_markup_per" style="width:10%;height: 8%;display:none;" />
<input type="text" class="costs" value="$<?php echo$grosstotal ?>" id="grosstotal" name="grosstotal" style="width:13%;height: 8%;display:none;" />
<input type="text" class="costs" value="0%" id="marginpercent" name="marginpercent" style="width:13%;height: 8%;display:none;" />
<input type="text"  value="$<?php echo$linepricetotal ?>" id="linepricetotal" name="linepricetotal" style="width:13%;height: 8%;" />
</div>

<?php
$get_job_id="SELECT * FROM `temp_save_price` WHERE `job_id` ='$job_no'";
		$exe_job_id=mysqli_query($con,$get_job_id);
		$fetch_job_id = mysqli_fetch_array($exe_job_id);
		$job_idssss=$fetch_job_id['job_id'];
		if($job_idssss){
		$sub_total='$'.$fetch_job_id['sub_total'].'';
		$gst_amount='$'.$fetch_job_id['gst_amount'].'';
		$total_amount='$'.$fetch_job_id['total_amount'].'';
		$tax_name=''.$fetch_job_id['tax_name'].'';
		$gst_percent=''.$fetch_job_id['gst_percent'].'';
		}
		else{
			$sub_total='$0.00';
			$gst_amount='$0.00';
			$total_amount='$0.00';
			$tax_name='GST';
			$gst_percent='9.75';
		}
?>


<div class="buttons-div">
<div class="button-container">
  <div class="button-container1">
  	<!-- <button>Add Item</button> <button>Remove Item</button> -->
  </div>
  <div class="button-container2"><button class="show_detail" id="show_detail" style="float:left;margin-right:4px;display:block;">Show Detail</button> <button class="hide_detail" id="hide_detail" style="float:left;margin-right:4px;display:none;">Hide Detail</button> 
  <button class="show_cost" onclick="show_cost(this.form)" id="show_cost">Show Cost</button> 
  <button class="hide_cost" onclick="hide_cost(this.form)" style="display:none;" id="hide_cost">Hide Cost</button></div>  
  <div class="button-container3"><button class="" onclick="update_calculated_subtotals(this.form);">Update Subtotal</button>
  <input type="text" id="sub_total" name="total" onchange="change_taxes(this.form)" value="<?php echo$sub_total ?>" style="width:40%;height: 8%;"></div>
</div>
</div>

<div class="totalcalculate-div" style="height: 9%;border-bottom:2px solid #c5c5c5;">
<div style="margin:5px;">
Sale Tax Method <select name="sale_price_method" id="sale_price_method">
<option>Quote Price excludes tax</option>
<option>Quote Price includes tax</option>
<option selected>Calculate tax quote price</option>
</select>

Tax Name <input type="text" name="tax_name" id="tax_name" onchange="change_taxes(this.form)" value="<?php echo$tax_name ?>" style="width:6%;">
Tax Rate <input type="text" name="tax_rate" id="tax_rate" onchange="change_taxes(this.form)" value="<?php echo$gst_percent ?>%" style="width:5%;">
Tax Amount <input type="text" name="tax_amount" id="tax_amount" disabled value="<?php echo$gst_amount ?>" style="width:9%;">
Total Price <input type="text" name="total_price" id="total_price" disabled value="<?php echo$total_amount ?>" style="width:9%;">
</div>

</div>

<!-- Bottom Section -->
<div style="width:100%;display:none;margin-top: 8px;border: 1px solid #c5c5c5;height: 60%;" id="show_detail_div">
			<div style="background-color: #c5c5c5;border-top: 1px solid #c5c5c5;text-align: left;width: 100%;height: 13px">
				<div style="width: 61.9%;font-size: 9px;float: left;"><b>Shower Components</b></div>
				<div style="width: 2px;float: left;background: white;height: 15px;margin-top: -2px;"></div>
				<div style="width: 37%;font-size: 9px;float: left;padding-left: 2px;"><b>Pricing</b></div>
			</div>
	<table style="width: 100%;background: #F6F6F6;">
		<tbody>
			<tr style="background-color: #c5c5c5;border-top: 1px solid #c5c5c5;text-align: left;">
				<th style="width: 22%;font-size: 9px;">Description</th>
				<th style="width: 10%;font-size: 9px;">Code</th>
				<th style="width: 10%;font-size: 9px;">Size</th>
				<th style="width: 10%;font-size: 9px;">Qty</th>
				<th style="width: 10%;font-size: 9px;">Rate Qty</th>
				<th style="width: 8%;font-size: 9px;">Cost Plus</th>
				<th style="width: 10%;font-size: 9px;">Price Rate</th>
				<th style="width: 10%;font-size: 9px;">Discount</th>
				<th style="width: 10%;font-size: 9px;">Total Price</th>
			</tr>
<?php
	
	$sql = "SELECT * FROM job_details where job_id =".$job_no;
	$result = mysqli_fetch_array(mysqli_query($con,$sql));
	echo "<pre>";
	$panel_qty=$result['panel_qty'];
	$glass_type=$result['glass_type'];
	$valA=$valB=$valC=$valD=$valE=$valF=$valG=$valH=$valI=0;
	$valueA=$valueB=$valueC=$valueD=$valueE=$valueF=$valueG=$valueH=$valueI=0;
	$panel_no = $panel_qty;

for($p=1; $p<=$panel_qty; $p++)
{

			$query_get_panwlwise_data="SELECT * FROM panelwise_dimension WHERE job_no='$job_no' AND panel_no='$p'";

			$exe_get_panwlwise_data=mysqli_query($con,$query_get_panwlwise_data);
			$get_panwlwise_data=mysqli_fetch_array($exe_get_panwlwise_data);
			$values=$get_panwlwise_data['values'];
			
			$first_part=explode(">>",$values);
			//print_r(count($first_part));echo'<br />';
			$char='A';
			$noss=count($first_part);
			$nossdd=$noss-1;
			
			$chararray=array();
			for($charn=0;$charn<$nossdd;$charn++)
			{
			$arr=explode("-",$first_part[$charn]);
			${"val$char"}=$arr[1];
			${"value$char"}=$arr[1];
			
			$chararray[]=$char;
			$char++;	
			}


			$prev_panel_no=$p-1;
			$next_panel_no=$p+1;	
			$paneltitle='paneltype'.$p.'';
			$query_panel_type="SELECT * FROM panel_types WHERE job_no='$job_no' AND title='$paneltitle'";
			$exe_panel_type=mysqli_query($con,$query_panel_type);
			$get_panel_type=mysqli_fetch_array($exe_panel_type);
			$panel_typess=$get_panel_type['value'];

			$glass_thickness_sql = "SELECT * FROM glassthickness where job_no =".$job_no." AND panel_no=".$p;
			$glass_thickness_result = mysqli_fetch_array(mysqli_query($con,$glass_thickness_sql));
			$glass_thickness_value = $glass_thickness_result['value'];

			
			echo '<tr style="text-align: left;padding: 4px;">';
				echo '<td style="width: 22%;font-size: 10px;padding-left:1%;">'.$glass_thickness_value.' Inch '.$glass_type.'</td>';
				echo '<td style="width: 10%;font-size: 10px;"></td>';
				echo '<td style="width: 10%;font-size: 10px;">'.$valueA.'*'.$valueB.'</td>';
				echo '<td style="width: 10%;font-size: 10px;">1</td>';
				echo '<td style="width: 10%;font-size: 10px;">1.3501</td>';
				echo '<td style="width: 8%;font-size: 10px;text-align: center;"><input type="checkbox" name="" checked="checked"></td>';
				echo '<td style="width: 10%;font-size: 10px;">?</td>';
				echo '<td style="width: 10%;font-size: 10px;text-align: right;">0</td>';
				echo '<td style="width: 10%;font-size: 10px;text-align: right;">0.00</td>';
			echo '</tr>';

			// echo '<tr style="text-align: left;padding: 4px;">';
			// 	echo '<td style="width: 22%;font-size: 10px;padding-left:1%;">0.38 Inch Clear Thoughened</td>';
			// 	echo '<td style="width: 10%;font-size: 10px;"></td>';
			// 	echo '<td style="width: 10%;font-size: 10px;">2077*650</td>';
			// 	echo '<td style="width: 10%;font-size: 10px;">1</td>';
			// 	echo '<td style="width: 10%;font-size: 10px;">1.3501</td>';
			// 	echo '<td style="width: 8%;font-size: 10px;text-align: center;"><input type="checkbox" name="" checked="checked"></td>';
			// 	echo '<td style="width: 10%;font-size: 10px;">?</td>';
			// 	echo '<td style="width: 10%;font-size: 10px;text-align: right;">0</td>';
			// 	echo '<td style="width: 10%;font-size: 10px;text-align: right;">0.00</td>';
			// echo '</tr>';
}
?>						
		</tbody>
	</table>
	<div style="width: 100%;"><input type="text" name="" style="text-align: right;width: 15%;margin-left: 85%;" value="$0.00"></div>

	<div style="float: left; width: 32%;margin-left: 1%;">
		<p style="margin: 5px 10px 5px 15px;">Glass Totals</p>
		<div style="border: 1px solid grey;padding: 12px;">
			<label>Area</label><input type="text" name="" value="7.0455"style="width: 30%;margin-left: 2%;background: #F2F2F2;border: 1px solid grey;">
			<label style="margin-left: 2%;">Weight</label><input type="text" name="" value="176.1375"style="width: 30%;margin-left: 2%;background: #F2F2F2;border: 1px solid grey;">
		</div>
	</div>
	<div style="float: left; width: 62%;margin-left: 1%;">
		<p style="margin: 5px 10px 5px 15px;">Defaults for 0.38 Inch Clear Toughened</p>
		<div style="border: 1px solid grey;padding: 12px;">
			<label>Cost Rate</label><input type="text" name="" value="7.0455" style="width: 14%;margin-left: 1%;background: #F2F2F2;border: 1px solid grey;">
			<label style="margin-left: 2%;">Price Rate</label><input type="text" name="" value="176.1375" style="width: 14%;margin-left: 1%;background: #F2F2F2;border: 1px solid grey;">
		<label style="margin-left: 2%;">Discount</label><input type="text" name="" value="176.1375" style="width: 14%;margin-left: 1%;background: #F2F2F2;border: 1px solid grey;">
		<button style="margin-left: 1%;">Use Defaults</button>
		<button style="margin-left: 1%;">Use Defaults For All</button>
		</div>
	</div>
</div>
<!-- End Bottom Section -->

<div class="calculate-price-footer">
<button onclick="

save_quote_prices(this.form);">Save</button>
<button onclick="window.close()">Close</button>

</div>

<script>
	$('.show_detail').click(function(){
		document.getElementById("show_detail").style.display = "none";
		document.getElementById("hide_detail").style.display = "block";
		document.getElementById("show_detail_div").style.display = "block";
		document.getElementById("calculate-price-main").style.height = '36%';
		window.resizeTo(800, 800);
  		window.focus();
    });
    $('.hide_detail').click(function(){
		document.getElementById("hide_detail").style.display = "none";
		document.getElementById("show_detail").style.display = "block";
		document.getElementById("show_detail_div").style.display = "none";
		document.getElementById("calculate-price-main").style.height = '61%';
		window.resizeTo(800, 470);
  		window.focus();
    });

function save_quote_prices(form){
	
	var discount_amoun=0.00;
	var taxamount=0.00;
	var total_price=0.00;
	var material_price=0.00;
	var labour_per_fixed_based_panel_rate=0.00;
	var labour_per_door_rate=0.00;
	var labour_per_glass_area_rate=0.00;
	var labour_travel_time_rate=0.00;
	var travell_expence_rate=0.00;
	var accommondation_rate=0.00;
	var other_rate=0.00;
	var discount_price=0.00;
	var linecosttotal=0.00;
	var grosstotal=0.00;
	var sub_total=0.00;

	
	
	var material_qty=$("#material_qty").text();
	var material_price1=$("#material_line_price").val();
	var material_price_arr=material_price1.split("$");
	var material_price=material_price_arr[1];
	
	var glass_qty=$("#glass_qty").text();
	
	var glass_price1=$("#glass_line_price").val();
	var glass_price_arr=glass_price1.split("$");
	var glass_price=glass_price_arr[1];
	
	var labour_per_fixed_based_panel_qty=$("#labour_per_fixed_based_panel_qty").text();
	var labour_per_fixed_based_panel_rate1=$("#labour_per_fix_cost_rate").val();
	var labour_per_fixed_based_panel_rate_arr=labour_per_fixed_based_panel_rate1.split("$");
	var labour_per_fixed_based_panel_rate=labour_per_fixed_based_panel_rate_arr[1];
	
	var labour_per_door_qty=$("#labour_per_door_qty").text();
	var labour_per_door_rate1=$("#labour_per_door_cost_rate").val();
	var labour_per_door_rate_arr=labour_per_door_rate1.split("$");
	var labour_per_door_rate=labour_per_door_rate_arr[1];
	
	var labour_per_glass_area_qty=$("#labour_per_glass_area_qty").text();
	var labour_per_glass_area_rate1=$("#labour_per_glass_cost_rate").val();
	var labour_per_glass_area_rate_arr=labour_per_glass_area_rate1.split("$");
	var labour_per_glass_area_rate=labour_per_glass_area_rate_arr[1];
	
	var labour_travel_time_qty=$("#labour_travel_time_qty").val();
	var labour_travel_time_rate1=$("#labour_travel_time_cost_rate").val();
	var labour_travel_time_rate_arr=labour_travel_time_rate1.split("$");
	var labour_travel_time_rate=labour_travel_time_rate_arr[1];
	
	var travell_expence_qty=$("#travelexpence_qty").val();
	var travell_expence_rate1=$("#travelexpence_cost_rate").val();
	var travell_expence_rate_arr=travell_expence_rate1.split("$");
	var travell_expence_rate=travell_expence_rate_arr[1];
	
	var accommondation_qty=$("#accommodation_qty").val();
	var accommondation_rate1=$("#accommodation_cost_rate").val();
	var accommondation_rate_arr=accommondation_rate1.split("$");
	var accommondation_rate=accommondation_rate_arr[1];
	
	var other_qty=$("#other_qty").val();
	var other_rate1=$("#other_cost_rate").val();
	var other_rate_arr=other_rate1.split("$");
	var other_rate=other_rate_arr[1];
	
	var discount_rate1=$("#discount_percent").val();
	var discount_rate_arr=discount_rate1.split("%");
	var discount_rate=discount_rate_arr[0];
	
	var discount_price=$("#dicount_amount_costrate").text();
	//var discount_price_arr=discount_price1.split("$");
	//var discount_price=discount_price_arr[1];
	//alert(discount_price1);
	var linecosttotal=$("#linecosttotal").val();
	//var linecosttotal_arr=linecosttotal1.split("$");
	//var linecosttotal=linecosttotal_arr[1];
	
	var total_markup_per1=$("#total_markup_per").val();
	var total_markup_per_arr=total_markup_per1.split("%");
	var total_markup_per=total_markup_per_arr[0];
	
	var grosstotal1=$("#grosstotal").val();
	var grosstotal_arr=grosstotal1.split("$");
	var grosstotal=grosstotal_arr[1];
	
	var marginpercent1=$("#marginpercent").val();
	var marginpercent_arr=marginpercent1.split("%");
	var marginpercent=marginpercent_arr[0];
	
	
	var sub_total1=$("#sub_total").val();
	var sub_total_arr=sub_total1.split("$");
	var sub_total=sub_total_arr[1];
	
	var tax_name=$("#tax_name").val();
	var tax_rate1=$("#tax_rate").val();
	var tax_rate_arr=tax_rate1.split("%");
	var tax_rate=tax_rate_arr[0];
	
	
	var linepricetotal1=$("#linepricetotal").val();
	var linepricetotal_arr=linepricetotal1.split("$");
	var linepricetotal=linepricetotal_arr[1];
	//alert(linepricetotal1);
	
	discount_amoun=(linepricetotal*discount_rate[0]/100).toFixed(2);
	
	taxamount=(linepricetotal*tax_rate/100).toFixed(2);
	
	total_price=(+linepricetotal+ +taxamount).toFixed(2);
	var job_no='<?php echo$job_no ?>';
	//alert(linepricetotal);
	//alert(taxamount);
	if(linepricetotal=='0.00')
	{
		alert('Please fill all prices');
	}
	else{
	update_calculated_subtotals(this.form);	
	$.ajax({
      type:'post',
      url:"date_save/save_quote_prices.php",
      data:{job_no:job_no,material_qty:material_qty,material_price:material_price,glass_qty:glass_qty,glass_price:glass_price,labour_per_fixed_based_panel_qty:labour_per_fixed_based_panel_qty,labour_per_fixed_based_panel_rate:labour_per_fixed_based_panel_rate,labour_per_door_qty:labour_per_door_qty,labour_per_door_rate:labour_per_door_rate,labour_per_glass_area_qty:labour_per_glass_area_qty,labour_per_glass_area_rate:labour_per_glass_area_rate,labour_travel_time_qty:labour_travel_time_qty,labour_travel_time_rate:labour_travel_time_rate,travell_expence_qty:travell_expence_qty,travell_expence_rate:travell_expence_rate,accommondation_qty:accommondation_qty,accommondation_rate:accommondation_rate,other_qty:other_qty,other_rate:other_rate,discount_rate:discount_rate,discount_price:discount_price,linecosttotal:linecosttotal,total_markup_per:total_markup_per,grosstotal:grosstotal,marginpercent:marginpercent,sub_total:sub_total,tax_name:tax_name,tax_rate:tax_rate,linepricetotal:linepricetotal,discount_amoun:discount_amoun,taxamount:taxamount,total_price:total_price},
      success: function(data){
		 window.location.reload(true);
       //alert(data);      
       //$("#added_hardware_detail").html(data);
                
	}
      });
	}
	
}

function update_calculated_subtotals(form){
	var total_line_price=0;
	var new_tax_amount=0;
	var new_sub_total =$("#linepricetotal").val();
	var tax_rate=$("#tax_rate").val();
	var tax_name=$("#tax_name").val();
	
	
	var new_sub_total_arr=new_sub_total.split("$");
	var sub_total_val=new_sub_total_arr[1];
	
	var tax_rate_arr=tax_rate.split("%");
	var new_tax_rate=tax_rate_arr[0];
	
	var new_tax_amount=(sub_total_val*new_tax_rate/100).toFixed(2);
	//alert(new_tax_amount);
	total_line_price=(+sub_total_val + +new_tax_amount).toFixed(2);
	
	var sub_total1='$'+sub_total_val+'';
	var tax_amount1='$'+new_tax_amount+'';
	var total_price1=	'$'+total_line_price+'';
	var job_no='<?php echo$job_no ?>';
	
	$.ajax({
      type:'post',
      url:"date_save/update_subtotal_prices.php",
      data:{job_no:job_no,amount_sub_total:sub_total_val,final_tax_price:new_tax_amount,final_total_price:total_line_price,tax_name:tax_name,tax_rate:tax_rate},
      success: function(data){
		 // window.location.reload(true);
       // alert(data);      
       //$("#added_hardware_detail").html(data);
                
	}
      });

	$("#sub_total").val(sub_total1);
	$("#tax_amount").val(tax_amount1);
	$("#total_price").val(total_price1);
}

function change_taxes(form){
	
	
	var sub_total =$("#sub_total").val();
	var tax_name =$("#tax_name").val();
	var tax_rate=$("#tax_rate").val();
	var tax_amount=$("#tax_amount").val();
	var total_price=$("#total_price").val();
	
	var sub_total_arr=sub_total.split("$");
	var new_sub_total=sub_total_arr[1];
	
	var tax_rate_arr=tax_rate.split("%");
	var new_tax_rate=tax_rate_arr[0];

	
	var new_tax_amount=(new_sub_total*new_tax_rate/100).toFixed(2);
	var new_tax_amount1='$'+new_tax_amount+'';
	
	var new_total_amount= (+new_sub_total + +new_tax_amount).toFixed(2);
	var new_total_amount1='$'+new_total_amount+'';
	
	
	//alert(new_tax_amount1);
	//alert(new_total_amount1);
	$("#tax_amount").val(new_tax_amount1);
	$("#total_price").val(new_total_amount1);
	
}

function update_price_calculated(form){
	var total_gross=0.00;
	var total_line_cost=0.00;
	var total_line_price=0.00;
	var discount_amount=0.00;
	var material_gross=0.00;
	var glass_gross=0.00;
	var labour_per_fix_line_cost=0.00;
	var labour_per_door_line_cost=0.00;
	var labour_per_glass_line_cost=0.00;
	var accommodation_line_cost=0.00;
	var other_line_cost=0.00;
	var travelexpence_line_cost=0.00;
	var labour_travel_time_line_cost=0.00;
	var total_line_pricess=0.00;
	var costratetotal=0.00;
	var grosstotal=0.00;
	var marginpercent=0.00;
	var amount_after_discount=0.00;
	
	var material_line_price =$("#material_line_price").val();
	var glass_line_price =$("#glass_line_price").val();
	var labour_per_fix_cost_rate=$("#labour_per_fix_cost_rate").val();
	var labour_per_door_cost_rate=$("#labour_per_door_cost_rate").val();
	var labour_per_glass_cost_rate=$("#labour_per_glass_cost_rate").val();
	var labour_travel_time_cost_rate=$("#labour_travel_time_cost_rate").val();
	var travelexpence_cost_rate= $("#travelexpence_cost_rate").val();
	var accommodation_cost_rate=$("#accommodation_cost_rate").val();
	var other_cost_rate=$("#other_cost_rate").val();
	var discount_percent=$("#discount_percent").val();
	var labour_travel_time_qty=$("#labour_travel_time_qty").val();
	var travelexpence_qty=$("#travelexpence_qty").val();
	var accommodation_qty=$("#accommodation_qty").val();
	var other_qty=$("#other_qty").val();
	//dicount_amount
	var res_material_line_price = material_line_price.split("$");
	material_gross = parseFloat(res_material_line_price[1]);
	material_gross1=material_gross.toFixed(2);
	
	var res_glass_line_price = glass_line_price.split("$");
	glass_gross = parseFloat(res_glass_line_price[1]);
	glass_gross1=glass_gross.toFixed(2);
	
	var res_labour_per_fix_cost_rate = labour_per_fix_cost_rate.split("$");
	 labour_per_fix_line_cost = parseFloat(res_labour_per_fix_cost_rate[1]*2);
	 labour_per_fix_line_cost1=labour_per_fix_line_cost.toFixed(2);
	
	var res_labour_per_door_cost_rate = labour_per_door_cost_rate.split("$");
	labour_per_door_line_cost = parseFloat(res_labour_per_door_cost_rate[1]*2);
	labour_per_door_line_cost1=labour_per_door_line_cost.toFixed(2);
	
	var res_labour_per_glass_cost_rate = labour_per_glass_cost_rate.split("$");
	labour_per_glass_line_cost =parseFloat(res_labour_per_glass_cost_rate[1]*2.14);
	labour_per_glass_line_cost1=labour_per_glass_line_cost.toFixed(2);
	
	var res_labour_travel_time_cost_rate = labour_travel_time_cost_rate.split("$");
	labour_travel_time_line_cost = parseFloat(res_labour_travel_time_cost_rate[1]*labour_travel_time_qty);
	labour_travel_time_line_cost1=labour_travel_time_line_cost.toFixed(2);
	
	var res_travelexpence_cost_rate = travelexpence_cost_rate.split("$");
	travelexpence_line_cost = parseFloat(res_travelexpence_cost_rate[1]*travelexpence_qty);
	travelexpence_line_cost1=travelexpence_line_cost.toFixed(2);
	
	var res_accommodation_cost_rate = accommodation_cost_rate.split("$");
	accommodation_line_cost = parseFloat(res_accommodation_cost_rate[1]*accommodation_qty);
	accommodation_line_cost1=accommodation_line_cost.toFixed(2);
	
	var res_other_cost_rate = other_cost_rate.split("$");
	other_line_cost = parseFloat(res_other_cost_rate[1]*other_qty);
	other_line_cost1=other_line_cost.toFixed(2);
	
	total_line_pricess=material_gross+glass_gross+labour_per_fix_line_cost+labour_per_door_line_cost+labour_per_glass_line_cost+labour_travel_time_line_cost+travelexpence_line_cost+accommodation_line_cost+other_line_cost;
	
	res_discount_percent= discount_percent.split("%");
	discount_amoun=(total_line_pricess*res_discount_percent[0]/100).toFixed(2);
	
	
	
	total_gross=((material_gross+glass_gross)-discount_amoun).toFixed(2);
	
	total_line_cost=((labour_per_fix_line_cost+labour_per_door_line_cost+labour_per_glass_line_cost+labour_travel_time_line_cost+travelexpence_line_cost+accommodation_line_cost+other_line_cost)-discount_amoun).toFixed(2);
	
	total_line_price=((material_gross+glass_gross+labour_per_fix_line_cost+labour_per_door_line_cost+labour_per_glass_line_cost+labour_travel_time_line_cost+travelexpence_line_cost+accommodation_line_cost+other_line_cost)-discount_amoun).toFixed(2);
	//alert(discount_amoun);
	
	//amount_after_discount=(total_line_pricess-discount_amoun).toFixed(2)
	
	var total_gross1='$'+total_gross+'';
	var total_line_cost1='$'+total_line_cost+'';
	var total_line_price1=	'$'+total_line_price+'';
	$("#grosstotal").val(total_gross1);
	$("#linecosttotal").val(total_line_cost1);
	$("#linepricetotal").val(total_line_price1);
	
	
	$("#material_gross").text(material_gross1);
	$("#glass_gross").text(glass_gross1);
	
	$("#glass_gross").text(glass_gross1);
	$("#glass_gross").text(glass_gross1);
	
	$("#labour_per_fix_line_cost").text(labour_per_fix_line_cost1);
	$("#labour_per_fix_line_price").text(labour_per_fix_line_cost1);

	$("#labour_per_door_line_cost").text(labour_per_door_line_cost1);
	$("#labour_per_door_line_price").text(labour_per_door_line_cost1);
	
	$("#labour_per_glass_line_cost").text(labour_per_glass_line_cost1);
	$("#labour_per_glass_line_price").text(labour_per_glass_line_cost1);
	
	$("#labour_travel_time_line_cost").text(labour_travel_time_line_cost1);
	$("#labour_travel_time_line_price").text(labour_travel_time_line_cost1);
	
	$("#travelexpence_line_cost").text(travelexpence_line_cost1);
	$("#travelexpence_line_price").text(travelexpence_line_cost1);
	
	$("#accommodation_line_cost").text(accommodation_line_cost1);
	$("#accommodation_line_price").text(accommodation_line_cost1);
	
	$("#other_line_cost").text(other_line_cost1);
	$("#other_line_price").text(other_line_cost1);
	
	
	
	$("#dicount_amount_costrate").text(discount_amoun);
	$("#dicount_amount_gross").text(discount_amoun);
	$("#dicount_amount_line_price").text(discount_amoun);
}
function show_cost(form){
	$('.costs').css("display","");
	$('.hide_cost').css("display","");
	$('.show_cost').css("display","none");
	
}
function hide_cost(form){
	$('.costs').css("display","none");
	$('.hide_cost').css("display","none");
	$('.show_cost').css("display","");
	
}
/*

    $(document).on("click", "[data-column]", function () {
      var button = $(this),
          header = $(button.data("column")),
          table = header.closest("table"),
          index = header.index() + 1, // convert to CSS's 1-based indexing
          selector = "tbody tr td:nth-child(" + index + ")",
          column = table.find(selector).add(header);

      column.toggleClass("hidden");
    });*/
</script>
<style>
.show_detail{}

</style>
<!--
<table class="print-glassorder-main">
<tr class="print-glassorder-head">
<td class="glassorderhead1"><h1>Glass Order</h1></td>
<td class="glassorderhead2">Printed On <?php echo date('d/m/Y') ?></td>
</tr>
</table>
-->

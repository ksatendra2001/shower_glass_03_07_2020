<?php
include('includes/header.php');
		//$lengthA=$_GET['lengthA'];
		//$lengthB=$_GET['lengthB'];
		//$lengthC=$_GET['lengthC'];
		//$lengthD=$_GET['lengthD'];

		//$img=$_GET['img'];
		//$job_id=$_GET['job_id'];
		// $job_id=$_GET['job_id'];
		// $glass_type=$_GET['glass_type'];
		
		
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>ADM Shower Glass</title>

<link href="magic360/magic360.css" rel="stylesheet" type="text/css" media="screen"/>
<script src="magic360/magic360.js" type="text/javascript"></script>

<style type="text/css">
    html { position: relative; min-height: 100%; }
    body { position: absolute; left:0; right: 0; min-height: 100%; background:#fff; margin:0; padding:0; font-size: 100%; }
    body, td {
        font-family: 'Helvetica Neue', Helvetica, 'Lucida Grande', Tahoma, Arial, Verdana, sans-serif;
        line-height: 1.5em;
        -webkit-text-rendering: geometricPrecision;
        text-rendering: geometricPrecision;
    }
    h1 { font-size:1.5em; font-weight:normal; color:#555; }
    h2 { font-size:1.3em; font-weight:normal; color:#555; }
    h2.caption { margin: 2.5em 0 0;}
    h3 { font-size:1.1em; font-weight: normal; color:#555; }
    h3.pad { margin: 2em 0 1em; }
    h4 { font-size: 1em; font-weight:normal; color:#555; }
    p.pad { margin-top: 1.5em; }
    a { outline: none;}


    .cfg-btn {
        background-color: rgb(55, 181, 114);
        color: #fff;
        border: 0;
        box-shadow: 0 0 1px 0px rgba(0,0,0,0.3);
        outline:0;
        cursor: pointer;
        width: 200px;
        padding: 10px;
        font-size: 1em;
        position: relative;
        display: inline-block;
        margin: 10px auto;
    }
    .cfg-btn:hover:not([disabled]) {
        background-color: rgb(37, 215, 120);
    }
    .mobile-magic .cfg-btn:hover:not([disabled]) { background: rgb(55, 181, 114); }
    .cfg-btn[disabled] { opacity: .5; color: #808080; background: #ddd; }

    .cfg-btn.btn-preview,
    .cfg-btn.btn-preview:active,
    .cfg-btn.btn-preview:focus {
        font-size: 1em;
        position: relative;
        display: block;
        margin: 10px auto;
    }

    .cfg-btn,
    .preview,
    .app-figure,
    .api-controls,
    .wizard-settings,
    .wizard-settings .inner,
    .wizard-settings .footer,
    .wizard-settings input,
    .wizard-settings select {
        -webkit-box-sizing: border-box;
           -moz-box-sizing: border-box;
                box-sizing: border-box;
    }
    .preview,
    .wizard-settings {
        padding: 10px;
        border: 0;
        min-height: 1px;
    }
    .preview {
        position: relative;
    }

    .api-controls {
        text-align: center;
    }
    .api-controls button,
    .api-controls button:active,
    .api-controls button:focus {
         width: 100px;
         font-size: .7em;
         white-space: nowrap;
    }
    .api-controls .sep { content: ''; display: table; }
    .api-controls .sep-sm { content: ''; display: none; }

    .app-figure {
        width: 100% !important;
        margin: 50px auto; border: 0px solid red;
        padding: 20px;
        position: relative;
        text-align: center;
    }
    .selectors { margin-top: 10px; }

    .app-code-sample {
        max-width: 80%;
        margin: 80px auto 0;
        text-align: center;
        position: relative;
    }
    .app-code-sample input[type="radio"] {
        display: none;
    }
    .app-code-sample label {
        display: inline-block;
        padding: 2px 12px;
        margin: 0;
        font-size: .8em;
        text-decoration: none;
        cursor: pointer;
        color: #666;
        border: 1px solid rgba(136, 136, 136, 0.5);
        background-color: transparent;
    }
    .app-code-sample label:hover {
        color: #fff;
        background-color: rgb(253, 154, 30);
        border-color: rgb(253, 154, 30);
    }
    .app-code-sample input[type="radio"]:checked+label {
        color: #fff;
        background-color: rgb(110, 110, 110) !important;
        border-color: rgba(110, 110, 110, 0.7) !important;
    }
    .app-code-sample label:first-of-type {
        border-radius: 4px 0 0 4px;
        border-right-color: transparent;
    }
    .app-code-sample label:last-of-type {
        border-radius: 0 4px 4px 0;
        border-left-color: transparent;
    }

    .app-code-sample .app-code-holder {
        padding: 0;
        position: relative;
        border: 1px solid #eee;
        border-radius: 0px;
        background-color: #fafafa;
        margin: 15px 0;
    }
    .app-code-sample .app-code-holder > div {
        display: none;
    }
    .app-code-sample .app-code-holder pre {
        text-align: left;
        white-space: pre-line;
        border: 0px solid #eee;
        border-radius: 0px;
        background-color: transparent;
        padding: 25px 50px 25px 25px;
        margin: 0;
        min-height: 25px;
    }
    .app-code-sample input[type="radio"]:nth-of-type(1):checked ~ .app-code-holder > div:nth-of-type(1) {
        display: block;
    }
    .app-code-sample input[type="radio"]:nth-of-type(2):checked ~ .app-code-holder > div:nth-of-type(2) {
        display: block;
    }

    .app-code-sample .app-code-holder .cfg-btn-copy {
        display: none;
        z-index: -1;
        position: absolute;
        top:10px; right: 10px;
        width: 44px;
        font-size: .65em;
        white-space: nowrap;
        margin: 0;
        padding: 4px;
    }
    .copy-msg {
        font: normal 11px/1.2em 'Helvetica Neue', Helvetica, 'Lucida Grande', 'Lucida Sans Unicode', Verdana, Arial, sans-serif;
        color: #2a4d14;
        border: 1px solid #2a4d14;
        border-radius: 4px;
        position: absolute;
        top: 8px;
        left: 0;
        right: 0;
        width: 200px;
        max-width: 70%;
        padding: 4px;
        margin: 0px auto;
        text-align: center;
    }
    .copy-msg-failed {
        color: #b80c09;
        border-color: #b80c09;
        width: 430px;
    }
    .mobile-magic .app-code-sample .cfg-btn-copy { display: none; }
    #code-to-copy { position: absolute; width: 0; height: 0; top: -10000px; }
    .lt-ie9-magic .app-code-sample { display: none; }

    .wizard-settings {
        background-color: #4f4f4f;
        color: #a5a5a5;
        position: absolute;
        right: 0;
        width: 340px;
    }
    .wizard-settings .inner {
        width: 100%;
        margin-bottom: 30px;
    }
    .wizard-settings .footer {
        color: #c7d59f;
        font-size: .75em;
        width: 100%;
        position: relative;
        vertical-align: bottom;
        text-align: center;
        padding: 6px;
        margin-top: 10px;
    }
    .wizard-settings .footer a { color: inherit; text-decoration: none; }
    .wizard-settings .footer a:hover { text-decoration: underline; }

    .wizard-settings a { color: #cc9933; }
    .wizard-settings a:hover { color: #dfb363; }
    .wizard-settings table > tbody > tr > td { vertical-align: top; }
    .wizard-settings table { min-width: 300px; max-width: 100%; font-size: .8em; margin: 0 auto; }
    .wizard-settings table caption { font-size: 1.5em; padding: 16px 8px; }
    .wizard-settings table td { padding: 4px 8px; }
    .wizard-settings table td:first-child { white-space: nowrap; }
    .wizard-settings table td:nth-child(2) { text-align: left; }
    .wizard-settings table td .values {
        color: #a08794;
        font-size: 0.8em;
        line-height: 1.3em;
        display: block;
        max-width: 170px;
        padding-top: 2px;
    }
    .wizard-settings table td .values:before { content: ''; display: block; }

    .wizard-settings input,
    .wizard-settings select {
        width: 170px;
    }
    .wizard-settings input {
        padding: 0px 4px;
    }
    .wizard-settings input[disabled] {
        color: #808080;
        background: #a7a7a7;
        border: 1px solid #a7a7a7;
    }

    .preview { width: 70%; float: left; }
    @media (min-width: 0px) {
        .preview { width: 100%; float: none; }
    }
    @media (min-width: 1024px) {
        .preview { width: calc(100% - 340px); }
        .wizard-settings { top: 0; min-height: 100%; }
        .wizard-settings .inner { margin-top: 60px; }
        .wizard-settings .footer { position: absolute; bottom: 0; left: 0; }
        .wizard-settings .settings-controls {
            position: fixed;
            top: 0; right: 0;
            width: 340px;
            padding: 10px 0 0;
            text-align: center;
            background-color: inherit;
        }
    }
    @media screen and (max-width: 1023px) {
        .app-figure { width: 98% !important; margin: 50px 0; padding: 0; }
        .app-code-sample { display: none; }
        .wizard-settings { width: 160%; }
    }
    @media screen and (max-width: 600px) {
        .api-controls button,
        .api-controls button:active,
        .api-controls button:focus {
            width: 96px;
        }
    }
    @media screen and (min-width: 435px) and (max-width: 767px) {
        .api-controls .sep { display: none; }
        .api-controls .sep-sm { display: table; }
    }
    @media screen and (min-width: 768px) {
        .api-controls .sep { display: table; }
        .api-controls .sep-sm { display: none; }
    }
    @media screen and (min-width: 1600px) {
        .preview { padding: 10px 160px; }
    }
	
	#crM360838208703099{display:none !important;}
</style>

<?php
for($im=1; $im<=200; $im++)
{

echo'
<style>	
	#spin-'.$im.' span{display:none !important;}
</style>
';
}
?>
<script>
    var Magic360Options = {};
    Magic360Options = {
        onready: function () {
            console.log('onready', arguments[0]);
        },
        onstart: function () {
            console.log('onstart', arguments[0]);
        },
        onstop: function () {
            console.log('onstop', arguments[0]);
        },
        onzoomin: function () {
            console.log('onzoomin', arguments[0]);
        },
        onzoomout: function () {
            console.log('onzoomout', arguments[0]);
        },
        onspin: function () {
            console.log('onspin', arguments[0]);
        },
    };

    function isDefaultOption(o) {
        return magicJS.$A(magicJS.$(o).byTag('option')).filter(function(opt){
            return opt.selected && opt.defaultSelected;
        }).length > 0;
    }

    function toOptionValue(v) {
        if ( /^(true|false)$/.test(v) ) {
            return 'true' === v;
        }
        if ( /^[0-9]{1,}$/i.test(v) ) {
            return parseInt(v,10);
        }
        return v;
    }

    function makeOptions(optType) {
        var  value = null, isDefault = true, newParams = Array(), newParamsS = '', options = {};
        magicJS.$(magicJS.$A(magicJS.$(optType).getElementsByTagName("INPUT"))
            .concat(magicJS.$A(magicJS.$(optType).getElementsByTagName('SELECT'))))
            .forEach(function(param){
                value = ('checkbox'==param.type) ? param.checked.toString() : param.value;

                isDefault = ('checkbox'==param.type) ? value == param.defaultChecked.toString() :
                    ('SELECT'==param.tagName) ? isDefaultOption(param) : value == param.defaultValue;

                if ( null !== value && !isDefault) {
                    options[param.name] = toOptionValue(value);
                }
        });
        return options;
    }

    function updateScriptCode() {
        var code = '&lt;script&gt;\nvar Magic360Options = ';
        code += JSON.stringify(Magic360Options, null, 2).replace(/\"(\w+)\":/g,"$1:")+';';
        code += '\n&lt;/script&gt;';

        magicJS.$('app-code-sample-script').changeContent(code);
    }

    function updateInlineCode() {
        var code = '&lt;div class="Magic360" data-options="';
        code += JSON.stringify(Magic360Options).replace(/\"([-\w]+)\":(?:\"([^"]+)\"|([^,}]+))(,)?/g, "$1: $2$3; ").replace(/^\{(.*)\}$/,"$1").replace(/\s*$/,'');
        code += '"&gt;';

        magicJS.$('app-code-sample-inline').changeContent(code);
    }

    function applySettings() {
        Magic360.stop('spin-1');
        Magic360Options = makeOptions('params');
        Magic360.start('spin-1');
        updateScriptCode();
        updateInlineCode();
        try {
            prettyPrint();
        } catch(e) {}
    }

    function copyToClipboard(src) {
        var
            copyNode,
            range, success;

        if (!isCopySupported()) {
            disableCopy();
            return;
        }
        copyNode = document.getElementById('code-to-copy');
        copyNode.innerHTML = document.getElementById(src).innerHTML;

        range = document.createRange();
        range.selectNode(copyNode);
        window.getSelection().addRange(range);

        try {
            success = document.execCommand('copy');
        } catch(err) {
            success = false;
        }
        window.getSelection().removeAllRanges();
        if (!success) {
            disableCopy();
        } else {
            new magicJS.Message('Settings code copied to clipboard.', 3000,
                document.querySelector('.app-code-holder'), 'copy-msg');
        }
    }

    function disableCopy() {
        magicJS.$A(document.querySelectorAll('.cfg-btn-copy')).forEach(function(node) {
            node.disabled = true;
        });
        new magicJS.Message('Sorry, cannot copy settings code to clipboard. Please select and copy code manually.', 3000,
            document.querySelector('.app-code-holder'), 'copy-msg copy-msg-failed');
    }

    function isCopySupported() {
        if ( !window.getSelection || !document.createRange || !document.queryCommandSupported ) { return false; }
        return document.queryCommandSupported('copy');
    }
	






//zoom in and out
function zoomin() {
  var myImg = document.getElementById("map");
  var currWidth = myImg.clientWidth;
  if (currWidth == 2500) return false;
  else {
    myImg.style.width = (currWidth + 50) + "px";
    myImg.style.height = (currWidth + 50) + "px";
  }
}

function zoomout() {
  var myImg = document.getElementById("map");
  var currWidth = myImg.clientWidth;
  if (currWidth == 100) return false;
  else {
    myImg.style.width = (currWidth - 50) + "px";
	myImg.style.height = (currWidth - 50) + "px";
  }
}



function rotation_image(){
	var rotaion_status=$("#rotate_img").val();
	var move_status=$("#move_img").val();
	
document.getElementById("move_image").src = "img/move_imgae.png";
document.getElementById("rotation_img").src = "img/rotaion_imgae_selected.png";

$('#rotate_img').val('1');
$('#move_img').val('0');
	
}
function move_image(){
var rotaion_status=$("#rotate_img").val();
var move_status=$("#move_img").val();


document.getElementById("move_image").src = "img/move_imgae_selected.png";
document.getElementById("rotation_img").src = "img/rotaion_imgaed.png";
$('#rotate_img').val('0');
$('#move_img').val('1');	
}
function view_3dview(){
var rotaion_status=$("#rotate_img").val();
	var move_status=$("#move_img").val();	
	
var link='date_save/images/panel_drawing_3d_view/<?php echo$job_id ?>/armani-bag-small-03-03.jpg';
//alert(link);
//document.getElementById("map").src = ""+link+"";
$("#spin-1").attr("href", link);
$("#map").prop("src", ""+link+"?" + new Date().valueOf());
//document.getElementById("map").src = ""+link+"";

}
function default_view(){
var rotaion_status=$("#rotate_img").val();
var move_status=$("#move_img").val();
var link='date_save/images/panel_drawing_3d_view/<?php echo$job_id ?>/armani-bag-small-01-01.jpg';

//document.getElementById("map").src = ""+link+"";
$("#spin-1").attr("href", link);
$("#map").prop("src", ""+link+"?" + new Date().valueOf());
//document.getElementById("map").src = ""+link+"";
//window.location.reload(true);	
}


function three_D_with_color_select(){
	
	
document.getElementById("with_color").src = "img/3d_with_color_selected.png";
document.getElementById("without_color").src = "img/3d_without_color.png";

$('.with_color_img').css("display","block");
$('.without_color_img').css("display","none");

$('#with_color_img_check').val('1');
		
}

function three_D_without_color_select(){


document.getElementById("with_color").src = "img/3d_with_color.png";
document.getElementById("without_color").src = "img/3d_without_color_selected.png";


$('.with_color_img').css("display","none");
$('.without_color_img').css("display","block");


$('#with_color_img_check').val('0');
	
}
//date_save/images/panel_drawing_3d_view/<?php echo$job_id ?>/without_color/armani-bag-small-01-01.jpg
</script>
</head>

<body>
<!-- <div style="position: absolute;z-index: 100;width:100%;">
<div style="float:left; width:45%;">
<button type="button" onclick="rotation_image()"><img id="rotation_img" src="img/rotaion_imgae_selected.png"></button>
<button type="button" onclick="move_image()"><img  id="move_image" src="img/move_imgae.png"></button>
<button type="button" onclick="zoomin()"><img src="img/zoom_in.png"></button>
<button type="button" onclick="zoomout()"><img src="img/zoom_out.png"></button>
<button type="button" onclick="view_3dview();view_main_img();maine_image();"><img src="img/view_3dview.png"></button>
<button type="button" onclick="default_view()"><img src="img/default_view.png"></button>


<input type="hidden" name="rotate_img" id="rotate_img" value="1">
<input type="hidden" name="move_img" id="move_img" value="0">


<input type="hidden" name="with_color_img_check" id="with_color_img_check" value="1">



</div>
</div> -->


<div id="contentContainer">


<div class="preview col with_color_img" id="thing">
    <div class="app-figure main dragscroll" id="zoom-fig" style="margin: 0px !important; padding: 0px !important;height: auto !important;">
        <a id="spin-1" class="Magic360" data-options="rows: 4; columns: 12"
            href="date_save/images/panel_drawing_3d_view/panel_preview/armani-bag-small-01-01.jpg"
        ><img id="map" class="imgsss" src="date_save/images/panel_drawing_3d_view/panel_preview/armani-bag-small-01-01.jpg"></a>
    </div>
</div>



<!-- <div class="preview col without_color_img" id="thing2" style="display:none;">
	 <div class="app-figure main dragscroll" id="zoom-fig">
        <a id="spin-1" class="Magic360" data-options="rows: 4; columns: 12"
            href="date_save/images/panel_drawing_3d_view/panel_preview/without_color/armani-bag-small-01-01.jpg"
        ><img id="map" class="imgsss" src="date_save/images/panel_drawing_3d_view/panel_preview/without_color/armani-bag-small-01-01.jpg"></a>
    </div> 
</div>

<div class="preview col without_color_img" id="thing3" style="display:none;">
	
       <img id="map" class="imgsss" src="date_save/images/panel_drawing_3d_view/panel_preview/animation_png/animation_panels.gif" ondblclick="maine_image();">
   
</div> -->
<button onclick="window.close()" style="padding: 5px;float: right;margin-right: 10px;"><u>C</u>lose</button>
</div>
<script>
$("a").dblclick(function(){
 // alert("date_save/images/panel_drawing_3d_view/27/animation_png/animation_panels.gif");
  //$('.imgsss').attr('src','date_save/images/panel_drawing_3d_view/27/animation_png/animation_panels.gif');
   $('#thing').css("display","none");
   $('#thing2').css("display","none");
   $('#thing3').css("display","block");
              
});

function maine_image(){
	
	$('#thing').css("display","block");
   $('#thing2').css("display","none");
   $('#thing3').css("display","none");
   
   $('#show_animation').css("display","block");
   $('#hide_animation').css("display","none");

   
}

function show_animation(){
	
	$('#thing').css("display","none");
   $('#thing2').css("display","none");
   $('#thing3').css("display","block");
   
    $('#show_animation').css("display","none");
   $('#hide_animation').css("display","block");
}
</script>

<style>


#map {
    position: relative;
    left: 50px;
    top: 50px;
    transition: left .5s ease-in, top .5s ease-in;
}

.Magic360-container desktop{max-width: 746px !important;}


.Magic360, .Magic360-container {
    border: 0px solid #f3f3f3 !important;
    box-sizing: content-box !important;
}
</style>



<div id="code-to-copy"></div>



<script src="//www.kirupa.com/prefixfree.min.js"></script>
<script>
var theThing = document.querySelector("#thing");
var theThing_withoutcolor = document.querySelector("#thing2");
var container = document.querySelector("#contentContainer");
 
container.addEventListener("click", getClickPosition, false);


function getClickPosition(e) {
	
	 var with_color_status=$("#with_color_img_check").val();
	 var without_color_status=$("#without_color_img_check").val();
if(with_color_status==0)
{
theThing=theThing_withoutcolor;	
}
else
{
theThing=theThing;	
}

    var parentPosition = getPosition(e.currentTarget);
    var xPosition = e.clientX - parentPosition.x - (theThing.clientWidth / 2);
    var yPosition = e.clientY - parentPosition.y - (theThing.clientHeight / 2);
     
    theThing.style.left = xPosition + "px";
    theThing.style.top = yPosition + "px";
}
 
// Helper function to get an element's exact position
function getPosition(el) {
  var xPos = 0;
  var yPos = 0;
  
  var rotaion_status=$("#rotate_img").val();
var move_status=$("#move_img").val();
 
 if(rotaion_status==0 && move_status==1)
{
  while (el) {
    if (el.tagName == "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;
 
      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
 
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
  
  
  }
else{
	
}
  
  
}

</script>

<style>


.main {
  padding: 16px;
  margin-top: 30px;
  width: 100%;
  height: 100vh;
  overflow: auto;
  cursor: grab;
  cursor: -o-grab;
  cursor: -moz-grab;
  cursor: -webkit-grab;
}

.main img {
  height: auto;
  width: 100%;
}

.button {
  width: 300px;
  height: 60px;
}



#spin-1 img{width:30%; !important}
.Magic360 img{width:30%;}
</style>

</body></html>